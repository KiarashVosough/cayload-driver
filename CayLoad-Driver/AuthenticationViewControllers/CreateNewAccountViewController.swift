//
//  CreateNewAccountViewController.swift
//  CayLoad-Driver
//
//  Created by Kiyarash Vosough on 1/21/1399 AP.
//  Copyright © 1399 CayLoad. All rights reserved.
//

import UIKit
import SwiftyJSON

class CreateNewAccountViewController: UIViewController,UITextFieldDelegate {
    
        /// `Outlets`
    @IBOutlet weak var LoginMethodChanger_Button: UIButton!
    @IBOutlet weak var Phone_Email_TextField: UITextField!
    @IBOutlet weak var FirstName_TextField: UITextField!
    @IBOutlet weak var LastName_TextField: UITextField!
    @IBOutlet weak var Password_TextField: UITextField!
    @IBOutlet weak var ChooseNationality_Button: UIButton!
    @IBOutlet weak var ChooseLanguage_Button: UIButton!
    @IBOutlet weak var SignUp_Button: UIButton!
    @IBOutlet weak var Login_Button: UIButton!
    @IBOutlet weak var LanguagePicker_ImageView: UIImageView!
    @IBOutlet weak var LanguagePicker_Label: UILabel!
    @IBOutlet weak var NationalityPicker_ImageView: UIImageView!
    @IBOutlet weak var NationalityPicker_Label: UILabel!
    
        /// `Dynamic Componnent`
    private let MailOrPhone_ImageView:UIImageView = {
        let instance = UIImageView(frame: CGRect(x: 15, y: 21, width: 16, height: 12))
        instance.image = UIImage(named: "MailIc")
        instance.contentMode = .scaleAspectFit
        return instance
    }()
    private let MailImageView_Container = UIView()
    private let ShowCountryCode_Button:UIButton = {
        let instance = UIButton(frame: CGRect(x: 0, y: 0, width: 40, height: 54))
        instance.setImage(UIImage(named: "DownArrowIc") , for: .normal)
        return instance
    }()
    private let CountryCode_Label:UILabel = {
        let instance = UILabel(frame: CGRect(x: 40, y: 0, width: 44, height: 54))
        instance.text = "Code".localized()
        instance.font = UIFont(name: "Poppins-Regular", size: 10)
        return instance
    }()
    private let Seperator:UIView = {
        let instance = UIView(frame: CGRect(x: 80, y: 15, width: 1, height: 24))
        instance.backgroundColor = .lightGray
        return instance
    }()
    
    
    // MARK: - KeyBoard Manager
    @IBOutlet weak var Main_ScrollView: UIScrollView!
    
    @objc func adjustForKeyboard(notification: Notification) {
        guard let keyboardValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue else { return }

        let keyboardScreenEndFrame = keyboardValue.cgRectValue
        let keyboardViewEndFrame = view.convert(keyboardScreenEndFrame, from: view.window)

        if notification.name == UIResponder.keyboardWillHideNotification {
            Main_ScrollView.contentInset = .zero
        } else {
            Main_ScrollView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardViewEndFrame.height - view.safeAreaInsets.bottom, right: 0)
        }
        Main_ScrollView.scrollIndicatorInsets = Main_ScrollView.contentInset
//        let selectedRange = Main_ScrollView.selectedRange
//        Main_ScrollView.scrollRangeToVisible(selectedRange)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        switch textField.tag {
        case 1:
            FirstName_TextField.becomeFirstResponder()
        case 2:
            LastName_TextField.becomeFirstResponder()
        case 3:
            Password_TextField.becomeFirstResponder()
        case 4:
            textField.resignFirstResponder()
        default:
            textField.resignFirstResponder()
        }
        return true
    }
    
    //MARK: - Localization
    
    private func Localize(){
        FirstName_TextField.placeholder =  "First name".localized()
        LastName_TextField.placeholder =  "Last name".localized()
        Password_TextField.placeholder =  "Password".localized()
        
    }
    
    
    // MARK: - Functions
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        Localize()
        imLanguageManager = LanguageManager()
        imLanguageManager.SetLanguageViewProperites(Lang_Image: LanguagePicker_ImageView)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UIView.appearance().semanticContentAttribute = .forceLeftToRight
        AddDelegate()
        AddActions()
        OnCreateSigninMethodComponnent(withEmail: true)
        OnCreatePassWordTextFieldLeftView()
        OnCreateFirstNameTextFieldLeftView()
        OnCreateLastNameTextFieldLeftView()
    }
    private func AddDelegate(){
        Phone_Email_TextField.delegate = self
        FirstName_TextField.delegate = self
        LastName_TextField.delegate = self
        Password_TextField.delegate = self
    }
    
    private func AddActions(){
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(adjustForKeyboard), name: UIResponder.keyboardWillHideNotification, object: nil)
        notificationCenter.addObserver(self, selector: #selector(adjustForKeyboard), name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
        Login_Button.addTarget(self, action: #selector(OnLoginButtonClicked(_:)), for: .touchUpInside)
        LoginMethodChanger_Button.addTarget(self, action: #selector(OnLoginMethodChangerButtonClicked(_:)), for: .touchUpInside)
        ChooseLanguage_Button.addTarget(self, action: #selector(OnChooseLanguageButtonClicked), for: .touchUpInside)
        ShowCountryCode_Button.addTarget(self, action: #selector(OnShowCountryCodeButtonClicked(_:)), for: .touchUpInside)
        SignUp_Button.addTarget(self, action: #selector(OnSignUpButtonClicked(_:)), for: .touchUpInside)
        ChooseNationality_Button.addTarget(self, action: #selector(OnNationalityButtonClicked(_:)), for: .touchUpInside)
    }

    @objc func OnLoginMethodChangerButtonClicked(_ sender: UIButton){
        if sender.tag == 1 {
            //email is active
            OnCreateSigninMethodComponnent(withEmail: false)
            WithEmail = false
        }else{
            //phone is active
            OnCreateSigninMethodComponnent(withEmail: true)
            WithEmail = true
        }
    }
    
    
    private func OnCreateSigninMethodComponnent(withEmail: Bool){
        if  withEmail {
            LoginMethodChanger_Button.tag = 1
            Seperator.removeFromSuperview()
            CountryCode_Label.removeFromSuperview()
            ShowCountryCode_Button.removeFromSuperview()
            
            Phone_Email_TextField.text = ""
            
//            Phone_Email_TextField.placeholder = "halooooo"
            
            LoginMethodChanger_Button.setTitle("  " + ("Using Phone".localized()), for: .normal)
           

            MailImageView_Container.frame = CGRect(x: 0, y: 0, width: 47, height: 54)
            MailImageView_Container.addSubview(MailOrPhone_ImageView)

            Phone_Email_TextField.leftViewMode = UITextField.ViewMode.always
            Phone_Email_TextField.leftView = MailImageView_Container
            Phone_Email_TextField.placeholder = "Email".localized()
            
            Phone_Email_TextField.delegate = self
       
        }
        else{
            LoginMethodChanger_Button.tag = 2
            MailOrPhone_ImageView.removeFromSuperview()
            MailImageView_Container.removeFromSuperview()

            Phone_Email_TextField.text = ""
//            Phone_Email_TextField.placeholder = "Phone Number"
            LoginMethodChanger_Button.setTitle("  " + ("Using Email".localized()), for: .normal)
            MailImageView_Container.frame = CGRect(x: 0, y: 0, width: 90, height: 54)
            MailImageView_Container.addSubview(Seperator)
            MailImageView_Container.addSubview(CountryCode_Label)
            MailImageView_Container.addSubview(ShowCountryCode_Button)

            Phone_Email_TextField.leftViewMode = UITextField.ViewMode.always
            Phone_Email_TextField.leftView = MailImageView_Container
            Phone_Email_TextField.placeholder = "Phone Number".localized()
            Phone_Email_TextField.delegate = self

        }
    }
    
    private func OnCreateFirstNameTextFieldLeftView(){
        let imageView = UIImageView(frame: CGRect(x: 17.0, y: 20, width: 14.0, height: 14.0))
        let image = UIImage(named: "PersonWithFrameIc")
        imageView.image = image
        imageView.contentMode = .scaleAspectFit
        let view = UIView(frame: CGRect(x: 0, y: 0, width: 47, height: 54))
        view.addSubview(imageView)
        FirstName_TextField.leftViewMode = UITextField.ViewMode.always
        FirstName_TextField.leftView = view
    }
    
    private func OnCreateLastNameTextFieldLeftView(){
        let imageView = UIImageView(frame: CGRect(x: 17.0, y: 20, width: 14.0, height: 14.0))
        let image = UIImage(named: "PersonWithFrameIc")
        imageView.image = image
        imageView.contentMode = .scaleAspectFit
        let view = UIView(frame: CGRect(x: 0, y: 0, width: 47, height: 54))
        view.addSubview(imageView)
        LastName_TextField.leftViewMode = UITextField.ViewMode.always
        LastName_TextField.leftView = view
    }
    
    private func OnCreatePassWordTextFieldLeftView(){
        let imageView = UIImageView(frame: CGRect(x: 18.0, y: 20, width: 13.0, height: 14.0))
        let image = UIImage(named: "LockIc")
        imageView.image = image
        imageView.contentMode = .scaleAspectFit
        let view = UIView(frame: CGRect(x: 0, y: 0, width: 47, height: 54))
        view.addSubview(imageView)
        Password_TextField.leftViewMode = UITextField.ViewMode.always
        Password_TextField.leftView = view
    }
    
    
    // MARK: - Network
    
        //USER INFO FOR REQUEST POSTING:
        private var WithEmail:Bool = true
        private var CountryPhoneCode:Int?
        private var UserNationality:UserNationalitiesModel!
        private var VerificationMethod_Id:Int = 0
        private var PrimaryEmailID:Int = 0
        private var PrimaryPhoneID:Int = 0
    
    private func RequestToSignUp(URL:String,Param:[String:Any]){
        UserConnectionManager.Request(URL: URL, RequestMethod: .post, Parameter: Param, { (HTTPStatus, RecievedJSON) in
            
            print("SignUp successed with code \(HTTPStatus)")
            let IDs = self.GetIDs(Data: RecievedJSON!)
            UserDefaults.standard.set(IDs.user_id, forKey: "user_id")
            self.RequestSetUserLanguage(User_id: IDs.user_id)
            self.RequestToSetUserNationality(User_id: IDs.user_id)
            
            if self.WithEmail {
                self.VerificationMethod_Id = IDs.email_id
                self.RequestToSendEmail(PrimaryEmailId: IDs.email_id)
                self.performSegue(withIdentifier: "From_SignUp_GoTo_IdentityAuthentication_Segue", sender: self)
            }else{
                self.VerificationMethod_Id = IDs.phone_id
                self.RequestToSendSMS(PrimaryPhoneId: IDs.phone_id)
                self.performSegue(withIdentifier: "From_SignUp_GoTo_IdentityAuthentication_Segue", sender: self)
            }
            
        }) { (HTTPStatus, ErrorArray) in
            print("SignUp failed with code \(HTTPStatus)")
            self.SetButtonToWatingState(Button: self.SignUp_Button, ButtonState: .Enabled, ButtonText: "Sign up")
            self.ShowAlert(Message: ErrorArray?[0].body ?? "Check Your Connection".localized() , Seconds: 2)
        }
    }
    
    
    
    private func RequestSetUserLanguage(User_id: Int){
        UserConnectionManager.RequestWithHeader(URL: "\(DOMAIN_URL)\(User_URL)\(User_id)\(Set_User_Language_URL)", RequestMethod: .post, Parameter: ["language":imLanguageManager.GetNotationFrom()], {(HTTPStatus, RecievedJson) in
            print("Setting Language Successed with code \(HTTPStatus)")
        }) { (HTTPStatus, ErrorArray) in
            self.SetButtonToWatingState(Button: self.SignUp_Button, ButtonState: .Enabled, ButtonText: "Sign up")
            self.ShowAlert(Message: ErrorArray?[0].body ?? "Check Your Connection".localized(), Seconds: 2)
        }
    }
    
    private func GetIDs(Data:JSON) -> (user_id:Int, email_id:Int,phone_id:Int){
        let user_id = Data["driver"]["user"]["id"].intValue
        let PrimaryEmailID = Data["driver"]["primary_email"]["id"].intValue
        let PrimaryPhoneID = Data["driver"]["primary_phone"]["id"].intValue
        return (user_id,PrimaryEmailID,PrimaryPhoneID)
    }
    
    private func RequestToSetUserNationality(User_id: Int){
        
        let Address = "\(DOMAIN_URL)\(User_URL)\(User_id)\(Set_User_Nationality_URL)"
        
        UserConnectionManager.RequestWithHeader(URL: Address, RequestMethod: .post, Parameter: ["nationality_id" : UserNationality.getID()], { (HTTPStatus, RecievedJson) in
            print("Setting UserNationality successed with code \(HTTPStatus)")
        }) { (HTTPStatus, ErrorArray) in
            print("Setting UserNationality failed with code \(HTTPStatus)")
            self.ShowAlert(Message: ErrorArray?[0].body ?? "Check Your Connection".localized(), Seconds: 2)
        }
    }
    
    private func RequestToSendEmail(PrimaryEmailId: Int){
        let Address = "\(DOMAIN_URL)\(Email_URL)\(PrimaryEmailId)\(Start_Verification_URL)"
        
        UserConnectionManager.RequestWithHeader(URL: Address, RequestMethod: .get, Parameter: nil, { (HTTPStatus, RecievedJson) in
            print("Sending Email successed with code \(HTTPStatus)")
        }) { (HTTPStatus, ErrorArray) in
            print("Sending Email failed with code \(HTTPStatus)")
            self.ShowAlert(Message: ErrorArray?[0].body ?? "Check Your Connection".localized(), Seconds: 2)
        }
    }
    
    private func RequestToSendSMS(PrimaryPhoneId: Int){
        let Address = "\(DOMAIN_URL)\(Phone_URL)\(PrimaryPhoneId)\(Start_Verification_URL)"
        
        UserConnectionManager.RequestWithHeader(URL: Address, RequestMethod: .get, Parameter: nil, { (HTTPStatus, RecievedJson) in
            print("Sending Phone successed with code \(HTTPStatus)")
        }) { (HTTPStatus, ErrorArray) in
            print("Sending Phone failed with code \(HTTPStatus)")
            self.ShowAlert(Message: ErrorArray?[0].body ?? "Check Your Connection".localized(), Seconds: 2)
        }
    }
    
    private func CheckForDataCompeletion()->Bool{
        if Phone_Email_TextField.text == "" {
            if WithEmail{
                ShowAlert(Message: "Enter your Email".localized(), Seconds: 2)
            }else{
                ShowAlert(Message: "Enter your Phone".localized(), Seconds: 2)
            }
            return false
        }
        if FirstName_TextField.text == "" {
            ShowAlert(Message: "Enter your FirstName".localized(), Seconds: 2)
            return false
        }
        if LastName_TextField.text == ""{
            ShowAlert(Message: "Enter your LastName".localized(), Seconds: 2)
            return false
        }
        if Password_TextField.text == "" {
            ShowAlert(Message: "Enter your Password".localized(), Seconds: 2)
            return false
        }
        if UserNationality == nil {
            ShowAlert(Message: "Select Your Nationality".localized(), Seconds: 2)
            return false
        }
        if !WithEmail {
            if CountryPhoneCode == nil  {
                ShowAlert(Message: "Select a Country Code".localized(), Seconds: 2)
                return false
            }
        }
        return true
    }
    
    // MARK: - Navigation
    
    private var imLanguageManager:LanguageManager!
    
    private func UpdateNationalityPicker(){
        NationalityPicker_Label.text = UserNationality.name
        guard let ImageAddres = UserNationality.flag?.file else {
            return
        }
        guard let URL = URL(string: PURE_DOMAIN_URL + ImageAddres ) else {
            return
        }
        NationalityPicker_ImageView.LoadImageFromServer(url:URL)
            
    }
    
    @IBAction func UnwindFrom_LanguagePickerViewControlller_To_SignUpViewControlller(segue:UIStoryboardSegue) {
        
        if let sourceViewController = segue.source as? LanguagePickerViewController {
            let Lang = sourceViewController.getSelectedLanguage().Language
            imLanguageManager = LanguageManager(UserSelectedLanguage_CompeleteForm:Lang)
            if LanguageManager.GetCompleteFormFrom(Notaion:imLanguageManager.GetLanguage_Default()) != Lang {
                let not = imLanguageManager.GetNotationFrom()
                if not == "en" || not == "fa" || not == "ar" || not == "ru"{
                    SetPreferedLangAlert(Lang: imLanguageManager.GetNotationFrom())
                    
                }else {
                    DispatchQueue.main.async {
                        self.ShowAlert(Message: "Feature not available".localized(), Seconds: 2)
                    }
                }
            }
        }
    }
    
    @IBAction func UnwindFrom_CountryCodePickerViewControlller_To_SignUpViewControlller(segue:UIStoryboardSegue) {
        
        if let sourceViewController = segue.source as? CountryCodePickerViewController {
            CountryPhoneCode = sourceViewController.getSelectedCountryCode()
            CountryCode_Label.text = "+\(CountryPhoneCode ?? 0)"//Updateview
        }
    }
    
    @IBAction func UnwindFrom_IdentityAuthenticationViewControlller_To_SignUpViewControlller(segue:UIStoryboardSegue) {
        
        /*if segue.source is IdentityAuthenticationViewController {
            
        }*/
    }
    @IBAction func UnwindFrom_NationalityPickerViewControlller_To_SignUpViewControlller(segue:UIStoryboardSegue) {
        
        if let sourceViewController = segue.source as? NationalityPickerViewController {
            UserNationality = sourceViewController.getSelectedNationalityModel()
            UpdateNationalityPicker()
        }
    }
    
    @objc func OnChooseLanguageButtonClicked(_ sender: UIButton){
           performSegue(withIdentifier: "From_SignUp_Goto_LanguagePicker_Segue", sender: self)
       }
       
    @objc func OnShowCountryCodeButtonClicked(_ sender: UIButton){
           performSegue(withIdentifier: "From_SignUp_GoTo_CountryCodePicker_Segue", sender: self)
    }

    @objc func OnSignUpButtonClicked(_ sender: UIButton){
        
        SetButtonToWatingState(Button: SignUp_Button, ButtonState: .Disabled, ButtonText: "")
        let FCM_Token:String = UserDefaults.standard.string(forKey: "fcmToken_Notif") ?? ""
        
        if WithEmail && CheckForDataCompeletion() {
            RequestToSignUp(URL:"\(DOMAIN_URL)\(EMAIL_SIGNUP_URL)" , Param: ["first_name" : FirstName_TextField.text!,"last_name": LastName_TextField.text!,"email": Phone_Email_TextField.text!,"password": Password_TextField.text!,"firebase_token":FCM_Token])
        }
            
        else if !WithEmail && CheckForDataCompeletion() {
            RequestToSignUp(URL:"\(DOMAIN_URL)\(PHONE_SIGNUP_URL)" , Param: ["first_name" : FirstName_TextField.text!,"last_name": LastName_TextField.text!,"phone": "00" + "\(CountryPhoneCode ?? 0)" + Phone_Email_TextField.text!,"password": Password_TextField.text!,"firebase_token":FCM_Token])
        }
        
    }
    @objc func OnNationalityButtonClicked(_ sender: UIButton){
           performSegue(withIdentifier: "From_SignUp_To_NationalityPicker_Segue", sender: self)
    }
    
    @objc func OnLoginButtonClicked(_ sender: UIButton){
        performSegue(withIdentifier: "UnwindFrom_SignUp_To_Login_Segue", sender: self)
    }

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        if segue.identifier == "From_SignUp_Goto_LanguagePicker_Segue" {
            if let LanguagePickerInstance = segue.destination as? LanguagePickerViewController {
                LanguagePickerInstance.setUnwind(withIdentifier: "UnwindFrom_LanguagePicker_To_SignUp_Segue")
            }
        }
        else if segue.identifier == "From_SignUp_GoTo_CountryCodePicker_Segue" {
            if let LanguagePickerInstance = segue.destination as? CountryCodePickerViewController {
                LanguagePickerInstance.setUnwind(withIdentifier: "UnwindFrom_CountryCodePicker_To_SignUp_Segue")
            }
        }
        else if segue.identifier == "From_SignUp_To_NationalityPicker_Segue" {
            if let LanguagePickerInstance = segue.destination as? NationalityPickerViewController {
                LanguagePickerInstance.UnwindToViewControllerID = "UnwindFrom_NationalityPicker_To_SignUp_Segue"
            }
        }
        else if segue.identifier == "From_SignUp_GoTo_IdentityAuthentication_Segue" {
            if let LanguagePickerInstance = segue.destination as? IdentityAuthenticationViewController {
                LanguagePickerInstance.SetIDForVerification(NeededInfo: (WithEmail,VerificationMethod_Id,Phone_Email_TextField.text!))
            }
        }
    }
    

}
