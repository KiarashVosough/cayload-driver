//
//  IdentityAuthenticationViewController.swift
//  CayLoad-Driver
//
//  Created by Kiyarash Vosough on 1/21/1399 AP.
//  Copyright © 1399 CayLoad. All rights reserved.
//

import UIKit

class IdentityAuthenticationViewController: UIViewController {
    
    /// `Outlets`
    @IBOutlet weak var VerificationMethodText_Label: UILabel!
    @IBOutlet weak var UserVerificationData_Label: UILabel!
    @IBOutlet weak var Code_ContainerView: UIView!
    @IBOutlet weak var CompleteRegistration_Button: UIButton!
    @IBOutlet weak var GoBack_Button: UIButton!
    @IBOutlet weak var FirstDigit_TextField: UITextField!
    @IBOutlet weak var FirstDigitBottomLine_View: UIView!
    @IBOutlet weak var SecondDigit_TextField: UITextField!
    @IBOutlet weak var SecondDigitBottomLine_View: UIView!
    @IBOutlet weak var ThirdDigit_TextField: UITextField!
    @IBOutlet weak var ThirdDigitBottomLine_View: UIView!
    @IBOutlet weak var FourthDigit_TextField: UITextField!
    @IBOutlet weak var FourthDigitBottomLine_View: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UIView.appearance().semanticContentAttribute = .forceLeftToRight
        AddActions()
        UpdateView()
        // Do any additional setup after loading the view.
    }
    
    private func AddActions(){
        FirstDigit_TextField.addTarget(self, action: #selector(ChangeTextFieldCursor(_:)), for: .editingChanged)
        SecondDigit_TextField.addTarget(self, action: #selector(ChangeTextFieldCursor(_:)), for: .editingChanged)
        ThirdDigit_TextField.addTarget(self, action: #selector(ChangeTextFieldCursor(_:)), for: .editingChanged)
        FourthDigit_TextField.addTarget(self, action: #selector(ChangeTextFieldCursor(_:)), for: .editingChanged)
        FirstDigit_TextField.becomeFirstResponder()
        view.endEditing(false)
        GoBack_Button.addTarget(self, action: #selector(OnGoBackButtonClicked(_:)), for: .touchUpInside)
        CompleteRegistration_Button.addTarget(self, action: #selector(OnCompeleteRegisterationButtonClicked(_:)), for: .touchUpInside)
    }
    
    private func UpdateView(){
        if NeededInfoForVerify!.With_Email {
            VerificationMethodText_Label.text = "Enter the code sent to your email :".localized()
            UserVerificationData_Label.text = NeededInfoForVerify?.email_or_phone
        }else{
            VerificationMethodText_Label.text = "Enter the code sent to your phone :".localized()
            UserVerificationData_Label.text =  NeededInfoForVerify?.email_or_phone
        }
    }
    
    @objc func ChangeTextFieldCursor(_ sender: UITextField){
        if sender.text?.utf16.count == 1 {
            switch sender {
            case FirstDigit_TextField:
                FirstDigitBottomLine_View.backgroundColor = .App_LightGray
                SecondDigitBottomLine_View.backgroundColor = .App_DarkBlue
                SecondDigit_TextField.becomeFirstResponder()
            case SecondDigit_TextField:
                SecondDigitBottomLine_View.backgroundColor = .App_LightGray
                ThirdDigitBottomLine_View.backgroundColor = .App_DarkBlue
                ThirdDigit_TextField.becomeFirstResponder()
            case ThirdDigit_TextField:
                ThirdDigitBottomLine_View.backgroundColor = .App_LightGray
                FourthDigitBottomLine_View.backgroundColor = .App_DarkBlue
                FourthDigit_TextField.becomeFirstResponder()
            case FourthDigit_TextField:
                view.endEditing(true)
            default:
                break
            }
        }
        else if sender.text?.utf16.count == 0 {
            switch sender {
            case FourthDigit_TextField:
                ThirdDigitBottomLine_View.backgroundColor = .App_DarkBlue
                FourthDigitBottomLine_View.backgroundColor = .App_LightGray
                ThirdDigit_TextField.becomeFirstResponder()
            case ThirdDigit_TextField:
                SecondDigitBottomLine_View.backgroundColor = .App_DarkBlue
                ThirdDigitBottomLine_View.backgroundColor = .App_LightGray
            SecondDigit_TextField.becomeFirstResponder()
            case SecondDigit_TextField:
                FirstDigitBottomLine_View.backgroundColor = .App_DarkBlue
                SecondDigitBottomLine_View.backgroundColor = .App_LightGray
                FirstDigit_TextField.becomeFirstResponder()
            case FirstDigit_TextField:
                FirstDigit_TextField.becomeFirstResponder()
            default:
                break
            }
        }
    }

    
    // MARK: - Network
    
    private var NeededInfoForVerify:(With_Email:Bool,Method_Id:Int,email_or_phone:String)? = (true,1,"sadfgh")
    
    private func RequestToCheckForVerification(URL:String,Param:[String:Any]){
        
        if GetEntredCodeFromTextField()?.utf16.count == 4{
            UserConnectionManager.RequestWithHeader(URL: URL, RequestMethod: .post, Parameter: Param, { (HTTPStatus, RecievedJSON) in
                print("Verifivation successed with code \(HTTPStatus)")
                self.performSegue(withIdentifier: "From_IdentityAuth_To_UserAgreement_Segue", sender: self)
            }) { (HTTPStatus, ErrorArray) in
                self.SetButtonToWatingState(Button: self.CompleteRegistration_Button, ButtonState: .Enabled, ButtonText: "Complete Registration".localized())
                print("Verifivation failed with code \(HTTPStatus)")
                self.ShowAlert(Message: ErrorArray?[0].body ?? "Check Your Connection".localized(), Seconds: 2)
            }
        }
    }
    
    private func GetEntredCodeFromTextField() -> String? {
        var EnteredCode = String()
        EnteredCode.append(FirstDigit_TextField.text ?? "")
        EnteredCode.append(SecondDigit_TextField.text ?? "")
        EnteredCode.append(ThirdDigit_TextField.text ?? "")
        EnteredCode.append(FourthDigit_TextField.text ?? "")
        return EnteredCode
    }
    
    
    // MARK: - Navigation
    
    func SetIDForVerification(NeededInfo: (With_Email:Bool,Method_Id:Int,email_or_phone:String)){
        NeededInfoForVerify = NeededInfo
    }

    @IBAction func UnwindFrom_UserAgreementViewControlller_To_IdentityAuthenticationViewControlller(segue:UIStoryboardSegue) {
        /*if segue.source is UserAgreementViewController {
        }*/
    }
    
    @objc func OnGoBackButtonClicked(_ sender: UIButton){
        performSegue(withIdentifier: "UnwindFrom_IdentityAuthentication_To_SignUp_Segue", sender: self)
    }
    @objc func OnCompeleteRegisterationButtonClicked(_ sender: UIButton){
        
        guard let code = GetEntredCodeFromTextField()  else {
            return
        }
        
        SetButtonToWatingState(Button: CompleteRegistration_Button, ButtonState: .Disabled, ButtonText: "")
        
        if NeededInfoForVerify!.With_Email {
            print("\(DOMAIN_URL)\(Email_URL)\(NeededInfoForVerify?.Method_Id ?? 0)\(Check_Verification_URL)")
            print(["code" : code])
            RequestToCheckForVerification(URL: "\(DOMAIN_URL)\(Email_URL)\(NeededInfoForVerify?.Method_Id ?? 0)\(Check_Verification_URL)", Param: ["code" : code])
        }else{
            RequestToCheckForVerification(URL: "\(DOMAIN_URL)\(Phone_URL)\( NeededInfoForVerify?.Method_Id ?? 0)\(Check_Verification_URL)", Param: ["code" : code])
        }
    }
    
    /* In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }*/
    

}
