//
//  UserAgreementViewController.swift
//  CayLoad-Driver
//
//  Created by Kiyarash Vosough on 1/23/1399 AP.
//  Copyright © 1399 CayLoad. All rights reserved.
//

import UIKit

class UserAgreementViewController: UIViewController {

    /// `Outlets`
    @IBOutlet weak var UserAgreement_TextView: UITextView!
    @IBOutlet weak var Accept_Button: UIButton!
    @IBOutlet weak var GoBack_Button: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UIView.appearance().semanticContentAttribute = .forceLeftToRight
        AddAction()
    }
    
    func AddAction(){
        GoBack_Button.addTarget(self, action: #selector(OnGoBackButtonClicked(_:)), for: .touchUpInside)
        Accept_Button.addTarget(self, action: #selector(OnAcceptButtonClicked), for: .touchUpInside)
    }

    // MARK: - Network
    
    private func GetUserAgreement(){
        
        UserConnectionManager.RequestWithHeader(URL: "\(DOMAIN_URL)\(Get_User_Agreement_URL)", RequestMethod: .post, Parameter: nil, { (HTTPStatus, RecievedJSON) in
            if let Text = RecievedJSON?["users"].string {
                self.UserAgreement_TextView.text = Text
            }else{
                self.UserAgreement_TextView.text = "Unavailable"
            }
            print("Agreement Text recieved successfully with code \(HTTPStatus)")
        }) { (HTTPStatus, ErrorArray) in
            print("Agreement Text recieved unsuccessfully with code \(HTTPStatus)")
        }
    }
    
    private func AcceptUserAgreement(){
        
        UserConnectionManager.RequestWithHeader(URL: "\(DOMAIN_URL)\(Accept_Agreement_URL)", RequestMethod: .get, Parameter: nil, { (HTTPStatus, RecievedJSON) in
            print("Agreement accepted successfully with code \(HTTPStatus)")
            self.performSegue(withIdentifier: "From_UserAgreement_To_PlanSelection_Segue", sender: self)
        }) { (HTTPStatus, ErrorArray) in
            self.SetButtonToWatingState(Button: self.Accept_Button, ButtonState: .Disabled, ButtonText: "I Accept Agreement".localized())
            print("Agreement accepted unsuccessfully with code \(HTTPStatus)")
        }
    }
    
    // MARK: - Navigation
    
    @objc func OnGoBackButtonClicked(_ sender: UIButton){
        performSegue(withIdentifier: "UnwindFrom_UserAgreement_To_IdentityAuth_Segue", sender: self)
    }
    @objc func OnAcceptButtonClicked(_ sender: UIButton){
        SetButtonToWatingState(Button: Accept_Button, ButtonState: .Disabled, ButtonText: "")
        AcceptUserAgreement()
    }

    /* In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }*/

}
