//
//  PlanFreeHeaderCollectionReusableView.swift
//  CayLoad-Driver
//
//  Created by Kiyarash Vosough on 1/23/1399 AP.
//  Copyright © 1399 CayLoad. All rights reserved.
//

import UIKit

class PlanFreeHeaderCollectionReusableView: UICollectionReusableView {
    
    var NavigationDelegate:OnRegistrationBuyingPlan?
    
    @IBOutlet weak var Free_Button: UIButton!
    
    func OnCreate(){
        Free_Button.addTarget(self, action: #selector(OnFreeButtonClicked(_:)), for: .touchUpInside)
    }
    
    @objc func OnFreeButtonClicked(_ sender: UIButton){
        NavigationDelegate?.PerforSegueToTruckInformation()
    }
}
