//
//  RegistrationPaymentViewController.swift
//  CayLoad-Driver
//
//  Created by Kiyarash Vosough on 1/25/1399 AP.
//  Copyright © 1399 CayLoad. All rights reserved.
//

import UIKit

class RegistrationPaymentViewController: UIViewController {
    
    
    @IBOutlet weak var PlanDuration_Label: UILabel!
    @IBOutlet weak var PlanPrice_Label: UILabel!
    @IBOutlet weak var Visa_Button: UIButton!
    @IBOutlet weak var MasterCard_Button: UIButton!
    @IBOutlet weak var GatewayCountry_Button: UIButton!
    @IBOutlet weak var Pay_Button: UIButton!
    @IBOutlet weak var GoBack_Button: UIButton!
    
    var PlanPrice:Int!
    var PlanDuration:String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UIView.appearance().semanticContentAttribute = .forceLeftToRight
        GoBack_Button.addTarget(self, action: #selector(OnGoBackButtonClicked(_:)), for: .touchUpInside)
        Pay_Button.addTarget(self, action: #selector(OnPayButtonClicked), for: .touchUpInside)
        SetPlanInfo()
        // Do any additional setup after loading the view.
    }
    
    private func SetPlanInfo(){
        PlanDuration_Label.text = PlanDuration
        PlanPrice_Label.text = "$\(PlanPrice ?? 0)"
    }
    
    // MARK: - Navigation

    @objc func OnGoBackButtonClicked(_ sender: UIButton){
        performSegue(withIdentifier: "UnwindFrom_RegistrationPayment_To_PlanSelection_Segue", sender: self)
    }
    
    @objc func OnPayButtonClicked(_ sender: UIButton){
        performSegue(withIdentifier: "From_RegistrationPayment_To_TruckInformation_Segue", sender: self)
    }
    
    /* In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }*/
    

}
