//
//  LoginViewController.swift
//  CayLoad-Driver
//
//  Created by Kiyarash Vosough on 1/18/1399 AP.
//  Copyright © 1399 CayLoad. All rights reserved.
//

import UIKit
import SwiftyJSON

class LoginViewController: UIViewController,UITextFieldDelegate {
    
    /// `Outlets`
    @IBOutlet weak var LoginMethodChanger_Button: UIButton!
    @IBOutlet weak var Phone_Email_TextField: UITextField!
    @IBOutlet weak var Password_TextField: UITextField!
    @IBOutlet weak var ChooseLanguage_Button: UIButton!
    @IBOutlet weak var Signin_Button: UIButton!
    @IBOutlet weak var CreateNewAccount_Button: UIButton!
    @IBOutlet weak var ForgotPassword_Button: UIButton!
    @IBOutlet weak var LanguagePicker_ImageView: UIImageView!
    @IBOutlet weak var LanguageButton_Label: UILabel!
    
    /// `Dynamic Componnent`
    private let MailOrPhone_ImageView:UIImageView = {
        let instance = UIImageView(frame: CGRect(x: 15, y: 21, width: 16, height: 12))
        instance.image = UIImage(named: "MailIc")
        instance.contentMode = .scaleAspectFit
        return instance
    }()
    private let MailImageView_Container = UIView()
    private let ShowCountryCode_Button:UIButton = {
        let instance = UIButton(frame: CGRect(x: 0, y: 0, width: 40, height: 54))
        instance.setImage(UIImage(named: "DownArrowIc") , for: .normal)
        return instance
    }()
    private let CountryCode_Label:UILabel = {
        let instance = UILabel(frame: CGRect(x: 40, y: 0, width: 44, height: 54))
        instance.text = "Code".localized()
        instance.font = UIFont(name: "Poppins-Regular", size: 10)
        return instance
    }()
    private let Seperator:UIView = {
        let instance = UIView(frame: CGRect(x: 80, y: 15, width: 1, height: 24))
        instance.backgroundColor = .lightGray
        return instance
    }()
    
    // MARK: - Localization
    
    private func Localize(){
        Phone_Email_TextField.placeholder = "Email".localized()
        Password_TextField.placeholder =  "Password".localized()
        LanguageButton_Label.text = "Language".localized()
        Signin_Button.setTitle("Sign in".localized(), for: .normal)
        CreateNewAccount_Button.setTitle("Create New Account".localized(), for: .normal)
        ForgotPassword_Button.setTitle("Forgot Password ?".localized(), for: .normal)
        LoginMethodChanger_Button.setTitle("  " + ("Using Phone".localized()), for: .normal)
    }
    
    private func SetupLanguage(){
        imLanguageManager = LanguageManager()
        imLanguageManager.SetLanguageViewProperites(Lang_Image: LanguagePicker_ImageView)
    }
    // MARK: - TextField Delegate
    
    func textFieldShouldReturn(_ Sender: UITextField) -> Bool{
        Sender.resignFirstResponder()
        return true
    }
    
    // MARK: - Functions
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        Localize()
        SetupLanguage()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        SetPreferedLangToEn()
        AddTarget()
        OnCreateSigninMethodComponnent(Tag: 1)
        OnCreatePassWordTextFieldLeftView()
    }
    
    private func AddTarget(){
        CreateNewAccount_Button.addTarget(self, action: #selector(OnCreateNewAccountButtonClicked(_:)), for: .touchUpInside)
        LoginMethodChanger_Button.addTarget(self, action: #selector(OnLoginMethodChangerButtonClicked(_:)), for: .touchUpInside)
        ChooseLanguage_Button.addTarget(self, action: #selector(OnChooseLanguageButtonClicked), for: .touchUpInside)
        ShowCountryCode_Button.addTarget(self, action: #selector(OnShowCountryCodeButtonClicked(_:)), for: .touchUpInside)
        Signin_Button.addTarget(self, action: #selector(OnLoginButtonClicked), for: .touchUpInside)
        ForgotPassword_Button.addTarget(self, action: #selector(OnForgotPasswordButtonClicked(_:)), for: .touchUpInside)
    }
    
    private func OnCreateSigninMethodComponnent(Tag:Int){
        if  Tag == 1 {
            WithEmail = true
            LoginMethodChanger_Button.tag = 0
            Seperator.removeFromSuperview()
            CountryCode_Label.removeFromSuperview()
            ShowCountryCode_Button.removeFromSuperview()
            
            Phone_Email_TextField.text = ""
            LoginMethodChanger_Button.setTitle("  " + ("Using Phone".localized()), for: .normal)
            
            MailImageView_Container.frame = CGRect(x: 0, y: 0, width: 47, height: 54)
            MailImageView_Container.addSubview(MailOrPhone_ImageView)
            
            Phone_Email_TextField.leftViewMode = UITextField.ViewMode.always
            Phone_Email_TextField.leftView = MailImageView_Container
            Phone_Email_TextField.placeholder = "Email".localized()
            Phone_Email_TextField.delegate = self
        }
        else{
            WithEmail = false
            LoginMethodChanger_Button.tag = 1
            MailOrPhone_ImageView.removeFromSuperview()
            MailImageView_Container.removeFromSuperview()
            
            Phone_Email_TextField.text = ""
            LoginMethodChanger_Button.setTitle("  " + ("Using Email".localized()), for: .normal)
            MailImageView_Container.frame = CGRect(x: 0, y: 0, width: 90, height: 54)
            MailImageView_Container.addSubview(Seperator)
            MailImageView_Container.addSubview(CountryCode_Label)
            MailImageView_Container.addSubview(ShowCountryCode_Button)
            
            Phone_Email_TextField.leftViewMode = UITextField.ViewMode.always
            Phone_Email_TextField.leftView = MailImageView_Container
            Phone_Email_TextField.placeholder = "Phone Number".localized()
            Phone_Email_TextField.delegate = self
            
        }
    }
    
    private func OnCreatePassWordTextFieldLeftView(){
        let imageView = UIImageView(frame: CGRect(x: 18.0, y: 20, width: 13.0, height: 14.0))
        let image = UIImage(named: "LockIc")
        imageView.image = image
        imageView.contentMode = .scaleAspectFit
        let view = UIView(frame: CGRect(x: 0, y: 0, width: 47, height: 54))
        view.addSubview(imageView)
        Password_TextField.leftViewMode = UITextField.ViewMode.always
        Password_TextField.leftView = view
    }

    // MARK: - Network
    
    var WithEmail:Bool = true
    var CountryCode:Int = 0
    var Driver:DriverModel?
    private var InfoForVerify:(With_Email:Bool,Method_Id:Int,email_or_phone:String)?
    
    private func RequestToLogin(Param:[String:Any], URL:String){
        UserConnectionManager.Request(URL: URL , RequestMethod:.post , Parameter: Param, { (HTTPStatus, RecievedJson) in
            print("Login Successfulyy with code \(HTTPStatus)")
            self.Driver = DriverModel(RecievedJson!["driver"])
            self.RequestToSetUserLanguage(User_id: self.Driver?.user?.id ?? 0)
            self.CheckForRegistrationCompletion()
//            self.performSegue(withIdentifier: "From_Login_To_MainTabBar_Segue", sender: self)
        }) { (HTTPStatus, ErrorArray) in
            print("Login Failed with code \(HTTPStatus)")
            self.SetButtonToWatingState(Button: self.Signin_Button, ButtonState: .Enabled, ButtonText: "Sign in".localized())
            self.ShowAlert(Message: ErrorArray?[0].GetErrorBody() ?? "Check Your Connection".localized(), Seconds: 2)
        }
    }
    
    private func RequestToSetUserLanguage(User_id: Int){
        UserConnectionManager.RequestWithHeader(URL: "\(DOMAIN_URL)\(User_URL)\(User_id)\(Set_User_Language_URL)", RequestMethod: .post, Parameter: ["language":imLanguageManager.GetNotationFrom()], { (HTTPStatus, RecievedJson) in
            print("Setting Language Successed with code \(HTTPStatus)")
        }) { (HTTPStatus, ErrorArray) in
            print("Setting Language failed with code \(HTTPStatus)")
            self.SetButtonToWatingState(Button: self.Signin_Button, ButtonState: .Enabled, ButtonText: "Sign in".localized())
            self.ShowAlert(Message: ErrorArray?[0].body ?? "Check Your Connection".localized(), Seconds: 2)
        }
    }
    
    private func GetUserID(Data:JSON) -> Int{
        if let id = Data["driver"]["user"]["id"].int {
            return id
        }
        else{
            return 0
        }
    }
    
    private func CheckForRegistrationCompletion(){
        
        UserDefaults.standard.set(Driver?.user?.id, forKey: "user_id")
        UserDefaults.standard.set(Driver?.id, forKey: "driver_id")
        
        if Driver?.primary_email?.date_verified == nil && Driver?.primary_email?.address != nil{
            InfoForVerify = (true,Driver?.primary_email?.id ?? 0, Driver?.primary_email?.address ?? "")
            performSegue(withIdentifier: "From_Login_To_IdentityAuth_Segue", sender: self)
        }
        else if Driver?.primary_phone?.date_verified == nil && Driver?.primary_phone?.number != nil {
            InfoForVerify = (false,Driver?.primary_email?.id ?? 0, Driver?.primary_phone?.number ?? "")
            performSegue(withIdentifier: "From_Login_To_IdentityAuth_Segue", sender: self)
        }
        else if Driver?.user?.date_agreement_accepted == nil {
            performSegue(withIdentifier: "From_Login_To_UserAgreement_Segue", sender: self)
        }
        else if Driver?.user?.nationality?.unlimited_usage == false && Driver?.plan == nil {
            performSegue(withIdentifier: "From_Login_To_PlanSelection_Segue", sender: self)
        }
        else if Driver?.truck == nil {
            performSegue(withIdentifier: "From_Login_To_TruckInformation_Segue", sender: self)
        }
        else if Driver?.vehicle_card == nil || Driver?.license == nil {
            performSegue(withIdentifier: "From_Login_To_Document_Segue", sender: self)
        }
        else{
            UserDefaults.standard.set(true, forKey: "Is_Login")
            UserDefaults.standard.set("\(TOKEN)", forKey: "VALID_TOKEN")
            self.performSegue(withIdentifier: "From_Login_To_MainTabBar_Segue", sender: self)
        }
    }
    
    // MARK: - Navigation
    private var imLanguageManager:LanguageManager!

    
    private func SetCountryCode(RawCode:Int){
        print(RawCode)
        CountryCode_Label.text = "+\(RawCode)"
        CountryCode = RawCode
    }
    
    @IBAction func UnwindFrom_LanguagePickerViewControlller_To_LoginViewControlller(segue:UIStoryboardSegue) {
        
        if let sourceViewController = segue.source as? LanguagePickerViewController {
            let Lang = sourceViewController.getSelectedLanguage().Language
            imLanguageManager = LanguageManager(UserSelectedLanguage_CompeleteForm:Lang)
            if LanguageManager.GetCompleteFormFrom(Notaion:imLanguageManager.GetLanguage_Default()) != Lang {
                let not = imLanguageManager.GetNotationFrom()
                if not == "en" || not == "fa" || not == "ar" || not == "ru"{
                    SetPreferedLangAlert(Lang: imLanguageManager.GetNotationFrom())
                    
                }else {
                    DispatchQueue.main.async {
                        self.ShowAlert(Message: "Feature not available".localized(), Seconds: 2)
                    }
                }
            }
        }
    }
    
    @IBAction func UnwindFrom_CountryCodePickerViewControlller_To_LoginViewControlller(segue:UIStoryboardSegue) {
        
        if let sourceViewController = segue.source as? CountryCodePickerViewController {
            SetCountryCode(RawCode:sourceViewController.getSelectedCountryCode())
        }
    }
    
    @IBAction func UnwindFrom_SignUpViewController_To_LoginViewControlller(segue:UIStoryboardSegue) {
        
//        if segue.source is CreateNewAccountViewController {
//        }
    }
    
    @IBAction func UnwindFrom_ResetPasswordViewController_To_LoginViewControlller(segue:UIStoryboardSegue) {
           
//        if segue.source is ResetPasswordViewController {
//        }
    }
    
    @objc func OnLoginMethodChangerButtonClicked(_ sender: UIButton){
        OnCreateSigninMethodComponnent(Tag: sender.tag)
    }
    
    @objc func OnForgotPasswordButtonClicked(_ sender: UIButton){
        performSegue(withIdentifier: "From_Login_To_ResetPassword_Segue", sender: self)
    }
    
    @objc func OnChooseLanguageButtonClicked(_ sender: UIButton){
        performSegue(withIdentifier: "FromLoginGotoLanguagePickerSegue", sender: self)
    }
    
    @objc func OnShowCountryCodeButtonClicked(_ sender: UIButton){
        performSegue(withIdentifier: "FromLoginGoToCountryCodePickerSegue", sender: self)
    }
    
    @objc func OnCreateNewAccountButtonClicked(_ sender: UIButton){
        performSegue(withIdentifier: "From_LoginViewControlller_ToSignUpViewControlller_Segue", sender: self)
    }
    
    
    @objc func OnLoginButtonClicked(_ sender: UIButton){
        if Phone_Email_TextField.text == "" || Phone_Email_TextField.text == nil {
            if WithEmail{
                ShowAlert(Message: "Enter your Email".localized(), Seconds: 2)
                return
            }else{
                ShowAlert(Message: "Enter your Phone".localized(), Seconds: 2)
                return
            }
        }
        if Password_TextField.text == "" {
            ShowAlert(Message: "Enter your Password".localized(), Seconds: 2)
            return
        }

        let PhoneOrEmail:String = Phone_Email_TextField.text ?? ""
        let Password:String = Password_TextField.text ?? ""
        
//        let PhoneOrEmail = "swcjheh@hi2.in"
//        let Password = "kia20121999"
        
//        let PhoneOrEmail = "vosough.k@gmail.com"
//        let Password = "20121999"
        
        SetButtonToWatingState(Button: Signin_Button, ButtonState: .Disabled, ButtonText: "")
        if let FCM_Token:String = UserDefaults.standard.string(forKey: "fcmToken_Notif"){
            if WithEmail {
                RequestToLogin(Param: ["email": PhoneOrEmail ,"password": Password,"firebase_token":FCM_Token] , URL: "\(DOMAIN_URL)\(EMAIL_LOGIN_URL)")
            }else{
                if CountryCode == 0 {
                    self.PopUpActionSheet(Message: "Select your country code".localized(), Seconds: 3)
                    self.SetButtonToWatingState(Button: self.Signin_Button, ButtonState: .Enabled, ButtonText: "Sign in".localized())
                    return
                }
                RequestToLogin(Param: ["email": "00" + "\(CountryCode)" + PhoneOrEmail ,"password": Password] , URL: "\(DOMAIN_URL)\(PHONE_LOGIN_URL)")
            }
        }else{
            ShowAlert(Message: "Check Your Connection".localized(), Seconds: 2)
        }
        
        
    }
    

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        if segue.identifier == "FromLoginGotoLanguagePickerSegue" {
            if let LanguagePickerInstance = segue.destination as? LanguagePickerViewController {
                LanguagePickerInstance.setUnwind(withIdentifier: "UnwindFrom_LanguagePicker_To_Login_Segue")
            }
        }
            
        else if segue.identifier == "FromLoginGoToCountryCodePickerSegue" {
            if let LanguagePickerInstance = segue.destination as? CountryCodePickerViewController {
                LanguagePickerInstance.setUnwind(withIdentifier: "UnwindFrom_CountryCodePicker_To_Login_Segue")
            }
        }
        else if segue.identifier == "From_Login_To_IdentityAuth_Segue" {
            if let instance = segue.destination as? IdentityAuthenticationViewController {
                instance.SetIDForVerification(NeededInfo: InfoForVerify!)
            }
        }
    }
    

}
