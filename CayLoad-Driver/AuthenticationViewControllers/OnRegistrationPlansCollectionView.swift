//
//  OnRegistrationPlansCollectionView.swift
//  CayLoad-Driver
//
//  Created by Kiyarash Vosough on 2/4/1399 AP.
//  Copyright © 1399 CayLoad. All rights reserved.
//

import UIKit

class OnRegistrationPlansCollectionView: UICollectionViewCell {
    
    var NavigationDelegate:OnRegistrationBuyingPlan?
    
    @IBOutlet weak var PlanDuration_Label: UILabel!
    @IBOutlet weak var PlanBuying_Button: UIButton!
    @IBOutlet weak var PlanInfo_Label: UILabel!
    
    var PlanPrice:Int!
    var PlanDuration:String!
    var PlanInfo:String!
    
    func SetPlansAtributte(Duration:String,Price:Int,PlanInfo:String){
        PlanDuration_Label.text = Duration
        PlanBuying_Button.setTitle("$ \(Price)", for: .normal)
        
        PlanPrice = Price
        PlanDuration = Duration
        self.PlanInfo = PlanInfo
        PlanBuying_Button.addTarget(self, action: #selector(OnPriceButtonClicked(_:)), for: .touchUpInside)
    }
    
    @objc func OnPriceButtonClicked(_ sender: UIButton){
        if PlanPrice > 0 {
            NavigationDelegate?.PerformSegueToPaymentGateway(PlanPrice: self.PlanPrice, PlanDuration: self.PlanDuration)
        }else{
            NavigationDelegate?.PerforSegueToTruckInformation()
        }
    }
}
