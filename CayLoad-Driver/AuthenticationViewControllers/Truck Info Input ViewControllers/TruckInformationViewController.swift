//
//  TruckInformationViewController.swift
//  CayLoad-Driver
//
//  Created by Kiyarash Vosough on 2/6/1399 AP.
//  Copyright © 1399 CayLoad. All rights reserved.
//

import UIKit

class TruckInformationViewController: UIViewController,UITextFieldDelegate {
    
    @IBOutlet weak var Main_ScrollView: UIScrollView!
    
    @IBOutlet weak var Back_Button: UIButton!
    @IBOutlet weak var TruckTypePicker_Button: UIButton!
    @IBOutlet weak var TruckModel_TextField: UITextField!
    @IBOutlet weak var TruckTypePicker_Label: UILabel!
    
    @IBOutlet weak var NationalPlate_First_TextField: UITextField!
    @IBOutlet weak var NationalPlate_Second_TextField: UITextField!
    @IBOutlet weak var NationalPlate_Third_TextField: UITextField!
    @IBOutlet weak var NationalPlate_Fourth_TextField: UITextField!
    @IBOutlet weak var NationalPlate_Fifth_TextField: UITextField!
    
    @IBOutlet weak var TruckPlateNo_First_TextField: UITextField!
    @IBOutlet weak var TruckPlateNo_Second_TextField: UITextField!
    @IBOutlet weak var TruckPlateNo_Third_TextField: UITextField!
    @IBOutlet weak var TruckPlateNo_Fourth_TextField: UITextField!
    @IBOutlet weak var TruckPlateNo_Fifth_TextField: UITextField!
    
    @IBOutlet weak var TrailerPlateNo_First_TextField: UITextField!
    @IBOutlet weak var TrailerPlateNo_Second_TextField: UITextField!
    @IBOutlet weak var TrailerPlateNo_Third_TextField: UITextField!
    @IBOutlet weak var TrailerPlateNo_Fourth_TextField: UITextField!
    @IBOutlet weak var TrailerPlateNo_Fifth_TextField: UITextField!
    
    @IBOutlet weak var Capacity_TextField: UITextField!
    @IBOutlet weak var Color_TextField: UITextField!
    @IBOutlet weak var SaveChanges_Button: UIButton!
    
    // MARK: - KeyBoardManager
    
    @objc func adjustForKeyboard(notification: Notification) {
        guard let keyboardValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue else { return }

        let keyboardScreenEndFrame = keyboardValue.cgRectValue
        let keyboardViewEndFrame = view.convert(keyboardScreenEndFrame, from: view.window)

        if notification.name == UIResponder.keyboardWillHideNotification {
            Main_ScrollView.contentInset = .zero
        } else {
            Main_ScrollView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardViewEndFrame.height - view.safeAreaInsets.bottom + 20, right: 0)
        }

        Main_ScrollView.scrollIndicatorInsets = Main_ScrollView.contentInset
    }
    
    func textFieldShouldReturn(_ Sender: UITextField) -> Bool
    {
        if Sender.tag == 1 || Sender.tag == 26 || Sender.tag == 27 {
            Sender.resignFirstResponder()
        }
        
        else if 11...15 ~= Sender.tag {
            switch Sender.tag {
            case 11:
                NationalPlate_Second_TextField.becomeFirstResponder()
            case 12:
                NationalPlate_Third_TextField.becomeFirstResponder()
            case 13:
                NationalPlate_Fourth_TextField.becomeFirstResponder()
            case 14:
                NationalPlate_Fifth_TextField.becomeFirstResponder()
            case 15:
                 Sender.resignFirstResponder()
            default:
                Sender.resignFirstResponder()
            }
        }
        
        else if 16...20 ~= Sender.tag {
            
            switch Sender.tag {
            case 16:
                TruckPlateNo_Second_TextField.becomeFirstResponder()
            case 17:
                TruckPlateNo_Third_TextField.becomeFirstResponder()
            case 18:
                TruckPlateNo_Fourth_TextField.becomeFirstResponder()
            case 19:
                TruckPlateNo_Fifth_TextField.becomeFirstResponder()
            case 20:
                Sender.resignFirstResponder()
            default:
                Sender.resignFirstResponder()
            }
        }
        
        else if 21...25 ~= Sender.tag {
            switch Sender.tag {
            case 21:
                TrailerPlateNo_Second_TextField.becomeFirstResponder()
            case 22:
                TrailerPlateNo_Third_TextField.becomeFirstResponder()
            case 23:
                TrailerPlateNo_Fourth_TextField.becomeFirstResponder()
            case 24:
                TrailerPlateNo_Fifth_TextField.becomeFirstResponder()
            case 25:
                Sender.resignFirstResponder()
            default:
                Sender.resignFirstResponder()
            }
        }
        
        return true
    }
    
    // MARK: - Localization
    
    func Localize(){
        Color_TextField.placeholder = "Please enter your truck color".localized()
        Capacity_TextField.placeholder = "Please enter your truck capacity".localized()
        TruckModel_TextField.placeholder = "Please select truck model".localized()
    }
    
    // MARK: - Life Cycle Func
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        Localize()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UIView.appearance().semanticContentAttribute = .forceLeftToRight
        AddDelegate()
        AddRadiusToTextFieldCorner()
        AddActions()
    }
    
    private func AddDelegate(){
        TruckModel_TextField.delegate = self
        
        NationalPlate_First_TextField.delegate = self
        NationalPlate_Second_TextField.delegate = self
        NationalPlate_Third_TextField.delegate = self
        NationalPlate_Fourth_TextField.delegate = self
        NationalPlate_Fifth_TextField.delegate = self
        
        TruckPlateNo_First_TextField.delegate = self
        TruckPlateNo_Second_TextField.delegate = self
        TruckPlateNo_Third_TextField.delegate = self
        TruckPlateNo_Fourth_TextField.delegate = self
        TruckPlateNo_Fifth_TextField.delegate = self
        
        TrailerPlateNo_First_TextField.delegate = self
        TrailerPlateNo_Second_TextField.delegate = self
        TrailerPlateNo_Third_TextField.delegate = self
        TrailerPlateNo_Fourth_TextField.delegate = self
        TrailerPlateNo_Fifth_TextField.delegate = self
        
        Capacity_TextField.delegate = self
        Color_TextField.delegate = self
    }
    
    private func AddActions(){
        SaveChanges_Button.addTarget(self, action: #selector(OnSaveChangesButtonClikced(_:)), for: .touchUpInside)
        Back_Button.addTarget(self, action: #selector(OnBackButtonClicked(_:)), for: .touchUpInside)
        TruckTypePicker_Button.addTarget(self, action: #selector(OnTrucktypePickerButtonClicked(_:)), for: .touchUpInside)
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(adjustForKeyboard), name: UIResponder.keyboardWillHideNotification, object: nil)
        notificationCenter.addObserver(self, selector: #selector(adjustForKeyboard), name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
    }
    
    private func AddRadiusToTextFieldCorner(){
        NationalPlate_First_TextField.SetSpeceficCornerRadius(Radius: 9, TopLeft: true, BottomLeft: true, TopRight: false, BottomRight: false)
        NationalPlate_Fifth_TextField.SetSpeceficCornerRadius(Radius: 9, TopLeft: false, BottomLeft: false, TopRight: true, BottomRight: true)
        
        TruckPlateNo_First_TextField.SetSpeceficCornerRadius(Radius: 9, TopLeft: true, BottomLeft: true, TopRight: false, BottomRight: false)
        TruckPlateNo_Fifth_TextField.SetSpeceficCornerRadius(Radius: 9, TopLeft: false, BottomLeft: false, TopRight: true, BottomRight: true)
        
        TrailerPlateNo_First_TextField.SetSpeceficCornerRadius(Radius: 9, TopLeft: true, BottomLeft: true, TopRight: false, BottomRight: false)
        TrailerPlateNo_Fifth_TextField.SetSpeceficCornerRadius(Radius: 9, TopLeft: false, BottomLeft: false, TopRight: true, BottomRight: true)
    }
    
    private func GetAllTrailerPlateNo() -> String{
        let TextFieldTextArray:[String] = [(TrailerPlateNo_First_TextField.text ?? ""),(TrailerPlateNo_Second_TextField.text ?? ""),(TrailerPlateNo_Third_TextField.text ?? ""),(TrailerPlateNo_Fourth_TextField.text ?? ""),(TrailerPlateNo_Fifth_TextField.text ?? "")]
        
        var TrailerPlate:String = ""
        for Text_Index in 0..<TextFieldTextArray.count {
            if !TextFieldTextArray[Text_Index].isEmpty && Text_Index != TextFieldTextArray.count-1{
                TrailerPlate.append(TextFieldTextArray[Text_Index])
                if !TextFieldTextArray[Text_Index + 1].isEmpty {
                    TrailerPlate.append("-")
                }
            }else{
                TrailerPlate.append(TextFieldTextArray[Text_Index])
            }
        }
        return TrailerPlate
    }
    
    private func GetPlate(TextFieldTextArray:[String]) -> String{
        var TrailerPlate:String = ""
        for Text_Index in 0..<TextFieldTextArray.count {
            if !TextFieldTextArray[Text_Index].isEmpty && Text_Index != TextFieldTextArray.count-1{
                TrailerPlate.append(TextFieldTextArray[Text_Index])
                if !TextFieldTextArray[Text_Index + 1].isEmpty {
                    TrailerPlate.append("-")
                }
            }else{
                TrailerPlate.append(TextFieldTextArray[Text_Index])
            }
        }
        return TrailerPlate
    }
    
    private func GetPlateTextFieldArray() -> (NationalPlate:[String],TruckPlate:[String],TrailerPlate:[String]){
        let NationalPlateArray:[String] = [(NationalPlate_First_TextField.text ?? ""),(NationalPlate_Second_TextField.text ?? ""),(NationalPlate_Third_TextField.text ?? ""),(NationalPlate_Fourth_TextField.text ?? ""),(NationalPlate_Fifth_TextField.text ?? "")]
        let TruckPlateArray:[String] = [(TruckPlateNo_First_TextField.text ?? ""),(TruckPlateNo_Second_TextField.text ?? ""),(NationalPlate_Third_TextField.text ?? ""),(TruckPlateNo_Fourth_TextField.text ?? ""),(TruckPlateNo_Fifth_TextField.text ?? "")]
         let TrailerPlateArray:[String] = [(TrailerPlateNo_First_TextField.text ?? ""),(TrailerPlateNo_Second_TextField.text ?? ""),(TrailerPlateNo_Third_TextField.text ?? ""),(TrailerPlateNo_Fourth_TextField.text ?? ""),(TrailerPlateNo_Fifth_TextField.text ?? "")]
        return (NationalPlateArray,TruckPlateArray,TrailerPlateArray)
    }
    
    // MARK: - Network
    
    private func GetParameterToRequest() -> [String:Any]?{
        var param = [String:Any]()
        let Tuple = GetPlateTextFieldArray()
        
        if (GetPlate(TextFieldTextArray: Tuple.NationalPlate) != "") && (GetPlate(TextFieldTextArray: Tuple.TruckPlate) != "") && (GetPlate(TextFieldTextArray: Tuple.TrailerPlate) != "") && (SelectedTruckType != nil) && (TruckModel_TextField.text != nil) && (Capacity_TextField.text != nil) && (Color_TextField.text != nil) {
            
            param.updateValue(SelectedTruckType?.id as Any, forKey: "truck_type_id")
            param.updateValue(TruckModel_TextField.text as Any, forKey: "model")
            param.updateValue(GetPlate(TextFieldTextArray: Tuple.NationalPlate), forKey: "country_plate")
            param.updateValue(GetPlate(TextFieldTextArray: Tuple.TruckPlate), forKey: "international_tractor_plate_data")
            param.updateValue(GetPlate(TextFieldTextArray: Tuple.TrailerPlate), forKey: "international_trailer_plate_data")
            param.updateValue(Capacity_TextField.text as Any, forKey: "capacity")
            param.updateValue(Color_TextField.text as Any, forKey: "color")
            return param
        }
        else{
            return nil
        }
        
    }
    
    private func RequestToCreateMytruck(Param:[String:Any]){
        UserConnectionManager.RequestWithHeader(URL: DOMAIN_URL + Create_My_Truck_URL, RequestMethod: .post, Parameter: Param, { (HTTPStatus, RecievedJson) in
            print("Truck info Set Successfully With Code \(HTTPStatus)")
            self.performSegue(withIdentifier: "From_TruckInformation_To_Document_Segue", sender: self)
        }) { (HTTPStatus, ErrorArray) in
            print("Setting Truck info Failed With Code \(HTTPStatus)")
            self.SetButtonToWatingState(Button: self.SaveChanges_Button, ButtonState: .Enabled, ButtonText: "Save Changes".localized())
            self.ShowAlert(Message: ErrorArray?[0].body ?? "Check Your Connection".localized(), Seconds: 2)
        }
    }
    
    
    // MARK: - Navigation
    
    var SelectedTruckType:TruckTypeModel?
    
    @IBAction func UnwindFrom_TruckPickerViewController_To_TruckInformationViewController(segue:UIStoryboardSegue) {
        
        if let sourceviewcontroller = segue.source as? TruckPickerViewController{
            SelectedTruckType = sourceviewcontroller.getSelectedTruckType()
            TruckTypePicker_Label.text = SelectedTruckType?.title
        }
    }
    
    @IBAction func UnwindFrom_DocumentViewController_To_TruckInformationViewController(segue:UIStoryboardSegue) {
        /*if segue.source is DoucumentViewController{
        }*/
    }
    
    @objc func OnTrucktypePickerButtonClicked(_ sender:UIButton){
        performSegue(withIdentifier: "From_TruckInformation_To_TruckTypePicker_Segue", sender: self)
    }
    
    @objc func OnBackButtonClicked(_ sender:UIButton){
        navigationController?.popToRootViewController(animated: true)
//        performSegue(withIdentifier: "UnwindFrom_TruckInformation_To_IdentityAuth_Segue", sender: self)
    }

    @objc func OnSaveChangesButtonClikced(_ sender:UIButton){

        guard let param = GetParameterToRequest() else {
            ShowAlert(Message: "Fill All The Fields".localized(), Seconds: 2)
            return
        }
        SetButtonToWatingState(Button: SaveChanges_Button, ButtonState: .Disabled, ButtonText: "")
        RequestToCreateMytruck(Param: param)
    }
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        if segue.identifier == "From_TruckInformation_To_TruckTypePicker_Segue" {
            if let LanguagePickerInstance = segue.destination as? TruckPickerViewController {
                LanguagePickerInstance.setUnwind(withIdentifier: "UnwindFrom_TruckPicker_To_TruckInformation_Segue")
            }
        }
    }
    

}
