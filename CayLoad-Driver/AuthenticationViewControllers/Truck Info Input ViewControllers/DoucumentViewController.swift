//
//  DoucumentViewController.swift
//  CayLoad-Driver
//
//  Created by Kiyarash Vosough on 2/6/1399 AP.
//  Copyright © 1399 CayLoad. All rights reserved.
//

import UIKit

class DoucumentViewController: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    
    @IBOutlet weak var Back_Button: UIButton!
    @IBOutlet weak var DriverLicensePicture_Label: UILabel!
    @IBOutlet weak var DriverLicense_Imageview: UIImageView!
    @IBOutlet weak var DriverLicensePicker_Button: UIButton!
    @IBOutlet weak var TruckCardPicture_Label: UILabel!
    @IBOutlet weak var TruckCard_Imageview: UIImageView!
    @IBOutlet weak var TruckCardPicker_Button: UIButton!
    @IBOutlet weak var SaveChanges_Button: UIButton!
    
    var ImagePicker = UIImagePickerController()
    var IsDriverLicenseImageEditing:Bool = false
    var IsTruckCardEditing:Bool = false
    var IsDriverLicenseImageUploaded:Bool = false
    var IsTruckCardUploaded:Bool = false
    
    var UploadCount:Int? {
        didSet{
            OnUploadCompletion(count: UploadCount)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UIView.appearance().semanticContentAttribute = .forceLeftToRight
        ImagePicker.delegate = self
        AddActions()
        AddLayerPropertyToimageViews()
    }
    
    private func AddActions(){
        Back_Button.addTarget(self, action: #selector(OnBackButtonClicked(_:)), for: .touchUpInside)
        TruckCardPicker_Button.addTarget(self, action: #selector(OnTruckCardPickerButtonClicked), for: .touchUpInside)
        DriverLicensePicker_Button.addTarget(self, action: #selector(OnDriverLicensePickerButtonClicked), for: .touchUpInside)
        SaveChanges_Button.addTarget(self, action: #selector(OnSaveChangesButtonClikced), for: .touchUpInside)
    }
    
    private func AddLayerPropertyToimageViews(){
        DriverLicense_Imageview.layer.cornerRadius = 9
        DriverLicense_Imageview.layer.masksToBounds = true
        TruckCard_Imageview.layer.cornerRadius = 9
        TruckCard_Imageview.layer.masksToBounds = true
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let pickedImage = info[UIImagePickerController.InfoKey.editedImage] as? UIImage {
            if self.IsTruckCardEditing {
                self.TruckCard_Imageview.image = pickedImage
                IsTruckCardEditing = false
            }
            else if self.IsDriverLicenseImageEditing {
                DriverLicense_Imageview.image = pickedImage
                IsDriverLicenseImageEditing = false
            }
            
        }
        dismiss(animated: true, completion: nil)
    }
    
    // MARK: - Network
    
    private func RequestToUploadImage(URL:String,ImageData:Data,ImageName:String){
        UserConnectionManager.UploadImage(Address: URL, ImageData: ImageData, ImageName: "image", { (HTTPStatus, RecievedJSON) in
            print("Uploading Imaged Successed Failed With Code \(HTTPStatus)")
            self.UploadCount = (self.UploadCount ?? 0) + 1
        }) { (HTTPStatus, ErrorArray) in
            print("Uploading Imaged info Failed With Code \(HTTPStatus)")
            self.SetButtonToWatingState(Button: self.SaveChanges_Button, ButtonState: .Enabled, ButtonText: "Save Changes")
            self.ShowAlert(Message: ErrorArray?[0].body ?? "Check Your Connection".localized(), Seconds: 2)
        }
    }
    
    private func OnUploadCompletion(count:Int?){
        if count != nil {
            if count == 2 {
                UserDefaults.standard.set(true, forKey: "Is_Login")
                UserDefaults.standard.set("\(TOKEN)", forKey: "VALID_TOKEN")
                performSegue(withIdentifier: "From_Document_To_MainNavBar_Segue", sender: self)
            }
        }
    }
    
    // MARK: - Navigation

    @objc func OnDriverLicensePickerButtonClicked(_ sender:UIButton){
        IsDriverLicenseImageEditing = true
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary){
            ImagePicker.sourceType = .savedPhotosAlbum
            ImagePicker.allowsEditing = true
            present(ImagePicker, animated: true, completion: nil)
        }
    }
    
    @objc func OnTruckCardPickerButtonClicked(_ sender:UIButton){
        IsTruckCardEditing = true
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary){
            ImagePicker.sourceType = .savedPhotosAlbum
            ImagePicker.allowsEditing = true
            present(ImagePicker, animated: true, completion: nil)
        }
    }
    
    @objc func OnBackButtonClicked(_ sender:UIButton){
        performSegue(withIdentifier: "UnwindFrom_Document_To_TruckInformation_Segue", sender: self)
    }
    
    @objc func OnSaveChangesButtonClikced(_ sender:UIButton){
        
        guard let TruckCard_Image = TruckCard_Imageview.image else {
            ShowAlert(Message: "Choose Truck Card Picture".localized(), Seconds: 2)
            return
        }
        
        guard let DriverLicense_Image = DriverLicense_Imageview.image else {
            ShowAlert(Message: "Choose Driving License Picture".localized(), Seconds: 2)
            return
        }
        
        guard let TruckCard_ImageData:Data = TruckCard_Image.jpegData(compressionQuality: 0.5) else {
            ShowAlert(Message: "There Is Somthing Wrong With Your Truck Card Picture".localized(), Seconds: 2)
          return
        }
        guard let DriverLicense_ImageData:Data = DriverLicense_Image.jpegData(compressionQuality: 0.5) else {
            ShowAlert(Message: "There Is Somthing Wrong With Your Driving License Picture".localized(), Seconds: 2)
          return
        }
        SetButtonToWatingState(Button: SaveChanges_Button, ButtonState: .Disabled, ButtonText: "")
        RequestToUploadImage(URL: DOMAIN_URL + Set_Vehicle_Card_Image_URL , ImageData: TruckCard_ImageData, ImageName: "Truck Card")
        RequestToUploadImage(URL: DOMAIN_URL + Set_License_Image_URL, ImageData: DriverLicense_ImageData, ImageName: "Driving License")

        
        
    }
    /* In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }*/


}
