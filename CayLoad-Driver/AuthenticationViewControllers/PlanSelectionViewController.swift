//
//  PlanSelectionViewController.swift
//  CayLoad-Driver
//
//  Created by Kiyarash Vosough on 1/23/1399 AP.
//  Copyright © 1399 CayLoad. All rights reserved.
//

import UIKit

class PlanSelectionViewController: UIViewController,OnRegistrationBuyingPlan {
    
    @IBOutlet weak var ChoosePlan_Label: UILabel!
    @IBOutlet weak var Plan_CollectionView: UICollectionView!
    
    var PlanPrice:Int!
    var PlanDuration:String!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Plan_CollectionView.delegate = self
        Plan_CollectionView.dataSource = self
    }

    func rgb(red: CGFloat, green: CGFloat, blue: CGFloat,ALPHA: CGFloat) -> UIColor {
      return UIColor(red: red/255, green: green/255, blue: blue/255, alpha: ALPHA)
    }
    
    // MARK: - Network
    
    var PlansArray = [PlanModel]()
    
    private func GetPlans(){
        let User_Id = UserDefaults.standard.integer(forKey: "user_id")
        UserConnectionManager.RequestWithHeader(URL: "\(DOMAIN_URL)\(User_URL)\(User_Id)\(Get_Plans_URL)", RequestMethod: .get, Parameter: nil, { (HTTPStatus, RecievedJSON) in
            print("Getting PLans Succeed wuth code",HTTPStatus)
            self.PlansArray = JsonParsingUtils.GetArrayList(From: RecievedJSON! , withRootTag: "users")
            self.Plan_CollectionView.reloadData()
        }) { (HTTPStatus, ErrorArray) in
            print("Getting PLans Failed wuth code",HTTPStatus)
            self.ShowAlert(Message:ErrorArray?[0].body ?? "Check Your Connection".localized() , Seconds: 2)
        }
        /*UserConnectionManager.GetPlans(User_Id: UserDefaults.standard.integer(forKey: "user_id")) { (HTTPStatus, PlansArray) in
            if(HTTPStatus==200){
                print("200")
                self.PlansArray = JsonParsingUtils.GetArrayList(From: PlansArray! , withRootTag: "users")
                self.Plan_CollectionView.reloadData()
            }
            else if(HTTPStatus==401){
                print("401")
            }
            else{
                print("no internet")
            }
        }*/
    }
    
    // MARK: - Navigation
    
    @IBAction func UnwindFrom_RegistrationPaymentViewController_To_PlanSelectionViewController(segue:UIStoryboardSegue) {
        
        /*if segue.source is RegistrationPaymentViewController {
        }*/
    }
    
    func PerformSegueToPaymentGateway(PlanPrice: Int, PlanDuration: String) {
        self.PlanPrice = PlanPrice
        self.PlanDuration = PlanDuration
        performSegue(withIdentifier: "From_PlanSelection_To_PaymentGateway_Segue", sender: self)
    }
    
    func PerforSegueToTruckInformation() {
        performSegue(withIdentifier: "From_PlanSelection_To_TruckInformation_Segue", sender: self)
    }
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        if segue.identifier == "From_PlanSelection_To_PaymentGateway_Segue" {
            if let instance = segue.destination as? RegistrationPaymentViewController {
                instance.PlanPrice = self.PlanPrice
                instance.PlanDuration = self.PlanDuration
            }
        }
    }

}



extension PlanSelectionViewController:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return PlansArray.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let plancelll = collectionView.dequeueReusableCell(withReuseIdentifier: "PlanCell", for: indexPath) as! OnRegistrationPlansCollectionView
        plancelll.SetPlansAtributte(Duration: PlansArray[indexPath.row].name, Price: PlansArray[indexPath.row].price, PlanInfo: PlansArray[indexPath.row].description)
        plancelll.NavigationDelegate = self
        return plancelll
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {

        if PlansArray.count == 0 {
            switch kind {
            
            case UICollectionView.elementKindSectionHeader:
                
              guard let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "FreePlan", for: indexPath) as? PlanFreeHeaderCollectionReusableView else { fatalError("Invalid view type") }
              
              headerView.frame = CGRect(x: 0 , y: 0, width: self.Plan_CollectionView.frame.width, height: 75)
              headerView.NavigationDelegate = self
              headerView.OnCreate()
              return headerView
            default:
              assert(false, "Invalid element type")
            }
        }
        return UICollectionReusableView()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (collectionView.frame.width-10)/2, height: (collectionView.frame.width-10)/2)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
            return 9
        }
        
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 9
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 10, left: 0, bottom: 10, right: 0)
    }
    
}
