//
//  ResetPasswordViewController.swift
//  CayLoad-Driver
//
//  Created by Kiyarash Vosough on 2/7/1399 AP.
//  Copyright © 1399 CayLoad. All rights reserved.
//

import UIKit

class ResetPasswordViewController: UIViewController,UITextFieldDelegate {
    
    @IBOutlet weak var ResetPassword_Label: UILabel!
    @IBOutlet weak var RestMehodChanger_Button: UIButton!
    @IBOutlet weak var EmailOrPhone_Textfield: UITextField!
    @IBOutlet weak var SendVerification_Button: UIButton!
    @IBOutlet weak var GoBack_Button: UIButton!
    
    /// `Dynamic Componnent`
    private let MailOrPhone_ImageView:UIImageView = {
        let instance = UIImageView(frame: CGRect(x: 15, y: 21, width: 16, height: 12))
        instance.image = UIImage(named: "MailIc")
        instance.contentMode = .scaleAspectFit
        return instance
    }()
    private let MailImageView_Container = UIView()
    private let ShowCountryCode_Button:UIButton = {
        let instance = UIButton(frame: CGRect(x: 0, y: 0, width: 40, height: 54))
        instance.setImage(UIImage(named: "DownArrowIc") , for: .normal)
        return instance
    }()
    private let CountryCode_Label:UILabel = {
        let instance = UILabel(frame: CGRect(x: 40, y: 0, width: 44, height: 54))
        instance.text = "+123"
        instance.font = UIFont(name: "Poppins-Regular", size: 13)
        return instance
    }()
    private let Seperator:UIView = {
        let instance = UIView(frame: CGRect(x: 80, y: 15, width: 1, height: 24))
        instance.backgroundColor = .lightGray
        return instance
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        AddTarget()
        OnCreateSigninMethodComponnent(Tag: 1)
    }
    
    private func AddTarget(){
        RestMehodChanger_Button.addTarget(self, action: #selector(OnResetMethodChangerButtonClicked(_:)), for: .touchUpInside)
          ShowCountryCode_Button.addTarget(self, action: #selector(OnShowCountryCodeButtonClicked(_:)), for: .touchUpInside)
        SendVerification_Button.addTarget(self, action: #selector(OnSendVerificationButtonClicked(_:)), for: .touchUpInside)
        GoBack_Button.addTarget(self, action: #selector(OnGoBackButtonClicked(_:)), for: .touchUpInside)
        
    }

    private func OnCreateSigninMethodComponnent(Tag:Int){
        if  Tag == 1 {
            WithEmail = true
            RestMehodChanger_Button.tag = 0
            Seperator.removeFromSuperview()
            CountryCode_Label.removeFromSuperview()
            ShowCountryCode_Button.removeFromSuperview()
            
            EmailOrPhone_Textfield.text = ""
//            EmailOrPhone_Textfield.placeholder = "Email Address"
            RestMehodChanger_Button.setTitle("  " + ("Using Phone".localized()), for: .normal)
            
            MailImageView_Container.frame = CGRect(x: 0, y: 0, width: 47, height: 54)
            MailImageView_Container.addSubview(MailOrPhone_ImageView)
            
            EmailOrPhone_Textfield.leftViewMode = UITextField.ViewMode.always
            EmailOrPhone_Textfield.leftView = MailImageView_Container
            EmailOrPhone_Textfield.placeholder = "Email".localized()
            EmailOrPhone_Textfield.delegate = self
        }
        else{
            WithEmail = false
            RestMehodChanger_Button.tag = 1
            MailOrPhone_ImageView.removeFromSuperview()
            MailImageView_Container.removeFromSuperview()
            
            EmailOrPhone_Textfield.text = ""
//            EmailOrPhone_Textfield.placeholder = "Phone Number"
            RestMehodChanger_Button.setTitle("  " + ("Using Email".localized()), for: .normal)
            MailImageView_Container.frame = CGRect(x: 0, y: 0, width: 90, height: 54)
            MailImageView_Container.addSubview(Seperator)
            MailImageView_Container.addSubview(CountryCode_Label)
            MailImageView_Container.addSubview(ShowCountryCode_Button)
            
            EmailOrPhone_Textfield.leftViewMode = UITextField.ViewMode.always
            EmailOrPhone_Textfield.leftView = MailImageView_Container
            EmailOrPhone_Textfield.placeholder = "Phone Number".localized()
            EmailOrPhone_Textfield.delegate = self
            
        }
    }
    
    private func SetCountryCode(RawCode:Int){
        CountryCode_Label.text = "+\(RawCode)"
        CountryCode = RawCode
    }
    
    // MARK: - Network
    
    private var WithEmail:Bool = true
    
    private func RequestToResetPassword(URL:String,Param:[String:Any]){
        UserConnectionManager.Request(URL: URL, RequestMethod: .post, Parameter: Param, { (HTTPStatus, RecievedJSON) in
            print("New Password Sent Successfully With Code \(HTTPStatus)")
            self.performSegue(withIdentifier: "From_ResetPassword_To_ResetPasswordVerification_Segue", sender: self)
        }) { (HTTPStatus, ErrorArray) in
            self.SetButtonToWatingState(Button: self.SendVerification_Button, ButtonState: .Enabled, ButtonText: "Send Verification Code".localized())
            print("New Password Failed To Send With Code \(HTTPStatus)")
            self.ShowAlert(Message: ErrorArray?[0].body ?? "Check Your Connection".localized(), Seconds: 2)
        }
    }
    
    // MARK: - Navigation
    
    var CountryCode:Int?
    
    @IBAction func UnwindFrom_CountryCodePickerViewControlller_To_ResetPasswordViewControlller(segue:UIStoryboardSegue) {
        
        if let sourceViewController = segue.source as? CountryCodePickerViewController {
            SetCountryCode(RawCode:sourceViewController.getSelectedCountryCode())
        }
    }
    
    @IBAction func UnwindFrom_ResetPasswordVerificationViewControlller_To_ResetPasswordViewControlller(segue:UIStoryboardSegue) {
        
        if segue.source is ResetPasswordVerificationViewController {

        }
    }
    
    @objc func OnResetMethodChangerButtonClicked(_ sender: UIButton){
        OnCreateSigninMethodComponnent(Tag: sender.tag)
    }

    @objc func OnSendVerificationButtonClicked(_ sender: UIButton){
        guard let Email_Or_Phone:String = EmailOrPhone_Textfield.text else {
            ShowAlert(Message: "Enter your Email Or Phone".localized(), Seconds: 2)
            return
        }
        
        SetButtonToWatingState(Button: SendVerification_Button, ButtonState: .Disabled, ButtonText: "")
        
        if WithEmail {
            RequestToResetPassword(URL: DOMAIN_URL + Forgot_Password_URL, Param: ["email":Email_Or_Phone])
        }else{
            guard let countrycode:Int = CountryCode else {
                ShowAlert(Message: "Select a Country Code".localized(), Seconds: 2)
                return
            }
            RequestToResetPassword(URL: DOMAIN_URL + Forgot_Password_URL, Param: ["phone": "00" + "\(countrycode)" + Email_Or_Phone])
        }
    }
    
    @objc func OnShowCountryCodeButtonClicked(_ sender: UIButton){
        performSegue(withIdentifier: "From_ResetPassword_To_CountryCodePicker_Segue", sender: self)
    }
    
    @objc func OnGoBackButtonClicked(_ sender: UIButton){
//        performSegue(withIdentifier: "UnwindFrom_ResetPassword_To_Login_Segue", sender: self)
        navigationController?.popToRootViewController(animated: true)
    }
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        if segue.identifier == "From_ResetPassword_To_CountryCodePicker_Segue" {
            if let LanguagePickerInstance = segue.destination as? CountryCodePickerViewController {
                LanguagePickerInstance.setUnwind(withIdentifier: "UnwindFrom_CountryCodePicker_To_ResetPassword_Segue")
            }
        }
        else if segue.identifier == "From_ResetPassword_To_ResetPasswordVerification_Segue" {
            if let instance = segue.destination as? ResetPasswordVerificationViewController {
                instance.SetIDForVerification(NeededInfo: (WithEmail,EmailOrPhone_Textfield.text ?? "",CountryCode ?? 0))
            }
        }
        
    }

}
