//
//  ChangePasswordViewController.swift
//  CayLoad-Driver
//
//  Created by Kiyarash Vosough on 2/7/1399 AP.
//  Copyright © 1399 CayLoad. All rights reserved.
//

import UIKit

class ChangePasswordViewController: UIViewController,UITextFieldDelegate {

    
    @IBOutlet weak var ResetPassword_Label: UILabel!
    @IBOutlet weak var PasswordConfirm_TextField: UITextField!
    @IBOutlet weak var Password_Textfield: UITextField!
    @IBOutlet weak var Submit_Button: UIButton!
    @IBOutlet weak var GoBack_Button: UIButton!
    
    //MARK: - Localization
    
    private func Localize(){
        PasswordConfirm_TextField.placeholder = "Confirm password".localized()
        Password_Textfield.placeholder = "Enter your new password".localized()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        Localize()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        AddActions()
        OnCreatePassWordTextFieldLeftView()
    }
    
    private func AddActions(){
        GoBack_Button.addTarget(self, action: #selector(OnGoBackButtonClicked(_:)), for: .touchUpInside)
        Submit_Button.addTarget(self, action: #selector(OnSubmitButtonClicked(_:)), for: .touchUpInside)
   
    }
    
    private func OnCreatePassWordTextFieldLeftView(){
        let imageView = UIImageView(frame: CGRect(x: 18.0, y: 20, width: 13.0, height: 14.0))
        let image = UIImage(named: "LockIc")
        imageView.image = image
        imageView.contentMode = .scaleAspectFit
        let view = UIView(frame: CGRect(x: 0, y: 0, width: 47, height: 54))
        view.addSubview(imageView)
        Password_Textfield.leftViewMode = UITextField.ViewMode.always
        Password_Textfield.leftView = view
        
        let imageView1 = UIImageView(frame: CGRect(x: 18.0, y: 20, width: 13.0, height: 14.0))
        let image1 = UIImage(named: "LockIc")
        imageView1.image = image1
        imageView1.contentMode = .scaleAspectFit
        let view1 = UIView(frame: CGRect(x: 0, y: 0, width: 47, height: 54))
        view1.addSubview(imageView1)
        PasswordConfirm_TextField.leftViewMode = UITextField.ViewMode.always
        PasswordConfirm_TextField.leftView = view1
    }
    
    
    // MARK: - Network
    
    private func RequestToChangePassword(Param:[String:Any]){
        UserConnectionManager.Request(URL: DOMAIN_URL + Forgot_Password_Change_URL, RequestMethod: .post, Parameter: Param, { (HTTPStatus, RecievedJSON) in
            print("Password Changed Successfully With Code \(HTTPStatus)")
            self.navigationController?.popToRootViewController(animated: true)
        }) { (HTTPStatus, ErrorArray) in
            print("Changeing Password Failed With Code \(HTTPStatus)")
            self.SetButtonToWatingState(Button: self.Submit_Button, ButtonState: .Enabled, ButtonText: "Submit".localized())
            self.ShowAlert(Message: ErrorArray?[0].body ?? "Check Your Connection".localized(), Seconds: 2)
        }
    }
    
    // MARK: - Navigation
    
    private var NeededInfoForVerify:(With_Email:Bool,email_or_phone:String,CountryCode:Int)?
    
    func SetIDForVerification(NeededInfo: (With_Email:Bool,email_or_phone:String,CountryCode:Int)){
        NeededInfoForVerify = NeededInfo
    }
    
    @objc func OnGoBackButtonClicked(_ sender: UIButton){
//        performSegue(withIdentifier: "UnwindFrom_ChangePassword_To_ResetPasswordVerification_Segue", sender: self)
        navigationController?.popToRootViewController(animated: true)
    }
    
    @objc func OnSubmitButtonClicked(_ sender: UIButton){
        guard let pass:String = Password_Textfield.text else {
            ShowAlert(Message: "Enter your new password".localized(), Seconds: 2)
            return
        }
        guard let confirm_pass:String = PasswordConfirm_TextField.text else {
            ShowAlert(Message: "Your Password Does Not Match".localized(), Seconds: 2)
            return
        }
        
        if pass == confirm_pass && !pass.isEmpty && !confirm_pass.isEmpty{
            
            SetButtonToWatingState(Button: Submit_Button, ButtonState: .Disabled, ButtonText: "")

            if NeededInfoForVerify?.With_Email == true {
                RequestToChangePassword(Param: ["password" : pass, "email":NeededInfoForVerify?.email_or_phone ?? ""])
            }else {
                RequestToChangePassword(Param:["password" : pass, "phone": "00" + "\(NeededInfoForVerify?.CountryCode ?? 0)" + (NeededInfoForVerify?.email_or_phone ?? "" )])
            }
        }else{
            ShowAlert(Message: "Your Password Does Not Match".localized(), Seconds: 3)
        }
    }

    /* In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }*/

}
