//
//  ResetPasswordVerificationViewController.swift
//  CayLoad-Driver
//
//  Created by Kiyarash Vosough on 2/7/1399 AP.
//  Copyright © 1399 CayLoad. All rights reserved.
//

import UIKit

class ResetPasswordVerificationViewController: UIViewController {

    @IBOutlet weak var ResetPassword_Label: UILabel!
    @IBOutlet weak var VerificationMethod_Label: UILabel!
    @IBOutlet weak var UserVerificationData_Label: UILabel!
    
    @IBOutlet weak var FirstDigit_TextField: UITextField!
    @IBOutlet weak var SecondDigit_TextField: UITextField!
    @IBOutlet weak var ThirdDigit_TextField: UITextField!
    @IBOutlet weak var FourthDigit_TextField: UITextField!
    
    @IBOutlet weak var FirstDigitBottomLine_View: UIView!
    @IBOutlet weak var SecondDigitBottomLine_View: UIView!
    @IBOutlet weak var ThirdDigitBottomLine_View: UIView!
    @IBOutlet weak var FourthDigitBottomLine_View: UIView!
    
    @IBOutlet weak var Submit_Button: UIButton!
    @IBOutlet weak var GoBack_Button: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        AddActions()
        UpdateView()
        // Do any additional setup after loading the view.
    }
    
    private func AddActions(){
        FirstDigit_TextField.addTarget(self, action: #selector(ChangeTextFieldCursor(_:)), for: .editingChanged)
        SecondDigit_TextField.addTarget(self, action: #selector(ChangeTextFieldCursor(_:)), for: .editingChanged)
        ThirdDigit_TextField.addTarget(self, action: #selector(ChangeTextFieldCursor(_:)), for: .editingChanged)
        FourthDigit_TextField.addTarget(self, action: #selector(ChangeTextFieldCursor(_:)), for: .editingChanged)
        FirstDigit_TextField.becomeFirstResponder()
        view.endEditing(false)
        GoBack_Button.addTarget(self, action: #selector(OnGoBackButtonClicked(_:)), for: .touchUpInside)
        Submit_Button.addTarget(self, action: #selector(OnSubmitButtonClicked(_:)), for: .touchUpInside)
    }
    
    @objc func ChangeTextFieldCursor(_ sender: UITextField){
        if sender.text?.utf16.count == 1 {
            switch sender {
            case FirstDigit_TextField:
                FirstDigitBottomLine_View.backgroundColor = .App_LightGray
                SecondDigitBottomLine_View.backgroundColor = .App_DarkBlue
                SecondDigit_TextField.becomeFirstResponder()
            case SecondDigit_TextField:
                SecondDigitBottomLine_View.backgroundColor = .App_LightGray
                ThirdDigitBottomLine_View.backgroundColor = .App_DarkBlue
                ThirdDigit_TextField.becomeFirstResponder()
            case ThirdDigit_TextField:
                ThirdDigitBottomLine_View.backgroundColor = .App_LightGray
                FourthDigitBottomLine_View.backgroundColor = .App_DarkBlue
                FourthDigit_TextField.becomeFirstResponder()
            case FourthDigit_TextField:
                view.endEditing(true)
            default:
                break
            }
        }
        else if sender.text?.utf16.count == 0 {
            switch sender {
            case FourthDigit_TextField:
                ThirdDigitBottomLine_View.backgroundColor = .App_DarkBlue
                FourthDigitBottomLine_View.backgroundColor = .App_LightGray
                ThirdDigit_TextField.becomeFirstResponder()
            case ThirdDigit_TextField:
                SecondDigitBottomLine_View.backgroundColor = .App_DarkBlue
                ThirdDigitBottomLine_View.backgroundColor = .App_LightGray
            SecondDigit_TextField.becomeFirstResponder()
            case SecondDigit_TextField:
                FirstDigitBottomLine_View.backgroundColor = .App_DarkBlue
                SecondDigitBottomLine_View.backgroundColor = .App_LightGray
                FirstDigit_TextField.becomeFirstResponder()
            case FirstDigit_TextField:
                FirstDigit_TextField.becomeFirstResponder()
            default:
                break
            }
        }
    }
    
    private func UpdateView(){
        if NeededInfoForVerify!.With_Email {
            VerificationMethod_Label.text = "Enter the code sent to your email :".localized()
            UserVerificationData_Label.text = NeededInfoForVerify?.email_or_phone
        }else{
            VerificationMethod_Label.text = "Enter the code sent to your phone :".localized()
            UserVerificationData_Label.text = NeededInfoForVerify?.email_or_phone
        }
    }
    
    // MARK: - Network
    
    private func RequestToCheckForVerification(Param:[String:Any]){
            UserConnectionManager.Request(URL: DOMAIN_URL + Forgot_Password_Verification_URL, RequestMethod: .post, Parameter: Param, { (HTTPStatus, RecievedJSON) in
                print("Verifivation Successed With Code \(HTTPStatus)")
                self.performSegue(withIdentifier: "From_ResetPasswordVerification_To_ChangePassword_Segue1", sender: self)
            }) { (HTTPStatus, ErrorArray) in
                print("Verifivation Failed With Code \(HTTPStatus)")
                self.SetButtonToWatingState(Button: self.Submit_Button, ButtonState: .Enabled, ButtonText: "Submit".localized())
                self.ShowAlert(Message: ErrorArray?[0].body ?? "Check Your Connection".localized(), Seconds: 2)
            }
    }
    
    private func GetEntredCodeFromTextField() -> String? {
        var EnteredCode = String()
        EnteredCode.append(FirstDigit_TextField.text ?? "")
        EnteredCode.append(SecondDigit_TextField.text ?? "")
        EnteredCode.append(ThirdDigit_TextField.text ?? "")
        EnteredCode.append(FourthDigit_TextField.text ?? "")
        if EnteredCode.utf16.count == 4 {
            return EnteredCode
        }else{
            return nil
        }
    }

    // MARK: - Navigation

    private var NeededInfoForVerify:(With_Email:Bool,email_or_phone:String,CountryCode:Int)?
    
    func SetIDForVerification(NeededInfo: (With_Email:Bool,email_or_phone:String,CountryCode:Int)){
        NeededInfoForVerify = NeededInfo
    }
    
    @IBAction func UnwindFrom_ChangePasswordViewController_To_ResetPasswordVerificationViewControlller(segue:UIStoryboardSegue) {
        
        if segue.source is ChangePasswordViewController {
        }
    }
    
    @objc func OnGoBackButtonClicked(_ sender: UIButton){
//        performSegue(withIdentifier: "UnwindFrom_ResetPasswordVerification_To_ResetPassword_Segue", sender: self)
        navigationController?.popToRootViewController(animated: true)
    }
    @objc func OnSubmitButtonClicked(_ sender: UIButton){
        guard let code:String = GetEntredCodeFromTextField() else {
            ShowAlert(Message: "Enter The Code", Seconds: 2)
            return
        }
        
        SetButtonToWatingState(Button: Submit_Button, ButtonState: .Disabled, ButtonText: "")
        
        if NeededInfoForVerify?.With_Email == true {
            RequestToCheckForVerification(Param: ["code" : code,"email":NeededInfoForVerify?.email_or_phone ?? ""])
        }else {
            RequestToCheckForVerification(Param: ["code" : code,"phone": "00" + "\(NeededInfoForVerify?.CountryCode ?? 0)" + (NeededInfoForVerify?.email_or_phone ?? "" )])
        }
    }
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        if segue.identifier == "From_ResetPasswordVerification_To_ChangePassword_Segue1" {
            if let instance = segue.destination as? ChangePasswordViewController {
                instance.SetIDForVerification(NeededInfo: self.NeededInfoForVerify!)
            }
        }
    }

}
