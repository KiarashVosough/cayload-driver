//
//  RequestCell.swift
//  Cayload-Customer
//
//  Created by Yalda Kheirkhah on 2/24/20.
//  Copyright © 2020 Yalda Kheirkhah. All rights reserved.
//

import UIKit

class RequestCell: UICollectionViewCell {
    
    var NavigationDelegate:OfferDetailsDelegate?
    
    var Origin_Label: UILabel = {
        let instance = UILabel()
        instance.adjustsFontSizeToFitWidth = true
        instance.adjustsFontForContentSizeCategory = true
        instance.minimumScaleFactor = 0.5
        instance.numberOfLines = 2
        instance.textAlignment = .left
        instance.font = UIFont(name: "Poppins-Bold", size: 15)
        instance.textColor = .black
        return instance
    }()
    
    var Destination_Label: UILabel = {
        let instance = UILabel()
        instance.adjustsFontSizeToFitWidth = true
        instance.adjustsFontForContentSizeCategory = true
        instance.minimumScaleFactor = 0.5
        instance.numberOfLines = 2
        instance.textAlignment = .left
        instance.font = UIFont(name: "Poppins-Bold", size: 15)
        instance.textColor = .black
        return instance
    }()
    
    var OriginDate_Label: UILabel = {
        let instance = UILabel()
        instance.font = UIFont(name: "Poppins", size: 11)
        instance.textColor = .black
        return instance
    }()
    
    var DestinationDate_Label: UILabel = {
        let instance = UILabel()
        instance.font = UIFont(name: "Poppins", size: 11)
        instance.textColor = .black
        return instance
    }()
    
    var Status_Label: UILabel = {
        let instance = UILabel()
        instance.text = "Status :".localized()
        instance.semanticContentAttribute = .forceRightToLeft
        instance.font = UIFont(name: "Poppins-Light", size: 13)
        instance.textColor = .lightGray
        return instance
    }()
    
    var StatusType_Label: UILabel = {
        let instance = UILabel()
        instance.font = UIFont(name: "Poppins-Regular", size: 15)
        instance.textColor = .black
        return instance
    }()
    
    var ArrowImage_View: UIImageView = {
        let instance = UIImageView()
        instance.contentMode = .scaleAspectFit
        instance.image = UIImage(named: "GreenArrowIc")
        return instance
    }()
    
    lazy var GoToDetail_Button: UIButton = {
        let instance = UIButton()
        instance.backgroundColor = .App_LightGray2
        instance.layer.cornerRadius = 8
        instance.setImage(UIImage(named: "ToRightBlueArrow"), for: .normal)
        return instance
    }()
    
    var Truck_Request:TruckRequestModel?

    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .white
        layer.cornerRadius = 9
        GoToDetail_Button.addTarget(self, action: #selector(OnGoToDetailButtonClicked(_:)), for: .touchUpInside)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func OnCreate(){
        
        addSubview(Origin_Label)
        addSubview(Destination_Label)
        addSubview(OriginDate_Label)
        addSubview(DestinationDate_Label)
        addSubview(Status_Label)
        addSubview(StatusType_Label)
        addSubview(ArrowImage_View)
        addSubview(GoToDetail_Button)
        
        
        Origin_Label.snp.makeConstraints { (make) in
            make.leading.equalToSuperview().offset(15)
            make.top.equalToSuperview().offset(15)
            make.height.equalTo(40)
            make.trailing.equalToSuperview().offset(-15)
//            make.width.equalTo((self.frame.width/2)-40)
        }
        
        OriginDate_Label.snp.makeConstraints { (make) in
            make.leading.equalTo(Origin_Label)
            make.top.equalTo(Origin_Label.snp.bottom).offset(6)
            make.height.equalTo(13)
        }
        
        ArrowImage_View.snp.makeConstraints { (make) in
            make.leading.equalTo(OriginDate_Label.snp.leading)
            make.top.equalTo(OriginDate_Label.snp.bottom).offset(15)
            make.height.equalTo(10.81)
            make.width.equalTo(15.9)
        }
        
        Destination_Label.snp.makeConstraints { (make) in
            make.leading.equalToSuperview().offset(15)
            make.trailing.equalToSuperview().offset(-15)
            make.top.equalTo(ArrowImage_View.snp.bottom).offset(6)
            make.height.equalTo(40)
        }
        
        DestinationDate_Label.snp.makeConstraints { (make) in
            make.leading.equalTo(Destination_Label.snp.leading)
            make.top.equalTo(Destination_Label.snp.bottom).offset(6)
            make.height.equalTo(13)
        }
        
        Status_Label.snp.makeConstraints { (make) in
            make.leading.equalTo(DestinationDate_Label)
            make.top.equalTo(DestinationDate_Label.snp.bottom).offset(15)
            make.height.equalTo(18)
        }
        
        StatusType_Label.snp.makeConstraints { (make) in
            make.leading.equalTo(DestinationDate_Label)
            make.top.equalTo(Status_Label.snp.bottom).offset(3)
            make.height.equalTo(20)
        }

        GoToDetail_Button.snp.makeConstraints { (make) in
            make.trailing.equalToSuperview().offset(-15)
            make.top.equalTo(Status_Label.snp.top)
            make.height.width.equalTo(41)
        }
        
    }
    
    func SetupConstantLabel(text:String){
        Status_Label.text = text.localized()
    }
    
    func setUpCell(origin:String,destination:String,originDate:String,destinationDate:String,status:String){
        
        Origin_Label.text = origin
        Destination_Label.text = destination
        OriginDate_Label.text = originDate
        DestinationDate_Label.text = destinationDate
        StatusType_Label.text = status
        
    }
    
    func SetTruckRequestModel(TruckRequest:TruckRequestModel?){
        self.Truck_Request = TruckRequest
    }
    
    func UpdateViews(){
        Origin_Label.text = Truck_Request?.route?.origin_point?.formatted_address ?? ""
        Destination_Label.text = Truck_Request?.route?.destination_point?.formatted_address ?? ""
        OriginDate_Label.text = Truck_Request?.route?.origin_datetime?.GetFormatedDate()
        DestinationDate_Label.text = Truck_Request?.route?.destination_datetime?.GetFormatedDate()
        StatusType_Label.text = Truck_Request?.truck_request_status?.status ?? ""
        
    }
    
    @objc func OnGoToDetailButtonClicked(_ sender: UIButton) {
        NavigationDelegate?.PerformSegueToDetails(With: Truck_Request?.id ?? 0, User_Id: Truck_Request?.customer?.id ?? 0)
    }
    
    
}
