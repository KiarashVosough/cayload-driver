//
//  LanguageTableViewCell.swift
//  CayLoad-Driver
//
//  Created by Kiyarash Vosough on 1/19/1399 AP.
//  Copyright © 1399 CayLoad. All rights reserved.
//

import UIKit

class LanguagesCell: UITableViewCell {
    
    var CountryName: UILabel = {
           let country = UILabel()
           return country
       }()
       
       var CountryFlag: UIImageView = {
           let flag = UIImageView()
           return flag
       }()
       
       
       required init?(coder aDecoder: NSCoder) {
           fatalError("init(coder:) has not been implemented")
       }
       
       override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?){
           super.init(style: style, reuseIdentifier: reuseIdentifier)
           setupview()
       }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    func setupview(){
        addSubview(CountryName)
        addSubview(CountryFlag)
           
           
           
        CountryFlag.snp.makeConstraints { (make) in
            make.leading.equalToSuperview().offset(23)
            make.centerY.equalToSuperview()
            make.height.width.equalTo(31)
            CountryFlag.contentMode = .scaleAspectFit
        }
        
        CountryName.snp.makeConstraints { (make) in
            make.leading.equalTo(CountryFlag.snp.trailing).offset(14)
            make.centerY.equalToSuperview()
            CountryName.font = UIFont(name: "Poppins-Regular", size: 17)
            CountryName.textColor = UIColor.black
        }
           
    }
    
    func ConfigCell(name: String){
        CountryName.text = name
        CountryFlag.image = UIImage(named: name)
    }
    func ConfigCellWithImage(TextName: String, ImageURL: URL){
        CountryName.text = TextName
        CountryFlag.LoadImageFromServer(url: ImageURL)
    }
    func ConfigCellWithInternalImage(TextName: String, ImageName: String){
        CountryName.text = TextName
        CountryFlag.image = UIImage(named: ImageName)
    }

}
