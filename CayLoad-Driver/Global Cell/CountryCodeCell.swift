//
//  CountryCodeCell.swift
//  Cayload-C
//
//  Created by Yalda Kheirkhah on 2/21/20.
//  Copyright © 2020 Yalda Kheirkhah. All rights reserved.
//

import UIKit
import SnapKit


class CountryCodeCell: UITableViewCell {
    
    var CountryName: UILabel = {
        let country = UILabel()
        return country
    }()
    
    var CountryCode: UILabel = {
        let country = UILabel()
        return country
    }()
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?){
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupview()
    }
           

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func SetData(DataTuple: (CountryName:String,CountryCode:Int)){
        CountryCode.text = "+\(DataTuple.CountryCode)"
        CountryName.text = DataTuple.CountryName
    }
    
    func setupview(){
        addSubview(CountryName)
        addSubview(CountryCode)
        
        CountryName.snp.makeConstraints { (make) in
            make.leading.equalToSuperview().offset(10)
            make.centerY.equalToSuperview()
            CountryName.font = UIFont(name: "Poppins-Regular", size: 17)
            CountryName.textColor = UIColor.black
            
            CountryName.text = "kdvjnsd"
        }
        
        CountryCode.snp.makeConstraints { (make) in
            make.trailing.equalToSuperview().offset(-10)
            make.centerY.equalToSuperview()
            
            CountryCode.font = UIFont(name: "Poppins-Regular", size: 17)
            CountryCode.textColor = UIColor.black
            CountryCode.text = "+123"
        }
        
    }

}
