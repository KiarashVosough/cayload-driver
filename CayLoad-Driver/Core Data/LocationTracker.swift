//
//  LocationTracker.swift
//  CayLoad-Driver
//
//  Created by Kiyarash Vosough on 3/25/1399 AP.
//  Copyright © 1399 CayLoad. All rights reserved.
//

import UIKit
import CoreData
import Alamofire

class LocationTracker {
    
    let FetchRequest:NSFetchRequest<NSFetchRequestResult>
    var Context:NSManagedObjectContext
    
    init(AppDelegate_Context:NSManagedObjectContext) {
        self.Context = AppDelegate_Context
        self.FetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Location")
    }
    
    func GetDataSize() throws -> Int {
        do {
            let items = try Context.fetch(FetchRequest) as! [NSManagedObject]
            return items.count
        }
        catch {
            throw CoreDataError.CantGetSize
        }
    }
    
    func SaveLocation(Latitude:Double, Longitude:Double, Accuracy:Double) throws{
        if !UIApplication.shared.isProtectedDataAvailable {
            return
        }
        let date = Date()
        let DataFormater = DateFormatter()
        DataFormater.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let dateString = DataFormater.string(from: date)
        
        let NewLocation = Location(context: Context)
        NewLocation.latitude = Latitude
        NewLocation.longitude = Longitude
        NewLocation.accuracy = Accuracy
        NewLocation.date_sampled = dateString
        do{
            try Context.save()
            print("Saved new Location")
        }
        catch {
            throw CoreDataError.CantSaveData
        }
    }
    
    private func DeleteLocation() throws {
        FetchRequest.includesPropertyValues = false
        do{
            let items = try Context.fetch(FetchRequest) as! [NSManagedObject]
            for item in 0..<50 {
              Context.delete(items[item])
            }
            try Context.save()
        }
        catch {
            throw CoreDataError.CantDeleteData
        }
    }
    
    private func LoadData() throws -> [Location]{
        
        let extractValues: [Location]
        let request = Location.LocationFetchRequest()
        request.returnsObjectsAsFaults = false
        do{
            extractValues = try Context.fetch(request)
            return extractValues
        }
        catch {
            throw CoreDataError.CantLoadData
        }
    }
    
    private func GetParam(Locations:[Location]) -> [String:[String: Any]] {
        var Array = [String:[String: Any]]()
        for item in Locations {
            Array.updateValue(["latitude":item.latitude], forKey: "location_points")
            Array.updateValue(["longitude":item.longitude], forKey: "location_points")
            Array.updateValue(["accuracy":item.accuracy], forKey: "location_points")
            Array.updateValue(["date_sampled":item.date_sampled as Any], forKey: "location_points")
        }
        return Array
    }
    
    private func RequestToSendLocationDataCore(Locations:[Location] ,_ Compeletion: @escaping (Int)-> Void){
        if NetworkReachabilityManager()!.isReachable {
            
            UserConnectionManager.RequestWithHeader(URL: DOMAIN_URL + Upload_Location_Point , RequestMethod: .post, Parameter: GetParam(Locations: Locations),
            { (HTTPStatus, JSONRecieved) in
                do{
                    try self.DeleteLocation()
                    Compeletion(HTTPStatus)
                }catch{
                    print("cant delete")
                    Compeletion(10)
                }
            }) { (HTTPStatus, ErrorArray) in
                print(HTTPStatus)
            }
        }else{
            print("network is not reacheable")
            Compeletion(0)
        }
        
    }
    
    func CheckForRequesting(_ Compeletion: @escaping (Int)-> Void) {
        if UserDefaults.standard.bool(forKey: "Is_Login") {
            do{
                let Count = try GetDataSize()
                let data = try LoadData()
                if Count == 50 {
                    RequestToSendLocationDataCore(Locations: data) { (Status) in
                        Compeletion(Status)
                    }
                }
                else if Count > 50 {
                    try DeleteLocation()
                    RequestToSendLocationDataCore(Locations: data) { (Status) in
                        Compeletion(Status)
                    }
                }
            }
            catch {
                Compeletion(0)
            }
            Compeletion(20)
        }
        else{
            Compeletion(30)
        }
    }
    
}
