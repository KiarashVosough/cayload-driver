//
//  Location+CoreDataProperties.swift
//  CayLoad-Driver
//
//  Created by Kiyarash Vosough on 3/18/1399 AP.
//  Copyright © 1399 CayLoad. All rights reserved.
//
//

import Foundation
import CoreData


extension Location {

    @nonobjc public class func LocationFetchRequest() -> NSFetchRequest<Location> {
        return NSFetchRequest<Location>(entityName: "Location")
    }

    @NSManaged public var accuracy: Double
    @NSManaged public var date_sampled: String?
    @NSManaged public var latitude: Double
    @NSManaged public var longitude: Double

}
