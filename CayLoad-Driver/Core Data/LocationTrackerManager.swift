//
//  LocationTrackerManager.swift
//  CayLoad-Driver
//
//  Created by Kiyarash Vosough on 3/18/1399 AP.
//  Copyright © 1399 CayLoad. All rights reserved.
//
import UIKit
import CoreData
import SwiftyJSON

class LocationTrackerManager {
    
    private static func setContext() -> NSManagedObjectContext {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        return context
    }
    
    static func SaveLocation(Latitude:Double, Longitude:Double, Accuracy:Double) throws{

        let date = Date()
        let DataFormater = DateFormatter()
        DataFormater.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let dateString = DataFormater.string(from: date)
        
        let context = setContext()
        let NewLocation = Location(context: context)
        NewLocation.latitude = Latitude
        NewLocation.longitude = Longitude
        NewLocation.accuracy = Accuracy
        NewLocation.date_sampled = dateString
        
        do{
            try context.save()
            print("Saved new Location")
        }
        catch {
            throw CoreDataError.CantSaveData
            
        }
    }
    
    static func deleteLocation() throws{
        let context = setContext()
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Location")
        fetchRequest.includesPropertyValues = false
        
        do{
            let items = try context.fetch(fetchRequest) as! [NSManagedObject]
            if items.count > 50 {
                for i in 0..<items.count {
                  context.delete(items[i])
                }
                try context.save()
            }else{
                for item in items {
                  context.delete(item)
                }
                try context.save()
            }
            
        }
        catch {
            throw CoreDataError.CantDeleteData
        }
    }
    
    static func loadWithCompletion(completion: @escaping ([Location]) -> Void) throws {
        
        let extractValues: [Location]
        let context = setContext()
        let request = Location.LocationFetchRequest()
        request.returnsObjectsAsFaults = false
        do{
            extractValues = try context.fetch(request)
        }
        catch {
            throw CoreDataError.CantLoadData
            
        }
        completion(extractValues)
    }
    
    static func GetSize() throws -> Int{
        let context = setContext()
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Location")
        do {
            let count = try context.count(for: fetchRequest)
            return count
        } catch {
            throw CoreDataError.NoDataRecord
        }
    }
    
    static func GetParam(Locations:[Location]) -> [String:[String: Any]] {
        var Array = [String:[String: Any]]()
        for item in Locations {
            Array.updateValue(["latitude":item.latitude], forKey: "location_points")
            Array.updateValue(["longitude":item.longitude], forKey: "location_points")
            Array.updateValue(["accuracy":item.accuracy], forKey: "location_points")
            Array.updateValue(["date_sampled":item.date_sampled as Any], forKey: "location_points")
        }
        return Array
    }
    
    static func RequestToSendLocationOnForeground(){
        if UserDefaults.standard.bool(forKey: "Is_Login") {
            do {
                let Count = try LocationTrackerManager.GetSize()
                print("try to sen din fore",Count)
                if Count == 50 {
                    try LocationTrackerManager.loadWithCompletion(completion: { (Location) in
                        RequestToSendLocation(Locations: Location)
                    })
                }//end 50  if
            }//end do
            catch  CoreDataError.NoDataRecord {
                print("Cant get size")
            }
            catch CoreDataError.CantLoadData {
                print("Cant load data")
            }
            catch{
                print("sth else")
            }
        }
    }
    
    static func RequestToSendLocation(Locations:[Location]){
        UserConnectionManager.RequestWithHeader(URL: DOMAIN_URL + Upload_Location_Point , RequestMethod: .post, Parameter: GetParam(Locations: Locations),
        { (HTTPStatus, JSONRecieved) in
            do{
                try deleteLocation()
            }catch{
                print("cant delete")
            }
        }) { (HTTPStatus, ErrorArray) in
            print(HTTPStatus)
        }
    }
}
