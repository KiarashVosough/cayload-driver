//
//  PlanCollectionViewCell.swift
//  CayLoad-Driver
//
//  Created by Kiyarash Vosough on 1/12/1399 AP.
//  Copyright © 1399 CayLoad. All rights reserved.
//

import UIKit
import SnapKit
class PlanCollectionViewCell: UICollectionViewCell {
    
    var NavigationDelegate:ProfilenavigationDelegate?
    
    lazy var CurrentPlan_Label:UILabel = {
        let instance = UILabel()
        instance.text = "Current Plan".localized()
        instance.font = UIFont(name: "Poppins-Bold", size: 13)
        instance.textColor = .black
        return instance
    }()
    lazy var CurrentPlanInfo_Label:UILabel = {
        let instance = UILabel()
        instance.font = UIFont(name: "Poppins", size: 13)
        instance.textColor = .black
        return instance
    }()
    
    lazy var Remaining_Label:UILabel = {
        let instance = UILabel()
        instance.text = "Remaining".localized()
        instance.font = UIFont(name: "Poppins-Bold", size: 13)
        instance.textColor = .black
        return instance
    }()
    lazy var RemainingInfo_Label:UILabel = {
        let instance = UILabel()
        instance.text = "-"
        instance.font = UIFont(name: "Poppins", size: 13)
        instance.textColor = .black
        return instance
    }()
    lazy var BuyNewPlan_Label:UILabel = {
        let instance = UILabel()
        instance.text = "Buy a New Plan".localized()
        instance.font = UIFont(name: "Poppins-Bold", size: 17)
        instance.textColor = .black
        return instance
    }()
    var PlanButton = [String:UIButton]()
    var PreviousButton = UIButton()
    
    //containers
    lazy var CurrentPlan_Container:UIView = {
        let instance = UIView()
        instance.backgroundColor = .white
        instance.layer.cornerRadius = 9
        return instance
    }()
    lazy var Remaining_Container:UIView = {
        let instance = UIView()
        instance.backgroundColor = .white
        instance.layer.cornerRadius = 9
        return instance
    }()
    
    
    lazy var MainViewSize = CGSize(width: self.frame.width, height: self.frame.height + 700)
    
    lazy var ScrollView: UIScrollView = {
        let ScrollViewLambda = UIScrollView()
        ScrollViewLambda.backgroundColor = rgb(red: 247, green: 247, blue: 247,ALPHA: 1)
        ScrollViewLambda.contentSize = MainViewSize
        self.frame = self.bounds
        ScrollViewLambda.autoresizingMask = .flexibleHeight
        ScrollViewLambda.showsHorizontalScrollIndicator = true
        ScrollViewLambda.showsVerticalScrollIndicator = true
        ScrollViewLambda.bounces = true
        return ScrollViewLambda
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = rgb(red: 242, green: 242, blue: 242, ALPHA: 1)
        OnCreateScrollView()
        OnCreateTopContainers()
        AddRefreshControl()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @objc func Goto(_ sender: UIButton){
        NavigationDelegate?.PerformSegueForBuyPlan(withIdentifier: "OnProfilePaymentSegue")
    }
    
    private func UpdateCurrentPlanView(){
        if Driver?.user?.nationality?.unlimited_usage == true {
            CurrentPlanInfo_Label.text = "Unlimited".localized()
        }else {
            CurrentPlanInfo_Label.text = "Limited".localized()
        }
    }
    private func UpdatePlanInfoView(){
        if UserPlan?.plan != nil {
            CurrentPlanInfo_Label.text = UserPlan?.plan?.name
            RemainingInfo_Label.text = "\(UserPlan?.plan?.valid_days ?? 0)" + "d"
        }
    }
    
    private func UpdatePlansSelectionViews(){
        if !PlansArray.isEmpty {
            for i in 0..<PlansArray.count {
                if i == 0 {
                    AddPlans(IsFirst: true, PlanDurationText: PlansArray[i].name, PlanPriceText: "\(PlansArray[i].price)" + "$", ButtonTag: i)
                }else{
                    AddPlans(IsFirst: false, PlanDurationText: PlansArray[i].name, PlanPriceText: "\(PlansArray[i].price)" + "$", ButtonTag: i)
                }
            }
        }else{
            BuyNewPlan_Label.removeFromSuperview()
        }
    }
    
    // MARK: - Network
    
    var PlansArray = [PlanModel]()
    var Driver:DriverModel?
    var UserPlan:UserPlanModel?
    var refreshControl = UIRefreshControl()
    var HTTP_Status:Int = 0
    
    private func AddRefreshControl(){
        refreshControl.attributedTitle = NSAttributedString(string: "Pull To Refresh".localized())
        refreshControl.addTarget(self, action: #selector(refresh(_:)), for: .valueChanged)
        ScrollView.addSubview(refreshControl)
    }
    
    @objc func refresh(_ sender: AnyObject) {
        DispatchQueue.global(qos: .userInitiated).async {
            let group = DispatchGroup()
            group.enter()
            self.RequestToGetProfile()
            group.wait()
            self.RequestToGetPlans()
            group.leave()
        }
    }
    
    func RequestToGetProfile(){
        UserConnectionManager.RequestWithHeader(URL: (DOMAIN_URL + Profile_URL), RequestMethod: .get, Parameter: nil, { (HTTPStatus, RecievedJson) in
            print("Getting Profile successed with code \(HTTPStatus)")
            self.Driver = DriverModel(RecievedJson!["driver"])
            self.UpdateCurrentPlanView()
            self.RequestToGetUserPlan()
            self.refreshControl.endRefreshing()
            self.HTTP_Status = HTTPStatus
        }) { (HTTPStatus, ErrorArray) in
            self.refreshControl.endRefreshing()
            self.HTTP_Status = HTTPStatus
            print("Getting Profile failed with code \(HTTPStatus)")
            self.NavigationDelegate?.ShowAlert(Message: ErrorArray?[0].body ?? "Check Your Connection")
        }
    }
    
    func RequestToGetUserPlan(){
        let URL:String = (DOMAIN_URL + Driver_URL + "\(Driver?.id ?? 0)" + Get_User_Current_Plan_URL)
        UserConnectionManager.RequestWithHeader(URL: URL, RequestMethod: .get, Parameter: nil, { (HTTPStatus, RecievedJson) in
            print("Getting UserPlan successed with code \(HTTPStatus)")
            if RecievedJson?["drivers"].null == nil {
                self.UserPlan = UserPlanModel(RecievedJson!["drivers"])
                self.UpdatePlanInfoView()
            }
            
        }) { (HTTPStatus, ErrorArray) in
            
            print("Getting UserPlan failed with code \(HTTPStatus)")
            self.NavigationDelegate?.ShowAlert(Message: ErrorArray?[0].body ?? "Check Your Connection")
            
        }
    }

    
     func RequestToGetPlans(){
        let URL:String = (DOMAIN_URL + User_URL + String(UserDefaults.standard.integer(forKey: "user_id")) + Get_Plans_URL)
        UserConnectionManager.RequestWithHeader(URL: URL, RequestMethod: .get, Parameter: nil, { (HTTPStatus, RecievedJson) in
            print("Getting Plans successed with code \(HTTPStatus)")
            self.PlansArray = JsonParsingUtils.GetArrayList(From: RecievedJson! , withRootTag: "users")
            self.UpdatePlansSelectionViews()
            self.refreshControl.endRefreshing()
            self.HTTP_Status = HTTPStatus
        }) { (HTTPStatus, ErrorArray) in
            self.refreshControl.endRefreshing()
            self.HTTP_Status = HTTPStatus
            print("Getting Plans failed with code \(HTTPStatus)")
            self.NavigationDelegate?.ShowAlert(Message: ErrorArray?[0].body ?? "Check Your Connection")
            
        }
    }
    
    // MARK: - OnCreate Views
    private func OnCreateScrollView(){
        addSubview(ScrollView)
        ScrollView.snp.makeConstraints{ (make) in
            make.width.equalToSuperview()
            make.height.equalToSuperview()
        }// end ScrollView.snp.makeConstraints
    }//end OnCreateScrollView
    
    private func OnCreateTopContainers(){
        
        ScrollView.addSubview(CurrentPlan_Container)
        CurrentPlan_Container.addSubview(CurrentPlan_Label)
        CurrentPlan_Container.addSubview(CurrentPlanInfo_Label)
        
        CurrentPlan_Container.snp.makeConstraints { (make) in
            //CurrentPlan_Container.semanticContentAttribute = .forceRightToLeft

            make.top.equalToSuperview().offset(23)
            make.centerX.equalToSuperview()
            make.height.equalTo(51)
            MatchwidthWithDevices(MatchedView: CurrentPlan_Container)
        }
        CurrentPlan_Label.snp.makeConstraints { (make) in
            make.centerY.equalToSuperview()
            make.left.equalToSuperview().offset(15)
        }
        CurrentPlanInfo_Label.snp.makeConstraints { (make) in
            make.centerY.equalToSuperview()
            make.right.equalToSuperview().offset(-15)
        }
        
        ScrollView.addSubview(Remaining_Container)
        Remaining_Container.addSubview(Remaining_Label)
        Remaining_Container.addSubview(RemainingInfo_Label)
        
        Remaining_Container.snp.makeConstraints { (make) in
            make.top.equalTo(CurrentPlan_Container.snp.bottom).offset(15)
            make.centerX.equalToSuperview()
            make.height.equalTo(51)
            MatchwidthWithDevices(MatchedView: Remaining_Container)
        }
        Remaining_Label.snp.makeConstraints { (make) in
            make.centerY.equalToSuperview()
            make.left.equalToSuperview().offset(15)
        }
        RemainingInfo_Label.snp.makeConstraints { (make) in
            make.centerY.equalToSuperview()
            make.right.equalToSuperview().offset(-15)
        }
        
        
        ScrollView.addSubview(BuyNewPlan_Label)
        BuyNewPlan_Label.snp.makeConstraints { (make) in
            make.top.equalTo(Remaining_Container.snp.bottom).offset(35)
            make.centerX.equalToSuperview()
        }
    }
    
    
    private func AddPlans(IsFirst: Bool, PlanDurationText: String, PlanPriceText: String, ButtonTag: Int){
        let Plan_Button:UIButton = {
            let instance = UIButton()
            instance.layer.cornerRadius = 9
            instance.tag = ButtonTag
            instance.backgroundColor = .white
            return instance
        }()
        let PlanDuration_Label:UILabel = {
            let instance = UILabel()
            instance.text = PlanDurationText
            instance.font = UIFont(name: "Poppins-Bold", size: 17)
            instance.textColor = .black
            return instance
        }()
        let PlanPrice_Label:UILabel = {
            let instance = UILabel()
            instance.layer.cornerRadius = 9
            instance.layer.masksToBounds = true
            instance.text = PlanPriceText
            instance.font = UIFont(name: "Poppins-Bold", size: 17)
            instance.textColor = .white
            instance.textAlignment = .center
            instance.backgroundColor = rgb(red: 36, green: 53, blue: 152, ALPHA: 1)
            return instance
        }()
        
        
        ScrollView.addSubview(Plan_Button)
        Plan_Button.addSubview(PlanDuration_Label)
        Plan_Button.addSubview(PlanPrice_Label)
        
        Plan_Button.snp.makeConstraints { (make) in
            if IsFirst {
                make.top.equalTo(BuyNewPlan_Label.snp.bottom).offset(20)
                Plan_Button.addTarget(self, action: #selector(Goto(_:)), for: .touchUpInside)
            }
            else{
                make.top.equalTo(PreviousButton.snp.bottom).offset(15)
            }
            make.centerX.equalToSuperview()
            make.height.equalTo(75)
            MatchwidthWithDevices(MatchedView: Plan_Button)
        }
        PlanDuration_Label.snp.makeConstraints { (make) in
            make.centerY.equalToSuperview()
            make.left.equalToSuperview().offset(15)
        }
        PlanPrice_Label.snp.makeConstraints { (make) in
            make.centerY.equalToSuperview()
            make.right.equalToSuperview().offset(-15)
            make.height.equalTo(50)
            make.width.equalTo(50)
        }
        PreviousButton = Plan_Button
        
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    func MatchwidthWithDevices( MatchedView: UIView) {
        if UIDevice.current.userInterfaceIdiom == .phone {
            MatchedView.snp.makeConstraints{ (make) in
                make.left.equalTo(self).offset(20)
                make.right.equalTo(self).offset(-20)
            }//end make
        }
        else if UIDevice.current.userInterfaceIdiom == .pad{
            MatchedView.snp.makeConstraints{ (make) in
                make.left.equalTo(self).offset(70)
                make.right.equalTo(self).offset(-70)
            }//end make
        }
        
    }
    
    func rgb(red: CGFloat, green: CGFloat, blue: CGFloat,ALPHA: CGFloat) -> UIColor {
         return UIColor(red: red/255, green: green/255, blue: blue/255, alpha: ALPHA)
       }
}
