//
//  PaymentOnProfilePageViewController.swift
//  CayLoad-Driver
//
//  Created by Kiyarash Vosough on 1/12/1399 AP.
//  Copyright © 1399 CayLoad. All rights reserved.
//

import UIKit

class PaymentOnProfilePageViewController: UIViewController {
    
    
    @IBOutlet weak var Header_View: UIView!
    @IBOutlet weak var Back_Button: UIButton!
    
    
    lazy var PaymentType_Label:UILabel = {
        let instance = UILabel()
        instance.text = "Payment Type"
        instance.font = UIFont(name: "Poppins-Bold", size: 15)
        instance.textColor = .black
        return instance
    }()
    lazy var Wallet_Button:UIButton = {
        let instance = UIButton()
        instance.setTitle("Wallet", for: .normal)
        instance.setTitleColor(.white, for: .normal)
        instance.titleLabel?.font = UIFont(name: "Poppins-Bold", size: 15)
        instance.backgroundColor = rgb(red: 36, green: 53, blue: 152, ALPHA: 1)
        instance.layer.cornerRadius = 9
        instance.layer.maskedCorners = [.layerMinXMinYCorner, .layerMinXMaxYCorner]
        return instance
    }()
    lazy var Gateway_Button:UIButton = {
        let instance = UIButton()
        instance.setTitle("Gateway", for: .normal)
        instance.setTitleColor(.black, for: .normal)
        instance.titleLabel?.font = UIFont(name: "Poppins-Bold", size: 15)
        instance.backgroundColor = .white
        instance.layer.cornerRadius = 9
        instance.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMaxXMaxYCorner]
        return instance
    }()
    
    
    lazy var Price_Label:UILabel = {
           let instance = UILabel()
           instance.text = "Price"
           instance.font = UIFont(name: "Poppins-Bold", size: 13)
           instance.textColor = .black
           return instance
       }()
    lazy var PriceInfo_Label:UILabel = {
        let instance = UILabel()
        instance.text = "20$"
        instance.font = UIFont(name: "Poppins", size: 13)
        instance.textColor = .black
        return instance
    }()
    
    lazy var Title_Label:UILabel = {
           let instance = UILabel()
           instance.text = "Title"
           instance.font = UIFont(name: "Poppins-Bold", size: 13)
           instance.textColor = .black
           return instance
       }()
    lazy var TitleInfo_Label:UILabel = {
        let instance = UILabel()
        instance.text = "Monthly"
        instance.font = UIFont(name: "Poppins", size: 13)
        instance.textColor = .black
        return instance
    }()
    
    lazy var CurrentBalance_Label:UILabel = {
           let instance = UILabel()
           instance.text = "Current Balance"
           instance.font = UIFont(name: "Poppins-Bold", size: 13)
           instance.textColor = .black
           return instance
       }()
    lazy var CurrentBalanceInfo_Label:UILabel = {
        let instance = UILabel()
        instance.text = "5649$"
        instance.font = UIFont(name: "Poppins", size: 13)
        instance.textColor = .black
        return instance
    }()
    lazy var Pay_Button:UIButton = {
        let instance = UIButton()
        instance.layer.cornerRadius = 9
        instance.setTitle("Pay", for: .normal)
        instance.titleLabel?.font = UIFont(name: "Poppins-Bold", size: 15)
        instance.backgroundColor = rgb(red: 36, green: 53, blue: 152, ALPHA: 1)
        return instance
    }()
    lazy var CardType_Label:UILabel = {
        let instance = UILabel()
        instance.text = "Select Card Type"
        instance.font = UIFont(name: "Poppins-Bold", size: 15)
        instance.textColor = .black
        return instance
    }()
    lazy var Visa_Button:UIButton = {
        let instance = UIButton()
        instance.BorderWidth = 2
        instance.BorderColor = rgb(red: 36, green: 53, blue: 152, ALPHA: 1)
        instance.setImage(UIImage(named: "VisaIc"), for: .normal)
        instance.layer.cornerRadius = 9
        instance.backgroundColor = .white
        return instance
    }()
    lazy var MasterCard_Button:UIButton = {
        let instance = UIButton()
        instance.BorderColor = rgb(red: 36, green: 53, blue: 152, ALPHA: 1)
        instance.setImage(UIImage(named: "MasterCardIc"), for: .normal)
        instance.layer.cornerRadius = 9
        instance.backgroundColor = .white
        return instance
    }()
    lazy var Nationality_Button:UIButton = {
        let instance = UIButton()
        instance.layer.cornerRadius = 9
        instance.backgroundColor = .white
        return instance
    }()
    lazy var Nationality_ImageView:UIImageView = {
        let instance = UIImageView()
        instance.image = UIImage(named: "NationalityIc")
        return instance
    }()
    lazy var DownArrow_ImageView:UIImageView = {
        let instance = UIImageView()
        instance.image = UIImage(named: "DownArrowIc")
        return instance
    }()
    lazy var GatewayCountry_Label:UILabel = {
        let instance = UILabel()
        instance.text = "Gateway Country"
        instance.font = UIFont(name: "Poppins-Bold", size: 15)
        instance.textColor = rgb(red: 60, green: 60, blue: 67, ALPHA: 0.3)
        return instance
    }()
    
    
    
    
    
    
    //container
    lazy var Space_View:UIView = {
        let instance = UIView()
        return instance
    }()
    lazy var CardSpace_View:UIView = {
        let instance = UIView()
        return instance
    }()
    lazy var ScrollView_ConrentView:UIView = {
        let instance = UIView()
        instance.backgroundColor = rgb(red: 247, green: 247, blue: 247,ALPHA: 1)
        return instance
    }()
    lazy var Price_Container:UIView = {
        let instance = UIView()
        instance.backgroundColor = .white
        instance.layer.cornerRadius = 9
        return instance
    }()
    lazy var Title_Container:UIView = {
        let instance = UIView()
        instance.backgroundColor = .white
        instance.layer.cornerRadius = 9
        return instance
    }()
    lazy var CurrentBalance_Container:UIView = {
        let instance = UIView()
        instance.backgroundColor = .white
        instance.layer.cornerRadius = 9
        return instance
    }()
    
    lazy var MainViewSize = CGSize(width: view.frame.width, height: view.frame.height + 1)
    
    lazy var ScrollView: UIScrollView = {
        let ScrollViewLambda = UIScrollView()
        ScrollViewLambda.backgroundColor = rgb(red: 247, green: 247, blue: 247,ALPHA: 1)
        ScrollViewLambda.contentSize = MainViewSize
        view.frame = view.bounds
        ScrollViewLambda.autoresizingMask = .flexibleHeight
        ScrollViewLambda.showsHorizontalScrollIndicator = true
        //ScrollViewLambda.showsVerticalScrollIndicator = true
        ScrollViewLambda.bounces = true
        return ScrollViewLambda
    }()
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        ScrollView.endEditing(true)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        Back_Button.addTarget(self, action: #selector(OnBackButtonClicked(_:)), for: .touchUpInside)
        Wallet_Button.addTarget(self, action: #selector(OnWalletButtonCliked(_:)), for: .touchUpInside)
        Gateway_Button.addTarget(self, action: #selector(OnGatewayButtonCliked(_:)), for: .touchUpInside)
        Visa_Button.addTarget(self, action: #selector(OnVisaButtonClicked(_:)), for: .touchUpInside)
        MasterCard_Button.addTarget(self, action: #selector(OnMasterCardButtonClicked(_:)), for: .touchUpInside)
        
        OnCreateScrollView()
        OnCreatePaymentMethodButtons()
        OnCreateWalletChoice()
        //OnCreateGatway()
    }
    
    @objc func OnBackButtonClicked(_ sender: UIButton){
        _ = navigationController?.popToRootViewController(animated: true)
    }
    
    @objc func OnWalletButtonCliked(_ sender: UIButton){
        if Wallet_Button.backgroundColor == UIColor.white {
            Wallet_Button.backgroundColor = rgb(red: 36, green: 53, blue: 152, ALPHA: 1)
            Wallet_Button.setTitleColor(.white, for: .normal)
            Gateway_Button.backgroundColor = .white
            Gateway_Button.setTitleColor(.black, for: .normal)
            DestroyGatewayView()
            OnCreateWalletChoice()
        }
    }
    
    @objc func OnGatewayButtonCliked(_ sender: UIButton){
        if Gateway_Button.backgroundColor == UIColor.white {
            Wallet_Button.backgroundColor = .white
            Wallet_Button.setTitleColor(.black, for: .normal)
            Gateway_Button.backgroundColor = rgb(red: 36, green: 53, blue: 152, ALPHA: 1)
            Gateway_Button.setTitleColor(.white, for: .normal)
            DestroyWalletView()
            OnCreateGatway()
        }
    }
    
    @objc func OnVisaButtonClicked(_ sender: UIButton){
        if sender.layer.borderWidth != 1 {
            sender.BorderWidth = 2
            MasterCard_Button.BorderWidth = 0
        }
    }
    @objc func OnMasterCardButtonClicked(_ sender: UIButton){
        if sender.layer.borderWidth != 1 {
            sender.BorderWidth = 2
            Visa_Button.BorderWidth = 0
        }
    }
    
    private func DestroyWalletView(){
        Pay_Button.removeFromSuperview()
        Title_Label.removeFromSuperview()
        TitleInfo_Label.removeFromSuperview()
        Title_Container.removeFromSuperview()
        Price_Label.removeFromSuperview()
        PriceInfo_Label.removeFromSuperview()
        Price_Container.removeFromSuperview()
        CurrentBalanceInfo_Label.removeFromSuperview()
        CurrentBalance_Label.removeFromSuperview()
        CurrentBalance_Container.removeFromSuperview()
    }
    
    private func DestroyGatewayView(){
        Pay_Button.removeFromSuperview()
        Nationality_ImageView.removeFromSuperview()
        GatewayCountry_Label.removeFromSuperview()
        DownArrow_ImageView.removeFromSuperview()
        Nationality_Button.removeFromSuperview()
        Visa_Button.removeFromSuperview()
        MasterCard_Button.removeFromSuperview()
        CardSpace_View.removeFromSuperview()
        CardType_Label.removeFromSuperview()
        Title_Label.removeFromSuperview()
        TitleInfo_Label.removeFromSuperview()
        Title_Container.removeFromSuperview()
        Price_Label.removeFromSuperview()
        PriceInfo_Label.removeFromSuperview()
        Price_Container.removeFromSuperview()
        
    }
    
    
    private func OnCreateScrollView(){
        view.addSubview(ScrollView)
        ScrollView.snp.makeConstraints{ (make) in
            make.width.equalToSuperview()
            make.bottom.equalToSuperview()
            make.top.equalTo(Header_View.snp.bottom)
        }// end ScrollView.snp.makeConstraints
        ScrollView.addSubview(ScrollView_ConrentView)
        ScrollView_ConrentView.snp.makeConstraints { (make) in
            make.centerX.equalTo(ScrollView.snp.centerX)
            make.centerY.equalTo(ScrollView.snp.centerY)
            make.top.equalToSuperview()
            make.width.equalTo(ScrollView.snp.width)
            make.height.equalToSuperview()
        }
    }//end OnCreateScrollView
    
    private func OnCreatePaymentMethodButtons(){
        ScrollView_ConrentView.addSubview(Space_View)
        Space_View.snp.makeConstraints { (make) in
            make.height.equalTo(51)
            make.width.equalTo(1)
            make.top.equalTo(ScrollView_ConrentView).offset(52)
            make.centerX.equalToSuperview()
        }
        
        ScrollView_ConrentView.addSubview(Wallet_Button)
        Wallet_Button.snp.makeConstraints { (make) in
            make.top.equalTo(ScrollView_ConrentView).offset(52)
            make.height.equalTo(51)
            if UIDevice.current.userInterfaceIdiom == .phone {
                make.leading.equalToSuperview().offset(23)
            }
            else{
                make.leading.equalToSuperview().offset(73)
            }
            make.trailing.equalTo(Space_View.snp.leading)
            
        }
        
        ScrollView_ConrentView.addSubview(Gateway_Button)
        Gateway_Button.snp.makeConstraints { (make) in
            make.top.equalTo(ScrollView_ConrentView).offset(52)
            make.height.equalTo(51)
            make.leading.equalTo(Space_View.snp.trailing)
            if UIDevice.current.userInterfaceIdiom == .phone {
             make.trailing.equalToSuperview().offset(-23)
            }
            else{
                make.trailing.equalToSuperview().offset(-73)
            }
        }
        ScrollView_ConrentView.addSubview(PaymentType_Label)
        PaymentType_Label.snp.makeConstraints { (make) in
            make.leading.equalTo(Wallet_Button.snp.leading).offset(15)
            make.bottom.equalTo(Wallet_Button.snp.top).offset(-7)
        }
    }
    
    
    
    
    
    private func OnCreateWalletChoice(){
        
        ScrollView_ConrentView.addSubview(CurrentBalance_Container)
        CurrentBalance_Container.addSubview(CurrentBalance_Label)
        CurrentBalance_Container.addSubview(CurrentBalanceInfo_Label)
        
        CurrentBalance_Container.snp.makeConstraints { (make) in
            make.top.equalTo(Space_View.snp.bottom).offset(16)
            MatchwidthWithDevices(MatchedView: CurrentBalance_Container)
            make.height.equalTo(51)
            make.centerX.equalToSuperview()
        }
        CurrentBalance_Label.snp.makeConstraints { (make) in
            make.centerY.equalToSuperview()
            make.leading.equalToSuperview().offset(15)
        }
        CurrentBalanceInfo_Label.snp.makeConstraints { (make) in
            make.centerY.equalToSuperview()
            make.trailing.equalToSuperview().offset(-15)
        }
        
        ScrollView_ConrentView.addSubview(Price_Container)
        Price_Container.addSubview(Price_Label)
        Price_Container.addSubview(PriceInfo_Label)

        Price_Container.snp.makeConstraints { (make) in
            make.top.equalTo(CurrentBalance_Container.snp.bottom).offset(16)
            MatchwidthWithDevices(MatchedView: Price_Container)
            make.height.equalTo(51)
            make.centerX.equalToSuperview()
        }
        Price_Label.snp.makeConstraints { (make) in
            make.centerY.equalToSuperview()
            make.leading.equalToSuperview().offset(15)
        }
        PriceInfo_Label.snp.makeConstraints { (make) in
            make.centerY.equalToSuperview()
            make.trailing.equalToSuperview().offset(-15)
        }

        ScrollView_ConrentView.addSubview(Title_Container)
        Title_Container.addSubview(Title_Label)
        Title_Container.addSubview(TitleInfo_Label)
        Title_Container.snp.makeConstraints { (make) in
            make.top.equalTo(Price_Container.snp.bottom).offset(16)
            MatchwidthWithDevices(MatchedView: Title_Container)
            make.height.equalTo(51)
            make.centerX.equalToSuperview()
        }
        Title_Label.snp.makeConstraints { (make) in
            make.centerY.equalToSuperview()
            make.leading.equalToSuperview().offset(15)
        }
        TitleInfo_Label.snp.makeConstraints { (make) in
            make.centerY.equalToSuperview()
            make.trailing.equalToSuperview().offset(-15)
        }

        ScrollView_ConrentView.addSubview(Pay_Button)
        Pay_Button.snp.makeConstraints { (make) in
            make.top.equalTo(Title_Container.snp.bottom).offset(15)
            MatchwidthWithDevices(MatchedView: Pay_Button)
            make.centerX.equalToSuperview()
            make.height.equalTo(51)
        }
        
    }
    
    
    private func OnCreateGatway(){
        
        ScrollView_ConrentView.addSubview(Price_Container)
        Price_Container.addSubview(Price_Label)
        Price_Container.addSubview(PriceInfo_Label)

        Price_Container.snp.makeConstraints { (make) in
            make.top.equalTo(Space_View.snp.bottom).offset(16)
            MatchwidthWithDevices(MatchedView: Price_Container)
            make.height.equalTo(51)
            make.centerX.equalToSuperview()
        }
        Price_Label.snp.makeConstraints { (make) in
            make.centerY.equalToSuperview()
            make.leading.equalToSuperview().offset(15)
        }
        PriceInfo_Label.snp.makeConstraints { (make) in
            make.centerY.equalToSuperview()
            make.trailing.equalToSuperview().offset(-15)
        }

        ScrollView_ConrentView.addSubview(Title_Container)
        Title_Container.addSubview(Title_Label)
        Title_Container.addSubview(TitleInfo_Label)
        Title_Container.snp.makeConstraints { (make) in
            make.top.equalTo(Price_Container.snp.bottom).offset(16)
            MatchwidthWithDevices(MatchedView: Title_Container)
            make.height.equalTo(51)
            make.centerX.equalToSuperview()
        }
        Title_Label.snp.makeConstraints { (make) in
            make.centerY.equalToSuperview()
            make.leading.equalToSuperview().offset(15)
        }
        TitleInfo_Label.snp.makeConstraints { (make) in
            make.centerY.equalToSuperview()
            make.trailing.equalToSuperview().offset(-15)
        }
        
        
        ScrollView_ConrentView.addSubview(CardSpace_View)
        ScrollView_ConrentView.addSubview(Visa_Button)
        ScrollView_ConrentView.addSubview(MasterCard_Button)
        ScrollView_ConrentView.addSubview(CardType_Label)
        CardSpace_View.snp.makeConstraints { (make) in
            make.height.equalTo(54)
            make.width.equalTo(9)
            make.top.equalTo(Title_Container.snp.bottom).offset(42)
            make.centerX.equalToSuperview()
        }
        Visa_Button.snp.makeConstraints { (make) in
            make.top.equalTo(Title_Container.snp.bottom).offset(42)
            make.height.equalTo(51)
            if UIDevice.current.userInterfaceIdiom == .phone {
                make.leading.equalToSuperview().offset(23)
            }
            else{
                make.leading.equalToSuperview().offset(73)
            }
            make.trailing.equalTo(CardSpace_View.snp.leading)
        }
        MasterCard_Button.snp.makeConstraints { (make) in
            make.top.equalTo(Title_Container.snp.bottom).offset(42)
            make.height.equalTo(51)
            make.leading.equalTo(CardSpace_View.snp.trailing)
            if UIDevice.current.userInterfaceIdiom == .phone {
             make.trailing.equalToSuperview().offset(-23)
            }
            else{
                make.trailing.equalToSuperview().offset(-73)
            }
        }
        CardType_Label.snp.makeConstraints { (make) in
            make.leading.equalTo(Visa_Button.snp.leading).offset(15)
            make.bottom.equalTo(Visa_Button.snp.top).offset(-7)
        }
        
        ScrollView_ConrentView.addSubview(Nationality_Button)
        Nationality_Button.addSubview(Nationality_ImageView)
        Nationality_Button.addSubview(DownArrow_ImageView)
        Nationality_Button.addSubview(GatewayCountry_Label)
        
        Nationality_Button.snp.makeConstraints { (make) in
            make.top.equalTo(Visa_Button.snp.bottom).offset(15)
            MatchwidthWithDevices(MatchedView: Nationality_Button)
            make.centerX.equalToSuperview()
            make.height.equalTo(54)
        }
        Nationality_ImageView.snp.makeConstraints { (make) in
            make.centerY.equalToSuperview()
            make.leading.equalToSuperview().offset(18)
        }
        GatewayCountry_Label.snp.makeConstraints { (make) in
            make.centerY.equalToSuperview()
            make.leading.equalTo(Nationality_ImageView.snp.trailing).offset(15)
        }
        DownArrow_ImageView.snp.makeConstraints { (make) in
            make.centerY.equalToSuperview()
            make.trailing.equalToSuperview().offset(-22)
        }
        
        ScrollView_ConrentView.addSubview(Pay_Button)
        Pay_Button.snp.makeConstraints { (make) in
            make.top.equalTo(Nationality_Button.snp.bottom).offset(15)
            MatchwidthWithDevices(MatchedView: Pay_Button)
            make.centerX.equalToSuperview()
            make.height.equalTo(51)
        }
        
    }
    
    
    func MatchwidthWithDevices( MatchedView: UIView) {
        if UIDevice.current.userInterfaceIdiom == .phone {
            MatchedView.snp.makeConstraints{ (make) in
                make.leading.equalToSuperview().offset(23)
                make.trailing.equalToSuperview().offset(-23)
            }//end make
        }
        else if UIDevice.current.userInterfaceIdiom == .pad{
            MatchedView.snp.makeConstraints{ (make) in
                make.leading.equalToSuperview().offset(73)
                make.trailing.equalToSuperview().offset(-73)
            }//end make
        }
        
    }
    
    func rgb(red: CGFloat, green: CGFloat, blue: CGFloat,ALPHA: CGFloat) -> UIColor {
         return UIColor(red: red/255, green: green/255, blue: blue/255, alpha: ALPHA)
       }
}
