//
//  ProfileTabCollectionViewCell.swift
//  CayLoad-Driver
//
//  Created by Kiyarash Vosough on 1/9/1399 AP.
//  Copyright © 1399 CayLoad. All rights reserved.
//

import UIKit
import SnapKit
import CoreLocation

class SpecialLabel: UILabel {
    override func drawText(in rect: CGRect) {
        let Insets = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 5)
        super.drawText(in: rect.inset(by: Insets))
    }
}

class ProfileTabCollectionViewCell: UICollectionViewCell,CLLocationManagerDelegate {
    
    //navigation delegate
    var NavigationDelegate:ProfilenavigationDelegate?
    
    //Dynamic component
    private lazy var CurrentLocation_Label:UILabel = {
        let instance = UILabel()
        instance.text = "Current Location".localized()
        instance.font = UIFont(name: "Poppins-Bold", size: 13)
        instance.textColor = .black
        return instance
    }()
    private lazy var CurrentLocationInfo_Label:UILabel = {
        let instance = UILabel()
        instance.adjustsFontSizeToFitWidth = true
        instance.adjustsFontForContentSizeCategory = true
        instance.minimumScaleFactor = 0.5
        instance.numberOfLines = 1
        instance.textAlignment = .center
        instance.font = UIFont(name: "Poppins", size: 13)
        instance.textColor = .black
        return instance
    }()
    private lazy var UpdateLocation_Button:UIButton = {
        let instance = UIButton()
        instance.layer.cornerRadius = 9
        instance.backgroundColor = .App_DarkBlue
        return instance
    }()
    private lazy var UpdateLocation_Label:UILabel = {
       let instance = UILabel()
        instance.text = "Update Location".localized()
        instance.font = UIFont(name: "Poppins-Bold", size: 15)
        instance.textColor = .white
        return instance
    }()
    private lazy var UpdateLocation_ImageView:UIImageView = {
       let instance = UIImageView()
        instance.image = UIImage(named: "RefreshIc")
        return instance
    }()
    
    private var TruckModelText_Label:UILabel = {
        let instance = UILabel()
        instance.text = "Truck Model".localized()
        instance.textColor = .black
        instance.font = UIFont(name: "Poppins-Bold", size: 13)
        return instance
    }()
    private var TruckModelDescription_Label:UILabel = {
        let instance = UILabel()
        instance.textColor = .black
        instance.font = UIFont(name: "Poppins", size: 13)
        return instance
       }()
         //triple container
           //national plate
    private var NationalPlateText_Label:UILabel = {
        let instance = UILabel()
        instance.text = "National Plate".localized()
        instance.textColor = .black
        instance.font = UIFont(name: "Poppins-Bold", size: 13)
        return instance
       }()
    private lazy var NationalPlateDescription_Container:UIView = {
        let instance = UIView()
        instance.layer.cornerRadius = 9
        instance.backgroundColor = rgb(red: 247, green: 247, blue: 247,ALPHA: 1)
        return instance
    }()
    private lazy var NationalPlateDescription_Label:SpecialLabel = {
        let instance = SpecialLabel()
        instance.textColor = .black
        instance.adjustsFontSizeToFitWidth = true
        instance.layer.masksToBounds = true
        instance.layer.cornerRadius = 9
        instance.textAlignment = .center
//        instance.adjustsFontForContentSizeCategory = true
        instance.numberOfLines = 1
//        instance.minimumScaleFactor = 0.5
        instance.backgroundColor = rgb(red: 247, green: 247, blue: 247,ALPHA: 1)
        instance.font = UIFont(name: "Poppins", size: 16)
        return instance
       }()
    private var NationalPlateCountry_ImageView:UIImageView = {
        let instance = UIImageView()
        instance.image = UIImage(named: "LebenonImage")
        return instance
    }()
        //truck plate number
    private var TruckPlateNoText_Label:UILabel = {
        let instance = UILabel()
        instance.text = "Truck Plate No.".localized()
        instance.textColor = .black
        instance.font = UIFont(name: "Poppins-Bold", size: 13)
        return instance
       }()
    private lazy var TruckPlateNo_Label:SpecialLabel = {
        let instance = SpecialLabel()
        instance.layer.masksToBounds = true
        instance.layer.cornerRadius = 9
        instance.adjustsFontSizeToFitWidth = true
//        instance.layer.masksToBounds = true
        instance.textAlignment = .center
//        instance.adjustsFontForContentSizeCategory = true
        instance.numberOfLines = 1
//        instance.minimumScaleFactor = 0.5
        instance.backgroundColor = rgb(red: 247, green: 247, blue: 247,ALPHA: 1)
        instance.textColor = .black
        instance.font = UIFont(name: "Poppins", size: 16)
        return instance
       }()
        //trailer plate number
    private var TrailerPlateNoText_Label:UILabel = {
        let instance = UILabel()
        instance.text = "Trailer Plate No.".localized()
        instance.textColor = .black
        instance.font = UIFont(name: "Poppins-Bold", size: 13)
        return instance
       }()
    private lazy var TrailerPlateNo_Label:SpecialLabel = {
        let instance = SpecialLabel()
        instance.layer.masksToBounds = true
        instance.layer.cornerRadius = 9
        instance.adjustsFontSizeToFitWidth = true
        instance.numberOfLines = 1
        instance.textAlignment = .center
        instance.backgroundColor = rgb(red: 247, green: 247, blue: 247,ALPHA: 1)
        instance.textColor = .black
        instance.font = UIFont(name: "Poppins", size: 16)
        return instance
       }()
        //Capacity plate number
    private var CapacityText_Label:UILabel = {
        let instance = UILabel()
        instance.text = "Capacity".localized()
        instance.textColor = .black
        instance.font = UIFont(name: "Poppins-Bold", size: 13)
        return instance
       }()
    private var Capacity_Label:UILabel = {
        let instance = UILabel()
        instance.adjustsFontSizeToFitWidth = true
        instance.numberOfLines = 1
        instance.textAlignment = .center
        instance.textColor = .black
        instance.font = UIFont(name: "Poppins", size: 15)
        return instance
       }()
    private var KG_Label:UILabel = {
        let instance = UILabel()
        instance.text = "Kg".localized()
        instance.textColor = .black
        instance.font = UIFont(name: "Poppins", size: 10)
        return instance
       }()
        //Type plate number
    private var TypeText_Label:UILabel = {
        let instance = UILabel()
        instance.text = "Type".localized()
        instance.textColor = .black
        instance.font = UIFont(name: "Poppins-Bold", size: 13)
        return instance
       }()
    private var Type_Label:UILabel = {
        let instance = UILabel()
        instance.adjustsFontSizeToFitWidth = true
        instance.numberOfLines = 1
        instance.textAlignment = .center
        instance.textColor = .black
        instance.font = UIFont(name: "Poppins", size: 15)
        return instance
       }()
        //Color plate number
    private var ColorText_Label:UILabel = {
        let instance = UILabel()
        instance.text = "Color".localized()
        instance.textColor = .black
        instance.font = UIFont(name: "Poppins-Bold", size: 13)
        return instance
       }()
    private var Color_Label:UILabel = {
        let instance = UILabel()
        instance.adjustsFontSizeToFitWidth = true
        instance.numberOfLines = 1
        instance.textAlignment = .center
        instance.textColor = .black
        instance.font = UIFont(name: "Poppins", size: 15)
        return instance
       }()
    private var Email_Label:UILabel = {
        let instance = UILabel()
        instance.text = "Email".localized()
        instance.font = UIFont(name: "Poppins-Bold", size: 13)
        instance.textColor = .black
        return instance
    }()
    private var EmailText_Label:UILabel = {
        let instance = UILabel()
        instance.font = UIFont(name: "Poppins", size: 13)
        instance.textColor = .black
        return instance
    }()
    
    private lazy var AddEmail_Button:UIButton = {
        let instance = UIButton()
        instance.setTitle("Add Email Address".localized(), for: .normal)
        instance.titleLabel?.font = UIFont(name: "Poppins-Bold", size: 15)
        instance.setTitleColor(.white, for: .normal)
        instance.layer.cornerRadius = 9
        instance.backgroundColor = .App_DarkBlue
        return instance
    }()
    
    private var Phone_Label:UILabel = {
        let instance = UILabel()
        instance.text = "Phone".localized()
        instance.font = UIFont(name: "Poppins-Bold", size: 13)
        instance.textColor = .black
        return instance
    }()
    private var PhoneText_Label:UILabel = {
        let instance = UILabel()
        instance.font = UIFont(name: "Poppins", size: 13)
        instance.textColor = .black
        return instance
    }()
    
    private lazy var AddPhone_Button:UIButton = {
        let instance = UIButton()
        instance.setTitle("Add Phone Number".localized(), for: .normal)
        instance.titleLabel?.font = UIFont(name: "Poppins-Bold", size: 15)
        instance.setTitleColor(.white, for: .normal)
        instance.layer.cornerRadius = 9
        instance.backgroundColor = .App_DarkBlue
        return instance
    }()
    
    private var CompanyNameText_Label:UILabel = {
        let instance = UILabel()
        instance.font = UIFont(name: "Poppins", size: 13)
        instance.textColor = .black
        return instance
    }()
    private var CompanyName_Label:UILabel = {
        let instance = UILabel()
        instance.text = "Company Name".localized()
        instance.font = UIFont(name: "Poppins-Bold", size: 13)
        instance.textColor = .black
        return instance
    }()
          //Company Detail
    private var CompanyDetailText_Label:UILabel = {
        let instance = UILabel()
        instance.text = "Company Details".localized()
        instance.font = UIFont(name: "Poppins-Bold", size: 13)
        instance.textColor = .black
        return instance
    }()
    private var CompanyDetail_TextView:UITextView = {
        let instance = UITextView()
        instance.isEditable = false
        instance.textContainer.maximumNumberOfLines = 3
        instance.textAlignment = .left
        instance.font = UIFont(name: "Poppins", size: 13)
        return instance
        }()
          //CompanyLogoOrImage
    private var CompanyLogoOrImageText_Label:UILabel = {
        let instance = UILabel()
        instance.text = "Company Logo or Picture".localized()
        instance.font = UIFont(name: "Poppins-Bold", size: 13)
        instance.textColor = .black
        return instance
    }()
    private var CompanyLogoOrImage_ImageView:UIImageView = {
        let instance = UIImageView()
        instance.layer.masksToBounds = true
        //CompanyLogoOrImage_ImageView.contentMode = .scaleAspectFit
        instance.layer.cornerRadius = 9
        if #available(iOS 13.0, *) {
            instance.layer.shadowColor = CGColor.init(srgbRed: 0, green: 0, blue: 0, alpha: 0.0186844)
        } else {
            // Fallback on earlier versions
        }
        instance.layer.shadowOpacity = 4
        return instance
    }()
    
    //Change Language
    private var LanguageLabel:UILabel = {
        let instance = UILabel()
        instance.text = "Language".localized()
        instance.textColor = .black
        instance.font = UIFont(name: "Poppins-Bold", size: 13)
        return instance
    }()
    private var ChangeLanguageButton:UIButton = {
        let instance = UIButton()
        instance.backgroundColor = UIColor.white
        instance.layer.cornerRadius = 9
        return instance
    }()
    private var LanguageImage:UIImageView = {
        let instance = UIImageView()
        return instance
    }()
    private var DownImage:UIImageView = {
        let instance = UIImageView()
        instance.image = UIImage(named: "DownIc")
        return instance
    }()
    
    private lazy var CHatWithAdminbutton:UIButton = {
        let instance = UIButton()
        instance.backgroundColor = .App_DarkBlue
        instance.layer.cornerRadius = 9
        instance.setTitle( "   " + "Chat With Admin".localized(), for: .normal)
        instance.titleLabel?.font = UIFont(name: "Poppins-Bold", size: 15)
        instance.setImage(UIImage(named: "AdminIc"), for: .normal)
        instance.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 20)
        return instance
    }()
    
    
        //Containers
    private lazy var CurrentLocation_Container:UIView = {
          let instance = UIView()
          instance.backgroundColor = .white
          instance.layer.cornerRadius = 9
          return instance
      }()
    private lazy var Truck_Container:UIView = {
       let instance = UIView()
        instance.backgroundColor = .white
        instance.layer.cornerRadius = 9
        return instance
    }()
    private lazy var NationalPlate_Container:UIView = {
       let instance = UIView()
        instance.layer.cornerRadius = 9
        instance.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        instance.backgroundColor = .white
        return instance
    }()
    private lazy var TruckPlateNo_Container:UIView = {
       let instance = UIView()
        instance.backgroundColor = .white
        return instance
    }()
    private lazy var TrailerPlateNo_Container:UIView = {
       let instance = UIView()
        instance.layer.cornerRadius = 9
        instance.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
        instance.backgroundColor = .white
        return instance
    }()
    private lazy var Capacity_Container:UIView = {
       let instance = UIView()
        instance.layer.cornerRadius = 9
        instance.layer.maskedCorners = [.layerMinXMinYCorner, .layerMinXMaxYCorner]
        instance.backgroundColor = .white
        return instance
    }()
    private lazy var Type_Container:UIView = {
       let instance = UIView()
        instance.backgroundColor = .white
        return instance
    }()
    private lazy var Color_Container:UIView = {
       let instance = UIView()
        instance.layer.cornerRadius = 9
        instance.layer.maskedCorners = [.layerMaxXMinYCorner,.layerMaxXMaxYCorner]
        instance.backgroundColor = .white
        return instance
    }()
    
    private lazy var Email_Container:UIView = {
       let instance = UIView()
        instance.layer.cornerRadius = 9
        instance.backgroundColor = .white
        return instance
    }()
    private lazy var Phone_Container:UIView = {
       let instance = UIView()
        instance.layer.cornerRadius = 9
        instance.backgroundColor = .white
        return instance
    }()
    
    private lazy var CompanyName_Container:UIView = {
       let instance = UIView()
        instance.layer.cornerRadius = 9
        instance.backgroundColor = .white
        return instance
    }()
    private lazy var CompanyDetail_Container:UIView = {
       let instance = UIView()
        instance.layer.cornerRadius = 9
        instance.backgroundColor = .white
        return instance
    }()
    private lazy var CompanyLogoOrImage_Container:UIView = {
       let instance = UIView()
        instance.layer.cornerRadius = 9
        instance.backgroundColor = .white
        return instance
    }()
    
    private lazy var MainViewSize = CGSize(width: self.frame.width, height: self.frame.height + 700)
    
    private lazy var ScrollView: UIScrollView = {
        let ScrollViewLambda = UIScrollView()
        ScrollViewLambda.backgroundColor = rgb(red: 247, green: 247, blue: 247,ALPHA: 1)
        ScrollViewLambda.contentSize = MainViewSize
        self.frame = self.bounds
        ScrollViewLambda.autoresizingMask = .flexibleHeight
        ScrollViewLambda.showsHorizontalScrollIndicator = true
        ScrollViewLambda.showsVerticalScrollIndicator = true
        ScrollViewLambda.bounces = true
        return ScrollViewLambda
    }()
    
    // MARK: - Network
    
    var currentLocation: CLLocation!
    var LocationManager:CLLocationManager = {
        let instance = CLLocationManager()
        instance.desiredAccuracy = kCLLocationAccuracyBest
//        instance.requestWhenInUseAuthorization()
        return instance
    }()
    var refreshControl = UIRefreshControl()
    
    private func AddRefreshControl(){
        refreshControl.attributedTitle = NSAttributedString(string: "Pull To Refresh".localized())
        refreshControl.addTarget(self, action: #selector(refresh(_:)), for: .valueChanged)
        ScrollView.addSubview(refreshControl)
    }
    
    @objc func refresh(_ sender: AnyObject) {
        NavigationDelegate?.RefreshSignal(For:0)
        refreshControl.endRefreshing()
    }
    
    private func GetLocationParam() -> [String:Any]? {
        if (CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedWhenInUse ||
            CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedAlways){
            guard let currentLocation = LocationManager.location else {
                return nil
            }
            return ["latitude":currentLocation.coordinate.latitude, "longitude":currentLocation.coordinate.longitude]
        }
        else {
//            LocationManager.requestWhenInUseAuthorization()
            NavigationDelegate?.PerformSegueInCell(withIdentifier: "From_Profile_To_LocationAccessGetter_Segue")
            return nil
        }
    }
    
    private func RequestToPostLocation(Param:[String:Any]){
        UserConnectionManager.RequestWithHeader(URL: DOMAIN_URL + Update_Location, RequestMethod: .post, Parameter: Param, { (HTTPStatus, RecievedJSON) in
            print("Updating Location Succedd with code \(HTTPStatus)")
            self.Driver = DriverModel(RecievedJSON!["driver"])
            self.UpdateLocationView(CityName: self.Driver?.driver_location?.city?.name ?? "", CountryName: self.Driver?.driver_location?.country?.name ?? "")
        }) { (HTTPStatus, ErrorArray) in
            print("Updating Location Failed with code \(HTTPStatus)")
            self.NavigationDelegate?.ShowAlert(Message:ErrorArray?[0].body ?? "Check Your Connection")
        }
    }
    
    private func UpdateLocationView(CityName:String,CountryName:String){
        CurrentLocationInfo_Label.text = CityName + "-" + CountryName
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error)
    {
        print("Error \(error)")
    }
    
    // MARK: - Function
    
    private var Driver:DriverModel?
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        ScrollView.endEditing(true)
    }
    
    override init(frame: CGRect) {
        super.init(frame:frame)
        LocationManager.delegate = self
        self.backgroundColor = .App_ViewController_Background_MilkyWhite
        AddRefreshControl()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func AddActions(){
        UpdateLocation_Button.addTarget(self, action: #selector(OnUpdateLocationClicked(_:)), for: .touchUpInside)
        ChangeLanguageButton.addTarget(self, action: #selector(OnChangeLanguageButtonClicked(_:)), for: .touchUpInside)
        CHatWithAdminbutton.addTarget(self, action: #selector(OnChatWithAdminButtonClicked(_:)), for: .touchUpInside)
    }
    @objc func OnUpdateLocationClicked(_ sender: UIButton){
        guard let param:[String:Any] = GetLocationParam() else {
            return
        }
        RequestToPostLocation(Param: param)
    }
    
    @objc func OnChangeLanguageButtonClicked(_ sender: UIButton){
        self.NavigationDelegate?.PerformSegueInCell(withIdentifier: "From_Profile_To_LanguagePicker_Segue")
    }
    
    @objc func OnChatWithAdminButtonClicked(_ sender: UIButton){
        self.NavigationDelegate?.PerformSegueInCell(withIdentifier: "From_Profile_To_Chat_Segue")
    }
    
    func OnCreateLanguagePickerButton(){
        let LangManager = LanguageManager()
        LangManager.SetLanguageViewProperites(Lang_Image: LanguageImage)
    }
    
    func SetDriverModel(Driver:DriverModel?){
        self.Driver = Driver
    }
    
    func UpdateViewData(){
        CurrentLocationInfo_Label.text = (Driver?.driver_location?.country?.name ?? "") + "-" + (Driver?.driver_location?.city?.name ?? "")
        TruckModelDescription_Label.text = (Driver?.truck?.model ?? "")
        
        NationalPlateDescription_Label.text = Driver?.truck?.national_plate?.data ?? ""
        NationalPlateCountry_ImageView.LoadImageFromServer(url: try! (PURE_DOMAIN_URL + (Driver?.truck?.national_plate?.country_plate?.country?.flag_image?.file ?? "")).asURL())
        
        TruckPlateNo_Label.text = Driver?.truck?.truck_plate_data ?? ""
        
        TrailerPlateNo_Label.text = Driver?.truck?.truck_plate_data ?? ""
        
        Capacity_Label.text =  "\(Driver?.truck?.capacity ?? 0)"
        Type_Label.text = Driver?.truck?.type?.title
        Color_Label.text = Driver?.truck?.color
        
        EmailText_Label.text = Driver?.primary_email?.address
        PhoneText_Label.text = Driver?.primary_phone?.number

        CompanyNameText_Label.text = Driver?.company_name
        CompanyDetail_TextView.text = Driver?.company_description
        CompanyLogoOrImage_ImageView.LoadImageFromServer(url: try! (PURE_DOMAIN_URL + (Driver?.company_logo?.file ?? "")).asURL())
    }
    
    
    // MARK: - OnCreate Views
    
    func OnCreateScrollView(){
        addSubview(ScrollView)
            ScrollView.snp.makeConstraints{ (make) in
                make.width.equalToSuperview()
                make.height.equalToSuperview()
            }// end ScrollView.snp.makeConstraints
    }//end OnCreateScrollView
    
    func OnCreateContainers(){
        ScrollView.addSubview(CurrentLocation_Container)
        CurrentLocation_Container.addSubview(CurrentLocation_Label)
        CurrentLocation_Container.addSubview(CurrentLocationInfo_Label)
        
        CurrentLocation_Container.snp.makeConstraints { (make) in
            make.top.equalTo(ScrollView).offset(23)
            MatchwidthWithDevices(MatchedView:CurrentLocation_Container)
//            make.leading.lessThanOrEqualTo(self).offset(23)
//            make.trailing.lessThanOrEqualTo(self).offset(-23)
            make.centerX.equalToSuperview()
            make.height.equalTo(51)
        }
        CurrentLocation_Label.snp.makeConstraints { (make) in
            make.centerY.equalToSuperview()
            make.leading.equalToSuperview().offset(15)
            make.trailing.lessThanOrEqualToSuperview()
        }
        CurrentLocationInfo_Label.snp.makeConstraints { (make) in
            make.centerY.equalToSuperview()
            make.leading.greaterThanOrEqualTo(CurrentLocation_Label.snp.trailing).offset(20)
            make.trailing.equalToSuperview().offset(-15)
        }
        
        
        ScrollView.addSubview(UpdateLocation_Button)
        UpdateLocation_Button.addSubview(UpdateLocation_Label)
        UpdateLocation_Button.addSubview(UpdateLocation_ImageView)
        
        UpdateLocation_Button.snp.makeConstraints { (make) in
            make.top.equalTo(CurrentLocation_Container.snp.bottom).offset(15)
            MatchwidthWithDevices(MatchedView:UpdateLocation_Button)
            make.centerX.equalToSuperview()
            make.height.equalTo(51)
        }
        UpdateLocation_Label.snp.makeConstraints { (make) in
            make.center.equalToSuperview()
        }
        UpdateLocation_ImageView.snp.makeConstraints { (make) in
            make.centerY.equalToSuperview()
            make.right.equalTo(UpdateLocation_Label.snp.left).offset(-9)
        }
        
       //truck container
        ScrollView.addSubview(Truck_Container)
        Truck_Container.addSubview(TruckModelText_Label)
        Truck_Container.addSubview(TruckModelDescription_Label)
        
        (Truck_Container).snp.makeConstraints{ (make) in
            make.height.equalTo(51)
            make.top.equalTo(UpdateLocation_Button.snp.bottom).offset(15)
            make.centerX.equalTo(ScrollView)
            MatchwidthWithDevices(MatchedView: Truck_Container)
        }
        TruckModelText_Label.snp.makeConstraints{ (make) in
            make.left.equalTo(Truck_Container).offset(15)
            make.centerY.equalTo(Truck_Container)
        }
        TruckModelDescription_Label.snp.makeConstraints{ (make) in
            make.right.equalTo(Truck_Container).offset(-15)
            make.centerY.equalTo(Truck_Container)
        }
        
        //national plate container
        ScrollView.addSubview(NationalPlate_Container)
        NationalPlate_Container.addSubview(NationalPlateText_Label)
        NationalPlate_Container.addSubview(NationalPlateDescription_Label)
//        NationalPlate_Container.addSubview(NationalPlateDescription_Container)
//        NationalPlateDescription_Container.addSubview(NationalPlateDescription_Label)
//        NationalPlateDescription_Container.addSubview(NationalPlateCountry_ImageView)
        
        (NationalPlate_Container).snp.makeConstraints{ (make) in
            make.height.equalTo(51)
            make.top.equalTo(Truck_Container.snp.bottom).offset(15)
            make.centerX.equalTo(ScrollView)
            MatchwidthWithDevices(MatchedView: NationalPlate_Container)
        }
        NationalPlateText_Label.snp.makeConstraints{ (make) in
            make.left.equalTo(NationalPlate_Container).offset(15)
            make.centerY.equalTo(NationalPlate_Container)
        }
//        NationalPlateDescription_Container.snp.makeConstraints{ (make) in
//            make.right.equalTo(NationalPlate_Container).offset(-9)
//            make.centerY.equalTo(NationalPlate_Container)
//            make.width.equalTo(119)
//            make.height.equalTo(32)
//        }
//        NationalPlateCountry_ImageView.snp.makeConstraints{ (make) in
//            make.width.equalTo(20)
//            make.height.equalTo(20)
//            make.left.equalTo(NationalPlateDescription_Container).offset(6)
//            make.centerY.equalTo(NationalPlateDescription_Container)
//        }
//        NationalPlateDescription_Label.snp.makeConstraints{ (make) in
////            make.trailing.equalTo(NationalPlateDescription_Container).offset(-6)
//            make.leading.equalToSuperview().offset(5)
//            make.trailing.equalToSuperview().offset(-5)
//            make.center.equalToSuperview()
//        }
        NationalPlateDescription_Label.snp.makeConstraints{ (make) in
            make.right.equalTo(NationalPlate_Container).offset(-9)
            make.centerY.equalTo(NationalPlate_Container)
            make.width.equalTo(113)
            make.height.equalTo(32)
        }
        
        
        //truck plate number
        ScrollView.addSubview(TruckPlateNo_Container)
        TruckPlateNo_Container.addSubview(TruckPlateNoText_Label)
        TruckPlateNo_Container.addSubview(TruckPlateNo_Label)
        
        (TruckPlateNo_Container).snp.makeConstraints{ (make) in
            make.height.equalTo(51)
            make.top.equalTo(NationalPlate_Container.snp.bottom).offset(1)
            make.centerX.equalTo(ScrollView)
            MatchwidthWithDevices(MatchedView: TruckPlateNo_Container)
        }
        TruckPlateNoText_Label.snp.makeConstraints{ (make) in
            make.left.equalTo(TruckPlateNo_Container).offset(15)
            make.centerY.equalTo(TruckPlateNo_Container)
        }
        TruckPlateNo_Label.snp.makeConstraints{ (make) in
            make.right.equalTo(TruckPlateNo_Container).offset(-9)
            make.centerY.equalTo(TruckPlateNo_Container)
            make.width.equalTo(113)
            make.height.equalTo(32)
        }
        
        //trailer plate number
        ScrollView.addSubview(TrailerPlateNo_Container)
        TrailerPlateNo_Container.addSubview(TrailerPlateNoText_Label)
        TrailerPlateNo_Container.addSubview(TrailerPlateNo_Label)
        
        (TrailerPlateNo_Container).snp.makeConstraints{ (make) in
            make.height.equalTo(51)
            make.top.equalTo(TruckPlateNo_Container.snp.bottom).offset(1)
            make.centerX.equalTo(ScrollView)
            MatchwidthWithDevices(MatchedView: TrailerPlateNo_Container)
        }
        TrailerPlateNoText_Label.snp.makeConstraints{ (make) in
            make.left.equalTo(TrailerPlateNo_Container).offset(15)
            make.centerY.equalTo(TrailerPlateNo_Container)
        }
        TrailerPlateNo_Label.snp.makeConstraints{ (make) in
            make.right.equalTo(TrailerPlateNo_Container).offset(-9)
            make.centerY.equalTo(TrailerPlateNo_Container)
            make.width.equalTo(113)
            make.height.equalTo(32)
        }
        
        //type container
        ScrollView.addSubview(Type_Container)
        Type_Container.addSubview(TypeText_Label)
        Type_Container.addSubview(Type_Label)
        Type_Container.addSubview(KG_Label)

        Type_Container.snp.makeConstraints{ (make) in
            make.top.equalTo(TrailerPlateNo_Container.snp.bottom).offset(15)
            make.centerX.equalTo(ScrollView)
            make.width.equalTo(109)
            make.height.equalTo(63)
        }
        TypeText_Label.snp.makeConstraints{ (make) in
            make.top.equalTo(Type_Container).offset(10)
            make.centerX.equalTo(Type_Container)
        }
        Type_Label.snp.makeConstraints{ (make) in
            make.top.equalTo(TypeText_Label.snp.bottom).offset(2.5)
            make.centerX.equalTo(Type_Container)
        }
        
        //Capacity container
        ScrollView.addSubview(Capacity_Container)
        Capacity_Container.addSubview(CapacityText_Label)
        Capacity_Container.addSubview(Capacity_Label)
        Capacity_Container.addSubview(KG_Label)

        Capacity_Container.snp.makeConstraints{ (make) in
            make.top.equalTo(TrailerPlateNo_Container.snp.bottom).offset(15)
            make.right.equalTo(Type_Container.snp.left).offset(-1)
            //make.width.equalTo(109)
            if UIDevice.current.userInterfaceIdiom == .phone {
                    make.left.equalTo(self).offset(23)
            }
            else if UIDevice.current.userInterfaceIdiom == .pad{
                    make.left.equalTo(self).offset(70)
            }
            make.height.equalTo(63)
            
        }
        CapacityText_Label.snp.makeConstraints{ (make) in
            make.top.equalTo(Capacity_Container).offset(10)
            make.centerX.equalTo(Capacity_Container)
        }
        Capacity_Label.snp.makeConstraints{ (make) in
            make.top.equalTo(CapacityText_Label.snp.bottom).offset(2.5)
            make.centerX.equalTo(Capacity_Container)
        }
        KG_Label.snp.makeConstraints{ (make) in
            make.top.equalTo(TypeText_Label.snp.bottom).offset(6.5)
            make.left.equalTo(Capacity_Label.snp.right).offset(2)
        }
        
        //Color container
        ScrollView.addSubview(Color_Container)
        Color_Container.addSubview(ColorText_Label)
        Color_Container.addSubview(Color_Label)

        Color_Container.snp.makeConstraints{ (make) in
            make.top.equalTo(TrailerPlateNo_Container.snp.bottom).offset(15)
            make.left.equalTo(Type_Container.snp.right).offset(1)
            if UIDevice.current.userInterfaceIdiom == .phone {
                    make.right.equalTo(self).offset(-23)
            }
            else if UIDevice.current.userInterfaceIdiom == .pad{
                    make.right.equalTo(self).offset(-70)
            }
            make.height.equalTo(63)
        }
        ColorText_Label.snp.makeConstraints{ (make) in
            make.top.equalTo(Color_Container).offset(10)
            make.centerX.equalTo(Color_Container)
        }
        Color_Label.snp.makeConstraints{ (make) in
            make.top.equalTo(ColorText_Label.snp.bottom).offset(2.5)
            make.centerX.equalTo(Color_Container)
        }
        
        if Driver?.primary_email?.address != nil {
               ScrollView.addSubview(Email_Container)
               Email_Container.addSubview(Email_Label)
               Email_Container.addSubview(EmailText_Label)
               
               Email_Container.snp.makeConstraints { (make) in
                   MatchwidthWithDevices(MatchedView: Email_Container)
                   make.top.equalTo(Type_Container.snp.bottom).offset(15)
                   make.height.equalTo(51)
               }
               Email_Label.snp.makeConstraints { (make) in
                   make.centerY.equalToSuperview()
                   make.left.equalToSuperview().offset(15)
               }
               EmailText_Label.snp.makeConstraints { (make) in
                   make.centerY.equalToSuperview()
                   make.right.equalToSuperview().offset(-15)
               }
           }else{
               ScrollView.addSubview(AddEmail_Button)
               AddEmail_Button.snp.makeConstraints { (make) in
                   MatchwidthWithDevices(MatchedView: AddEmail_Button)
                   make.top.equalTo(Type_Container.snp.bottom).offset(15)
                   make.height.equalTo(51)
               }
        }
        
        if Driver?.primary_phone?.number != nil{
            ScrollView.addSubview(Phone_Container)
            Phone_Container.addSubview(Phone_Label)
            Phone_Container.addSubview(PhoneText_Label)
            
            Phone_Container.snp.makeConstraints { (make) in
                MatchwidthWithDevices(MatchedView: Phone_Container)
                if ScrollView.contains(Email_Container){
                    make.top.equalTo(Email_Container.snp.bottom).offset(15)
                }else{
                    make.top.equalTo(AddEmail_Button.snp.bottom).offset(15)
                }
                make.height.equalTo(51)
            }
            Phone_Label.snp.makeConstraints { (make) in
                make.centerY.equalToSuperview()
                make.left.equalToSuperview().offset(15)
            }
            PhoneText_Label.snp.makeConstraints { (make) in
                make.centerY.equalToSuperview()
                make.right.equalToSuperview().offset(-15)
            }
        }else{
            ScrollView.addSubview(AddPhone_Button)
            AddPhone_Button.snp.makeConstraints { (make) in
                MatchwidthWithDevices(MatchedView: AddPhone_Button)
                if ScrollView.contains(Email_Container){
                    make.top.equalTo(Email_Container.snp.bottom).offset(15)
                }else{
                    make.top.equalTo(AddEmail_Button.snp.bottom).offset(15)
                }
                make.height.equalTo(51)
            }
        }
        
        
        
        
        
        
        
        
        ScrollView.addSubview(CompanyName_Container)
        CompanyName_Container.addSubview(CompanyName_Label)
        CompanyName_Container.addSubview(CompanyNameText_Label)
        
        CompanyName_Container.snp.makeConstraints { (make) in
            if ScrollView.contains(Phone_Container){
                make.top.equalTo(Phone_Container.snp.bottom).offset(15)
            }else{
                make.top.equalTo(AddPhone_Button.snp.bottom).offset(15)
            }
            make.height.equalTo(51)
            MatchwidthWithDevices(MatchedView: CompanyName_Container)
            make.centerX.equalToSuperview()
        }
        CompanyName_Label.snp.makeConstraints { (make) in
            make.centerY.equalToSuperview()
            make.left.equalToSuperview().offset(15)
        }
        CompanyNameText_Label.snp.makeConstraints { (make) in
            make.centerY.equalToSuperview()
            make.right.equalToSuperview().offset(-15)
        }
        
        ScrollView.addSubview(CompanyDetail_Container)
        CompanyDetail_Container.addSubview(CompanyDetailText_Label)
        CompanyDetail_Container.addSubview(CompanyDetail_TextView)
        
        CompanyDetail_Container.snp.makeConstraints { (make) in
            make.top.equalTo(CompanyName_Container.snp.bottom).offset(15)
            make.height.equalTo(123)
            MatchwidthWithDevices(MatchedView: CompanyDetail_Container)
            make.centerX.equalToSuperview()
        }
        CompanyDetailText_Label.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(16)
            make.left.equalToSuperview().offset(18)
        }
        CompanyDetail_TextView.snp.makeConstraints { (make) in
            make.height.equalTo(70)
            make.right.equalTo(CompanyDetail_Container).offset(-18)
            make.top.equalTo(CompanyDetailText_Label.snp.bottom).offset(11)
            make.left.equalTo(CompanyDetail_Container).offset(18)
        }
        
        ScrollView.addSubview(CompanyLogoOrImage_Container)
        CompanyLogoOrImage_Container.addSubview(CompanyLogoOrImageText_Label)
        CompanyLogoOrImage_Container.addSubview(CompanyLogoOrImage_ImageView)
        
        CompanyLogoOrImage_Container.snp.makeConstraints { (make) in
            make.top.equalTo(CompanyDetail_Container.snp.bottom).offset(15)
            make.height.equalTo(207)
            MatchwidthWithDevices(MatchedView: CompanyLogoOrImage_Container)
            make.centerX.equalToSuperview()
        }
        CompanyLogoOrImageText_Label.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(16)
            make.left.equalToSuperview().offset(18)
        }
        CompanyLogoOrImage_ImageView.snp.makeConstraints { (make) in
            make.height.equalTo(142)
            make.right.equalTo(CompanyLogoOrImage_Container).offset(-18)
            make.top.equalTo(CompanyLogoOrImageText_Label.snp.bottom).offset(11)
            make.left.equalTo(CompanyLogoOrImage_Container).offset(18)
        }
        
        //Change Language
        ScrollView.addSubview(ChangeLanguageButton)
        ChangeLanguageButton.addSubview(LanguageLabel)
        ChangeLanguageButton.addSubview(DownImage)
        ChangeLanguageButton.addSubview(LanguageImage)
        ChangeLanguageButton.snp.makeConstraints { (make) in
            make.centerX.equalTo(ScrollView)
            make.top.equalTo(CompanyLogoOrImage_Container.snp.bottom).offset(15)
            make.height.equalTo(51)
            MatchwidthWithDevices(MatchedView: ChangeLanguageButton )
        }
        LanguageLabel.snp.makeConstraints { (make) in
            make.centerY.equalTo(ChangeLanguageButton)
            make.left.equalTo(ChangeLanguageButton.snp.left).offset(15)
            make.height.equalTo(18)
        }
        DownImage.snp.makeConstraints { (make) in
            make.centerY.equalTo(ChangeLanguageButton)
            make.right.equalTo(ChangeLanguageButton.snp.right).offset(-11.67)
            make.height.equalTo(8.36)
            make.width.equalTo(15.33)
        }
        LanguageImage.snp.makeConstraints { (make) in
            
            make.centerY.equalTo(ChangeLanguageButton)
            make.right.equalTo(DownImage.snp.left).offset(-9)
            make.width.equalTo(31)
            make.height.equalTo(31)
        }
        
        ScrollView.addSubview(CHatWithAdminbutton)
        CHatWithAdminbutton.snp.makeConstraints{ (make) in
            make.top.equalTo(ChangeLanguageButton.snp.bottom).offset(15)
            make.centerX.equalTo(ScrollView)
            MatchwidthWithDevices(MatchedView: CHatWithAdminbutton)
            make.height.equalTo(51)
        }
        ScrollView.contentSize = CGSize(width: self.frame.width, height: self.frame.height + 900)
    }
    
    
    private func OnCreateEmail(IsEmailAvailable: Bool){
        
        if IsEmailAvailable {
            ScrollView.addSubview(Email_Container)
            Email_Container.addSubview(Email_Label)
            Email_Container.addSubview(EmailText_Label)
            
            Email_Container.snp.makeConstraints { (make) in
                MatchwidthWithDevices(MatchedView: Email_Container)
                make.top.equalTo(Type_Container.snp.bottom).offset(15)
                make.height.equalTo(51)
            }
            Email_Label.snp.makeConstraints { (make) in
                make.centerY.equalToSuperview()
                make.left.equalToSuperview().offset(15)
            }
            EmailText_Label.snp.makeConstraints { (make) in
                make.centerY.equalToSuperview()
                make.right.equalToSuperview().offset(-15)
            }
        }else{
            ScrollView.addSubview(AddEmail_Button)
            AddEmail_Button.snp.makeConstraints { (make) in
                MatchwidthWithDevices(MatchedView: AddEmail_Button)
                make.top.equalTo(Type_Container.snp.bottom).offset(15)
                make.height.equalTo(51)
            }
        }
        
    }
    private func OnCreatePhone(IsPhoneAvailable: Bool){
        
        if IsPhoneAvailable {
            ScrollView.addSubview(Phone_Container)
            Phone_Container.addSubview(Phone_Label)
            Phone_Container.addSubview(PhoneText_Label)
            
            Phone_Container.snp.makeConstraints { (make) in
                MatchwidthWithDevices(MatchedView: Phone_Container)
                if ScrollView.contains(Email_Container){
                    make.top.equalTo(Email_Container.snp.bottom).offset(15)
                }else{
                    make.top.equalTo(AddEmail_Button.snp.bottom).offset(15)
                }
                make.height.equalTo(51)
            }
            Phone_Label.snp.makeConstraints { (make) in
                make.centerY.equalToSuperview()
                make.left.equalToSuperview().offset(15)
            }
            PhoneText_Label.snp.makeConstraints { (make) in
                make.centerY.equalToSuperview()
                make.right.equalToSuperview().offset(-15)
            }
        }else{
            ScrollView.addSubview(AddPhone_Button)
            AddPhone_Button.snp.makeConstraints { (make) in
                MatchwidthWithDevices(MatchedView: AddPhone_Button)
                if ScrollView.contains(Email_Container){
                    make.top.equalTo(Email_Container.snp.bottom).offset(15)
                }else{
                    make.top.equalTo(AddEmail_Button.snp.bottom).offset(15)
                }
                make.height.equalTo(51)
            }
        }
        
    }
    
    
    func MatchwidthWithDevices( MatchedView: UIView) {
        if UIDevice.current.userInterfaceIdiom == .phone {
            MatchedView.snp.makeConstraints{ (make) in
                make.left.equalTo(self).offset(23)
                make.right.equalTo(self).offset(-23)
            }//end make
        }
        else if UIDevice.current.userInterfaceIdiom == .pad{
            MatchedView.snp.makeConstraints{ (make) in
                make.left.equalTo(self).offset(70)
                make.right.equalTo(self).offset(-70)
            }//end make
        }
        
    }
    
    func rgb(red: CGFloat, green: CGFloat, blue: CGFloat,ALPHA: CGFloat) -> UIColor {
         return UIColor(red: red/255, green: green/255, blue: blue/255, alpha: ALPHA)
       }

}
