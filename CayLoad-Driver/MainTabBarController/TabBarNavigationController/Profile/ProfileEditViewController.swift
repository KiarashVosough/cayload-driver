//
//  ProfileEditViewController.swift
//  CayLoad-Driver
//
//  Created by Kiyarash Vosough on 1/10/1399 AP.
//  Copyright © 1399 CayLoad. All rights reserved.
//

import UIKit

class ProfileEditViewController: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate ,UITextFieldDelegate{
   
    //header outlet
    @IBOutlet weak var Back_Button: UIButton!
    @IBOutlet weak var Profile_ImageView: UIImageView!
    @IBOutlet weak var Username_Label: UILabel!
    @IBOutlet weak var DriverId_Label: UILabel!
    @IBOutlet weak var ChangeProfileImage_Button: UIButton!
    
    //main scroll view outlet
    @IBOutlet weak var Main_ScrolView: UIScrollView!
    @IBOutlet weak var FirstName_TextField: UITextField!
    @IBOutlet weak var LastName_TextField: UITextField!
    @IBOutlet weak var CompanyName_TextField: UITextField!
    @IBOutlet weak var CompanyDetails_TextField: UITextField!
    @IBOutlet weak var CompanyLogo_ImageView: UIImageView!
    @IBOutlet weak var ChooseImage_Button: UIButton!
    @IBOutlet weak var SaveChange_Butoon: UIButton!
    
    
    var ImagePicker = UIImagePickerController()
    var IsProfileImageEditing:Bool = false
    var IsCompanyLogoEditing:Bool = false
    var IsProfileImageEdited:Bool = false
    var IsCompanyLogoEdited:Bool = false
    
    // MARK: - KeyBoardManager
        
    @objc func adjustForKeyboard(notification: Notification) {
        guard let keyboardValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue else { return }

        let keyboardScreenEndFrame = keyboardValue.cgRectValue
        let keyboardViewEndFrame = view.convert(keyboardScreenEndFrame, from: view.window)

        if notification.name == UIResponder.keyboardWillHideNotification {
            Main_ScrolView.contentInset = .zero
        } else {
            Main_ScrolView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardViewEndFrame.height - view.safeAreaInsets.bottom + 20, right: 0)
        }

        Main_ScrolView.scrollIndicatorInsets = Main_ScrolView.contentInset
    }
    
    func textFieldShouldReturn(_ Sender: UITextField) -> Bool
    {
        Sender.resignFirstResponder()
        return true
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        Main_ScrolView.endEditing(true)
    }
    
    
    // MARK: - Localization
    
    private func Localize(){
        FirstName_TextField.placeholder = "Enter a text".localized()
        LastName_TextField.placeholder = "Enter a text".localized()
        CompanyName_TextField.placeholder = "Enter a text".localized()
        CompanyDetails_TextField.placeholder = "Enter a text".localized()
    }
    
    // MARK: - Func
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        Localize()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        AddDelegate()
        RequestToGetProfile()
        AddTarget()
        
    }
    
    private func AddDelegate(){
        ImagePicker.delegate = self
        FirstName_TextField.delegate = self
        LastName_TextField.delegate = self
        CompanyName_TextField.delegate = self
        CompanyDetails_TextField.delegate = self
    }
    
    private func AddTarget(){
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(adjustForKeyboard), name: UIResponder.keyboardWillHideNotification, object: nil)
        notificationCenter.addObserver(self, selector: #selector(adjustForKeyboard), name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
        
        //buttons
        Back_Button.addTarget(self, action: #selector(OnBackButtonClicked(_:)), for: .touchUpInside)
        SaveChange_Butoon.addTarget(self, action: #selector(OnSaveChangesButtonClicked(_:)), for: .touchUpInside)
        ChangeProfileImage_Button.addTarget(self, action: #selector(OnChangeProfileImageClicked), for: .touchUpInside)
        ChooseImage_Button.addTarget(self, action: #selector(OnChooseImageForCompanyLogoClicked), for: .touchUpInside)
    }

    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let pickedImage = info[.editedImage] as? UIImage {
            if self.IsCompanyLogoEditing {
                CompanyLogo_ImageView.image = pickedImage
                IsCompanyLogoEditing = false
                IsCompanyLogoEdited = true
            }
            else if self.IsProfileImageEditing {
                Profile_ImageView.image = pickedImage
                IsProfileImageEditing = false
                IsProfileImageEdited = true
            }
            
        }
        dismiss(animated: true, completion: nil)
    }
    
    private func UpdateViews(){
        Username_Label.text = (Driver?.user?.first_name ?? "") + " " + (Driver?.user?.last_name ?? "")
        Profile_ImageView.LoadImageFromServer(url: try! (PURE_DOMAIN_URL + (Driver?.profile_image?.file ?? "")).asURL())
        DriverId_Label.text = ("Driver ID : ".localized()) + "\(Driver?.unique_id ?? 0)"
        CompanyLogo_ImageView.LoadImageFromServer(url: try! (PURE_DOMAIN_URL + (Driver?.company_logo?.file ?? "")).asURL())
    }
    
    // MARK: - Network
    
    var ShouldUpload:Int = 1
    var UploadCount:Int? {
        didSet{
            OnUploadCompletion(count: UploadCount)
        }
    }
    
    private func OnUploadCompletion(count:Int?){
        if count != nil {
            if count == ShouldUpload {
                navigationController?.popToRootViewController(animated: true)
            }
        }
    }
    
    private func RequestToUploadImage(URL:String,ImageData:Data,ImageName:String){
        UserConnectionManager.UploadImage(Address: URL, ImageData: ImageData, ImageName: "image", { (HTTPStatus, RecievedJSON) in
            print("Uploading Imaged Successed Failed With Code \(HTTPStatus)")
            self.UploadCount = (self.UploadCount ?? 0) + 1
        }) { (HTTPStatus, ErrorArray) in
            print("Uploading Imaged info Failed With Code \(HTTPStatus)")
            self.SetButtonToWatingState(Button: self.SaveChange_Butoon, ButtonState: .Enabled, ButtonText: "Save Changes")
            self.ShowAlert(Message: ErrorArray?[0].body ?? "Check Your Connection", Seconds: 2)
        }
    }
    
    private func RequestToUpdateProfile(){
        UserConnectionManager.RequestWithHeader(URL: DOMAIN_URL + Edit_Profile_URL, RequestMethod: .post, Parameter: GetUpdatedParam(), { (HTTPStatus, RecievedJson) in
            print("Editing Profile successed with code \(HTTPStatus)")
            self.UploadCount = (self.UploadCount ?? 0) + 1
        }) { (HTTPStatus, ErrorArray) in
            print("Editing Profile failed with code \(HTTPStatus)")
            self.SetButtonToWatingState(Button: self.SaveChange_Butoon, ButtonState: .Enabled, ButtonText: "Save Changes")
            self.ShowAlert(Message: ErrorArray?[0].body ?? "Check Your Connection", Seconds: 2)
        }
    }
    
    var Driver:DriverModel?
    
    private func RequestToGetProfile(){
        UserConnectionManager.RequestWithHeader(URL: (DOMAIN_URL + Profile_URL), RequestMethod: .get, Parameter: nil, { (HTTPStatus, RecievedJson) in
            print("Getting Profile successed with code \(HTTPStatus)")
            self.Driver = DriverModel(RecievedJson!["driver"])
            self.UpdateViews()
        }) { (HTTPStatus, ErrorArray) in
            print("Getting Profile failed with code \(HTTPStatus)")
            self.ShowAlert(Message: ErrorArray?[0].body ?? "Check Your Connection", Seconds: 2)
        }
    }
    
    private func GetUpdatedParam() -> [String:Any]{
        var user = [String:Any]()
        var driver = [String:Any]()
        
        if FirstName_TextField.text != "" {
            user.updateValue(FirstName_TextField.text ?? "", forKey: "first_name")
        }else {
            user.updateValue(Driver?.user?.first_name ?? "", forKey: "first_name")
        }
        
        if LastName_TextField.text != "" {
            user.updateValue(LastName_TextField.text ?? "", forKey: "last_name")
        }else {
            user.updateValue(Driver?.user?.last_name ?? "", forKey: "last_name")
        }
        
        driver.updateValue(user, forKey: "user")
        
        driver.updateValue(Driver?.primary_email?.address ?? "", forKey: "primary_email")
        driver.updateValue(Driver?.primary_phone?.number ?? "", forKey: "primary_phone")
        
        if CompanyName_TextField.text != "" {
            driver.updateValue(CompanyName_TextField.text ?? "", forKey: "company_name")
        }else {
            driver.updateValue(Driver?.company_name ?? "", forKey: "company_name")
        }
        
        if CompanyDetails_TextField.text != "" {
            driver.updateValue(CompanyDetails_TextField.text ?? "", forKey: "company_description")
        }else {
            driver.updateValue(Driver?.company_description ?? "", forKey: "company_description")
        }
        
        driver.updateValue(["":""], forKey: "additional_emails")
        driver.updateValue(["":""], forKey: "additional_phones")
        
        return driver
    }
    
    // MARK: - Navigation
    
    @objc func OnBackButtonClicked(_ sender: UIButton){
        _ = navigationController?.popToRootViewController(animated: true)
    }
    @objc func OnSaveChangesButtonClicked(_ sender: UIButton){

        if self.IsProfileImageEdited {
            guard let Profile_ImageData:Data = self.Profile_ImageView.image?.jpegData(compressionQuality: 0.5) else {
                self.ShowAlert(Message: "There Is Somthing Wrong With Your Profile Picture", Seconds: 2)
              return
            }
            ShouldUpload+=1
            RequestToUploadImage(URL: DOMAIN_URL + Set_Profile_Image_URL, ImageData: Profile_ImageData, ImageName: "")
        }

        
        if self.IsCompanyLogoEdited {
            guard let CompanyLogo_ImageData:Data = self.CompanyLogo_ImageView.image?.jpegData(compressionQuality: 0.5) else {
                self.ShowAlert(Message: "There Is Somthing Wrong With Your Company Logo Picture", Seconds: 2)
              return
            }
            ShouldUpload+=1
            RequestToUploadImage(URL: DOMAIN_URL + Set_Company_Image_URL, ImageData: CompanyLogo_ImageData, ImageName: "")
        }
        
        RequestToUpdateProfile()
    }
    
    @objc func OnChangeProfileImageClicked(_ sender: UIButton) {
        IsProfileImageEditing = true
           if UIImagePickerController.isSourceTypeAvailable(.photoLibrary){
               ImagePicker.sourceType = .savedPhotosAlbum
               ImagePicker.allowsEditing = true
               present(ImagePicker, animated: true, completion: nil)
           }
       }
       
       
       @objc func OnChooseImageForCompanyLogoClicked(_ sender: UIButton) {
        IsCompanyLogoEditing = true
           if UIImagePickerController.isSourceTypeAvailable(.photoLibrary){
               ImagePicker.sourceType = .savedPhotosAlbum
               ImagePicker.allowsEditing = true
               present(ImagePicker, animated: true, completion: nil)
           }
       }
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    

}
