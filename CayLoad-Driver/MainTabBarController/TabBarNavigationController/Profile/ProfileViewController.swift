//
//  ProfileViewController.swift
//  CayLoad-Driver
//
//  Created by Kiyarash Vosough on 1/9/1399 AP.
//  Copyright © 1399 CayLoad. All rights reserved.
//

import UIKit
import SwiftyJSON

class ProfileViewController: UIViewController,ProfilenavigationDelegate {
    
    @IBOutlet weak var Logout_Button: UIButton!
    @IBOutlet weak var Header_Container: UIView!
    @IBOutlet weak var EditProfile_Button: UIButton!
    @IBOutlet weak var Profile_ImageView: UIImageView!
    @IBOutlet weak var Profile_Label: UILabel!
    @IBOutlet weak var DriverID_Label: UILabel!
    @IBOutlet weak var UserName_Label: UILabel!
    @IBOutlet weak var ProfileTabLine_View: UIView!
    @IBOutlet weak var Plan_Label: UILabel!
    @IBOutlet weak var PlanTabLine_View: UIView!
    
    lazy var PagesCollectionView: UICollectionView = {
        let CollectionViewFlowLayout = UICollectionViewFlowLayout()
        let PCV = UICollectionView(frame: CGRect(x: 0, y: Header_Container.frame.height, width: (view.frame.width) * 3, height: 0) , collectionViewLayout: CollectionViewFlowLayout)
        CollectionViewFlowLayout.scrollDirection = .horizontal
        PCV.showsVerticalScrollIndicator = false
        PCV.showsHorizontalScrollIndicator = false
        PCV.backgroundColor = rgb(red: 247, green: 247, blue: 247,ALPHA: 1)
        PCV.isScrollEnabled = true
        PCV.translatesAutoresizingMaskIntoConstraints = false
        return PCV
    }()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        RequestToGetProfile()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UIView.appearance().semanticContentAttribute = .forceLeftToRight
        AddDelegate()
        AddTarget()
        OnCreateCollectionView()
    }
    
    private func AddTarget(){
        PagesCollectionView.register(ProfileTabCollectionViewCell.self, forCellWithReuseIdentifier: "ProfileTabCollectionViewCell")
        PagesCollectionView.register(PlanCollectionViewCell.self, forCellWithReuseIdentifier: "PlanCollectionViewCell")
        
        EditProfile_Button.addTarget(self, action: #selector(OnEditProfileButtonTapped(_:)), for: .touchUpInside)
        Logout_Button.addTarget(self, action: #selector(OnLogOutButtonTapped), for: .touchUpInside)
        
        Plan_Label.isUserInteractionEnabled = true
        Plan_Label.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(OnPlanTabClicked(_:))))
        
        Profile_Label.isUserInteractionEnabled = true
        Profile_Label.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(OnProfileTabClicked(_:))))
    }
    
    private func AddDelegate(){
        PagesCollectionView.delegate = self
        PagesCollectionView.dataSource = self
    }
    
    func OnCreateCollectionView() {
        view.addSubview(PagesCollectionView)
        if view.contains(PagesCollectionView) {
            PagesCollectionView.snp.makeConstraints{ (make) in
                make.width.equalToSuperview()
                make.top.equalTo(Header_Container.snp.bottom)
                make.bottom.equalTo(view)
                make.width.equalTo(view)
            }// end make  PagesCollectionView.snp.makeConstraints
        }//end if view.contains(PagesCollectionView)
    }//end Oncreate Collection view
    
    func rgb(red: CGFloat, green: CGFloat, blue: CGFloat,ALPHA: CGFloat) -> UIColor {
      return UIColor(red: red/255, green: green/255, blue: blue/255, alpha: ALPHA)
    }
    
    private func UpdateViews(){
        UserName_Label.text = (Driver?.user?.first_name ?? "") + " " + (Driver?.user?.last_name ?? "")
        Profile_ImageView.LoadImageFromServer(url: try! (PURE_DOMAIN_URL + (Driver?.profile_image?.file ?? "")).asURL())
        DriverID_Label.text = ("Driver ID : ".localized()) + "\(Driver?.unique_id ?? 0)"
    }
    
    private func ChangeTabHandler(Index:Int){
        if Index == 0 {
            Profile_Label.textColor = .white
            ProfileTabLine_View.backgroundColor = .white
            Plan_Label.textColor.withAlphaComponent(0.6)
            PlanTabLine_View.backgroundColor = .blue
            PagesCollectionView.reloadData()
        }else{
            Plan_Label.textColor = .white
            PlanTabLine_View.backgroundColor = .white
            Profile_Label.textColor.withAlphaComponent(0.6)
            ProfileTabLine_View.backgroundColor = .blue
            PagesCollectionView.reloadData()
        }
    }
    
    // MARK: - Network
    
    var PlansArray = [PlanModel]()
    var Driver:DriverModel?
    var HTTP_Status:Int = 0
    
    private func RequestToGetPlans(){
        let URL:String = (DOMAIN_URL + User_URL + String(UserDefaults.standard.integer(forKey: "user_id")) + Get_Plans_URL)
        UserConnectionManager.RequestWithHeader(URL: URL, RequestMethod: .get, Parameter: nil, { (HTTPStatus, RecievedJson) in
            print("Getting Plans successed with code \(HTTPStatus)")
            self.PlansArray = JsonParsingUtils.GetArrayList(From: RecievedJson! , withRootTag: "users")
            self.HTTP_Status = HTTPStatus
        }) { (HTTPStatus, ErrorArray) in
            self.HTTP_Status = HTTPStatus
            print("Getting Plans failed with code \(HTTPStatus)")
            self.ShowAlert(Message: ErrorArray?[0].body ?? "Check Your Connection", Seconds: 2)
        }
    }
    
    private func RequestToGetProfile(){
        UserConnectionManager.RequestWithHeader(URL: (DOMAIN_URL + Profile_URL), RequestMethod: .get, Parameter: nil, { (HTTPStatus, RecievedJson) in
            print("Getting Profile successed with code \(HTTPStatus)")
            self.Driver = DriverModel(RecievedJson!["driver"])
            self.UpdateViews()
            self.PagesCollectionView.reloadItems(at: [IndexPath(row: 0, section: 0)])
            self.HTTP_Status = HTTPStatus
        }) { (HTTPStatus, ErrorArray) in
            self.HTTP_Status = HTTPStatus
            print("Getting Profile failed with code \(HTTPStatus)")
            self.ShowAlert(Message: ErrorArray?[0].body ?? "Check Your Connection".localized(), Seconds: 2)
        }
    }
    
    private func RequestToSetUserLanguage(){
        let User_id = UserDefaults.standard.integer(forKey: "user_id")
        UserConnectionManager.RequestWithHeader(URL: "\(DOMAIN_URL)\(User_URL)\(User_id)\(Set_User_Language_URL)", RequestMethod: .post, Parameter: ["language":imLanguageManager.GetNotationFrom()], { (HTTPStatus, RecievedJson) in
            print("Setting Language Successed with code \(HTTPStatus)")
            self.SetPreferedLangAlert(Lang: self.imLanguageManager.GetNotationFrom())
        }) { (HTTPStatus, ErrorArray) in
            print("Setting Language failed with code \(HTTPStatus)")
            self.ShowAlert(Message: ErrorArray?[0].body ?? "Check Your Connection".localized(), Seconds: 2)
        }
    }

    // MARK: - Navigation
    
    var SelectedLanguageTuple:(Language:String,ImageId:String)?
    private var imLanguageManager:LanguageManager!
    
    func RefreshSignal(For:Int) {
        if HTTP_Status != 0 {
            DispatchQueue.global(qos: .userInitiated).async {
                let group = DispatchGroup()
                group.enter()
                if For == 0{
                    self.RequestToGetProfile()
                }
                group.leave()
            }
        }
    }
    
    func PerformSegueToLanguagePicker() {
        performSegue(withIdentifier: "From_Profile_To_LanguagePicker_Segue", sender: self)
    }
    
    func PerformSegueInCell(withIdentifier: String) {
        performSegue(withIdentifier: withIdentifier , sender: self)
    }
    
    func PerformSegueForBuyPlan(withIdentifier: String) {
        performSegue(withIdentifier: withIdentifier , sender: self)
    }
    
    func ShowAlert(Message: String) {
        self.ShowAlert(Message: Message, Seconds: 3)
    }
    
    @objc func OnEditProfileButtonTapped(_ sender: UIButton){
        performSegue(withIdentifier: "ProfileEditSegue", sender: self)
    }
    
    @objc func OnLogOutButtonTapped(_ sender: UIButton){
        UserDefaults.standard.set(0, forKey: "user_id")
        UserDefaults.standard.set(false, forKey: "Is_Login")
        UserDefaults.standard.set("", forKey: "VALID_TOKEN")
        performSegue(withIdentifier: "Unwind_From_Profile_To_StartNavigation", sender: self)
    }
    
    @IBAction func OnProfileTabClicked(_ sender: UITapGestureRecognizer) {
        PagesCollectionView.scrollToItem(at: IndexPath(item: 0, section: 0), at: .left, animated: true)
        ChangeTabHandler(Index: 0)
    }
    
    @IBAction func OnPlanTabClicked(_ sender: UITapGestureRecognizer) {
        PagesCollectionView.scrollToItem(at: IndexPath(item: 1, section: 0), at: .right, animated: true)
        ChangeTabHandler(Index: 1)
    }
    
    @IBAction func UnwindFrom_LanguagePickerViewControlller_To_ProfileViewControlller(segue:UIStoryboardSegue) {
        
        if let sourceViewController = segue.source as? LanguagePickerViewController {
            let Lang = sourceViewController.getSelectedLanguage().Language
            imLanguageManager = LanguageManager(UserSelectedLanguage_CompeleteForm:Lang)
            if LanguageManager.GetCompleteFormFrom(Notaion:imLanguageManager.GetLanguage_Default()) != Lang {
                let not = imLanguageManager.GetNotationFrom()
                if not == "en" || not == "fa" || not == "ar" || not == "ru"{
                    RequestToSetUserLanguage()
                    
                }else {
                    DispatchQueue.main.async {
                        self.ShowAlert(Message: "Feature not available".localized(), Seconds: 2)
                    }
                }
            }
//            PagesCollectionView.reloadItems(at: [IndexPath(row: 0, section: 0)])
        }
    }
    
    @IBAction func UnwindFrom_LocationAccessGetter_To_ProfileViewController(segue: UIStoryboardSegue){
//       if segue.source is LocationAccessGetterViewController {
//
//        }
    }
    
    @IBAction func UnwindFrom_ChatViewController_To_ProfileController(segue: UIStoryboardSegue){
        /*if let source = segue.source as? ChatViewController {
        }*/
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "ProfileEditSegue"){
            if segue.destination is ProfileEditViewController{
                
            }
        }
        else if segue.identifier == "From_Profile_To_LanguagePicker_Segue" {
            if let LanguagePickerInstance = segue.destination as? LanguagePickerViewController{
                LanguagePickerInstance.setUnwind(withIdentifier: "Unwind_From_LanguagePicker_To_Profile_Segue")
            }
        }
        else if segue.identifier == "From_Profile_To_LocationAccessGetter_Segue" {
            if let instance = segue.destination as? LocationAccessGetterViewController{
                instance.Unwind_Identifier = "Unwind_From_LocationAccessGetter_To_Profile_Segue"
            }
        }else if segue.identifier == "From_Profile_To_Chat_Segue" {
            if let instance = segue.destination as? ChatViewController {
                instance.Unwind_Identifier = "Unwind_From_Chat_To_Profile_Segue"
                instance.With = .Admin
                instance.Username = "Admin".localized()
                instance.ThisUserId = Driver?.id ?? 0
            }
        }
        
    }


}

extension ProfileViewController:  UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        2
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let pageWidth = scrollView.frame.size.width
        let page = Int(floor((scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1)
        if page == 0 {
           ChangeTabHandler(Index: 0)
        }
        else if page == 1{
           ChangeTabHandler(Index: 1)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.row == 0 {
            let cell0 = PagesCollectionView.dequeueReusableCell(withReuseIdentifier: "ProfileTabCollectionViewCell", for: indexPath) as! ProfileTabCollectionViewCell
            cell0.NavigationDelegate = self
            cell0.OnCreateLanguagePickerButton()
            cell0.SetDriverModel(Driver: Driver)
            cell0.OnCreateScrollView()
            cell0.OnCreateContainers()
            cell0.AddActions()
            cell0.UpdateViewData()
            return cell0
        }
        if indexPath.row == 1 {
            let cell1 = PagesCollectionView.dequeueReusableCell(withReuseIdentifier: "PlanCollectionViewCell", for: indexPath) as! PlanCollectionViewCell
            cell1.NavigationDelegate = self
            cell1.RequestToGetProfile()
            cell1.RequestToGetPlans()
            return cell1
        }

        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.PagesCollectionView.frame.width , height: view.frame.height - Header_Container.frame.height )
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
            return 4
        }
        
        func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
            return 4
        }
        
        func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        }
//    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
//        if indexPath.row == 0 {
//            print("ProfileTabCollectionViewCell")
//            let y = cell as! ProfileTabCollectionViewCell
//        }
//        if indexPath.row == 1 {
//            print("PlanCollectionViewCell")
//        }
//    }
    
}
