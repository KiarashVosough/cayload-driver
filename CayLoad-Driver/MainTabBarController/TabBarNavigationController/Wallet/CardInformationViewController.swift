//
//  CardInformationViewController.swift
//  CayLoad-Driver
//
//  Created by Kiyarash Vosough on 1/16/1399 AP.
//  Copyright © 1399 CayLoad. All rights reserved.
//

import UIKit

class CardInformationViewController: UIViewController {
    
    
    @IBOutlet weak var Main_ScrollView: UIScrollView!
    @IBOutlet weak var Back_Button: UIButton!
    @IBOutlet weak var Visa_Button: UIButton!
    @IBOutlet weak var MasterCard_Button: UIButton!
    @IBOutlet weak var CardNumber_TextField: UITextField!
    @IBOutlet weak var Owner_TextField: UITextField!
    @IBOutlet weak var SaveCardInfo_Button: UIButton!
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        CardNumber_TextField.resignFirstResponder()
        Owner_TextField.resignFirstResponder()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Main_ScrollView.keyboardDismissMode = .onDrag
        AddBorderToCardTypeButton()
        AddTarget()
        
    }
    
    private func AddBorderToCardTypeButton(){
        Visa_Button.BorderColor = rgb(red: 36, green: 53, blue: 152, ALPHA: 1)
        Visa_Button.BorderWidth = 2
        MasterCard_Button.BorderColor = rgb(red: 36, green: 53, blue: 152, ALPHA: 1)
    }

    private func AddTarget(){
        Back_Button.addTarget(self, action: #selector(OnBackButtonClicked(_:)), for: .touchUpInside)
        Visa_Button.addTarget(self, action: #selector(OnVisaButtonClicked), for: .touchUpInside)
        MasterCard_Button.addTarget(self, action: #selector(OnMasterCardButtonClicked), for: .touchUpInside)
    }
    
    @objc func OnBackButtonClicked(_ sender: UIButton){
        _ = navigationController?.popToRootViewController(animated: true)
    }
    
    @objc func OnVisaButtonClicked(_ sender: UIButton){
        if sender.BorderWidth == 0 {
            sender.BorderWidth = 2
            MasterCard_Button.BorderWidth = 0
        }
    }
    
    @objc func OnMasterCardButtonClicked(_ sender: UIButton){
        if sender.BorderWidth == 0 {
            sender.BorderWidth = 2
            Visa_Button.BorderWidth = 0
        }
    }
    
    
    
    func rgb(red: CGFloat, green: CGFloat, blue: CGFloat,ALPHA: CGFloat) -> UIColor {
      return UIColor(red: red/255, green: green/255, blue: blue/255, alpha: ALPHA)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
