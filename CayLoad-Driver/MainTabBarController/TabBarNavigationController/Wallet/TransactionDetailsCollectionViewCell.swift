//
//  TransactionDetailsCollectionViewCell.swift
//  CayLoad-Driver
//
//  Created by Kiyarash Vosough on 2/30/1399 AP.
//  Copyright © 1399 CayLoad. All rights reserved.
//

import UIKit

class TransactionDetailsCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var Date_View: UIView!
    @IBOutlet weak var TransactionNum_Label: UILabel!
    @IBOutlet weak var Date_Label: UILabel!
    @IBOutlet weak var TransactionAmount_Label: UILabel!
    
    @IBOutlet weak var Description_View: UIView!
    @IBOutlet weak var DescriptionData_Label: UILabel!
    
    @IBOutlet weak var AccountFinancial_View: UIView!
    @IBOutlet weak var CreditData_Label: UILabel!
    @IBOutlet weak var DebitData_Label: UILabel!
    @IBOutlet weak var BalanceData_Label: UILabel!
    
    func UpdateViews(Transaction: TransactionModel?){
        TransactionNum_Label.text = "\(Transaction?.id ?? 0)"
        TransactionAmount_Label.text = "\(Transaction?.amount ?? 0)"
        Date_Label.text = Transaction?.date_created ?? ""
        DescriptionData_Label.text = Transaction?.description ?? ""
    }
    func SetLayer(){
        self.layer.cornerRadius = 9
    }
}
