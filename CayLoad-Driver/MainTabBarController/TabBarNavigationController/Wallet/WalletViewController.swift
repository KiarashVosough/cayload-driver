//
//  WalletViewController.swift
//  CayLoad-Driver
//
//  Created by Kiyarash Vosough on 1/9/1399 AP.
//  Copyright © 1399 CayLoad. All rights reserved.
//

import UIKit

class WalletViewController: UIViewController {

    @IBOutlet weak var Header_View: UIView!
    @IBOutlet weak var Balance_Label: UILabel!
    @IBOutlet weak var Withdraw_Buttton: UIButton!
    @IBOutlet weak var Deposit_Button: UIButton!
    @IBOutlet weak var Card_Information_Button: UIButton!
    
    @IBOutlet weak var Transaction_CollectionView: UICollectionView!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        GetTransactions()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        AddAction()
    }
    
    private func AddAction(){
        Transaction_CollectionView.dataSource = self
        Transaction_CollectionView.delegate = self
        
        Card_Information_Button.addTarget(self, action: #selector(OnCardinfoButtonClicked(_:)), for: .touchUpInside)
        Deposit_Button.addTarget(self, action: #selector(OnDepositeButtonClicked), for: .touchUpInside)
        Withdraw_Buttton.addTarget(self, action: #selector(OnWithdrawButtonClicked), for: .touchUpInside)
    }
    
    // MARK: - Network
    
    var Transactions = [TransactionModel]()
    
    private func GetTransactions(){
        print(DOMAIN_URL + Pure_User_URL + "/\(UserDefaults.standard.integer(forKey: "user_id"))" + Get_Transactions)
        UserConnectionManager.RequestWithHeader(URL: DOMAIN_URL + Pure_User_URL + "/\(UserDefaults.standard.integer(forKey: "user_id"))" + Get_Transactions, RequestMethod: .get, Parameter: nil, { (HTTPStatus, RecievedJSON) in
            self.Transactions = JsonParsingUtils.GetArrayList(From: RecievedJSON! , withRootTag: "users")
            self.Transaction_CollectionView.reloadData()
        }) { (HTTPStatus, ErrorArray) in
            print("getting Transactions failed with code \(HTTPStatus)")
            self.ShowAlert(Message: ErrorArray?[0].title ?? "Check Your Connection", Seconds: 2)
        }
    }
    
    // MARK: - Navigation
    
    @objc func OnCardinfoButtonClicked(_ sender: UIButton){
        performSegue(withIdentifier: "GoToCardInformationSegue", sender: self)
//        Bundle.setLanguage(lang: "fa")
    }
    
    @objc func OnDepositeButtonClicked(_ sender: UIButton){
        performSegue(withIdentifier: "GoToPaymentSegue", sender: self)
    }
    
    @objc func OnWithdrawButtonClicked(_ sender: UIButton){
        performSegue(withIdentifier: "GoToWithdrawSegue", sender: self)
    }

}

extension WalletViewController:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return Transactions.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TransactionCell", for: indexPath) as! TransactionDetailsCollectionViewCell
        cell.SetLayer()
        cell.UpdateViews(Transaction: Transactions[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (collectionView.frame.width-46), height: 225)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
            return 15
        }
        
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 15
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 20, left: 0, bottom: 20, right: 0)
    }
    
}
