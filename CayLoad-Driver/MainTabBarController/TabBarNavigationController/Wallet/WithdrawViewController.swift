//
//  WithdrawViewController.swift
//  CayLoad-Driver
//
//  Created by Kiyarash Vosough on 1/17/1399 AP.
//  Copyright © 1399 CayLoad. All rights reserved.
//

import UIKit

class WithdrawViewController: UIViewController,UITextFieldDelegate {
    
    @IBOutlet weak var Main_ScrollView: UIScrollView!
    @IBOutlet weak var ScrollView_Content_View: UIView!
    @IBOutlet weak var Back_Button: UIButton!
    @IBOutlet weak var Amount_TextField: UITextField!
    @IBOutlet weak var Tax_Label: UILabel!
    @IBOutlet weak var TaxInfo_Label: UILabel!
    @IBOutlet weak var Total_Info_Label: UILabel!
    @IBOutlet weak var SendWithdrawRequest_Button: UIButton!
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        Amount_TextField.resignFirstResponder()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Main_ScrollView.keyboardDismissMode = .onDrag
        AddTarget()
        Tax_Label.text = "Tax".localized() + "(2%)"
        // Do any additional setup after loading the view.
    }
    
    private func AddTarget(){
        Back_Button.addTarget(self, action: #selector(OnBackButtonClicked(_:)), for: .touchUpInside)
    }
    
    @objc func OnBackButtonClicked(_ sender: UIButton){
        _ = navigationController?.popToRootViewController(animated: true)
    }
    

    func rgb(red: CGFloat, green: CGFloat, blue: CGFloat,ALPHA: CGFloat) -> UIColor {
      return UIColor(red: red/255, green: green/255, blue: blue/255, alpha: ALPHA)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
