//
//  FirstViewController.swift
//  CayLoad-Driver
//
//  Created by Kiyarash Vosough on 1/9/1399 AP.
//  Copyright © 1399 CayLoad. All rights reserved.
//

import UIKit
import SnapKit
import CoreLocation

class AcceptedOffersViewController: UIViewController,CLLocationManagerDelegate,OfferDetailsDelegate {
    
    @IBOutlet weak var Header_View: UIView!
    @IBOutlet weak var Notification_Button: UIButton!
    @IBOutlet weak var Offers_Button: UIButton!
    @IBOutlet weak var LocationOnRecord_View: UIView!
    
    lazy var AcceptedOffers_CollectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        layout.minimumLineSpacing = 12
        let collectionview = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collectionview.register(RequestCell.self, forCellWithReuseIdentifier: "AcceptedOfferCell")
        collectionview.delegate = self
        collectionview.dataSource = self
        collectionview.backgroundColor = .clear
        return collectionview
    }()
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        GetPendingOffers()
        AddLocationonRecordView()
        CheckLocationService()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        SetPreferedLangToEn()
        view.backgroundColor = .App_ViewController_Background_MilkyWhite
        OnCreateCollectionView()
        view.bringSubviewToFront(Offers_Button)
        view.bringSubviewToFront(LocationOnRecord_View)
        Offers_Button.addTarget(self, action: #selector(OnOffersButtonClicked(_:)), for: .touchUpInside)
        AddRefreshControl()
        
    }
    
    private func AddRefreshControl(){
        refreshControl.attributedTitle = NSAttributedString(string: "Pull To Refresh".localized())
        refreshControl.addTarget(self, action: #selector(refresh(_:)), for: .valueChanged)
        AcceptedOffers_CollectionView.addSubview(refreshControl)
    }
    
    @objc func refresh(_ sender: AnyObject) {
        if HTTP_Status != 0 {
            GetPendingOffers()
        }
    }
    
    // MARK: - Network
    var refreshControl = UIRefreshControl()
    var HTTP_Status:Int = 0
    var PendingRequest = [TruckRequestModel]()
        
    private func GetPendingOffers(){
        UserConnectionManager.RequestWithHeader(URL: DOMAIN_URL + Truck_Request_URL , RequestMethod: .get, Parameter: nil, { (HTTPStatus,RecievedJson) in
            print("getting Pending Offer succeed with code \(HTTPStatus)")
            self.PendingRequest =  JsonParsingUtils.GetArrayList(From: RecievedJson! , withRootTag: "truck_requests")
            self.AcceptedOffers_CollectionView.reloadData()
            self.HTTP_Status = HTTPStatus
            self.refreshControl.endRefreshing()
        }) { (HTTPStatus, ErrorArray) in
            print("getting Pending Offer failed with code \(HTTPStatus)")
            self.HTTP_Status = HTTPStatus
            self.refreshControl.endRefreshing()
            self.ShowAlert(Message: ErrorArray?[0].body ?? "Check Your Connection", Seconds: 2)
        }
    }
    
    private func AddLocationonRecordView(){
        if UserDefaults.standard.bool(forKey: "Is_Location_OnRecord"){
            LocationOnRecord_View.isHidden = false
        }else{
            LocationOnRecord_View.isHidden = true;
        }
    }
    
    private func CheckLocationService(){
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
                case .notDetermined, .restricted, .denied, .authorizedWhenInUse:
                    print("No access")
                    performSegue(withIdentifier: "From_PendingOffer_To_LocationAccessGetter_Segue", sender: self)
            case .authorizedAlways:
                    print("Access")
                @unknown default:
                break
            }
        }
        else {
            ShowAlert(Message: "Location services are not enabled", Seconds: 2)
        }
    }
    // MARK: - Navigation
    
    var SelectedRequestID_ToShowDetail:Int = 0
    var SelectedUserID_ToShowDetail:Int = 0
    
    @objc func OnOffersButtonClicked(_ sender: UIButton){
        performSegue(withIdentifier: "GoToOffersSegue", sender: self)
    }
    
    func PerformSegueToDetails(With Request_Id: Int, User_Id: Int) {
        SelectedRequestID_ToShowDetail = Request_Id
        SelectedUserID_ToShowDetail = User_Id
        performSegue(withIdentifier: "From_HomeTab_To_AcceptedOffersDetails_Segue", sender: self)
    }
    
    @IBAction func UnwindFrom_OffersControlller_To_AcceptedOffersViewController(segue:UIStoryboardSegue) {}
    
    @IBAction func UnwindFrom_LocationAccessGetter_To_PendingOffersViewController(segue: UIStoryboardSegue){
        if let source = segue.source as? LocationAccessGetterViewController {
            if source.AccessGranted == true{
                LocationOnRecord_View.isHidden = false
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "From_HomeTab_To_AcceptedOffersDetails_Segue" {
            if let Instance = segue.destination as? AcceptedOffersDetailsViewController{
                Instance.Request_Id = SelectedRequestID_ToShowDetail
                Instance.User_Id = SelectedUserID_ToShowDetail
            }
        }
        else if segue.identifier == "From_PendingOffer_To_LocationAccessGetter_Segue" {
            if let instance = segue.destination as? LocationAccessGetterViewController{
                instance.Unwind_Identifier = "Unwind_From_LocationAccessGetter_To_PendingOffer_Segue"
            }
        }
    }
    
    private func OnCreateCollectionView(){
        view.addSubview(AcceptedOffers_CollectionView)
        AcceptedOffers_CollectionView.snp.makeConstraints { (make) in
            make.top.equalTo(Header_View.snp.bottom)
            make.leading.equalToSuperview()
            make.trailing.equalToSuperview()
            make.bottom.equalToSuperview()
        }
        view.setNeedsLayout()
    }

}

extension AcceptedOffersViewController:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return PendingRequest.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AcceptedOfferCell", for: indexPath) as! RequestCell
        cell.NavigationDelegate = self
        cell.OnCreate()
        cell.SetTruckRequestModel(TruckRequest:PendingRequest[indexPath.row])
        cell.UpdateViews()
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
            return CGSize(width: AcceptedOffers_CollectionView.frame.width-40, height: 230)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.init(top: 55, left: 0, bottom: 78, right: 0)
    }
    
    /*func GetEstimatedSize(Text:String) -> CGRect {
        let size = CGSize(width: 250, height: 1000)
        let font = UIFont(name: "Poppins-Bold", size: 15) ?? UIFont.systemFont(ofSize: 15)
        let option = NSStringDrawingOptions.usesFontLeading.union(.usesLineFragmentOrigin)
        let EstimatedFrame = NSString(string: Text).boundingRect(with: size, options: option , attributes: [NSAttributedString.Key.font: font], context: nil)
        return EstimatedFrame
    }*/
}
