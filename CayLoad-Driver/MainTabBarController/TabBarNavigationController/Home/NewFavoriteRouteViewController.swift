//
//  NewFavoriteRouteViewController.swift
//  CayLoad-Driver
//
//  Created by Kiyarash Vosough on 2/12/1399 AP.
//  Copyright © 1399 CayLoad. All rights reserved.
//

import UIKit

class NewFavoriteRouteViewController: UIViewController {
    
    @IBOutlet weak var Back_Button: UIButton!
    
    @IBOutlet weak var Origin_Label: UILabel!
    @IBOutlet weak var Origin_View: UIView!
    @IBOutlet weak var OriginHeight_LayoutConstraint: NSLayoutConstraint!
    @IBOutlet weak var Origin_Textview: UITextView!
    @IBOutlet weak var SelectOrigin_Button: UIButton!
    
    @IBOutlet weak var Destination_Label: UILabel!
    @IBOutlet weak var Destination_View: UIView!
    @IBOutlet weak var Destination_Textview: UITextView!
    @IBOutlet weak var SelectDestination_Button: UIButton!
    
    @IBOutlet weak var Submit_Button: UIButton!
    
    // MARK: - Localization
    
    private func Localize(){
        Origin_Textview.text = "Select Your Origin".localized()
        Destination_Textview.text = "Select Your Destination".localized()
    }
    
    // MARK: - Life Cycle
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        AddTarget()
        Localize()
    }
    
    private func AddTarget(){
        Back_Button.addTarget(self, action: #selector(OnBackButtonClicked), for: .touchUpInside)
        SelectOrigin_Button.addTarget(self, action: #selector(OnSelectOriginButtonClicked), for: .touchUpInside)
        SelectDestination_Button.addTarget(self, action: #selector(OnSelectDestinationClicked), for: .touchUpInside)
        Submit_Button.addTarget(self, action: #selector(OnSubmitButtonClicked), for: .touchUpInside)
    }

    // MARK: - Network
     
    var Origin_Lat:Double = -1
    var Origin_Long: Double = -1
    var Destination_Lat:Double = -1
    var Destination_Long: Double = -1
    
    private func RequestToGetLocation(TextView:UITextView,Latitude:Double,Longitude:Double){
        
        UserConnectionManager.RequestWithHeader(URL: DOMAIN_URL + Get_Location_URL, RequestMethod: .post, Parameter: ["latitude":Latitude,"longitude":Longitude], { (HTTPStatus, RecievedJson) in
            print("Getting Location succeed with code \(HTTPStatus)")
            let Location = LocationPointModel(RecievedJson!["location_point"])
            TextView.text = Location.formatted_address
        }) { (HTTPStatus, ErrorArray) in
            print("Getting Location failed with code \(HTTPStatus)")
            self.ShowAlert(Message: ErrorArray?[0].body ?? "Check Your Connection".localized(), Seconds: 2)
        }
    }
    
    private func RequestToSaveRoute(){
        UserConnectionManager.RequestWithHeader(URL: DOMAIN_URL + Add_New_Route, RequestMethod: .post, Parameter: GetNewRouteParam(),
        { (HTTPStatus, RecievedJson) in
            print("Setting Location Succeed with code \(HTTPStatus)")
            self.performSegue(withIdentifier: "Unwind_From_AddNewRoute_ToFavoriteRoutes_Segue", sender: self)
        }) { (HTTPStatus, ErrorArray) in
            print("Setting Location failed with code \(HTTPStatus)")
            self.ShowAlert(Message: ErrorArray?[0].body ?? "Check Your Connection".localized(), Seconds: 2)
        }
    }
    
    private func GetNewRouteParam() ->[String:Any] {
        return ["origin_latitude":Origin_Lat,
                "origin_longitude":Origin_Long,
                "destination_latitude":Destination_Lat,
                "destination_longitude":Destination_Long]
    }
    // MARK: - Navigation
    
    var isorigin:Bool = true
    
    @IBAction func UnwindFrom_MapViewController_To_NewFavoriteRouteViewController(segue:UIStoryboardSegue) {
    }
    
    @IBAction func UnwindFrom_SelectableMapViewController_To_NewFavoriteRouteViewController(segue:UIStoryboardSegue) {
        
        if let map = segue.source as? SelectableMapViewController {
            switch map.IsOrigin {
            case true:
                if let coor = map.GetLocation(){
                    Origin_Lat = coor.latitude
                    Origin_Long = coor.longitude
                    RequestToGetLocation(TextView: Origin_Textview, Latitude: Origin_Lat, Longitude: Origin_Long)
                }
            case false:
                if let coor = map.GetLocation(){
                    Destination_Lat = coor.latitude
                    Destination_Long = coor.longitude
                    RequestToGetLocation(TextView: Destination_Textview, Latitude: Destination_Lat, Longitude: Destination_Long)
                }
            }
        }
    }
    
    @objc func OnBackButtonClicked(){
        performSegue(withIdentifier: "Unwind_From_AddNewRoute_ToFavoriteRoutes_Segue", sender: self)
    }
    
    @objc func OnSelectOriginButtonClicked(){
        isorigin = true
        performSegue(withIdentifier: "From_AddNewRoute_To_SelectableMap_Segue", sender: self)
    }
    
    @objc func OnSelectDestinationClicked(){
        isorigin = false
        performSegue(withIdentifier: "From_AddNewRoute_To_SelectableMap_Segue", sender: self)
    }
    
    @objc func OnSubmitButtonClicked(){
        if Origin_Lat == -1 || Origin_Long == -1 || Destination_Lat == -1 || Destination_Long == -1{
            self.ShowAlert(Message: "Select Your Origin And Destination On Map".localized(), Seconds: 3)
        }
        else {
            RequestToSaveRoute()
        }
    }

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        if segue.identifier == "From_AddNewRoute_To_SelectableMap_Segue"{
            if let map = segue.destination as? SelectableMapViewController {
                switch isorigin {
                case true:
                    map.IsOrigin = true
                case false:
                    map.IsOrigin = false
                }
            }
        }
    }

}
