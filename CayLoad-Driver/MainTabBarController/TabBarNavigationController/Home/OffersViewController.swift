//
//  OffersViewController.swift
//  CayLoad-Driver
//
//  Created by Kiyarash Vosough on 2/14/1399 AP.
//  Copyright © 1399 CayLoad. All rights reserved.
//

import UIKit

class OffersViewController: UIViewController,OfferDetailsDelegate {
    
    @IBOutlet weak var Offers_Label: UILabel!
    @IBOutlet weak var Back_Button: UIButton!
    @IBOutlet weak var RouteChanger_View: UIView!
    @IBOutlet weak var From_Label: UILabel!
    @IBOutlet weak var OriginData_Label: UILabel!
    @IBOutlet weak var To_Label: UILabel!
    @IBOutlet weak var DestinationData_Label: UILabel!
    @IBOutlet weak var Buttons_StackView: UIStackView!
    @IBOutlet weak var Cancel_Button: UIButton!
    @IBOutlet weak var ChangeRoute_Button: UIButton!
    @IBOutlet weak var GreenArrow_ImageView: UIImageView!
//    @IBOutlet weak var Offers_CollectionView: UICollectionView!
    @IBOutlet weak var RouteChanger_Height: NSLayoutConstraint!
    
    lazy var Offers_CollectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        layout.minimumLineSpacing = 12
        let collectionview = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collectionview.register(RequestCell.self, forCellWithReuseIdentifier: "AcceptedOfferCell")
        collectionview.delegate = self
        collectionview.dataSource = self
        collectionview.backgroundColor = .clear
        return collectionview
    }()
    
    private lazy var SelectOriginAndDestination_Button:UIButton = {
        let instance = UIButton()
        instance.setTitle("Select Origin And Destination".localized(), for: .normal)
        instance.titleLabel?.font = UIFont(name: "Poppins-Bold", size: 13)
        instance.setTitleColor(.white, for: .normal)
        instance.layer.cornerRadius = 9
        instance.backgroundColor = .App_DarkBlue
        return instance
    }()
    private var ButtonHolder_View:UIView = {
       let instance = UIView()
        instance.backgroundColor = .white
        return instance
    }()
    
    //MARK: - Func
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if !HasFiltered {
            RequestToGetTotalOffers()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        AddTarget()
        RemoveRouteOverview()
        Offers_CollectionView.dataSource = self
        Offers_CollectionView.delegate = self
        Offers_CollectionView.register(RequestCell.self, forCellWithReuseIdentifier: "AcceptedOfferCell")
        AddRefreshControl()
        OnCreateCollectionView()
    }
    
    private func AddTarget(){
        Back_Button.addTarget(self, action: #selector(OnBackButtonClicked(_:)), for: .touchUpInside)
        ChangeRoute_Button.addTarget(self, action: #selector(OnChangeRouteButtonClicked(_:)), for: .touchUpInside)
        Cancel_Button.addTarget(self, action: #selector(OnCancelButtonClicked), for: .touchUpInside)
    }
    
    private func AddRouteOverview(){
        RouteChanger_Height.constant = 119
        ButtonHolder_View.removeFromSuperview()
    }

    private func RemoveRouteOverview(){
        
        RouteChanger_Height.constant = 75
        RouteChanger_View.addSubview(ButtonHolder_View)
        RouteChanger_View.bringSubviewToFront(ButtonHolder_View)
        ButtonHolder_View.snp.makeConstraints { (make) in
            make.top.equalToSuperview()
            make.leading.equalToSuperview()
            make.bottom.equalToSuperview()
            make.trailing.equalToSuperview()
        }
        ButtonHolder_View.addSubview(SelectOriginAndDestination_Button)
        SelectOriginAndDestination_Button.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(15)
            make.height.equalTo(40)
            make.centerX.equalToSuperview()
            make.leading.equalToSuperview().offset(23)
            make.trailing.equalToSuperview().offset(-23)
        }
        SelectOriginAndDestination_Button.addTarget(self, action: #selector(OnSelectOriginAnddestinationButtonClicked), for: .touchUpInside)
    }
    
    private func UpdateViewForFilterData(SelectedRoute:DriverFavoriteRouteModel){
        OriginData_Label.text = SelectedRoute.route?.origin_point?.formatted_address
        DestinationData_Label.text = SelectedRoute.route?.destination_point?.formatted_address
    }
    
    private func OnCreateCollectionView(){
        view.addSubview(Offers_CollectionView)
        Offers_CollectionView.snp.makeConstraints { (make) in
            make.top.equalTo(RouteChanger_View.snp.bottom)
            make.leading.equalToSuperview()
            make.trailing.equalToSuperview()
            make.bottom.equalToSuperview()
        }
        view.setNeedsLayout()
    }
    
    // MARK: - Network
    
    var Offers = [TruckRequestModel]()
    var refreshControl = UIRefreshControl()
    var HTTP_Status:Int = 0
    
    private func AddRefreshControl(){
        refreshControl.attributedTitle = NSAttributedString(string: "Pull To Refresh".localized())
        refreshControl.addTarget(self, action: #selector(refresh(_:)), for: .valueChanged)
        Offers_CollectionView.addSubview(refreshControl)
    }
    
    @objc func refresh(_ sender: AnyObject) {
        if HTTP_Status != 0 {
            if !HasFiltered {
                RequestToGetTotalOffers()
            }else{
                RequestToGetFilteredOffers(With:FavoriteRoute_Id)
            }
        }
    }
    
    private func RequestToGetTotalOffers(){
        UserConnectionManager.RequestWithHeader(URL: DOMAIN_URL + Total_Offers_URL, RequestMethod: .get, Parameter: nil,
            { (HTTPStatus, RecievedJson) in
            print("Getting Total Offers Succeed with code \(HTTPStatus)")
            self.Offers = JsonParsingUtils.GetArrayList(From: RecievedJson!["truck_requests"], withRootTag: "truck_requests")
            if self.Offers.count == 0 {
                self.ShowAlert(Message: "Nothing to show".localized(), Seconds: 3)
            }
            self.Offers_CollectionView.reloadData()
            self.HTTP_Status = HTTPStatus
            self.refreshControl.endRefreshing()
        }) { (HTTPStatus, ErrorArray) in
            print("Getting Total Offers failed with code \(HTTPStatus)")
            self.HTTP_Status = HTTPStatus
            self.refreshControl.endRefreshing()
            self.ShowAlert(Message: ErrorArray?[0].body ?? "Check Your Connection", Seconds: 2)
        }
    }
    
    private func RequestToGetFilteredOffers(With Id:Int){
        UserConnectionManager.RequestWithHeader(URL: DOMAIN_URL + Favorite_Route_URL + "\(Id)" + Get_Offer_URL, RequestMethod: .get, Parameter: nil,
            { (HTTPStatus, RecievedJson) in
            print("Getting Filtered Offers Succeed with code \(HTTPStatus)")
            self.Offers = JsonParsingUtils.GetArrayList(From: RecievedJson!, withRootTag: "driver_favorite_routes")
            if self.Offers.count == 0 {
                self.PopUpActionSheet(Message: "Nothing match with current filter".localized(), Seconds: 3)
            }
            self.HTTP_Status = HTTPStatus
            self.Offers_CollectionView.reloadData()
            self.refreshControl.endRefreshing()
        }) { (HTTPStatus, ErrorArray) in
            self.HTTP_Status = HTTPStatus
            self.refreshControl.endRefreshing()
            print("Getting Filtered Offers failed with code \(HTTPStatus)")
            self.ShowAlert(Message: ErrorArray?[0].body ?? "Check Your Connection", Seconds: 2)
        }
    }
    
    
    // MARK: - Navigation
    
    var HasFiltered:Bool = false
    var SelectedRequestID_ToShowDetail:Int = 0
    var FavoriteRoute_Id:Int = 0
    
    @IBAction func UnwindFrom_FavoriteRoutesRouteController_To_OffersControlller(segue:UIStoryboardSegue) {
        if let instance = segue.source as? FavoriteRoutesViewController{
            if instance.SelectedRoute != nil{
                HasFiltered = true
                UpdateViewForFilterData(SelectedRoute: instance.SelectedRoute!)
                AddRouteOverview()
                FavoriteRoute_Id = instance.SelectedRoute?.id ?? 0
                RequestToGetFilteredOffers(With: instance.SelectedRoute?.id ?? 0)
            }
        }
    }
    
    @IBAction func UnwindFrom_PendingOffersDetailsViewController_To_OffersControlller(segue:UIStoryboardSegue) {
        /*if segue.source is PendingOffersDetailsViewController{
            
        }*/
    }
    
    func PerformSegueToDetails(With Request_Id: Int, User_Id: Int) {
        SelectedRequestID_ToShowDetail = Request_Id
        performSegue(withIdentifier: "From_Offers_To_PendingOfferDetails_Segue", sender: self)
    }
    

    @objc func OnBackButtonClicked(_ sender:UIButton){
        performSegue(withIdentifier: "UnwindFrom_Offers_AcceptedOffers_Segue", sender: self)
    }
    @objc func OnSelectOriginAnddestinationButtonClicked(_ sender:UIButton){
        performSegue(withIdentifier: "From_Offers_To_FavoriteRoutes_Segue", sender: self)
    }
    @objc func OnChangeRouteButtonClicked(_ sender:UIButton){
        performSegue(withIdentifier: "From_Offers_To_FavoriteRoutes_Segue", sender: self)
    }
    @objc func OnCancelButtonClicked(_ sender:UIButton){
        HasFiltered = false
        RemoveRouteOverview()
        RequestToGetTotalOffers()
    }
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        if segue.identifier == "From_Offers_To_PendingOfferDetails_Segue" {
            if let Instance = segue.destination as? PendingOffersDetailsViewController{
                Instance.Request_Id = SelectedRequestID_ToShowDetail
                Instance.Unwind_Identifier = "Unwind_From_PendingDetails_To_Offers_Segue"
            }
        }
    }
    

}

extension OffersViewController:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return Offers.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AcceptedOfferCell", for: indexPath) as! RequestCell
        cell.NavigationDelegate = self
        cell.OnCreate()
        cell.SetTruckRequestModel(TruckRequest:Offers[indexPath.row])
        cell.UpdateViews()
//        cell.SetupConstantLabel(text: "Offered Price :")
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: Offers_CollectionView.frame.width-40, height: 230)
    }
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
//            return 10
//        }
//        
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
//        return 10
//    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 20, left: 0, bottom: 10, right: 0)
    }
    
}
