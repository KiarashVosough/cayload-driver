//
//  FavoriteRoutesViewController.swift
//  CayLoad-Driver
//
//  Created by Kiyarash Vosough on 2/12/1399 AP.
//  Copyright © 1399 CayLoad. All rights reserved.
//

import UIKit

class FavoriteRoutesViewController: UIViewController,FavoriteRouteDelegate {
    
    @IBOutlet weak var PageTitle_Label: UILabel!
    @IBOutlet weak var Back_Button: UIButton!
    @IBOutlet weak var AddRoute_Button: UIButton!
    @IBOutlet weak var FavoriteRoute_CollectionView: UICollectionView!
    @IBOutlet weak var Header_View: UIView!
    @IBOutlet weak var FavoriteRouteCollectionview_TopAnchor_NSLayout: NSLayoutConstraint!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        RequestToGetFavoriteRoutes()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        RegisterCollectionView()
        AddTargets()
    }
    
    private func RegisterCollectionView(){
        FavoriteRoute_CollectionView.backgroundColor = .clear
        FavoriteRoute_CollectionView.delegate = self
        FavoriteRoute_CollectionView.dataSource = self
        FavoriteRoute_CollectionView.register(RoutesCollectionViewCell.self, forCellWithReuseIdentifier: "FavoriteRouteCell")
        AddRefreshControl()
    }
    
    private func AddTargets(){
        Back_Button.addTarget(self, action: #selector(OnBackButtonClicked), for: .touchUpInside)
        AddRoute_Button.addTarget(self, action: #selector(OnAddNewRouteButtonClicked(_:)), for: .touchUpInside)
    }
    
    // MARK: - Network
    
    var Favorite_Routes = [DriverFavoriteRouteModel]()
    var HTTP_Status:Int = 0
    var refreshControl = UIRefreshControl()
    
    private func AddRefreshControl(){
        refreshControl.attributedTitle = NSAttributedString(string: "Pull To Refresh".localized())
        refreshControl.addTarget(self, action: #selector(refresh(_:)), for: .valueChanged)
        FavoriteRoute_CollectionView.addSubview(refreshControl)
    }
    
    @objc func refresh(_ sender: AnyObject) {
        if HTTP_Status != 0 {
             RequestToGetFavoriteRoutes()
        }
    }
    private func RequestToGetFavoriteRoutes(){
        UserConnectionManager.RequestWithHeader(URL: DOMAIN_URL + My_Favorite_Route_URL, RequestMethod: .get, Parameter: nil,
        { (HTTPStatus, RecievedJson) in
            print("Getting Favorite Routes suceed with code \(HTTPStatus)")
            self.Favorite_Routes = JsonParsingUtils.GetArrayList(From: RecievedJson!, withRootTag: "driver_favorite_routes")
            self.FavoriteRoute_CollectionView.reloadData()
            self.HTTP_Status = HTTPStatus
            self.refreshControl.endRefreshing()
        }) { (HTTPStatus, ErrorArray) in
            self.HTTP_Status = HTTPStatus
            self.refreshControl.endRefreshing()
            print("Getting Favorite Routes failed with code \(HTTPStatus)")
            self.ShowAlert(Message: ErrorArray?[0].body ?? "Check Your Connection", Seconds: 2)
        }
    }
    
    private func RequestToDeleteRoute(With Id:Int){
        UserConnectionManager.RequestWithHeader(URL: DOMAIN_URL + Favorite_Route_URL + "\(Id)", RequestMethod: .delete, Parameter: nil,
            { (HTTPStatus, RecievedJson) in
            print("Deleting Favorite Routes succeed with code \(HTTPStatus)")
            self.RequestToGetFavoriteRoutes()
        }) { (HTTPStatus, ErrorArray) in
            print("Deleting Favorite Routes failed with code \(HTTPStatus)")
            self.ShowAlert(Message: ErrorArray?[0].body ?? "Check Your Connection", Seconds: 2)
        }
    }
    
    
    // MARK: - Navigation
    
    var SelectedRoute:DriverFavoriteRouteModel?
    
    @IBAction func UnwindFrom_AddNewRouteController_To_FavoriteRoutesControlller(segue:UIStoryboardSegue) {
        
        /*if segue.source is NewFavoriteRouteViewController {
        }*/
    }
    
    func DeleteRoute(With id: Int) {
        if id != 0 {
            RequestToDeleteRoute(With: id)
        }
    }

    func OnSelectRouteAction(SelectedRoute: DriverFavoriteRouteModel?) {
        self.SelectedRoute = SelectedRoute
        performSegue(withIdentifier: "UnwindFrom_FavoriteRoute_To_Offers_Segue", sender: self)
    }
    
    @objc func OnBackButtonClicked(_ sender:UIButton){
        performSegue(withIdentifier: "UnwindFrom_FavoriteRoute_To_Offers_Segue", sender: self)
    }

    @objc func OnAddNewRouteButtonClicked(_ sender:UIButton){
        performSegue(withIdentifier: "From_FavoriteRoutes_To_AddNewRoute_Segue", sender: self)
    }
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    

}

extension FavoriteRoutesViewController:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return Favorite_Routes.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let FavoriteRouteCell = collectionView.dequeueReusableCell(withReuseIdentifier: "FavoriteRouteCell", for: indexPath) as! RoutesCollectionViewCell
        FavoriteRouteCell.NavigationDelegate = self
        FavoriteRouteCell.OnCreate()
        FavoriteRouteCell.SetData(Favorite_Route_Model:Favorite_Routes[indexPath.row])
        return FavoriteRouteCell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: FavoriteRoute_CollectionView.frame.width-40, height: 250)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
            return 10
        }
        
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
    }
    
}
