//
//  RoutesCollectionViewCell.swift
//  CayLoad-Driver
//
//  Created by Kiyarash Vosough on 2/12/1399 AP.
//  Copyright © 1399 CayLoad. All rights reserved.
//

import UIKit

class RoutesCollectionViewCell: UICollectionViewCell {
    
    private var Origin_Label: UILabel = {
        let instance = UILabel()
        instance.text = "Origin".localized()
        instance.font = UIFont(name: "Poppins-Bold", size: 15)
        instance.textColor = UIColor.black
        return instance
    }()
    private var Origin_Textview: UITextView = {
        let instance = UITextView()
        instance.isEditable = false
        
        instance.font = UIFont(name: "Poppins", size: 13)
        instance.textColor = UIColor.black
        return instance
    }()
    
    private lazy var Space_View: UIView = {
        let instance = UIView()
        instance.backgroundColor = .App_SeperatorMilkyWhite
        return instance
    }()
    
    private var Destination_Label: UILabel = {
           let instance = UILabel()
        instance.text = "Destination".localized()
           instance.font = UIFont(name: "Poppins-Bold", size: 15)
           instance.textColor = UIColor.black
           return instance
       }()
    private var Destination_Textview: UITextView = {
        let instance = UITextView()
        instance.isEditable = false
        instance.font = UIFont(name: "Poppins", size: 13)
        instance.textColor = UIColor.black
        return instance
    }()
    
    lazy var Select_Button: UIButton = {
        let instance = UIButton()
        instance.setTitle("Select".localized(), for: .normal)
        instance.backgroundColor = .App_LightBlueGray
        instance.setTitleColor(.App_DarkBlue, for: .normal)
        instance.titleLabel?.font = UIFont(name: "Poppins-Bold", size: 13)
        instance.layer.cornerRadius = 9
        return instance
    }()
    lazy var Delete_Button: UIButton = {
        let instance = UIButton()
        instance.setImage(UIImage(named: "TrashIC"), for: .normal)
        instance.backgroundColor = .App_LightBlueGray
        instance.layer.cornerRadius = 9
        return instance
    }()
    lazy var Button_Stackview: UIStackView = {
        let instance = UIStackView()
        instance.axis = .horizontal
        instance.spacing = 7
        instance.alignment = .fill
        instance.distribution = .fill
        return instance
    }()
    
    var NavigationDelegate:FavoriteRouteDelegate?
    var Favorite_Route:DriverFavoriteRouteModel?
    
    // MARK: - Functions
    override init(frame: CGRect) {
        super.init(frame: frame)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func AddTarget(){
        Select_Button.addTarget(self, action: #selector(OnSelectClicked(_:)), for: .touchUpInside)
        Delete_Button.addTarget(self, action: #selector(OnDeleteClicked(_:)), for: .touchUpInside)
    }
    
    // MARK: - Network
    func SetData(Favorite_Route_Model:DriverFavoriteRouteModel?){
        Favorite_Route = Favorite_Route_Model
        Origin_Textview.text = Favorite_Route_Model?.route?.origin_point?.formatted_address
        Destination_Textview.text = Favorite_Route_Model?.route?.destination_point?.formatted_address
    }
    
    @objc func OnSelectClicked(_ sender:UIButton){
        sender.BorderColor = .App_DarkBlue
        sender.BorderWidth = 1
        NavigationDelegate?.OnSelectRouteAction(SelectedRoute: Favorite_Route)
    }
    
    @objc func OnDeleteClicked(_ sender:UIButton){
        if Favorite_Route?.id != nil {
            NavigationDelegate?.DeleteRoute(With: Favorite_Route?.id ?? 0)
        }
    }
    
    func OnCreate(){
        
        backgroundColor = .white
        layer.cornerRadius = 9
        
        addSubview(Origin_Label)
        Origin_Label.snp.makeConstraints { (make) in
            make.leading.equalToSuperview().offset(18)
            make.trailing.lessThanOrEqualToSuperview()
            make.top.lessThanOrEqualToSuperview().offset(15)
            make.height.equalTo(20)
        }
        
        addSubview(Origin_Textview)
        Origin_Textview.snp.makeConstraints { (make) in
            make.leading.equalToSuperview().offset(18)
            make.trailing.greaterThanOrEqualToSuperview().offset(-18)
            make.top.lessThanOrEqualTo(Origin_Label.snp.bottom)
            make.height.equalTo(50)
        }
        
        addSubview(Space_View)
        Space_View.snp.makeConstraints { (make) in
            make.leading.equalToSuperview()
            make.trailing.greaterThanOrEqualToSuperview()
            make.top.lessThanOrEqualTo(Origin_Textview.snp.bottom).offset(10)
            make.height.equalTo(1)
        }
        
        addSubview(Destination_Label)
        Destination_Label.snp.makeConstraints { (make) in
            make.leading.equalToSuperview().offset(18)
            make.trailing.lessThanOrEqualToSuperview()
            make.top.lessThanOrEqualTo(Space_View.snp.bottom).offset(15)
            make.height.equalTo(20)
        }
        
        addSubview(Destination_Textview)
        Destination_Textview.snp.makeConstraints { (make) in
            make.leading.equalToSuperview().offset(18)
            make.trailing.greaterThanOrEqualToSuperview().offset(-18)
            make.top.lessThanOrEqualTo(Destination_Label.snp.bottom)
            make.height.equalTo(50)
        }
        
        addSubview(Button_Stackview)
        Button_Stackview.addArrangedSubview(Select_Button)
        Button_Stackview.addArrangedSubview(Delete_Button)
        
        Delete_Button.snp.makeConstraints { (make) in
            make.width.equalTo((self.frame.width-36)/6)
        }
        
        Button_Stackview.snp.makeConstraints { (make) in
            make.top.lessThanOrEqualTo(Destination_Textview.snp.bottom).offset(10)
            make.left.equalToSuperview().offset(18)
            make.height.equalTo(40)
            make.width.equalTo(self.frame.width-36)
        }
        
        AddTarget()
    }
    
}
