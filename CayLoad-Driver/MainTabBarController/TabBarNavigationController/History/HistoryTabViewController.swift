//
//  SecondViewController.swift
//  CayLoad-Driver
//
//  Created by Kiyarash Vosough on 1/9/1399 AP.
//  Copyright © 1399 CayLoad. All rights reserved.
//

import UIKit
import CoreData

class HistoryTabViewController: UIViewController,OfferDetailsDelegate {
    
    @IBOutlet weak var Header_View: UIView!
    @IBOutlet weak var Notification_Button: UIButton!
    
    
    lazy var AcceptedOffers_CollectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        layout.minimumLineSpacing = 12
        let collectionview = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collectionview.register(RequestCell.self, forCellWithReuseIdentifier: "AcceptedOfferCell")
        collectionview.delegate = self
        collectionview.dataSource = self
        collectionview.backgroundColor = .clear
        return collectionview
    }()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        GetRequestHistory()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        OnCreateCollectionView()
        AddRefreshControl()
    }

    
    private func OnCreateCollectionView(){
        view.addSubview(AcceptedOffers_CollectionView)
        AcceptedOffers_CollectionView.snp.makeConstraints { (make) in
            make.top.equalTo(Header_View.snp.bottom)
            make.leading.equalToSuperview()
            make.trailing.equalToSuperview()
            make.bottom.equalToSuperview()
        }
        view.setNeedsLayout()
    }
    
    func rgb(red: CGFloat, green: CGFloat, blue: CGFloat,ALPHA: CGFloat) -> UIColor {
      return UIColor(red: red/255, green: green/255, blue: blue/255, alpha: ALPHA)
    }
    
    // MARK: - Network
    
    var RequestHistory = [TruckRequestModel]()
    var refreshControl = UIRefreshControl()
    var HTTP_Status:Int = 0
    
    private func AddRefreshControl(){
        refreshControl.attributedTitle = NSAttributedString(string: "Pull To Refresh".localized())
        refreshControl.addTarget(self, action: #selector(refresh(_:)), for: .valueChanged)
        AcceptedOffers_CollectionView.addSubview(refreshControl)
    }
    
    @objc func refresh(_ sender: AnyObject) {
        if HTTP_Status != 0 {
            GetRequestHistory()
        }
    }
    
    private func GetRequestHistory(){
        UserConnectionManager.RequestWithHeader(URL: DOMAIN_URL +  Pending_Offers_URL, RequestMethod: .get, Parameter: nil, { (HTTPStatus,RecievedJson) in
            print("getting Offer succeed with code \(HTTPStatus)")
            self.RequestHistory = JsonParsingUtils.GetArrayList(From: RecievedJson!, withRootTag: "truck_requests")
            self.HTTP_Status = HTTPStatus
            self.refreshControl.endRefreshing()
            self.AcceptedOffers_CollectionView.reloadData()
        }) { (HTTPStatus, ErrorArray) in
            self.HTTP_Status = HTTPStatus
            self.refreshControl.endRefreshing()
            print("getting Offer failed with code \(HTTPStatus)")
            self.ShowAlert(Message: ErrorArray?[0].body ?? "Check Your Connection", Seconds: 2)
        }
    }
    
    //MARK: - Navigation
    
    var SelectedRequestID_ToShowDetail:Int = 0
    var SelectedUserID_ToShowDetail:Int = 0
    
    func PerformSegueToDetails(With Request_Id:Int,User_Id:Int) {
        SelectedRequestID_ToShowDetail = Request_Id
        SelectedUserID_ToShowDetail = User_Id
        performSegue(withIdentifier: "From_PendingOffers_To_PendingOfferDetails_Segue", sender: self)
//        performSegue(withIdentifier: "From_HistoryTab_To_AcceptedOffersDetails_Segue", sender: self)
    }
       
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "From_PendingOffers_To_PendingOfferDetails_Segue" {
            if let Instance = segue.destination as? PendingOffersDetailsViewController{
                Instance.Request_Id = SelectedRequestID_ToShowDetail
            }
        }
    }
}


extension HistoryTabViewController:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return RequestHistory.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AcceptedOfferCell", for: indexPath) as! RequestCell
        cell.NavigationDelegate = self
        cell.OnCreate()
        cell.SetTruckRequestModel(TruckRequest:RequestHistory[indexPath.row])
        cell.UpdateViews()
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
            return CGSize(width: AcceptedOffers_CollectionView.frame.width-40, height: 230)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.init(top: 55, left: 0, bottom: 55, right: 0)
    }
    
    
}
