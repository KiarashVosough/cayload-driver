//
//  UILocalizationButton.swift
//  CayLoad-Driver
//
//  Created by Kiyarash Vosough on 2/27/1399 AP.
//  Copyright © 1399 CayLoad. All rights reserved.
//

import UIKit

final class UILocalizationButton: UIButton {

    override func awakeFromNib() {
        self.setTitle(self.titleLabel?.text?.localized(), for: .normal)
    }
}
