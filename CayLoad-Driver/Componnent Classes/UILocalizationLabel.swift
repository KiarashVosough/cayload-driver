//
//  UILocalizationLabel.swift
//  CayLoad-Driver
//
//  Created by Kiyarash Vosough on 2/27/1399 AP.
//  Copyright © 1399 CayLoad. All rights reserved.
//

import UIKit

final class UILocalizationLabel: UILabel {
    override func awakeFromNib() {
        self.text =  self.text?.localized()
    }
}
