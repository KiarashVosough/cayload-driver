//
//  DelegateProtocols.swift
//  CayLoad-Driver
//
//  Created by Kiyarash Vosough on 1/10/1399 AP.
//  Copyright © 1399 CayLoad. All rights reserved.
//

import Foundation

protocol ProfilenavigationDelegate {
    func PerformSegueInCell(withIdentifier: String)
    func PerformSegueForBuyPlan(withIdentifier: String)
    func PerformSegueToLanguagePicker()
    func ShowAlert(Message:String)
    func RefreshSignal(For:Int)
}

protocol OnRegistrationBuyingPlan {
    func PerformSegueToPaymentGateway(PlanPrice:Int,PlanDuration:String)
    func PerforSegueToTruckInformation()
}

protocol FavoriteRouteDelegate {
    func DeleteRoute(With id:Int)
    func OnSelectRouteAction(SelectedRoute:DriverFavoriteRouteModel?)
}

protocol OfferDetailsDelegate {
    func PerformSegueToDetails(With Request_Id:Int,User_Id:Int)
}
protocol CustomerProfileDelegate {
    func PerformSegueToCustomerProfile(With Id:Int)
}

protocol MessageOption {
    func PerformSegueForItem(at:IndexPath)
}
