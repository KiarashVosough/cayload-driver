//
//  ModelProtocol.swift
//  CayLoad-Driver
//
//  Created by Kiyarash Vosough on 1/28/1399 AP.
//  Copyright © 1399 CayLoad. All rights reserved.
//

import Foundation
import SwiftyJSON

protocol ModelInitializer {
    init(_ JsonData: JSON)
}
