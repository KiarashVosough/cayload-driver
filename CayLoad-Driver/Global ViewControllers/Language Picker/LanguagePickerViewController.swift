//
//  LanguagePickerViewController.swift
//  CayLoad-Driver
//
//  Created by Kiyarash Vosough on 1/19/1399 AP.
//  Copyright © 1399 CayLoad. All rights reserved.
//

import UIKit

class LanguagePickerViewController: UIViewController, UITextFieldDelegate {
    
    private var UnwindToViewControllerID:String = ""
    
    private var TapGesture:UITapGestureRecognizer!
    
    private var EditingChanged: Bool = false
    
    private let menuView = UIView()
    
    private var menuHeight = CGFloat()
    
    private var SelectedLang:String!
    
    private let AllLanguages:[String] =  ["English","Russian","Chinese","Hindi","Arabic","Persian"]
    
    private var DisplayingArrayOfLanguages:[String]!
    
    lazy var LanguagesTableView: UITableView = {
      let tbView = UITableView()
      tbView.dataSource = self
      tbView.delegate = self
      tbView.register(LanguagesCell.self, forCellReuseIdentifier: "LanguageCell")
      return tbView
    }()
      
    var Search_TextField:UITextField = {
        let search = UITextField()
        search.returnKeyType = .search
        return search
    }()
      

    lazy var BackDrop_View: UIView = {
        let bdView = UIView(frame: self.view.bounds)
        bdView.backgroundColor = UIColor.white.withAlphaComponent(0)
        return bdView
    }()


    override func viewDidLoad() {
        super.viewDidLoad()
        UIView.appearance().semanticContentAttribute = .forceLeftToRight
        //add gesture
        TapGesture = UITapGestureRecognizer(target: self, action: #selector(TapOnBackDropViewAction(_:)))
        BackDrop_View.addGestureRecognizer(TapGesture)
        
        //Add action for search
        Search_TextField.addTarget(self, action: #selector(SearchLanguage), for: .editingChanged)
        DisplayingArrayOfLanguages = AllLanguages
            
        menuHeight = CGFloat(DisplayingArrayOfLanguages.count*65 + 80)
        
        //add views
        addSearchBar()
        view.backgroundColor = .clear
        view.addSubview(BackDrop_View)
        OnCreateMenuView()
        
      }
    
    @objc func TapOnBackDropViewAction(_ sender: UITapGestureRecognizer) {
        if EditingChanged {
            Search_TextField.text = ""
            DisplayingArrayOfLanguages = AllLanguages
            self.LanguagesTableView.reloadData()
            view.endEditing(true)
            EditingChanged = false
            
        }else{
            dismiss(animated: true) {
            }
        }
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        Search_TextField.text = ""
        Search_TextField.resignFirstResponder()
        DisplayingArrayOfLanguages = AllLanguages
        self.LanguagesTableView.reloadData()
        EditingChanged = false
        return true
    }
    
    @objc func SearchLanguage(_ sender: UITextField){
        EditingChanged = true
        if sender.text != nil {
            DisplayingArrayOfLanguages.removeAll()
            for Lang in AllLanguages {
                if Lang.localizedCaseInsensitiveContains(sender.text!) {
                    DisplayingArrayOfLanguages.append(Lang)
                }
            }
            self.LanguagesTableView.reloadData()
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let char = string.cString(using: String.Encoding.utf8){
            let isBackSpace = strcmp(char, "\\b")
            if (isBackSpace == -92) {
                DisplayingArrayOfLanguages = AllLanguages
                self.LanguagesTableView.reloadData()
            }
        }
        return true
    }
      
    private func addSearchBar() {
        menuView.addSubview(Search_TextField)
        menuView.addSubview(LanguagesTableView)
          
        Search_TextField.snp.makeConstraints { (make) in
          make.top.equalToSuperview().offset(10)
          make.leading.equalToSuperview().offset(8)
          make.trailing.equalToSuperview().offset(-8)
          make.height.equalTo(60)
          Search_TextField.delegate = self
          
          let view = UIView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
          
          Search_TextField.layer.cornerRadius = 30
          Search_TextField.backgroundColor = UIColor.lightGray.withAlphaComponent(0.1)
            Search_TextField.placeholder = "Type country name".localized()
          Search_TextField.leftView = view
          Search_TextField.leftViewMode = UITextField.ViewMode.always
        }
          
        LanguagesTableView.snp.makeConstraints { (make) in
          make.top.equalTo(Search_TextField.snp.bottom).offset(10)
          make.leading.trailing.equalToSuperview()
          make.bottom.equalToSuperview()
        }
          
    }
    
    private func OnCreateMenuView(){
        view.addSubview(menuView)
        menuView.layer.cornerRadius = 30
        menuView.backgroundColor = .white
        menuView.translatesAutoresizingMaskIntoConstraints = false
        menuView.heightAnchor.constraint(equalToConstant: menuHeight).isActive = true
        menuView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        menuView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        menuView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
    }
    func getSelectedLanguage() -> (Language:String,ImageId:String) {
        switch SelectedLang {
        case "English":
            return ("English","EnglishIc")
        case "Persian":
            return ("Persian","PersianIc")
        case "Russian":
            return ("Russian","RussianIc")
        case "Chinese":
            return ("Chinese","ChineseIc")
        case "Hindi":
            return ("Hindi","HindiIc")
        case "Arabic":
            return ("Arabic","ArabicIc")
        default:
            return ("English","EnglishIc")
        }
    }
    
    
    func setUnwind(withIdentifier Identifier:String) {
        UnwindToViewControllerID = Identifier
    }

}//end class

extension LanguagePickerViewController: UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let LangCell = tableView.dequeueReusableCell(withIdentifier: "LanguageCell", for: indexPath) as? LanguagesCell
        LangCell?.ConfigCellWithInternalImage(TextName: DisplayingArrayOfLanguages[indexPath.row], ImageName: DisplayingArrayOfLanguages[indexPath.row] + "Ic")
        return LangCell!
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return DisplayingArrayOfLanguages.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 65
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        SelectedLang = DisplayingArrayOfLanguages[indexPath.row]
        performSegue(withIdentifier: UnwindToViewControllerID, sender: self)
    }
}
