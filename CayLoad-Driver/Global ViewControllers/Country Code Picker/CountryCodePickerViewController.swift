//
//  CountryCodePickerViewController.swift
//  CayLoad-Driver
//
//  Created by Kiyarash Vosough on 1/20/1399 AP.
//  Copyright © 1399 CayLoad. All rights reserved.
//

import UIKit
import SwiftyJSON

class CountryCodePickerViewController: UIViewController,UITextFieldDelegate {
    
    private var UnwindToViewControllerID:String = ""

    private var TapGesture:UITapGestureRecognizer!
    
    private let Menu_View = UIView()
    
    private var Menu_Height = CGFloat()
    
    private var SelectedCode:Int = 0
    
    private var CodeTupleArray:[(CountryName:String,CountryCode:Int)] = []
    
    private var AllCodeTupleArray:[(CountryName:String,CountryCode:Int)] = []
        
//        [("Iran",98),("Russia",7),("Afghanistan",966),("UAE",91),("India",86),("USA",1)]
    
    lazy var CodesTableView: UITableView = {
      let tbView = UITableView()
      tbView.dataSource = self
      tbView.delegate = self
      tbView.register(CountryCodeCell.self, forCellReuseIdentifier: "CodeCell")
      return tbView
    }()
      
    var Search_TextField:UITextField = {
        let search = UITextField()
        search.keyboardType = .asciiCapable
        search.returnKeyType = .search
        return search
    }()
      

    lazy var BackDrop_View: UIView = {
        let bdView = UIView(frame: self.view.bounds)
        bdView.backgroundColor = UIColor.black.withAlphaComponent(0)
        return bdView
    }()


    override func viewDidLoad() {
        super.viewDidLoad()
        UIView.appearance().semanticContentAttribute = .forceLeftToRight
        //add gesture
        TapGesture = UITapGestureRecognizer(target: self, action: #selector(TapOnBackDropViewAction(_:)))
        BackDrop_View.addGestureRecognizer(TapGesture)
        LoadCountryCode()
        //Add action for search
        Search_TextField.addTarget(self, action: #selector(SearchLanguage), for: .editingChanged)
        
        Menu_Height = CGFloat(6*65 + 80)
        
        //add views
        addSearchBar()
        view.backgroundColor = .clear
        view.addSubview(BackDrop_View)
        OnCreateMenuView()
        
      }
    
    private func LoadCountryCode() {
        guard let Codes = JsonParsingUtils.readJSONFromFile(fileName: "phone") else {
            return
        }
        guard let Country = JsonParsingUtils.readJSONFromFile(fileName: "names") else {
            return
        }
        for (index,subJson):(String, JSON) in Codes {
            AllCodeTupleArray.append((CountryName: Country[index].stringValue, CountryCode:subJson.intValue))
        }
        CodeTupleArray = AllCodeTupleArray
        CodesTableView.reloadData()
    }
    
    @objc func TapOnBackDropViewAction(_ sender: UITapGestureRecognizer) {
        dismiss(animated: true) {
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let textFieldRange = NSRange(location: 0, length: textField.text?.count ?? 0)
        if NSEqualRanges(range, textFieldRange) && string.count == 0 {
            Search_TextField.text = ""
            CodeTupleArray = AllCodeTupleArray
            self.CodesTableView.reloadData()
            Search_TextField.resignFirstResponder()
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        Search_TextField.resignFirstResponder()
        return true
    }
    
    @objc func SearchLanguage(_ sender: UITextField){
        if sender.text != nil {
            CodeTupleArray.removeAll()
            for Codes in AllCodeTupleArray {
                if Codes.CountryName.localizedCaseInsensitiveContains(sender.text!) || Codes.CountryCode == Int(sender.text!)  {
                    CodeTupleArray.append((CountryName: Codes.CountryName, CountryCode: Codes.CountryCode))
                }
            }
            self.CodesTableView.reloadData()
        }
    }
      
    private func addSearchBar() {
        Menu_View.addSubview(Search_TextField)
        Menu_View.addSubview(CodesTableView)
          
        Search_TextField.snp.makeConstraints { (make) in
          make.top.equalToSuperview().offset(10)
          make.leading.equalToSuperview().offset(8)
          make.trailing.equalToSuperview().offset(-8)
          make.height.equalTo(60)
          Search_TextField.delegate = self

          let view = UIView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))

          Search_TextField.layer.cornerRadius = 30
          Search_TextField.backgroundColor = UIColor.lightGray.withAlphaComponent(0.1)
            Search_TextField.placeholder = "Type country name or code".localized()
          Search_TextField.leftView = view
          Search_TextField.leftViewMode = UITextField.ViewMode.always
        }
          
        CodesTableView.snp.makeConstraints { (make) in
          make.top.equalTo(Search_TextField.snp.bottom).offset(10)
            make.leading.equalTo(15)
            make.trailing.equalTo(-15)
//            make.top.equalToSuperview()
            CodesTableView.layer.cornerRadius = 30
          make.bottom.equalToSuperview()
        }
          
    }
    
    private func OnCreateMenuView(){
        view.addSubview(Menu_View)
        Menu_View.layer.cornerRadius = 30
        Menu_View.backgroundColor = .white
        Menu_View.translatesAutoresizingMaskIntoConstraints = false
        Menu_View.heightAnchor.constraint(equalToConstant: Menu_Height).isActive = true
        Menu_View.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        Menu_View.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        Menu_View.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
    }
    func setUnwind(withIdentifier Identifier:String) {
        UnwindToViewControllerID = Identifier
    }
    func getSelectedCountryCode() -> Int {
        return SelectedCode
    }
}

extension CountryCodePickerViewController: UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let LangCell = tableView.dequeueReusableCell(withIdentifier: "CodeCell", for: indexPath) as? CountryCodeCell
        LangCell?.SetData(DataTuple: CodeTupleArray[indexPath.row])
        return LangCell!
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return CodeTupleArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 65
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        SelectedCode = CodeTupleArray[indexPath.row].CountryCode
        performSegue(withIdentifier: UnwindToViewControllerID, sender: self)
    }
}
