//
//  ChatViewController+Network.swift
//  CayLoad-Driver
//
//  Created by Kiyarash Vosough on 4/17/1399 AP.
//  Copyright © 1399 CayLoad. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON

extension ChatViewController {
    
    func RequestToUploadImage(ImageData:Data, _ Completion: @escaping (Int)-> Void){
        UserConnectionManager.UploadImage(Address: "https://chat.cayload.com/chat/messages/set_image/", ImageData: ImageData, ImageName: "image",
            { (HTTPStatus, RecievedJSON) in
            print("Uploading Imaged Successed succeed With Code \(HTTPStatus)")
            let image = MessagePhotoModel(RecievedJSON!["image"])
            self.RemovePickedImage()
            Completion(image.id)
        }) { (HTTPStatus, ErrorArray) in
            print("Uploading Imaged info Failed With Code \(HTTPStatus)")
            self.ShowAlert(Message: ErrorArray?[0].body ?? "Check Your Connection".localized(), Seconds: 2)
            Completion(0)
        }
    }
    
    func RequstToGetMessages(){
        let url = "https://chat.cayload.com/chat/\(GetRoomName())/messages/0/"
        UserConnectionManager.Request(URL: url, RequestMethod: .get, Parameter: nil,
            { (HTTPStatus, RecievedJSON) in
            self.Chat_Data_Array = self.GetArrayList(From: RecievedJSON!, withRootTag: "messages")
            self.Chat_CollectionView.reloadData()
            self.Chat_CollectionView.ScrollToBottom(animated: true)
        }) { (HTTPStatus, ErrorArray) in
            print("getting messages Failed With Code \(HTTPStatus)")
            self.ShowAlert(Message: ErrorArray?[0].body ?? "Check Your Connection".localized(), Seconds: 2)
        }
    }
    
    func GetRoomName() ->String {
        //    0_d65/"
        switch self.With {
        case .Admin:
            return  "0_d\(ThisUserId ?? 0)"
        case .Customer:
            return "c\(CustomerId ?? 0)_d\(ThisUserId ?? 0)"
        default:
            return ""
        }
    }

    func GetArrayList(From Json:JSON,withRootTag RootTag: String) -> [ChatModel] {
        var DataArray = [ChatModel]()
        if Json[RootTag].arrayValue.count > 0 {
            for item in Json[RootTag].arrayValue {
                do{
                    let inst = try ChatModel(JsonData: item, thisID: self.ThisUserId ?? 0, is_Server: true)
                    DataArray.append(inst)
                }catch {
                    print(error)
                }
            }
        }
        return DataArray
    }
}
