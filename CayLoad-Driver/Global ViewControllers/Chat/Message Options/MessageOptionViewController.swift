//
//  MessageOptionViewController.swift
//  CayLoad-Driver
//
//  Created by Kiyarash Vosough on 4/19/1399 AP.
//  Copyright © 1399 CayLoad. All rights reserved.
//

import UIKit

class MessageOptionViewController: UIViewController {

    private var TapGesture:UITapGestureRecognizer!
    
    private let Menu_View = UIView()
    
    private var Menu_Height = CGFloat()
    
    private var SelectedOption:MessageOption_enum = .None
    
    private var OptionArray:[MessageOption_enum] = [.Copy,.Edit,.Delete,.Report,.Cancel]
    
    lazy var CodesTableView: UITableView = {
      let tbView = UITableView()
      tbView.dataSource = self
      tbView.delegate = self
      tbView.register(MessageOptionsTableViewCell.self, forCellReuseIdentifier: "OptionCell")
      return tbView
    }()

    lazy var BackDrop_View: UIView = {
        let bdView = UIView(frame: self.view.bounds)
        bdView.backgroundColor = UIColor.black.withAlphaComponent(0)
        return bdView
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //add gesture
        TapGesture = UITapGestureRecognizer(target: self, action: #selector(TapOnBackDropViewAction(_:)))
        BackDrop_View.addGestureRecognizer(TapGesture)
        Menu_Height = CGFloat(4*65 + 80)
        
        //add views
        addSearchBar()
        view.backgroundColor = .clear
        view.addSubview(BackDrop_View)
        OnCreateMenuView()
        // Do any additional setup after loading the view.
    }
    
    private func addSearchBar() {
        Menu_View.addSubview(CodesTableView)
          
        CodesTableView.snp.makeConstraints { (make) in
//            make.top.equalTo(self).offset(10)
            make.leading.equalTo(15)
            make.trailing.equalTo(-15)
            make.top.equalToSuperview().offset(10)
            CodesTableView.layer.cornerRadius = 30
          make.bottom.equalToSuperview()
        }
              
        }
    
    private func OnCreateMenuView(){
        view.addSubview(Menu_View)
        Menu_View.layer.cornerRadius = 30
        Menu_View.backgroundColor = .white
        Menu_View.translatesAutoresizingMaskIntoConstraints = false
        Menu_View.heightAnchor.constraint(equalToConstant: Menu_Height).isActive = true
        Menu_View.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        Menu_View.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        Menu_View.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
    }

    
    @objc func TapOnBackDropViewAction(_ sender: UITapGestureRecognizer) {
        dismiss(animated: true) {
        }
    }
    
    func GetSelectedOption() -> MessageOption_enum{
        return SelectedOption
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension MessageOptionViewController: UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let Cell = tableView.dequeueReusableCell(withIdentifier: "OptionCell", for: indexPath) as! MessageOptionsTableViewCell
        Cell.SetData(Option: OptionArray[indexPath.row])
        return Cell
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return OptionArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 65
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        SelectedOption = OptionArray[indexPath.row]
        performSegue(withIdentifier: "Unwind_From_MessageOption_To_Chat_Segue", sender: self)
    }
}
