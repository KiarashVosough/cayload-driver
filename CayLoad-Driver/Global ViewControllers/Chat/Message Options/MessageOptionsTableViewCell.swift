//
//  MessageOptionsTableViewCell.swift
//  CayLoad-Driver
//
//  Created by Kiyarash Vosough on 4/19/1399 AP.
//  Copyright © 1399 CayLoad. All rights reserved.
//

import UIKit

class MessageOptionsTableViewCell: UITableViewCell {

    
    var OptionName_Label: UILabel = {
        let instance = UILabel()
        instance.font = UIFont(name: "Poppins-Regular", size: 17)
        instance.textColor = .black
        return instance
    }()
    
    var OptionImage_Imageview: UIImageView = {
        let instance = UIImageView()
        return instance
    }()
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?){
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        SetupView()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func SetData(Option:MessageOption_enum){
        OptionName_Label.text = Option.rawValue
        if Option == .Delete {
            OptionName_Label.textColor = .red
        }
        OptionImage_Imageview.image = UIImage(named: Option.rawValue + "Ic")
    }
    
    func SetupView(){
        addSubview(OptionName_Label)
        addSubview(OptionImage_Imageview)
        
        OptionName_Label.snp.makeConstraints { (make) in
            make.leading.equalToSuperview().offset(10)
            make.centerY.equalToSuperview()
        }
        
        OptionImage_Imageview.snp.makeConstraints { (make) in
            make.trailing.equalToSuperview().offset(-10)
            make.centerY.equalToSuperview()
            make.width.equalTo(20)
            make.height.equalTo(20)
        }
    }

}
