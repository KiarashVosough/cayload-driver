//
//  ChatViewController.swift
//  CayLoad-Driver
//
//  Created by Kiyarash Vosough on 4/15/1399 AP.
//  Copyright © 1399 CayLoad. All rights reserved.
//

import UIKit
import SwiftyJSON
import Starscream

class ChatViewController: UIViewController {
    
    @IBOutlet weak var Header_View: UIView!
    @IBOutlet weak var Title_Label: UILabel!
    @IBOutlet weak var Back_Button: UIButton!
    @IBOutlet weak var Chat_CollectionView: UICollectionView!
    @IBOutlet weak var Chat_View: UIView!
    @IBOutlet weak var AddOmage_Button: UIButton!
    @IBOutlet weak var Send_Button: UIButton!
    @IBOutlet weak var Message_TextView: UITextView!
    @IBOutlet weak var ChatView_Height: NSLayoutConstraint!
    @IBOutlet weak var ChatView_BottomAnchor: NSLayoutConstraint!
    
    var ImagePicker = UIImagePickerController()
    
    let PickedImage_ImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.layer.cornerRadius = 15
        imageView.layer.masksToBounds = true
        return imageView
    }()
    let ImageHolder_View: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.layer.masksToBounds = true
        return view
    }()
    let CancelImage_Button: UIButton = {
        let view = UIButton()
        view.addTarget(self, action: #selector(OnCancelButtonClick), for: .touchUpInside)
        view.setImage(UIImage(named: "CancelButton"), for: .normal)
        view.backgroundColor = .white
        view.layer.masksToBounds = true
        return view
    }()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        RequstToGetMessages()
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Title_Label.text = Username
        PrepareCollectionView()
        ConfigWebSocket()
        Connect()
        AddTarget()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        socket?.disconnect()
    }
    
    func AddTarget(){
        NotificationCenter.default.addObserver(self, selector: #selector(HandleKeyboardEvent), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(HandleKeyboardEvent), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        Send_Button.addTarget(self, action: #selector(OnSendButtonClick), for: .touchUpInside)
        AddOmage_Button.addTarget(self, action: #selector(OnImagePickerButtonClicked), for: .touchUpInside)
        Back_Button.addTarget(self, action: #selector(OnBackButtonClick), for: .touchUpInside)
        
        Message_TextView.returnKeyType = .done
        Message_TextView.delegate = self
        ImagePicker.delegate = self
    }
    
    //MARK: - Socket
    let Host = "wss://chat.cayload.com/ws/chat/"
    var ContainImage:Bool = false
    var socket:WebSocket?
    var MessageAction:MessageAction = .Create
    var selectedMessageIndexpath:IndexPath?
    
    // MARK: - Navigation
    var Unwind_Identifier:String = ""
    var With:ChatWith = .Admin
    var Chat_Data_Array = [ChatModel]()
    var ThisUserId:Int?
    var CustomerId:Int?
    var Username:String = ""
    
    @IBAction func UnwindFrom_MessageOptionViewController_To_ChatViewController(segue: UIStoryboardSegue){
        if let source = segue.source as? MessageOptionViewController {
            switch source.GetSelectedOption() {
                case .Delete:
                    MessageAction = .Delete
                    break
                case .Edit:
                    MessageAction = .Edit
                    break
                case .Copy:
                    MessageAction = .Create
                    if let message = Message_TextView.text,!message.isEmpty {
                        UIPasteboard.general.string = message
                    }
                    break
                default:
                    MessageAction = .Create
                    break
            }
        }
    }
    
    @objc func OnSendButtonClick(_ sender:UIButton){
        if socket?.isConnected ?? false {
            switch self.MessageAction {
                case .Create:
                    CreateMessageParam()
                    break
                case .Delete:
                    WriteToSocket(dic:DeleteMessageParam(chat:Chat_Data_Array[selectedMessageIndexpath?.item ?? 0]) )
                    break
                case .Edit:
                    WriteToSocket(dic: EditMessageParam(chat: Chat_Data_Array[selectedMessageIndexpath?.item ?? 0]))
                    break
                default:
                    break
            }
        }
    }
    
    @objc func OnCancelButtonClick(_ sender:UIButton){
        RemovePickedImage()
    }
    
    @objc func OnBackButtonClick(_ sender:UIButton){
        if !Unwind_Identifier.isEmpty {
            performSegue(withIdentifier: Unwind_Identifier, sender: self)
        }
    }
    
}
