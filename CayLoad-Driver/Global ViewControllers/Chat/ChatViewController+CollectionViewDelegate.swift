//
//  ChatViewController+CollectionViewDelegate.swift
//  CayLoad-Driver
//
//  Created by Kiyarash Vosough on 4/16/1399 AP.
//  Copyright © 1399 CayLoad. All rights reserved.
//

import Foundation
import UIKit

extension ChatViewController: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,MessageOption {
    
    func PerformSegueForItem(at: IndexPath) {
        selectedMessageIndexpath = at
        performSegue(withIdentifier: "From_Chat_To_MessageOption_Segue", sender: self)
    }
   
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return Chat_Data_Array.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MessageCell", for: indexPath) as! MesseageCollectionViewCell
        cell.MessageOption = self
        cell.selfIndexpath = indexPath
        cell.SetData(ChatModel: Chat_Data_Array[indexPath.item])
        cell.SetupViews()
        cell.SetupConstaintsAndViews(CollectionView_Frame: collectionView.frame)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width, height: Chat_Data_Array[indexPath.row].GetEstimatedHeight())
    }
    
    func PrepareCollectionView(){
        Chat_CollectionView.delegate = self
        Chat_CollectionView.dataSource = self
        Chat_CollectionView.register(MesseageCollectionViewCell.self, forCellWithReuseIdentifier: "MessageCell")
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 25, left: 0, bottom: 25, right: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 20
    }
}
