//
//  ChatViewController+SocketHandler.swift
//  CayLoad-Driver
//
//  Created by Kiyarash Vosough on 4/16/1399 AP.
//  Copyright © 1399 CayLoad. All rights reserved.
//

import Foundation
import Starscream
import SwiftyJSON

extension ChatViewController:WebSocketDelegate {
    func websocketDidConnect(socket: WebSocketClient) {
        print("Socket Connected")
    }
    
    func websocketDidDisconnect(socket: WebSocketClient, error: Error?) {
        print("Socket Disconnected")
    }
    
    func websocketDidReceiveMessage(socket: WebSocketClient, text: String) {
        print("Socket recieved String Message")
        do{
            self.Message_TextView.text = ""
            let json:JSON =  try text.GetJSONFromString()
            let chat = try ChatModel(JsonData: json, thisID: ThisUserId ?? 0)
            
            switch chat.action {
                case "create":
                    CreateMessage(chat: chat)
                break
                case "update":
                    UpdateMessage(chat: chat)
                break
                case "delete":
                    DeleteMessage(chat: chat)
                break
                default:
                    break
            }
        }
        catch  JSONError.BadJSON {
            print(JSONError.BadJSON)
        }
        catch JSONError.MissingAnAtrribute {
            print(JSONError.MissingAnAtrribute)
        }
        catch  {
            print("Error")
        }
    }
    
    func websocketDidReceiveData(socket: WebSocketClient, data: Data) {
        print("Socket recieved Data Message")
    }
    
    func ConfigWebSocket(){
        do {
            self.socket = WebSocket(request: try GetSocketURL(),protocols: ["chat"])
            socket?.delegate = self
        }catch SocketError.BadURL {
            print(SocketError.BadURL)
        }catch {
            
        }
        
    }
    
    func Connect(){
        socket?.connect()
    }
    
    func Discoonect(){
        if socket?.isConnected ?? false {
            socket?.disconnect()
        }
    }
    
    func WriteToSocket(dic:[String:Any]){
        if socket?.isConnected ?? false {
            do{
                socket?.write(string: try dic.GetStringFromDic())
            }catch SocketError.CantGetString {
                print(SocketError.CantGetString)
            }
            catch{
                
            }
        }
    }
    
    func GetSocketURL() throws ->URLRequest {
        //    0_d65/"
        switch self.With {
        case .Admin:
            return URLRequest(url: URL(string: Host + "0_d\(ThisUserId ?? 0)/" )!)
        case .Customer:
            return URLRequest(url: URL(string: Host + "c\(CustomerId ?? 0)_d\(ThisUserId ?? 0)/" )!)
        default:
            throw SocketError.BadURL
        }
    }
    
    // Handle Data that recieve it through socket
    func CreateMessage(chat:ChatModel) {
        Chat_Data_Array.append(chat)
//        self.Chat_CollectionView.insertItems(at: [IndexPath(row: Chat_Data_Array.count - 1, section: 0)])
        self.Chat_CollectionView.reloadData()
        self.Chat_CollectionView.ScrollToBottom(animated: true)
    }
    
    func UpdateMessage(chat:ChatModel){
        Chat_Data_Array.forEach { (item) in
            if item.id == chat.id {
                item.text = chat.text
                self.Chat_CollectionView.reloadData()
                self.Chat_CollectionView.ScrollToBottom(animated: true)
            }
        }
    }
    
    func DeleteMessage(chat:ChatModel){
        var index:Int = 0
        for i in 0..<Chat_Data_Array.count {
            if Chat_Data_Array[i].id == chat.id {
                index = i
            }
        }
        if index != 0 {
            Chat_Data_Array.remove(at: index)
            self.Chat_CollectionView.deleteItems(at: [IndexPath(row: index, section: 0)])
            self.Chat_CollectionView.ScrollToBottom(animated: true)
        }
    }
    
    
    
    
    // Handle Data to send it through socket
    func CreateMessageParam() {
        
        var dic:[String:Any] = ["action":"create"]
        
        if let id = ThisUserId {
            dic.updateValue("d\(id)", forKey:"user_id")
        }else {
            return
        }
        
        // send message and text
        if  let message = Message_TextView.text, ContainImage && !message.isEmpty {
            dic.updateValue(message, forKey: "message")
            if let image_data:Data = PickedImage_ImageView.image?.jpegData(compressionQuality: 0.5) {
                RequestToUploadImage(ImageData: image_data) { (sts) in
                    if sts != 0 {
                        dic.updateValue(sts, forKey: "image_id")
                        self.WriteToSocket(dic: dic)
                    }
                }
            }
        }
        //send just imge
        else if ContainImage  {
            dic.updateValue("", forKey: "message")
            if let image_data:Data = PickedImage_ImageView.image?.jpegData(compressionQuality: 0.5) {
                RequestToUploadImage(ImageData: image_data) { (sts) in
                    if sts != 0 {
                        dic.updateValue(sts, forKey: "image_id")
                        self.WriteToSocket(dic: dic)
                    }
                }
            }
        }
        //just send message
        else if let message = Message_TextView.text,!message.isEmpty {
            dic.updateValue(message, forKey: "message")
            dic.updateValue(0, forKey: "image_id")
            self.WriteToSocket(dic: dic)
        }
    }
    
    
    func DeleteMessageParam(chat:ChatModel)-> [String:Any]{
        var param:[String:Any] = ["action":"delete"]
            
        if let id = ThisUserId {
            param.updateValue("d\(id)", forKey:"user_id")
        }
        param.updateValue(["id":chat.id], forKey: "message")
        return param
    }
    
    
    func EditMessageParam(chat:ChatModel) -> [String:Any]{
        var param:[String:Any] = ["action":"update"]
        
        if let id = ThisUserId {
            param.updateValue("d\(id)", forKey:"user_id")
        }
        param.updateValue(["id":chat.id,"text":Message_TextView.text ?? ""], forKey: "message")
        return param
    }
}
