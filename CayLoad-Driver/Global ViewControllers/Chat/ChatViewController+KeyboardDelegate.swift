//
//  ChatViewController+KeyboardDelegate.swift
//  CayLoad-Driver
//
//  Created by Kiyarash Vosough on 4/17/1399 AP.
//  Copyright © 1399 CayLoad. All rights reserved.
//

import Foundation
import UIKit

extension ChatViewController:UITextViewDelegate {
    
    /* Updated for Swift 4 */
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n") {
            self.view.endEditing(true)
            return false
        }
        return true
    }

    @objc func HandleKeyboardEvent(_ notif:Notification){
        guard let keyboardValue = notif.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue else {
            return }
        let keyboardScreenEndFrame = keyboardValue.cgRectValue
        let keyboardViewEndFrame = view.convert(keyboardScreenEndFrame, from: view.window)
        
        let duration:TimeInterval = (notif.userInfo![UIResponder.keyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue ?? 0
        let animationCurveRawNSN = notif.userInfo![UIResponder.keyboardAnimationCurveUserInfoKey] as? NSNumber
        let animationCurveRaw = animationCurveRawNSN?.uintValue ?? UIView.AnimationOptions.curveEaseInOut.rawValue
        let animationCurve:UIView.AnimationOptions = UIView.AnimationOptions(rawValue: animationCurveRaw)
        
        if notif.name == UIResponder.keyboardWillShowNotification {
            ChatView_BottomAnchor.constant = keyboardViewEndFrame.height
        }else{
            ChatView_BottomAnchor.constant = 0
            
        }
        UIView.animate(withDuration: duration,delay: 0,options: animationCurve,animations: { self.view.layoutIfNeeded() },
        completion: nil)
    }
}
