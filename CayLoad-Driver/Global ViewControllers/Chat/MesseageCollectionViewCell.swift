//
//  MesseageCollectionViewCell.swift
//  CayLoad-Driver
//
//  Created by Kiyarash Vosough on 4/16/1399 AP.
//  Copyright © 1399 CayLoad. All rights reserved.
//

import UIKit
import SwiftyJSON

class MesseageCollectionViewCell: UICollectionViewCell {
    
    let Message_TextView: UITextView = {
        let textView = UITextView()
        textView.text = "qwertyuiopasdfghjklzxcvbnmasdfghjk"
        textView.textColor = .black
        textView.isEditable = false
        textView.font = UIFont(name: "Poppins-Regular", size: 15)
        textView.backgroundColor = .clear
        return textView
    }()
    let Bubble_View: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.layer.cornerRadius = 15
        view.layer.masksToBounds = true
        return view
    }()
    
    let Profile_ImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.layer.cornerRadius = 15
        imageView.layer.masksToBounds = true
        return imageView
    }()
    
    let Time_Label: UILabel = {
        let label = UILabel()
        label.text = "13:45"
        label.textAlignment = .left
        label.textColor = .black
        label.font = UIFont(name: "Poppins-Regular", size: 11)
        return label
    }()
    
    let Message_ImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleToFill
        imageView.layer.cornerRadius = 10
        imageView.layer.masksToBounds = true
        return imageView
    }()
    
    var Chat:ChatModel?
    
    var MessageOption:MessageOption?
    
    var selfIndexpath:IndexPath?
    
    func SetupViews() {
        
        addSubview(Bubble_View)
        addSubview(Profile_ImageView)
        addSubview(Time_Label)
        
        
        if Chat?.MessageType == .TextAndImage {
            addSubview(Message_ImageView)
            addSubview(Message_TextView)
        }
        else if Chat?.MessageType == .JustImage {
            addSubview(Message_ImageView)
        }
        else {
            addSubview(Message_TextView)
        }
        if Chat?.sender_user == Chat?.this_User_Id {
//            print(Chat?.sender_user," ",Chat?.this_User_Id)
            let gest = UITapGestureRecognizer(target: self, action: #selector(OnMessageSelected))
            Message_TextView.addGestureRecognizer(gest)
        }
        
        addConstraintsWithFormat(format: "H:|-8-[v0(30)]", views: Profile_ImageView)
        addConstraintsWithFormat(format: "V:[v0(30)]|", views: Profile_ImageView)
    }
    
    func SetupConstaintsAndViews(CollectionView_Frame:CGRect){
        
        Message_TextView.text = Chat?.text
        Time_Label.text = Chat?.date_created?.GetFormatedTime()
        
        if Chat?.Sender == .Me {
            //views attr
            Profile_ImageView.isHidden = true
            //views attr
            Bubble_View.backgroundColor = .white
            Message_TextView.textColor = .black
            Time_Label.textColor = .black
            
            if Chat?.MessageType == .JustText {
                Message_ImageView.isHidden = true
                Message_TextView.frame = CGRect(x:CollectionView_Frame.width - (25 + (Chat?.estimated_Width ?? 0)), y: 0, width: (Chat?.estimated_Width ?? 0) + 16, height: (Chat?.estimated_Height ?? 0) + 35)
                
                Bubble_View.frame = CGRect(x: CollectionView_Frame.width -  (28 + (Chat?.estimated_Width ?? 0)), y: 0, width: (Chat?.estimated_Width ?? 0) + 16 + 8 + 8, height: (Chat?.estimated_Height ?? 0) + 50)
                
                Time_Label.frame = CGRect(x:CollectionView_Frame.width - (50), y: Message_TextView.frame.height - 20, width: 40 , height: 20)
                
            }
            else if Chat?.MessageType == .JustImage {
                Message_ImageView.isHidden = false
                Message_ImageView.image = Chat?.image?.image
//                LoadImageFromServer(url:try! ("https://chat.cayload.com" + (Chat.image?.url ?? "")).asURL())
                
                Message_ImageView.frame = CGRect(x: CollectionView_Frame.width - (25 + 210), y: 10, width: 205 + 16, height: 130 + 20)
                
                Bubble_View.frame = CGRect(x: CollectionView_Frame.width -  (28 + 220), y: 0, width: 220 + 8 + 16 , height: 130 + 60)
                
                Time_Label.frame = CGRect(x: CollectionView_Frame.width - (40) , y: Message_ImageView.frame.height + 15, width: 30, height: 20)
            }
            else if Chat?.MessageType == .TextAndImage {
                Message_ImageView.isHidden = false
//                Message_ImageView.LoadImageFromServer(url:try! ("https://chat.cayload.com" + (Chat.image?.url ?? "")).asURL())
                Message_ImageView.image = Chat?.image?.image
                
                Message_ImageView.frame = CGRect(x: CollectionView_Frame.width - ((Chat?.estimated_Width ?? 0) - 11 + 30 ), y: 10, width: Chat?.estimated_Width ?? 0, height: 130 + 20)
                
                Message_TextView.frame = CGRect(x:CollectionView_Frame.width - (20 + (Chat?.estimated_Width ?? 0)), y: Message_ImageView.frame.height + 10, width: (Chat?.estimated_Width ?? 0) + 11, height: (Chat?.estimated_Height ?? 0) + 20)
                
                Bubble_View.frame = CGRect(x: CollectionView_Frame.width - (28 + (Chat?.estimated_Width ?? 0) ), y: 0, width: (Chat?.estimated_Width ?? 0) + 8 + 16 , height: (Chat?.estimated_Height ?? 0) + 130 + 70)
                
                Time_Label.frame = CGRect(x: CollectionView_Frame.width - (40) , y: Message_TextView.frame.maxY , width: 30, height: 15)
            }
        }
        else {
            //views attr
            Profile_ImageView.isHidden = false
            Bubble_View.backgroundColor = .App_DarkBlue
            Message_TextView.textColor = .white
            Time_Label.textColor = .white
            
            if Chat?.With == .Admin {
                Profile_ImageView.backgroundColor = .App_DarkBlue
            }
            
            if Chat?.MessageType == .JustText {
                Message_ImageView.isHidden = true
                Message_TextView.frame = CGRect(x:48 + 8, y: 0, width: (Chat?.estimated_Width ?? 0) + 16, height: (Chat?.estimated_Height ?? 0) + 20)
                
                Bubble_View.frame = CGRect(x: 48, y: 0, width: (Chat?.estimated_Width ?? 0) + 16 + 10 , height: (Chat?.estimated_Height ?? 0) + 40)
                            
                Time_Label.frame = CGRect(x:Message_TextView.frame.width + 15, y: Message_TextView.frame.maxY - 6, width: 30 , height: 15)
                
            }
            else if Chat?.MessageType == .JustImage {
                Message_ImageView.isHidden = false
//                Message_ImageView.LoadImageFromServer(url:try! (PURE_DOMAIN_URL + (Chat.image?.url ?? "")).asURL())
                Message_ImageView.image = Chat?.image?.image
                
                Message_ImageView.frame = CGRect(x: 48 + 10, y: 10, width: 205 + 16, height: 130 + 20)
                
                Bubble_View.frame = CGRect(x: 48, y: 0, width: 220 + 8 + 16 , height: 130 + 60)
                
                Time_Label.frame = CGRect(x: Message_ImageView.frame.width + 27, y: Message_ImageView.frame.maxY + 7, width: 30, height: 15)
            }
            else if Chat?.MessageType == .TextAndImage {
                Message_ImageView.isHidden = false
//                Message_ImageView.LoadImageFromServer(url:try! (PURE_DOMAIN_URL + (Chat.image?.url ?? "")).asURL())
                Message_ImageView.image = Chat?.image?.image
                
                Message_ImageView.frame = CGRect(x: 48 + 10, y: 10, width: (Chat?.estimated_Width ?? 0), height: 130 + 20)
                
                Message_TextView.frame = CGRect(x:48 + 8 , y: Message_ImageView.frame.height + 10, width: (Chat?.estimated_Width ?? 0) + 11, height: (Chat?.estimated_Height ?? 0 ) + 20)
                
                Bubble_View.frame = CGRect(x: 48, y: 0, width: (Chat?.estimated_Width ?? 0) + 8 + 16 , height: (Chat?.estimated_Height ?? 0) + 130 + 70)
                
                Time_Label.frame = CGRect(x: Message_ImageView.frame.width + 27 , y: Message_TextView.frame.maxY - 4 , width: 30, height: 15)
            }
        }
        
        
    }
    
    
    
    func SetData(ChatModel:ChatModel){
        self.Chat = ChatModel
    }
    
    @objc func OnMessageSelected(_ sender:UITapGestureRecognizer){
        guard let IP = selfIndexpath else {
            return
        }
        MessageOption?.PerformSegueForItem(at: IP)
    }
}
