//
//  ChatViewController+ImagePickerDelegate.swift
//  CayLoad-Driver
//
//  Created by Kiyarash Vosough on 4/17/1399 AP.
//  Copyright © 1399 CayLoad. All rights reserved.
//

import Foundation
import UIKit

extension ChatViewController:UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let pickedImage = info[UIImagePickerController.InfoKey.editedImage] as? UIImage {
            PickedImage_ImageView.image = pickedImage
            ContainImage = true
            AddPickedImage()
        }
        dismiss(animated: true, completion: nil)
    }
    
    @objc func OnImagePickerButtonClicked(_ sender:UIButton){
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary){
            ImagePicker.sourceType = .savedPhotosAlbum
            ImagePicker.allowsEditing = true
            present(ImagePicker, animated: true, completion: nil)
        }
    }
    
    func RemovePickedImage(){
        if view.contains(ImageHolder_View) {
            CancelImage_Button.snp.removeConstraints()
            CancelImage_Button.removeFromSuperview()
            PickedImage_ImageView.snp.removeConstraints()
            PickedImage_ImageView.removeFromSuperview()
            ImageHolder_View.snp.removeConstraints()
            ImageHolder_View.removeFromSuperview()
            ContainImage = false
        }
    }
    
    func AddPickedImage(){
        view.addSubview(ImageHolder_View)
        view.bringSubviewToFront(ImageHolder_View)
        ImageHolder_View.addSubview(PickedImage_ImageView)
        ImageHolder_View.addSubview(CancelImage_Button)
        
        ImageHolder_View.snp.makeConstraints { (make) in
            make.leading.equalToSuperview()
            make.trailing.equalToSuperview()
            make.height.equalTo(170)
            make.bottom.equalTo(Chat_View.snp.top)
        }
        PickedImage_ImageView.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(9)
            make.bottom.equalToSuperview()
            make.leading.equalToSuperview().offset(53)
            make.trailing.equalToSuperview().offset(-9)
        }
        CancelImage_Button.snp.makeConstraints { (make) in
            make.centerY.equalTo(PickedImage_ImageView)
            make.leading.equalToSuperview().offset(19)
            make.height.equalTo(17)
            make.width.equalTo(17)
        }
    }
}
