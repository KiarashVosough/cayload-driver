//
//  TruckPickerTableViewCell.swift
//  CayLoad-Driver
//
//  Created by Kiyarash Vosough on 2/6/1399 AP.
//  Copyright © 1399 CayLoad. All rights reserved.
//

import UIKit

class TruckPickerTableViewCell: UITableViewCell {
    
    var TruckName_Label: UILabel = {
        let instance = UILabel()
        return instance
    }()
    
    var TruckImage_ImageView: UIImageView = {
        let instance = UIImageView()
        instance.contentMode = .scaleAspectFit
        return instance
    }()

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        OnCreateViews()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func SetData(TruckName:String,ImageURL:String) {
        TruckName_Label.text = TruckName
        TruckImage_ImageView.LoadImageFromServer(url: try! ImageURL.asURL())
    }

    func OnCreateViews(){
        addSubview(TruckName_Label)
        addSubview(TruckImage_ImageView)

        TruckName_Label.translatesAutoresizingMaskIntoConstraints = false
        TruckName_Label.leadingAnchor.constraint(equalTo: self.leadingAnchor,constant: 15).isActive = true
        TruckName_Label.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        TruckName_Label.trailingAnchor.constraint(lessThanOrEqualTo: self.trailingAnchor).isActive = true
        
        TruckImage_ImageView.translatesAutoresizingMaskIntoConstraints = false
        TruckImage_ImageView.trailingAnchor.constraint(equalTo: self.trailingAnchor,constant: -15).isActive = true
        TruckImage_ImageView.topAnchor.constraint(equalTo: self.topAnchor,constant: 5).isActive = true
        TruckImage_ImageView.bottomAnchor.constraint(equalTo: self.bottomAnchor,constant: -5).isActive = true
        TruckImage_ImageView.widthAnchor.constraint(equalToConstant: 120).isActive = true
           
    }
}
