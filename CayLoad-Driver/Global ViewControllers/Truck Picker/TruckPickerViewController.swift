//
//  TruckPickerViewController.swift
//  CayLoad-Driver
//
//  Created by Kiyarash Vosough on 2/6/1399 AP.
//  Copyright © 1399 CayLoad. All rights reserved.
//

import UIKit

class TruckPickerViewController: UIViewController {

    private var UnwindToViewControllerID:String = ""
    private var TapGesture:UITapGestureRecognizer!
    private let Menu_View = UIView()
    private var SelectedTruckType:TruckTypeModel!
        
    private lazy var TruckTypeTableView: UITableView = {
      let tbView = UITableView()
      tbView.dataSource = self
      tbView.delegate = self
      tbView.register(TruckPickerTableViewCell.self, forCellReuseIdentifier: "TruckPickerTableViewCell")
      return tbView
    }()
      

    private lazy var BackDrop_View: UIView = {
        let bdView = UIView(frame: self.view.bounds)
        bdView.backgroundColor = UIColor.black.withAlphaComponent(0)
        return bdView
    }()


    override func viewDidLoad() {
        super.viewDidLoad()
        UIView.appearance().semanticContentAttribute = .forceLeftToRight
        RequestToGetTruckTypes()
        //add gesture
        TapGesture = UITapGestureRecognizer(target: self, action: #selector(TapOnBackDropViewAction(_:)))
        BackDrop_View.addGestureRecognizer(TapGesture)
        
        //add views
        OnCreateTruckTypeTableView()
        view.backgroundColor = .clear
        view.addSubview(BackDrop_View)
        OnCreateMenuView()
        
      }
    
    @objc func TapOnBackDropViewAction(_ sender: UITapGestureRecognizer) {
        dismiss(animated: true) {
        }
    }

    private func OnCreateTruckTypeTableView() {
        Menu_View.addSubview(TruckTypeTableView)
        TruckTypeTableView.snp.makeConstraints { (make) in
            make.top.equalToSuperview()
            TruckTypeTableView.layer.cornerRadius = 30
            make.leading.trailing.equalToSuperview()
            make.bottom.equalToSuperview()
        }
    }
    
    private func OnCreateMenuView(){
        view.addSubview(Menu_View)
        Menu_View.layer.cornerRadius = 30
        Menu_View.backgroundColor = .white
        Menu_View.translatesAutoresizingMaskIntoConstraints = false
        Menu_View.heightAnchor.constraint(equalToConstant: 5*65 + 80).isActive = true
        Menu_View.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        Menu_View.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        Menu_View.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
    }
    
    func setUnwind(withIdentifier Identifier:String) {
        UnwindToViewControllerID = Identifier
    }
    func getSelectedTruckType() -> TruckTypeModel {
        return SelectedTruckType
    }
    
    // MARK: - Network
    var TruckTypesArray:[TruckTypeModel]?
    
    private func RequestToGetTruckTypes(){
        UserConnectionManager.RequestWithHeader(URL: "\(DOMAIN_URL)\(TruckType_URL)", RequestMethod: .get, Parameter: nil, { (HTTPStatus, RecievedJson) in
            self.TruckTypesArray = JsonParsingUtils.GetArrayList(From: RecievedJson!, withRootTag: "truck_types")
            self.TruckTypeTableView.reloadData()
        }) { (HTTPStatus, ErrorArray) in
            self.ShowAlert(Message: ErrorArray?[0].body ?? "Check Your Connection".localized(), Seconds: 2)
        }
    }

}


extension TruckPickerViewController: UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let LangCell = tableView.dequeueReusableCell(withIdentifier: "TruckPickerTableViewCell", for: indexPath) as? TruckPickerTableViewCell
        LangCell?.SetData(TruckName: TruckTypesArray?[indexPath.row].title ?? "", ImageURL: PURE_DOMAIN_URL + (TruckTypesArray?[indexPath.row].icon?.file ?? ""))
        return LangCell!
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return TruckTypesArray?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 65
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        SelectedTruckType = TruckTypesArray?[indexPath.row]
        performSegue(withIdentifier: UnwindToViewControllerID, sender: self)
    }
}
