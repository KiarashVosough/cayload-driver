//
//  AcceptedOffersDetailsViewController.swift
//  CayLoad-Driver
//
//  Created by Kiyarash Vosough on 2/19/1399 AP.
//  Copyright © 1399 CayLoad. All rights reserved.
//

import UIKit
import CoreLocation

class AcceptedOffersDetailsViewController: UIViewController {

    @IBOutlet weak var Detail_Label: UILabel!
    @IBOutlet weak var Back_Button: UIButton!
    
    @IBOutlet weak var Main_Scrollview: UIScrollView!
    @IBOutlet weak var Scrolview_Content_View: UIView!
    
    @IBOutlet weak var Container1_View: UIView!
    @IBOutlet weak var Container1_Const_Label: UILabel!
    @IBOutlet weak var Container1_Data_Label: UILabel!
    
    @IBOutlet weak var Container2_View: UIView!
    @IBOutlet weak var Container2_Const_Label: UILabel!
    @IBOutlet weak var Container2_Data_Label: UILabel!
    
    @IBOutlet weak var Container3_View: UIView!
    @IBOutlet weak var Container3_Const_Label: UILabel!
    @IBOutlet weak var Container3_Data_Label: UILabel!
    
    @IBOutlet weak var Container4_View: UIView!
    @IBOutlet weak var Container4_Const_Label: UILabel!
    @IBOutlet weak var Container4_Data_Label: UILabel!
    
    @IBOutlet weak var Container5_View: UIView!
    @IBOutlet weak var Container5_Const_Label: UILabel!
    @IBOutlet weak var Container5_Data_Label: UILabel!
    
    @IBOutlet weak var Container6_View: UIView!
    @IBOutlet weak var Container6_Const_Label: UILabel!
    @IBOutlet weak var Container6_Data_Label: UILabel!
    
    @IBOutlet weak var Container7_View: UIView!
    @IBOutlet weak var Container7_Const_Label: UILabel!
    @IBOutlet weak var Container7_Data_Label: UILabel!
    
    @IBOutlet weak var Container8_View: UIView!
    @IBOutlet weak var Container8_Const_Label: UILabel!
    @IBOutlet weak var Container8_Data_Label: UILabel!
    
    @IBOutlet weak var CargoDescription_View: UIView!
    @IBOutlet weak var CargoDescription_Label: UILabel!
    @IBOutlet weak var CargoDescription_Textview: UITextView!
    
    @IBOutlet weak var Origin_View: UIView!
    @IBOutlet weak var Origin_Label: UILabel!
    @IBOutlet weak var Origin_Textview: UITextView!
    @IBOutlet weak var OriginMap_Button: UIButton!
    @IBOutlet weak var OriginDate_View: UIView!
    @IBOutlet weak var OriginDate_Label: UILabel!
    
    @IBOutlet weak var Destination_View: UIView!
    @IBOutlet weak var Destination_Label: UILabel!
    @IBOutlet weak var Destination_Textview: UITextView!
    @IBOutlet weak var DestinationMap_Button: UIButton!
    @IBOutlet weak var DestinationDate_View: UIView!
    @IBOutlet weak var DestinationDate_Label: UILabel!
    
    @IBOutlet weak var RequestDescription_View: UIView!
    @IBOutlet weak var RequestDescription_Label: UILabel!
    @IBOutlet weak var RequestDescription_Textview: UITextView!
    
    @IBOutlet weak var CustomerProfile_VIew: UIView!
    @IBOutlet weak var Profile_Imageview: UIImageView!
    @IBOutlet weak var CustomerDetail_Label: UILabel!
    @IBOutlet weak var CustomerUsername_Label: UILabel!
    @IBOutlet weak var GoToCustomerProfile_Button: UIButton!
    
    @IBOutlet weak var Trackers_Label: UILabel!
    @IBOutlet weak var Trackers_Button: UIButton!
    
    @IBOutlet weak var Number_Label: UILabel!
    @IBOutlet weak var View_Label: UILabel!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        RequestToGetTruckDetail()
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        AddTarget()
        Number_Label.layer.cornerRadius = Number_Label.frame.width/2
        UpdateViewCornerRadius()
    }
    
    private func AddTarget(){
        Back_Button.addTarget(self, action: #selector(OnBackButtonClicked), for: .touchUpInside)
        Trackers_Button.addTarget(self, action: #selector(OnTrackButtonClicked(_:)), for: .touchUpInside)
        GoToCustomerProfile_Button.addTarget(self, action: #selector(OnProfileShowButtonButtonClicked(_:)), for: .touchUpInside)
        OriginMap_Button.addTarget(self, action: #selector(OnShowOriginButtonClicked(_:)), for: .touchUpInside)
        DestinationMap_Button.addTarget(self, action: #selector(OnShowDestButtonClicked(_:)), for: .touchUpInside)
    }
    
    private func UpdateViewCornerRadius(){
        Container8_View.SetSpeceficCornerRadius(Radius: 9, TopLeft: false, BottomLeft: true, TopRight: false, BottomRight: true)
        Origin_View.SetSpeceficCornerRadius(Radius: 9, TopLeft: true, BottomLeft: false, TopRight: true, BottomRight: false)
        OriginDate_View.SetSpeceficCornerRadius(Radius: 9, TopLeft: false, BottomLeft: true, TopRight: false, BottomRight: true)
        Destination_View.SetSpeceficCornerRadius(Radius: 9, TopLeft: true, BottomLeft: false, TopRight: true, BottomRight: false)
        DestinationDate_View.SetSpeceficCornerRadius(Radius: 9, TopLeft: false, BottomLeft: true, TopRight: false, BottomRight: true)
        
    }
    
    private func UpdateShipmentTypeViews(){
        
        if RequestData?.cargo?.container_type == nil {
            //bulk
            if Container1_View != nil &&  Container2_View != nil && Container3_View != nil {
                Container1_View.removeFromSuperview()
                Container2_View.removeFromSuperview()
                Container3_View.topAnchor.constraint(equalTo: Scrolview_Content_View.topAnchor,constant: 15).isActive = true
            }
            Container3_View.SetSpeceficCornerRadius(Radius: 9, TopLeft: true, BottomLeft: false, TopRight: true, BottomRight: false)
            Container3_Const_Label.text = "Shipment Type".localized()
            Container3_Data_Label.text = "Bulk".localized()
            
            Container4_Const_Label.text = "Commodity".localized()
            Container4_Data_Label.text = RequestData?.cargo?.commodity
            
            Container5_Const_Label.text = "Net Weight".localized()
            Container5_Data_Label.text = "\(RequestData?.cargo?.net_weight ?? 0)"
            
            Container6_Const_Label.text = "Gross Weight".localized()
            Container6_Data_Label.text = "\(RequestData?.cargo?.total_weight ?? 0)"
            
            Container7_Const_Label.text = "Number of packages".localized()
            Container7_Data_Label.text = "\(RequestData?.cargo?.packages_count ?? 0)"
            
            Container8_Const_Label.text = "HSCode".localized()
            Container8_Data_Label.text = RequestData?.cargo?.hs_code
        }
        else {
            Container1_View.SetSpeceficCornerRadius(Radius: 9, TopLeft: true, BottomLeft: false, TopRight: true, BottomRight: false)
            Container1_Const_Label.text = "Shipment Type".localized()
            Container1_Data_Label.text = "Containerized".localized()
            
            Container2_Const_Label.text = "Container Details".localized()
            Container2_Data_Label.text = (RequestData?.cargo?.container_type?.title ?? "") + " | " + (RequestData?.cargo?.container_size?.title ?? "")
            
            Container3_Const_Label.text = "Owner Container".localized()
            Container3_Data_Label.text = RequestData?.cargo?.container_owner
            
            Container4_Const_Label.text = "Commodity".localized()
            Container4_Data_Label.text = RequestData?.cargo?.commodity
            
            Container5_Const_Label.text = "Cargo Weight".localized()
            Container5_Data_Label.text = "\(RequestData?.cargo?.net_weight ?? 0)"
            
            Container6_Const_Label.text = "Total Weight include Tare Weight".localized()
            Container6_Data_Label.text = "\(RequestData?.cargo?.total_weight ?? 0)"
            
            Container7_Const_Label.text = "Number of packages".localized()
            Container7_Data_Label.text = "\(RequestData?.cargo?.packages_count ?? 0)"
            
            Container8_Const_Label.text = "HSCode".localized()
            Container8_Data_Label.text = RequestData?.cargo?.hs_code
        }
    }
    
    private func UpdateIncommonViews(){
        
        if RequestData?.cargo != nil {
            CargoDescription_Textview.text = RequestData?.cargo?.description
            
        }
        Origin_Textview.text = RequestData?.route?.origin_point?.formatted_address
        
        OriginDate_Label.text = RequestData?.route?.origin_datetime?.GetFormatedDate()
        
        Destination_Textview.text = RequestData?.route?.destination_point?.formatted_address
        
        DestinationDate_Label.text = RequestData?.route?.destination_datetime?.GetFormatedDate()
        
        RequestDescription_Textview.text = RequestData?.description
       
        
        CustomerUsername_Label.text = (RequestData?.customer?.user?.first_name ?? "") + (RequestData?.customer?.user?.last_name ?? "")
        
        Profile_Imageview.LoadImageFromServer(url: try! (PURE_DOMAIN_URL + (RequestData?.customer?.profile_image?.file ?? "")).asURL())
        
        if let count = RequestData?.tracks_count ,count>0{
            Number_Label.text = "\(count)"
        }else {
            Trackers_Label.isHidden = true
            Trackers_Button.isHidden = true
            Number_Label.isHidden = true
            View_Label.isHidden = true
        }
    }
    
    // MARK: - Navigation
    
    var Request_Id:Int = 0
    var User_Id:Int = 0
    private var Origin_Point:CLLocationCoordinate2D?
    private var Dest_Point:CLLocationCoordinate2D?
    
    @IBAction func UnwindFrom_TrackersController_To_AcceptedOffersDetailsViewController(segue:UIStoryboardSegue) {
        
        if segue.source is TrackersViewController {
        }
    }
    
    @IBAction func UnwindFrom_CustomerProfileController_To_AcceptedOffersDetailsViewController(segue:UIStoryboardSegue) {
    }
    
    @IBAction func UnwindFrom_MapViewController_To_AcceptedOffersDetailsViewController(segue:UIStoryboardSegue) {
        
    }
    
    @objc private func OnBackButtonClicked(_ sender:UIButton){
        navigationController?.popToRootViewController(animated: true)
    }
    
    @objc private func OnProfileShowButtonButtonClicked(_ sender:UIButton){
        performSegue(withIdentifier: "From_AcceptedOffersDetails_To_CustomerProfile_Segue", sender: self)
    }
    
    @objc private func OnTrackButtonClicked(_ sender:UIButton){
        performSegue(withIdentifier: "From_AcceptedOffersDetails_To_Trackers_Segue", sender: self)
    }
    
    @objc private func OnShowOriginButtonClicked(_ sender:UIButton){
        
        guard let Org_lat:Double = RequestData?.route?.origin_point?.latitude else {
            ShowAlert(Message: "There is no Location To Show", Seconds: 2)
            return
        }
        guard let Org_long:Double = RequestData?.route?.origin_point?.longitude else {
            ShowAlert(Message: "There is no Location To Show", Seconds: 2)
            return
        }
        Dest_Point = nil
        Origin_Point = CLLocationCoordinate2D(latitude: Org_lat, longitude: Org_long)
        performSegue(withIdentifier: "From_AcceptedOfferDetails_To_Map_Segue", sender: self)
    }
    
    @objc private func OnShowDestButtonClicked(_ sender:UIButton){
        guard let Org_lat:Double = RequestData?.route?.destination_point?.latitude else {
            ShowAlert(Message: "There is no Location To Show", Seconds: 2)
            return
        }
        guard let Org_long:Double = RequestData?.route?.destination_point?.longitude else {
            ShowAlert(Message: "There is no Location To Show", Seconds: 2)
            return
        }
        Origin_Point = nil
        Dest_Point = CLLocationCoordinate2D(latitude: Org_lat, longitude: Org_long)
        performSegue(withIdentifier: "From_AcceptedOfferDetails_To_Map_Segue", sender: self)
    }

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        if segue.identifier == "From_AcceptedOffersDetails_To_Trackers_Segue" {
            if let Instance = segue.destination as? TrackersViewController{
                Instance.Request_Id = self.Request_Id
                Instance.Unwind_Segue_Identifier = "Unwind_From_Trackers_To_AcceptedOfferDetails_Segue"
            }
        }
        else if segue.identifier == "From_AcceptedOffersDetails_To_CustomerProfile_Segue" {
            if let Instance = segue.destination as? CustomerProfileViewController{
                Instance.User_Id = self.User_Id
                Instance.Unwind_Segue_Identifier = "Unwind_From_CustomerProfile_To_AcceptedOfferDetails_Segue"
            }
        }
        else if segue.identifier == "From_AcceptedOfferDetails_To_Map_Segue" {
            if let Instance = segue.destination as? MapViewController{
                Instance.SetCoordinate(Origin_Point: Origin_Point, Dest_Point: Dest_Point)
                Instance.Unwind_Identifier = "UnwindFrom_Map_To_AcceptedOffersDetails_Segue"
            }
        }
    }
    
    // MARK: - Network
    
    var RequestData:TruckRequestModel?
    
    private func RequestToGetTruckDetail(){
        if Request_Id != 0 {
            UserConnectionManager.RequestWithHeader(URL: DOMAIN_URL + Truck_Request_URL + "/\(Request_Id)", RequestMethod: .get, Parameter: nil,
            { (HTTPStatus, RecievedJSON) in
                print("getting Request Details Succeed with code \(HTTPStatus)")
                self.RequestData = TruckRequestModel(RecievedJSON!["truck_request"])
//                self.User_Id = self.RequestData?.customer?.id ?? 0
                self.UpdateShipmentTypeViews()
                self.UpdateIncommonViews()
            }) { (HTTPStatus, ErrorArray) in
                print("getting Request Details failed with code \(HTTPStatus)")
                self.ShowAlert(Message: ErrorArray?[0].body ?? "Check Your Connection", Seconds: 2)
            }
        }else {
            print("Request Id Is zero There is somthing wrong")
        }
    }
    

}
