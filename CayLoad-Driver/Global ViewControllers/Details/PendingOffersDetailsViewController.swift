//
//  PendingOffersDetailsViewController.swift
//  CayLoad-Driver
//
//  Created by Kiyarash Vosough on 2/24/1399 AP.
//  Copyright © 1399 CayLoad. All rights reserved.
//

import UIKit
import CoreLocation

class PendingOffersDetailsViewController: UIViewController,UITextFieldDelegate {
    
    @IBOutlet weak var Detail_Label: UILabel!
    @IBOutlet weak var Back_Button: UIButton!
   
    @IBOutlet weak var Main_Scrollview: UIScrollView!
    @IBOutlet weak var Scrolview_Content_View: UIView!
   
    @IBOutlet weak var Container1_View: UIView!
    @IBOutlet weak var Container1_Const_Label: UILabel!
    @IBOutlet weak var Container1_Data_Label: UILabel!
   
    @IBOutlet weak var Container2_View: UIView!
    @IBOutlet weak var Container2_Const_Label: UILabel!
    @IBOutlet weak var Container2_Data_Label: UILabel!
   
    @IBOutlet weak var Container3_View: UIView!
    @IBOutlet weak var Container3_Const_Label: UILabel!
    @IBOutlet weak var Container3_Data_Label: UILabel!
   
    @IBOutlet weak var Container4_View: UIView!
    @IBOutlet weak var Container4_Const_Label: UILabel!
    @IBOutlet weak var Container4_Data_Label: UILabel!
   
    @IBOutlet weak var Container5_View: UIView!
    @IBOutlet weak var Container5_Const_Label: UILabel!
    @IBOutlet weak var Container5_Data_Label: UILabel!
   
    @IBOutlet weak var Container6_View: UIView!
    @IBOutlet weak var Container6_Const_Label: UILabel!
    @IBOutlet weak var Container6_Data_Label: UILabel!
   
    @IBOutlet weak var Container7_View: UIView!
    @IBOutlet weak var Container7_Const_Label: UILabel!
    @IBOutlet weak var Container7_Data_Label: UILabel!
   
    @IBOutlet weak var Container8_View: UIView!
    @IBOutlet weak var Container8_Const_Label: UILabel!
    @IBOutlet weak var Container8_Data_Label: UILabel!
   
    @IBOutlet weak var CargoDescription_View: UIView!
    @IBOutlet weak var CargoDescription_Label: UILabel!
    @IBOutlet weak var CargoDescription_Textview: UITextView!
   
    @IBOutlet weak var Origin_View: UIView!
    @IBOutlet weak var Origin_Label: UILabel!
    @IBOutlet weak var Origin_Textview: UITextView!
    @IBOutlet weak var OriginMap_Button: UIButton!
    @IBOutlet weak var OriginDate_View: UIView!
    @IBOutlet weak var OriginDate_Label: UILabel!
   
    @IBOutlet weak var Destination_View: UIView!
    @IBOutlet weak var Destination_Label: UILabel!
    @IBOutlet weak var Destination_Textview: UITextView!
    @IBOutlet weak var DestinationMap_Button: UIButton!
    @IBOutlet weak var DestinationDate_View: UIView!
    @IBOutlet weak var DestinationDate_Label: UILabel!
   
    @IBOutlet weak var RequestDescription_View: UIView!
    @IBOutlet weak var RequestDescription_Label: UILabel!
    @IBOutlet weak var RequestDescription_Textview: UITextView!
    
    
    @IBOutlet weak var CustomerProfile_ImageView: UIImageView!
    @IBOutlet weak var CustomerUsername_Label: UILabel!
    @IBOutlet weak var Goto_ProfileDetails_Button: UIButton!
    
    
    @IBOutlet weak var YourOffer_Label: UILabel!
    
    @IBOutlet weak var OfferPrice_Label: UILabel!
    @IBOutlet weak var OfferedPrice_TextField: UITextField!
    
    @IBOutlet weak var OfferDescription_Label: UILabel!
    @IBOutlet weak var OfferDescription_TextField: UITextField!
    
    @IBOutlet weak var SendOffer_Button: UIButton!
    
    
    // MARK: - KeyBoardManager
    
    @objc func adjustForKeyboard(notification: Notification) {
        guard let keyboardValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue else { return }

        let keyboardScreenEndFrame = keyboardValue.cgRectValue
        let keyboardViewEndFrame = view.convert(keyboardScreenEndFrame, from: view.window)

        if notification.name == UIResponder.keyboardWillHideNotification {
            Main_Scrollview.contentInset = .zero
        } else {
            Main_Scrollview.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardViewEndFrame.height - view.safeAreaInsets.bottom + 20, right: 0)
        }
        Main_Scrollview.scrollIndicatorInsets = Main_Scrollview.contentInset
    }
        
    func textFieldShouldReturn(_ Sender: UITextField) -> Bool
    {
        Sender.resignFirstResponder()
        return true
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        RequestToGetTruckDetail()
        AddTarget()
        UpdateViewCornerRadius()
        AddDelegate()
        // Do any additional setup after loading the view.
    }
    
    
    private func AddDelegate(){
        OfferDescription_TextField.delegate = self
        OfferedPrice_TextField.delegate = self
        OfferDescription_TextField.delegate = self
    }

    private func AddTarget(){
        Back_Button.addTarget(self, action: #selector(OnBackButtonClicked(_:)), for: .touchUpInside)
        SendOffer_Button.addTarget(self, action: #selector(OnSendOfferButtonClicked(_:)), for: .touchUpInside)
        OriginMap_Button.addTarget(self, action: #selector(OnShowOriginButtonClicked(_:)), for: .touchUpInside)
        DestinationMap_Button.addTarget(self, action: #selector(OnShowDestButtonClicked(_:)), for: .touchUpInside)
        
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(adjustForKeyboard), name: UIResponder.keyboardWillHideNotification, object: nil)
        notificationCenter.addObserver(self, selector: #selector(adjustForKeyboard), name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
    }
    
    private func UpdateViewCornerRadius(){
        Container8_View.SetSpeceficCornerRadius(Radius: 9, TopLeft: false, BottomLeft: true, TopRight: false, BottomRight: true)
        Origin_View.SetSpeceficCornerRadius(Radius: 9, TopLeft: true, BottomLeft: false, TopRight: true, BottomRight: false)
        OriginDate_View.SetSpeceficCornerRadius(Radius: 9, TopLeft: false, BottomLeft: true, TopRight: false, BottomRight: true)
        Destination_View.SetSpeceficCornerRadius(Radius: 9, TopLeft: true, BottomLeft: false, TopRight: true, BottomRight: false)
        DestinationDate_View.SetSpeceficCornerRadius(Radius: 9, TopLeft: false, BottomLeft: true, TopRight: false, BottomRight: true)
        
    }
    
    private func UpdateShipmentTypeViews(){
        
        if RequestData?.cargo?.container_type == nil {
            //bulk
            if Container1_View != nil && Scrolview_Content_View.contains(Container1_View){
                Container1_View.removeFromSuperview()
            }
            if Container2_View != nil && Scrolview_Content_View.contains(Container2_View){
                Container2_View.removeFromSuperview()
            }
            Container3_View.topAnchor.constraint(equalTo: Scrolview_Content_View.topAnchor,constant: 15).isActive = true
            
            Container3_View.SetSpeceficCornerRadius(Radius: 9, TopLeft: true, BottomLeft: false, TopRight: true, BottomRight: false)
            
            Container3_Const_Label.text = "Shipment Type".localized()
            Container3_Data_Label.text = "Bulk".localized()
            
            Container4_Const_Label.text = "Commodity".localized()
            Container4_Data_Label.text = RequestData?.cargo?.commodity
            
            Container5_Const_Label.text = "Net Weight".localized()
            Container5_Data_Label.text = "\(RequestData?.cargo?.net_weight ?? 0)"
            
            Container6_Const_Label.text = "Gross Weight".localized()
            Container6_Data_Label.text = "\(RequestData?.cargo?.total_weight ?? 0)"
            
            Container7_Const_Label.text = "Number of packages".localized()
            Container7_Data_Label.text = "\(RequestData?.cargo?.packages_count ?? 0)"
            
            Container8_Const_Label.text = "HSCode".localized()
            Container8_Data_Label.text = RequestData?.cargo?.hs_code
        }
        else {
            Container1_View.SetSpeceficCornerRadius(Radius: 9, TopLeft: true, BottomLeft: false, TopRight: true, BottomRight: false)
            Container1_Const_Label.text = "Shipment Type".localized()
            Container1_Data_Label.text = "Containerized".localized()
            
            Container2_Const_Label.text = "Container Details".localized()
            Container2_Data_Label.text = (RequestData?.cargo?.container_type?.title ?? "") + " | " + (RequestData?.cargo?.container_size?.title ?? "")
            
            Container3_Const_Label.text = "Owner Container".localized()
            Container3_Data_Label.text = RequestData?.cargo?.container_owner
            
            Container4_Const_Label.text = "Commodity".localized()
            Container4_Data_Label.text = RequestData?.cargo?.commodity
            
            Container5_Const_Label.text = "Cargo Weight".localized()
            Container5_Data_Label.text = "\(RequestData?.cargo?.net_weight ?? 0)"
            
            Container6_Const_Label.text = "Total Weight include Tare Weight".localized()
            Container6_Data_Label.text = "\(RequestData?.cargo?.total_weight ?? 0)"
            
            Container7_Const_Label.text = "Number of packages".localized()
            Container7_Data_Label.text = "\(RequestData?.cargo?.packages_count ?? 0)"
            
            Container8_Const_Label.text = "HSCode".localized()
            Container8_Data_Label.text = RequestData?.cargo?.hs_code
        }
    }
    
    private func UpdateIncommonViews(){
        
        if RequestData?.cargo != nil {
            CargoDescription_Textview.text = RequestData?.cargo?.description
            CargoDescription_Textview.isEditable = false
        }
        Origin_Textview.text = RequestData?.route?.origin_point?.formatted_address
        OriginDate_Label.text = RequestData?.route?.origin_datetime?.GetFormatedDate()
        Origin_Textview.isEditable = false
        
        Destination_Textview.text = RequestData?.route?.destination_point?.formatted_address
        DestinationDate_Label.text = RequestData?.route?.destination_datetime?.GetFormatedDate()
        Destination_Textview.isEditable = false
        
        RequestDescription_Textview.text = RequestData?.description
        RequestDescription_Textview.isEditable = false
    }
    
    private func UpdateCustomerView(){
        CustomerProfile_ImageView.LoadImageFromServer(url: try! (PURE_DOMAIN_URL + (RequestData?.customer?.profile_image?.file ?? "")).asURL())
        CustomerUsername_Label.text = (RequestData?.customer?.user?.first_name ?? "") + " " + (RequestData?.customer?.user?.last_name ?? "")
        Goto_ProfileDetails_Button.addTarget(self, action: #selector(OnGotoProfileDetailsButtonClicked(_:)), for: .touchUpInside)
        
    }
    

    // MARK: - Network
    
    var RequestData:TruckRequestModel?
    
    private func RequestToGetTruckDetail(){
        if Request_Id != 0 {
            UserConnectionManager.RequestWithHeader(URL: DOMAIN_URL + Truck_Request_URL + "/\(Request_Id)", RequestMethod: .get, Parameter: nil,
            { (HTTPStatus, RecievedJSON) in
                print("getting Pending Request Details Succeed with code \(HTTPStatus)")
                self.RequestData = TruckRequestModel(RecievedJSON!["truck_request"])
                self.UpdateShipmentTypeViews()
                self.UpdateIncommonViews()
                self.UpdateCustomerView()
            }) { (HTTPStatus, ErrorArray) in
                print("getting Pending Request Details failed with code \(HTTPStatus)")
                self.ShowAlert(Message: ErrorArray?[0].body ?? "Check Your Connection", Seconds: 2)
            }
        }else {
            print("Request Id Is zero There is somthing wrong")
        }
    }
    
    private func RequestToSendOffer(param:[String:Any]){
        
        UserConnectionManager.RequestWithHeader(URL: DOMAIN_URL + Truck_Request_URL + "/\(Request_Id)" + Send_Offer_URL, RequestMethod: .post, Parameter: param,
        { (HTTPStatus, RecievedJSON) in
            print("Sending Offer Succeed with code \(HTTPStatus)")
            self.ShowAlert(Message: "Offer Sent", Seconds: 3)
            Thread.sleep(forTimeInterval: 2)
            self.NavigateBack()
        }) { (HTTPStatus, ErrorArray) in
            print("Sending Offer failed with code \(HTTPStatus)")
        }
    }
    
    private func GetSendOfferParam() ->[String:Any]? {
        guard let price = OfferedPrice_TextField.text else {
            ShowAlert(Message: "Enter Your Offer Price", Seconds: 2)
            return nil
        }
        let  description:String = OfferDescription_TextField.text ?? ""
        
        return ["description":description,"price":price]
    }
    
    // MARK: - Navigation
    
    var Request_Id:Int = 0
    var Unwind_Identifier:String = ""
    private var Origin_Point:CLLocationCoordinate2D?
    private var Dest_Point:CLLocationCoordinate2D?
    
    private func NavigateBack(){
        if !Unwind_Identifier.isEmpty {
            performSegue(withIdentifier: Unwind_Identifier , sender: self)
        }else{
            navigationController?.popToRootViewController(animated: true)
        }
    }
    
    @IBAction func UnwindFrom_MapViewController_To_PendingOffersDetailsViewController(segue: UIStoryboardSegue){
    }
    
    @IBAction func UnwindFrom_CustomerProfileViewController_To_PendingOffersDetailsViewController(segue:UIStoryboardSegue) {
        
//        if segue.source is CustomerProfileViewController {
//        }
    }
    
    @objc private func OnBackButtonClicked(_ sender:UIButton){
        NavigateBack()
    }
    
    @objc private func OnGotoProfileDetailsButtonClicked(_ sender:UIButton){
        performSegue(withIdentifier: "From_PendingOfferDetails_To_CustomerProfile_Segue", sender: self)
    }
    
    @objc private func OnShowOriginButtonClicked(_ sender:UIButton){
        
        guard let Org_lat:Double = RequestData?.route?.origin_point?.latitude else {
            ShowAlert(Message: "There is no Location To Show", Seconds: 2)
            return
        }
        guard let Org_long:Double = RequestData?.route?.origin_point?.longitude else {
            ShowAlert(Message: "There is no Location To Show", Seconds: 2)
            return
        }
        Dest_Point = nil
        Origin_Point = CLLocationCoordinate2D(latitude: Org_lat, longitude: Org_long)
        performSegue(withIdentifier: "From_PendingOfferDetails_To_Map_Segue", sender: self)
    }
    
    @objc private func OnShowDestButtonClicked(_ sender:UIButton){
        guard let Org_lat:Double = RequestData?.route?.destination_point?.latitude else {
            ShowAlert(Message: "There is no Location To Show", Seconds: 2)
            return
        }
        guard let Org_long:Double = RequestData?.route?.destination_point?.longitude else {
            ShowAlert(Message: "There is no Location To Show", Seconds: 2)
            return
        }
        Origin_Point = nil
        Dest_Point = CLLocationCoordinate2D(latitude: Org_lat, longitude: Org_long)
        performSegue(withIdentifier: "From_PendingOfferDetails_To_Map_Segue", sender: self)
    }
    
    @objc private func OnSendOfferButtonClicked(_ sender:UIButton){
        
        guard let param = GetSendOfferParam() else {
            return
        }
        RequestToSendOffer(param: param)
    }

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        if segue.identifier == "From_PendingOfferDetails_To_Map_Segue" {
            if let Map = segue.destination as? MapViewController{
                Map.SetCoordinate(Origin_Point: Origin_Point, Dest_Point: Dest_Point)
                Map.Unwind_Identifier = "UnwindFrom_Map_To_PendingOffersDetails_Segue"
            }
        }
        else if segue.identifier == "From_PendingOfferDetails_To_CustomerProfile_Segue" {
            if let Profile = segue.destination as? CustomerProfileViewController{
                Profile.User_Id = RequestData?.customer?.id ?? 0
                Profile.Unwind_Segue_Identifier = "UnwindFrom_CustomerProfile_To_PendingOffersDetails_Segue"
            }
        }
    }
    

}
