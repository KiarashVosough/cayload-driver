//
//  LocationAccessGetterViewController.swift
//  CayLoad-Driver
//
//  Created by Kiyarash Vosough on 2/28/1399 AP.
//  Copyright © 1399 CayLoad. All rights reserved.
//

import UIKit
import CoreLocation

class LocationAccessGetterViewController: UIViewController,CLLocationManagerDelegate {

    @IBOutlet weak var EnableLocationAccess_Button: UIButton!
    
    var locationManager: CLLocationManager?
    var AccessGranted:Bool = false
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if (status == .denied || status == .notDetermined || status == .restricted || status == .authorizedWhenInUse) && CLLocationManager.locationServicesEnabled() {
            locationManager?.requestAlwaysAuthorization()
        }
        else{
            if !Unwind_Identifier.isEmpty {
                let appDelegate: AppDelegate? = UIApplication.shared.delegate as? AppDelegate
                appDelegate?.applicationWillEnterForeground(UIApplication.shared)
                AccessGranted = true
                performSegue(withIdentifier: Unwind_Identifier, sender: self)
            }else {
                navigationController?.popViewController(animated: true)
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        EnableLocationAccess_Button.addTarget(self, action: #selector(OnEnableLocationAccessButtonClicked(_:)), for: .touchUpInside)
    }
    

    
    
    // MARK: - Navigation
    
    var Unwind_Identifier:String = ""
    
    @objc func OnEnableLocationAccessButtonClicked(_ sender:UIButton){
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .notDetermined, .restricted, .denied, .authorizedWhenInUse:
                    print("No access")
                    locationManager = CLLocationManager()
                    locationManager?.delegate = self
//                    locationManager?.requestWhenInUseAuthorization()
                    locationManager?.requestAlwaysAuthorization()
            case .authorizedAlways:
                AccessGranted = true
                if !Unwind_Identifier.isEmpty {
                    let appDelegate: AppDelegate? = UIApplication.shared.delegate as? AppDelegate
                    appDelegate?.applicationWillEnterForeground(UIApplication.shared)
                    performSegue(withIdentifier: Unwind_Identifier, sender: self)
                }else{
                    navigationController?.popViewController(animated: true)
                }
                @unknown default:
                break
            }
        }
        else {
            ShowAlert(Message: "Location services are not enabled", Seconds: 2)
        }
    }


}
