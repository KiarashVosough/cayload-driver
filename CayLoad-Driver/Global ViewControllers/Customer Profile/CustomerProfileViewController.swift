//
//  CustomerProfileViewController.swift
//  CayLoad-Driver
//
//  Created by Kiyarash Vosough on 2/11/1399 AP.
//  Copyright © 1399 CayLoad. All rights reserved.
//

import UIKit

class CustomerProfileViewController: UIViewController {
    
    @IBOutlet weak var Profile_ImageView: UIImageView!
    @IBOutlet weak var Back_Button: UIButton!
    @IBOutlet weak var Call_Button: UIButton!
    @IBOutlet weak var UserName_Label: UILabel!
    
    @IBOutlet weak var ScrollContent_View: UIView!
    ///
        @IBOutlet weak var Email_View: UIView!
        @IBOutlet weak var Email_Label: UILabel!
        @IBOutlet weak var EmailAddress_Label: UILabel!
        
        @IBOutlet weak var Phone_View: UIView!
        @IBOutlet weak var Phone_Label: UILabel!
        @IBOutlet weak var PhoneNumber_Label: UILabel!
        
        @IBOutlet weak var CompanyName_View: UIView!
        @IBOutlet weak var CompanyName_Label: UILabel!
        @IBOutlet weak var CompanyNameInfo_Label: UILabel!
        
        @IBOutlet weak var CompanyDetail_View: UIView!
        @IBOutlet weak var CompanyDetail_Label: UILabel!
        @IBOutlet weak var CompanyDetail_TextView: UITextView!
        
        @IBOutlet weak var CompanyLogo_View: UIView!
        @IBOutlet weak var CompanyLogo_Label: UILabel!
        @IBOutlet weak var CompanyLogo_ImageView: UIImageView!
        
        @IBOutlet weak var ChatWithCustomer_Button: UIButton!
    ///
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        RequestToGetProfile()
        AddTarget()
        SetViewsAttributes()
    }
    
    private func AddTarget(){
        Call_Button.addTarget(self, action: #selector(OnCallButtonClicked(_:)), for: .touchUpInside)
        Back_Button.addTarget(self, action: #selector(OnBackButtonClicked), for: .touchUpInside)
        ChatWithCustomer_Button.addTarget(self, action: #selector(OnChatWithCustomerButtonClicked), for: .touchUpInside)
    }
    
    private func SetViewsAttributes(){
        CompanyDetail_TextView.isEditable = false
    }
    
    private func UpdateViews(){
        Profile_ImageView.LoadImageFromServer(url: try! (PURE_DOMAIN_URL + (Customer?.profile_image?.file ?? "")).asURL())
        UserName_Label.text = (Customer?.user?.first_name ?? "") + " " + (Customer?.user?.last_name ?? "")
        if Customer?.primary_email != nil {
            EmailAddress_Label.text = Customer?.primary_email?.address
        }
        if Customer?.primary_phone != nil {
            Phone_Number = Int(Customer?.primary_phone?.number ?? "")
            PhoneNumber_Label.text = Customer?.primary_phone?.number
        }
        CompanyNameInfo_Label.text = Customer?.company_name
        CompanyDetail_TextView.text = Customer?.company_description
        
        CompanyLogo_ImageView.LoadImageFromServer(url: try! (PURE_DOMAIN_URL + (Customer?.company_logo?.file ?? "")).asURL())
    }
    
    // MARK: - Network
    
    var User_Id:Int = 0
    var Customer:CustomerModel?
    var Phone_Number:Int?
    
    private func RequestToGetProfile(){
        UserConnectionManager.RequestWithHeader(URL: DOMAIN_URL + Customers_URL + "\(User_Id)", RequestMethod: .get, Parameter: nil, { (HTTPStatus, RecievedJSON) in
            self.Customer = CustomerModel(RecievedJSON!["customer"])
            print("Getting Customer Profile Succeed With Code \(HTTPStatus)")
            self.UpdateViews()
        }) { (HTTPStatus, ErrorArray) in
            self.ShowAlert(Message: ErrorArray?[0].body ?? "Check Your Connection".localized(), Seconds: 2)
            print("Getting Customer Profile Failed With Code \(HTTPStatus)")
        }
    }
    
    // MARK: - Navigation
    
    var Unwind_Segue_Identifier:String?
    
    @objc func OnCallButtonClicked(_ sender:UIButton){
        guard let phone = Customer?.primary_phone?.number else {
            return
        }
        if let PhoneURL = URL(string: "tel://\(phone)"){
            let application = UIApplication.shared
            if application.canOpenURL(PhoneURL) {
                application.open(PhoneURL, options: [:], completionHandler: nil)
            }
        }
    }
    
    @objc func OnBackButtonClicked(_ sender:UIButton){
        if Unwind_Segue_Identifier != nil || Unwind_Segue_Identifier != "" {
            performSegue(withIdentifier: Unwind_Segue_Identifier ?? "", sender: self)
        }
    }
    
    @objc func OnChatWithCustomerButtonClicked(_ sender:UIButton){
        performSegue(withIdentifier: "From_CustomerProfile_To_Chat_Segue", sender: self)
    }

    @IBAction func UnwindFrom_ChatViewController_To_CustomerProfileController(segue: UIStoryboardSegue){
        /*if let source = segue.source as? ChatViewController {
        }*/
    }
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        if segue.identifier == "From_CustomerProfile_To_Chat_Segue" {
            if let instance = segue.destination as? ChatViewController {
                instance.Unwind_Identifier = "Unwind_From_Chat_To_CustomerProfile_Segue"
                instance.With = .Customer
                
                if let first = Customer?.user?.first_name,let last = Customer?.user?.last_name {
                    instance.Username = first + " " + last
                }else{
                    instance.Username = "Customer".localized()
                }
                instance.CustomerId = Customer?.id ?? 0
                instance.ThisUserId = UserDefaults.standard.integer(forKey: "driver_id")
            }
        }
    }

}
