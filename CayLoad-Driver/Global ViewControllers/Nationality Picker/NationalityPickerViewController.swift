//
//  NationalityPickerViewController.swift
//  CayLoad-Driver
//
//  Created by Kiyarash Vosough on 1/28/1399 AP.
//  Copyright © 1399 CayLoad. All rights reserved.
//

import UIKit
import SwiftyJSON

class NationalityPickerViewController: UIViewController,UITextFieldDelegate {

    var UnwindToViewControllerID:String = ""
    private var TapGesture:UITapGestureRecognizer!
    private var EditingChanged: Bool = false
    private let Menu_View = UIView()
    private var Menu_Height = CGFloat()
    private var SelectedNationality:UserNationalitiesModel!
    private var DisplayingArrayOfNationality = [UserNationalitiesModel]()
    
    lazy var NationalityTableView: UITableView = {
      let tbView = UITableView()
      tbView.dataSource = self
      tbView.delegate = self
      tbView.register(LanguagesCell.self, forCellReuseIdentifier: "NationalitiesCell")
      return tbView
    }()
      
    var Search_TextField:UITextField = {
        let search = UITextField()
        search.keyboardType = .alphabet
        search.returnKeyType = .search
        return search
    }()
      

    lazy var BackDrop_View: UIView = {
        let bdView = UIView(frame: self.view.bounds)
        bdView.backgroundColor = UIColor.black.withAlphaComponent(0)
        return bdView
    }()


    override func viewDidLoad() {
        super.viewDidLoad()
        GetNationalities()
        //add gesture
        TapGesture = UITapGestureRecognizer(target: self, action: #selector(TapOnBackDropViewAction(_:)))
        BackDrop_View.addGestureRecognizer(TapGesture)
        
        //Add action for search
        Search_TextField.addTarget(self, action: #selector(SearchLanguage), for: .editingChanged)
            
        Menu_Height = CGFloat(7*65 + 80)
        
        //add views
        addSearchBar()
        view.backgroundColor = .clear
        view.addSubview(BackDrop_View)
        OnCreateMenuView()
        
      }
    
    @objc func TapOnBackDropViewAction(_ sender: UITapGestureRecognizer) {
        if EditingChanged {
            Search_TextField.text = ""
            DisplayingArrayOfNationality = UserNationalities
            self.NationalityTableView.reloadData()
            view.endEditing(true)
            EditingChanged = false
            
        }else{
            dismiss(animated: true) {
            }
        }
        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let textFieldRange = NSRange(location: 0, length: textField.text?.count ?? 0)
        if NSEqualRanges(range, textFieldRange) && string.count == 0 {
            Search_TextField.text = ""
            DisplayingArrayOfNationality = UserNationalities
            self.NationalityTableView.reloadData()
            Search_TextField.resignFirstResponder()
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        Search_TextField.text = ""
        Search_TextField.resignFirstResponder()
        DisplayingArrayOfNationality = UserNationalities
        self.NationalityTableView.reloadData()
        EditingChanged = false
        return true
    }
    
    @objc func SearchLanguage(_ sender: UITextField){
        EditingChanged = true
        if sender.text != nil {
            DisplayingArrayOfNationality.removeAll()
            for Item in UserNationalities {
                if Item.name.localizedCaseInsensitiveContains(sender.text!) {
                    DisplayingArrayOfNationality.append(Item)
                }
            }
            self.NationalityTableView.reloadData()
        }
    }
      
    private func addSearchBar() {
        Menu_View.addSubview(Search_TextField)
        Menu_View.addSubview(NationalityTableView)
          
        Search_TextField.snp.makeConstraints { (make) in
          make.top.equalToSuperview().offset(10)
          make.leading.equalToSuperview().offset(8)
          make.trailing.equalToSuperview().offset(-8)
          make.height.equalTo(60)
          Search_TextField.delegate = self
          
          let view = UIView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
          
          Search_TextField.layer.cornerRadius = 30
          Search_TextField.backgroundColor = UIColor.lightGray.withAlphaComponent(0.1)
          Search_TextField.placeholder = "Type country name".localized()
          Search_TextField.leftView = view
          Search_TextField.leftViewMode = UITextField.ViewMode.always
        }
          
        NationalityTableView.snp.makeConstraints { (make) in
          make.top.equalTo(Search_TextField.snp.bottom).offset(10)
          make.leading.trailing.equalToSuperview()
          make.bottom.equalToSuperview()
        }
          
    }
    
    private func OnCreateMenuView(){
        view.addSubview(Menu_View)
        Menu_View.layer.cornerRadius = 30
        Menu_View.backgroundColor = .white
        Menu_View.translatesAutoresizingMaskIntoConstraints = false
        Menu_View.heightAnchor.constraint(equalToConstant: Menu_Height).isActive = true
        Menu_View.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        Menu_View.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        Menu_View.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
    }
    // MARK: - Network
    var UserNationalities = [UserNationalitiesModel]()
    
    func GetNationalities(){
        
        UserConnectionManager.Request(URL: "\(DOMAIN_URL)\(Get_User_Nationalities_URL)", RequestMethod: .get, Parameter: nil, { (HTTPStatus, RecievedJSON) in
            print("Nationalities list recieved successfully with code \(HTTPStatus)")
            self.UserNationalities = JsonParsingUtils.GetArrayList(From: RecievedJSON!, withRootTag: "user_nationalities")
            self.DisplayingArrayOfNationality = self.UserNationalities
            self.NationalityTableView.reloadData()
            
        }) { (HTTPStatus, ErrorArray) in
            print("getting Nationalities list failed with code \(HTTPStatus)")
            
        }
    }
    // MARK: - Navigation
    
    func getSelectedNationalityModel() -> UserNationalitiesModel {
        return SelectedNationality
    }
    
    /*
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    

}

extension NationalityPickerViewController: UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let LangCell = tableView.dequeueReusableCell(withIdentifier: "NationalitiesCell", for: indexPath) as? LanguagesCell
        LangCell?.ConfigCellWithImage(TextName: DisplayingArrayOfNationality[indexPath.row].name, ImageURL:try! "\(PURE_DOMAIN_URL)\(DisplayingArrayOfNationality[indexPath.row].flag?.file ?? "")".asURL())
        return LangCell!
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return DisplayingArrayOfNationality.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 65
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        SelectedNationality = DisplayingArrayOfNationality[indexPath.row]
        performSegue(withIdentifier: UnwindToViewControllerID, sender: self)
    }
}
