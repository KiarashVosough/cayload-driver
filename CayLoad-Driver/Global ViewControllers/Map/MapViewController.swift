//
//  MapViewController.swift
//  Cayload-Driver
//
//  Created by Saeid Hosseinabadi on 5/28/20.
//  Copyright © 2020 Yalda Kheirkhah. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class MapViewController: UIViewController {

    @IBOutlet weak var map: MKMapView!
    @IBOutlet weak var Back_Button: UIButton!
    
    fileprivate let locationManager = CLLocationManager()
    private var originPoint:CLLocationCoordinate2D?
    private var endPoint:CLLocationCoordinate2D?
    var Unwind_Identifier:String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.prepareMapView()
        Back_Button.addTarget(self, action: #selector(OnBackButtonClicked), for: .touchUpInside)
    }
    
    @objc private func OnBackButtonClicked(_ sender:UIButton){
        if !Unwind_Identifier.isEmpty {
            performSegue(withIdentifier: Unwind_Identifier , sender: self)
        }else{
            navigationController?.popToRootViewController(animated: true)
        }
    }
    
    func SetCoordinate(Origin_Point:CLLocationCoordinate2D?, Dest_Point:CLLocationCoordinate2D?){
        originPoint = Origin_Point
        endPoint = Dest_Point
    }
    
    // Map View
    fileprivate func prepareMapView(){
        
        // For use in foreground
        self.locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            
            self.locationManager.delegate = self
            self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
            self.locationManager.startUpdatingLocation()
        }
        
        _ = UILongPressGestureRecognizer(target: self, action: #selector(DropPinOnMap(_:)))
        self.map.delegate = self
        self.map.showsUserLocation = true
        self.map.showsBuildings = true
//        self.map.addGestureRecognizer(DropPinGesture)
        
        if let coor = self.locationManager.location?.coordinate{//self.mapView.userLocation.location?.coordinate{
            
            self.map.setCenter(coor, animated: true)
            
            let span = MKCoordinateSpan(latitudeDelta: 0.03, longitudeDelta: 0.03)
            let region = MKCoordinateRegion(center: coor, span: span)
            self.map.setRegion(region, animated: true)
        }
        
        if originPoint != nil {
            let myAnnotation: MKPointAnnotation = MKPointAnnotation()
            
            myAnnotation.coordinate = originPoint!
            myAnnotation.title = 0.description

            self.map.addAnnotation(myAnnotation)
            fitAllAnnotations(Annotation: myAnnotation)
        }else {
            let myAnnotation: MKPointAnnotation = MKPointAnnotation()
            
            myAnnotation.coordinate = endPoint!
            myAnnotation.title = 1.description

            self.map.addAnnotation(myAnnotation)
            fitAllAnnotations(Annotation: myAnnotation)
        }
        
        /*var i = 0
        [originPoint,endPoint].forEach({ (point) in

            let myAnnotation: MKPointAnnotation = MKPointAnnotation()
            
            myAnnotation.coordinate = point
            myAnnotation.title = i.description

            self.map.addAnnotation(myAnnotation)
            fitAllAnnotations(Annotation: myAnnotation)
            i += 1
        })*/
    }
    
    @objc func DropPinOnMap(_ sender:UILongPressGestureRecognizer){
        
        if sender.state == .began{
            let location = sender.location(in: self.map)
            let coordinate = self.map.convert(location, toCoordinateFrom: map)
            // Add annotation:
            let annotation = MKPointAnnotation()
            annotation.coordinate = coordinate
            self.map.addAnnotation(annotation)
        }
    }
    
    func fitAllAnnotations(Annotation:MKPointAnnotation) {
        var zoomRect = MKMapRect.null;
        
        let annotationPoint = MKMapPoint(Annotation.coordinate)
        let pointRect       = MKMapRect(x: annotationPoint.x, y: annotationPoint.y, width: 0.01, height: 0.01);
        zoomRect            = zoomRect.union(pointRect)
        
        self.map.setVisibleMapRect(zoomRect, edgePadding: UIEdgeInsets(top: 100, left: 100, bottom: 100, right: 100), animated: true)
    }

}

// MARK: Map  Delegate
extension MapViewController: MKMapViewDelegate{
    
    // Set Property Of Marker
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {

        guard !(annotation is MKUserLocation) else {
            return nil
        }

        let annotationIdentifier = "Identifier"
        var annotationView: MKAnnotationView?

        if let dequeuedAnnotationView = mapView.dequeueReusableAnnotationView(withIdentifier: annotationIdentifier) {

            annotationView = dequeuedAnnotationView
            annotationView?.annotation = annotation
        }else {

            annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: annotationIdentifier)
        }

        if let annotationView = annotationView {

            annotationView.canShowCallout = true
            annotationView.frame = CGRect(origin: CGPoint.zero, size: CGSize(width: 29, height: 41))
            if annotation.title == "0"{
                annotationView.image = UIImage(named: "OriginPointIc")
            }
            else if annotation.title == "1"{
                annotationView.image = UIImage(named: "DestPointIc")
            }
        }
 
        return annotationView
    }
    
    /*func mapView(_ mapView: MKMapView,viewFor annotation: MKAnnotation) -> MKAnnotationView? {

      // Leave default annotation for user location
      if annotation is MKUserLocation {
        return nil
      }

      let reuseID = "Location"
      var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: reuseID)
      if annotationView == nil {
        let pin = MKAnnotationView(annotation: annotation,
                                   reuseIdentifier: reuseID)
            pin.image = UIImage(named: "OriginPointIc")
            pin.isEnabled = true
            pin.canShowCallout = true

        let label = UILabel(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
            label.textColor = .white
            label.text = "S"//annotation.id // set text here
            pin.addSubview(label)

        annotationView = pin
      } else {
        annotationView?.annotation = annotation
      }

      return annotationView
    }*/
    
    func mapView(_ mapView: MKMapView, didDeselect view: MKAnnotationView) {
//        view.image = UIImage(named: "OriginPointIc")
    }
    

    // Dis Select On Marker
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
//        view.image = UIImage(named: "OriginPointIc")
    }
    
    // Set property of Polygon
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        
        if overlay is MKPolygon {
            let polygonView = MKPolygonRenderer(overlay: overlay)
            polygonView.strokeColor = UIColor.red.withAlphaComponent(0.3)
            polygonView.fillColor = UIColor.red.withAlphaComponent(0.1)
            return polygonView
        }
        
        return MKOverlayRenderer(overlay: overlay)
    }
}

// MARK: Core Location Delegate
extension MapViewController: CLLocationManagerDelegate{
    
    // Did Update User Location
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
    }
}
