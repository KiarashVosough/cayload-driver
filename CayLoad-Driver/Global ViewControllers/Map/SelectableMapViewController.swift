//
//  SelectableMapViewController.swift
//  CayLoad-Driver
//
//  Created by Kiyarash Vosough on 3/8/1399 AP.
//  Copyright © 1399 CayLoad. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class SelectableMapViewController: UIViewController {
    
    @IBOutlet weak var map: MKMapView!
    @IBOutlet weak var Back_Button: UIButton!
    @IBOutlet weak var Remove_Mark_Button: UILocalizationButton!
    @IBOutlet weak var Confirm_Button: UILocalizationButton!
    
    fileprivate let locationManager = CLLocationManager()
    private var Selected_Location:CLLocationCoordinate2D?
    var IsOrigin:Bool = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.prepareMapView()
        Back_Button.addTarget(self, action: #selector(OnBackButtonClicked), for: .touchUpInside)
        Remove_Mark_Button.addTarget(self, action: #selector(OnRemoveMarkButtonClicked), for: .touchUpInside)
        Confirm_Button.addTarget(self, action: #selector(OnConfirmButtonClicked), for: .touchUpInside)
    }
    
     fileprivate func prepareMapView(){
            
        // For use in foreground
        self.locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            
            self.locationManager.delegate = self
            self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
            self.locationManager.startUpdatingLocation()
        }
        let PinGesture = UILongPressGestureRecognizer(target: self, action: #selector(DropPinOnMap(_:)))
        self.map.delegate = self
        self.map.showsUserLocation = true
        self.map.showsBuildings = true
        self.map.addGestureRecognizer(PinGesture)
        
        if let coor = self.locationManager.location?.coordinate{//self.mapView.userLocation.location?.coordinate{
            
            self.map.setCenter(coor, animated: true)
            
            let span = MKCoordinateSpan(latitudeDelta: 0.03, longitudeDelta: 0.03)
            let region = MKCoordinateRegion(center: coor, span: span)
            self.map.setRegion(region, animated: true)
        }
            
    }
        
    private func fitAllAnnotations(Annotation:MKPointAnnotation) {
        var zoomRect = MKMapRect.null
        let annotationPoint = MKMapPoint(Annotation.coordinate)
        let pointRect = MKMapRect(x: annotationPoint.x, y: annotationPoint.y, width: 0.01, height: 0.01)
        zoomRect = zoomRect.union(pointRect)
        self.map.setVisibleMapRect(zoomRect, edgePadding: UIEdgeInsets(top: 100, left: 100, bottom: 100, right: 100), animated: true)
    }

    
    // MARK: - Navigation
    
    var Pin_Num:Int = 0
    
    @objc private func OnBackButtonClicked(_ sender:UIButton){
        performSegue(withIdentifier: "Unwind_From_SelectableMap_To_NewFavoriteRoute_Segue", sender: self)
    }
    
    @objc private func OnRemoveMarkButtonClicked(_ sender:UIButton){
        Selected_Location = nil
        if !self.map.annotations.isEmpty{
            self.map.annotations.forEach { (Annotation) in
                self.map.removeAnnotation(Annotation)
                Pin_Num = Pin_Num - 1
                
            }
        }
    }
    
    @objc private func OnConfirmButtonClicked(_ sender:UIButton){
        if Selected_Location != nil {
            performSegue(withIdentifier: "Unwind_From_SelectableMap_To_NewFavoriteRoute_Segue", sender: self)
        }
    }
    
    @objc func DropPinOnMap(_ sender:UILongPressGestureRecognizer){
        if sender.state == .began && Pin_Num <= 0{
            let location = sender.location(in: self.map)
            let coordinate = self.map.convert(location, toCoordinateFrom: map)
            Selected_Location = coordinate
            // Add annotation:
            let annotation = MKPointAnnotation()
            annotation.coordinate = coordinate
            self.map.addAnnotation(annotation)
            Pin_Num = Pin_Num + 1
        }
    }
    
    func GetLocation() -> CLLocationCoordinate2D?{
        if Selected_Location != nil{
            return Selected_Location
        }else{
            return nil
        }
    }

}//end class

// MARK: Map  Delegate
extension SelectableMapViewController: MKMapViewDelegate{
    
    func mapView(_ mapView: MKMapView,viewFor annotation: MKAnnotation) -> MKAnnotationView? {

      // Leave default annotation for user location
      if annotation is MKUserLocation {
        return nil
      }

      let reuseID = "Location"
      var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: reuseID)
      if annotationView == nil {
        let pin = MKAnnotationView(annotation: annotation,reuseIdentifier: reuseID)
            pin.image = UIImage(named: "OriginPointIc")
            pin.isEnabled = true
            pin.canShowCallout = true

        annotationView = pin
      } else {
        annotationView?.annotation = annotation
      }
      return annotationView
    }
    
    func mapView(_ mapView: MKMapView, didDeselect view: MKAnnotationView) {
//        view.image = UIImage(named: "OriginPointIc")
    }
    

    // Dis Select On Marker
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
//        view.image = UIImage(named: "OriginPointIc")
    }
    
    // Set property of Polygon
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        
        if overlay is MKPolygon {
            let polygonView = MKPolygonRenderer(overlay: overlay)
            polygonView.strokeColor = UIColor.red.withAlphaComponent(0.3)
            polygonView.fillColor = UIColor.red.withAlphaComponent(0.1)
            return polygonView
        }
        
        return MKOverlayRenderer(overlay: overlay)
    }
}

// MARK: Core Location Delegate
extension SelectableMapViewController: CLLocationManagerDelegate{
    
    // Did Update User Location
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
    }
}
