//
//  TrackersCollectionViewCell.swift
//  CayLoad-Driver
//
//  Created by Kiyarash Vosough on 2/18/1399 AP.
//  Copyright © 1399 CayLoad. All rights reserved.
//

import UIKit

class TrackersCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var Profile_Imageview: UIImageView!
    @IBOutlet weak var CustomerName_Label: UILabel!
    @IBOutlet weak var GoToTrackersProfile_Button: UIButton!
    
    var NavigationDelegate:CustomerProfileDelegate?
    
    func SetCellViews(){
        layer.cornerRadius = 9
        Profile_Imageview.layer.cornerRadius = 25
    }
    
    func SetCellData(Tracker:CustomerModel?){
        Profile_Imageview.LoadImageFromServer(url: try! (PURE_DOMAIN_URL + (Tracker?.profile_image?.file ?? "")).asURL())
        CustomerName_Label.text = (Tracker?.user?.first_name ?? "") + (Tracker?.user?.last_name ?? "")
        GoToTrackersProfile_Button.tag = Tracker?.id ?? 0
        GoToTrackersProfile_Button.addTarget(self, action: #selector(OnOpenProfileButtonClicked), for: .touchUpInside)
    }
    
    @objc func OnOpenProfileButtonClicked(_ sender: UIButton){
        NavigationDelegate?.PerformSegueToCustomerProfile(With: sender.tag)
    }
}
