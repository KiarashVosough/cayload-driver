//
//  TrackersViewController.swift
//  CayLoad-Driver
//
//  Created by Kiyarash Vosough on 2/18/1399 AP.
//  Copyright © 1399 CayLoad. All rights reserved.
//

import UIKit

class TrackersViewController: UIViewController,CustomerProfileDelegate {
    
    @IBOutlet weak var Trackers_CollectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Trackers_CollectionView.dataSource = self
        Trackers_CollectionView.delegate = self
        // Do any additional setup after loading the view.
    }
    
     // MARK: - Network
    
    var Request_Id:Int = 0
    var Trackers_Array = [TruckRequestTrackModel]()
    
    private func RequestToGetTrackers(){
        UserConnectionManager.RequestWithHeader(URL: DOMAIN_URL + Truck_Request_URL + "\(Request_Id)", RequestMethod: .get, Parameter: nil,
        { (HTTPStatus, RecievedJSON) in
            print("Getting Tracker succeed with code \(HTTPStatus)")
            self.Trackers_Array = JsonParsingUtils.GetArrayList(From: RecievedJSON!, withRootTag: "truck_requests")
            self.Trackers_CollectionView.reloadData()
        }) { (HTTPStatus, ErrorArray) in
            print("Getting Tracker failed with code \(HTTPStatus)")
            self.ShowAlert(Message: ErrorArray?[0].body ?? "Check Your Connection".localized(), Seconds: 2)
        }
    }
    
    // MARK: - Navigation
    
    var Unwind_Segue_Identifier:String?
    var User_Id:Int = 0
    
    @IBAction func UnwindFrom_CustomerProfileViewController_To_TrackersViewController(segue:UIStoryboardSegue) {
        
//        if segue.source is CustomerProfileViewController {
//        }
    }
    
    func PerformSegueToCustomerProfile(With Id: Int) {
        User_Id = Id
        performSegue(withIdentifier: "From_Trackers_To_CustomerProfile_Segue", sender: self)
    }

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        if segue.identifier == "From_Trackers_To_CustomerProfile_Segue" {
            if let Instance = segue.destination as? CustomerProfileViewController{
                Instance.User_Id = self.User_Id
                Instance.Unwind_Segue_Identifier = "Unwind_From_CustomerProfile_To_Trackers_Segue"
            }
        }
    }
    

}

extension TrackersViewController:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return Trackers_Array.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TrackerCell", for: indexPath) as! TrackersCollectionViewCell
        cell.SetCellData(Tracker: Trackers_Array[indexPath.row].tracker)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
            return CGSize(width: Trackers_CollectionView.frame.width-40, height: 83)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.init(top: 30, left: 0, bottom: 30, right: 0)
    }
    
    
}
