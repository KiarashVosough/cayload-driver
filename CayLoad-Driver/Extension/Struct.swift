//
//  Struct.swift
//  CayLoad-Driver
//
//  Created by Kiyarash Vosough on 1/31/1399 AP.
//  Copyright © 1399 CayLoad. All rights reserved.
//

import Foundation

 struct VerificationMethod: RawRepresentable, Equatable, Hashable {
    /// `CONNECT` method.
    public static let Email = VerificationMethod(rawValue: "ByEmail")
    
    public static let Phone = VerificationMethod(rawValue: "ByPhone")

    public let rawValue: String

    public init(rawValue: String) {
        self.rawValue = rawValue
    }
}

struct ButtonWaitingState: RawRepresentable, Equatable, Hashable {
    /// `CONNECT` method.
    public static let Enabled = ButtonWaitingState(rawValue: "enable")
    
    public static let Disabled = ButtonWaitingState(rawValue: "disable")

    public let rawValue: String

    public init(rawValue: String) {
        self.rawValue = rawValue
    }
}

 struct HTTPError: RawRepresentable, Equatable, Hashable {
    /// `CONNECT` method.
    public static let NoInternet = HTTPError(rawValue: 12163)
    
    public static let Forbidden = HTTPError(rawValue: 403)

    public let rawValue: Int

    public init(rawValue: Int) {
        self.rawValue = rawValue
    }
}

enum LocationError: Error {
    case NoLocationSelected
}

enum CoreDataError: Error {
    case CantSaveData
    case CantLoadData
    case CantDeleteData
    case NoDataRecord
    case CantGetSize
    case CantSendData
}

enum ChatWith {
    case Admin
    case Customer
    case None
}

enum MessageType {
    case JustText
    case JustImage
    case TextAndImage
    case Empty
    case None
}

enum MessageSender {
    case Me
    case Itself
    case None
}

enum MessageAction:String {
    case Create = "create"
    case Delete = "delete"
    case Edit = "update"
    case None
}

enum MessageOption_enum:String {
    case Copy = "Copy"
    case Edit = "Edit"
    case Delete = "Delete"
    case Report = "Report"
    case Cancel = "Cancel"
    case None
}

enum JSONError:Error {
    case BadJSON
    case MissingAnAtrribute
}

enum SocketError:Error {
    case BadURL
    case ErrorOnConnection
    case CantWrite
    case CantGetString
}
