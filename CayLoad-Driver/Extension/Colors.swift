//
//  Colors.swift
//  CayLoad-Driver
//
//  Created by Kiyarash Vosough on 1/31/1399 AP.
//  Copyright © 1399 CayLoad. All rights reserved.
//

import Foundation
import  UIKit

extension UIColor {
    static let App_DarkBlue = UIColor(red: 36/255, green: 53/255, blue: 152/255, alpha: 1)
    static let App_LightGray = UIColor(red: 60/255, green: 60/255, blue: 67/255, alpha: 0.3)
    static let App_ViewController_Background_MilkyWhite = UIColor(red: 242/255, green: 242/255, blue: 242/255, alpha: 1)
    static let App_LightBlueGray = UIColor(red: 237/255, green: 240/255, blue: 244/255, alpha: 1)
    static let App_SeperatorMilkyWhite = UIColor(red: 231/255, green: 235/255, blue: 243/255, alpha: 0.392619)
    static let App_LightGray2 = UIColor(red: 247/255, green: 247/255, blue: 247/255, alpha: 1)
}
