//
//  StoryBoardExtensions.swift
//  CayLoad-Driver
//
//  Created by Kiyarash Vosough on 1/9/1399 AP.
//  Copyright © 1399 CayLoad. All rights reserved.
//

import Foundation
import UIKit

extension UIView {

    @IBInspectable var AllCornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
            layer.masksToBounds = true
        }
    }
    @IBInspectable var TopLeftCornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            
            layer.cornerRadius = newValue
            layer.maskedCorners = [.layerMinXMinYCorner]
            layer.masksToBounds = true
        }
    }
    
    @IBInspectable var TopRightCornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
            layer.maskedCorners = [.layerMaxXMinYCorner]
            layer.masksToBounds = true
        }
    }
    
    @IBInspectable var BottomLeftCornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
            layer.maskedCorners = [.layerMinXMaxYCorner]
            layer.masksToBounds = true
        }
    }
    
    @IBInspectable var BottomRightCornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
            layer.maskedCorners = [.layerMaxXMaxYCorner]
            layer.masksToBounds = true
        }
    }
    @IBInspectable var BorderWidth: CGFloat {
        get {
            return layer.borderWidth
        }
        set {
            layer.borderWidth = newValue
        }
    }

    @IBInspectable var BorderColor: UIColor? {
        get {
            return UIColor(cgColor: layer.borderColor!)
        }
        set {
            layer.borderColor = newValue?.cgColor
        }
    }
}
