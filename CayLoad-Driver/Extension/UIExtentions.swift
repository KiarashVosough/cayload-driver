//
//  UIExtentions.swift
//  CayLoad-Driver
//
//  Created by Kiyarash Vosough on 1/20/1399 AP.
//  Copyright © 1399 CayLoad. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON

extension UIView {
    func SetSpeceficCornerRadius(Radius:CGFloat, TopLeft:Bool, BottomLeft:Bool, TopRight:Bool,BottomRight:Bool){
        self.layer.cornerRadius = Radius
        var CornerArray = CACornerMask()
        if TopLeft {
            CornerArray.insert(.layerMinXMinYCorner)
        }
        if BottomLeft {
            CornerArray.insert(.layerMinXMaxYCorner)
        }
        if TopRight {
            CornerArray.insert(.layerMaxXMinYCorner)
        }
        if BottomRight {
            CornerArray.insert(.layerMaxXMaxYCorner)
        }
        self.layer.maskedCorners = CornerArray
    }
    
    func SetAllCornerRadius(Radius:CGFloat){
        self.layer.cornerRadius = Radius
    }
        
    func addConstraintsWithFormat(format: String, views: UIView...) {
        
        var viewsDictionary = [String: UIView]()
        for (index, view) in views.enumerated() {
            let key = "v\(index)"
            viewsDictionary[key] = view
            view.translatesAutoresizingMaskIntoConstraints = false
        }
        
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: format, options: NSLayoutConstraint.FormatOptions(), metrics: nil, views: viewsDictionary))
    }
        
}

extension UIViewController {
    
    public func SetPreferedLangToEn() {
        if !Bundle.main.preferredLocalizations[0].hasPrefix("en"){
            let alert = UIAlertController(title: "Do you want to restart?", message: "You have changed the Cayload-Driver language in setting in order to have better user experience please reastart app".localized(), preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Yes".localized(), style: .default, handler: {
                action in
                Bundle.SetDefaultLoc()
            }))
            if let popover = alert.popoverPresentationController{
                popover.sourceView = self.view
                popover.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
                popover.permittedArrowDirections = []
            }
            self.present(alert, animated: true)
        }
    }
    
    func SetPreferedLangAlert(Lang:String) {
        let alert = UIAlertController(title: "Do you want to restart?".localized(), message: "We should restart the CayLoad-Driver to apply your language".localized(), preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Yes".localized(), style: .default, handler: {
            action in
            Bundle.setLanguage(lang: Lang)
        }))
        alert.addAction(UIAlertAction(title: "No".localized(), style: .default, handler: {
            action in
            alert.dismiss(animated: true)
        }))
        if let popover = alert.popoverPresentationController{
            popover.sourceView = self.view
            popover.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
            popover.permittedArrowDirections = []
        }
        DispatchQueue.main.async {
           self.present(alert, animated: true, completion: nil)
        }
        
        
    }
    
     func ShowAlert(Message : String = "Error", Seconds: Double) {
            var alert:UIAlertController!
            if Message.count > 21 {
                alert = UIAlertController(title: nil, message: Message, preferredStyle: .alert)
            }else{
                alert = UIAlertController(title: nil, message: Message, preferredStyle: .actionSheet)
            }
            if let popover = alert.popoverPresentationController{
                popover.sourceView = self.view
                popover.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
                popover.permittedArrowDirections = []
            }
            alert.view.backgroundColor = UIColor.white
            alert.view.alpha = 1
            alert.view.layer.cornerRadius = 9
            DispatchQueue.main.async {
               self.present(alert, animated: true, completion: nil)
            }

            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Seconds) {
                alert.dismiss(animated: true)
            }
        
    }
    
    func PopUpAlert(Message : String = "Error", Seconds: Double) {
            let alert = UIAlertController(title: nil, message: Message, preferredStyle: .alert)
            if let popover = alert.popoverPresentationController{
                popover.sourceView = self.view
                popover.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
                popover.permittedArrowDirections = []
            }
            alert.view.backgroundColor = UIColor.white
            alert.view.alpha = 1
            alert.view.layer.cornerRadius = 9
            DispatchQueue.main.async {
               self.present(alert, animated: true, completion: nil)
            }


            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Seconds) {
                alert.dismiss(animated: true)
            }
        
    }
    
    func PopUpActionSheet(Message : String = "Error", Seconds: Double) {
            let alert = UIAlertController(title: nil, message: Message, preferredStyle: .actionSheet)
            if let popover = alert.popoverPresentationController{
                popover.sourceView = self.view
                popover.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
                popover.permittedArrowDirections = []
            }
            alert.view.backgroundColor = UIColor.white
            alert.view.alpha = 1
            alert.view.layer.cornerRadius = 9
            DispatchQueue.main.async {
               self.present(alert, animated: true, completion: nil)
            }


            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Seconds) {
                alert.dismiss(animated: true)
            }
        
    }
    
    func SetButtonToWatingState(Button:UIButton,ButtonState:ButtonWaitingState,ButtonText:String){
        if ButtonState == .Enabled {
            Button.setTitle(ButtonText.localized(), for: .normal)
            Button.isEnabled = true
        }
        else if ButtonState == .Disabled {
            Button.setTitle("Wait...".localized(), for: .normal)
            Button.isEnabled = false
        }
    }
}

extension String {
//    func localized(_ lang:String) ->String {
//
//        let path = Bundle.main.path(forResource: lang, ofType: "lproj")
//        let bundle = Bundle(path: path!)
//
//        return NSLocalizedString(self, tableName: nil, bundle: bundle!, value: "", comment: "")
//    }
}


extension Bundle {
    private static var bundle: Bundle!

    public static func localizedBundle() -> Bundle! {
        if bundle == nil {
            //UserDefaults.standard.string(forKey: "AppleLanguages") ??
            let appLang =  UserDefaults.standard.string(forKey: "Language_Def") ?? "en"
            let path = Bundle.main.path(forResource: appLang, ofType: "lproj")
            bundle = Bundle(path: path!)
        }

        return bundle;
    }

    public static func setLanguage(lang: String) {
        UserDefaults.standard.set(["en"], forKey: "AppleLanguages")
        UserDefaults.standard.set(lang, forKey: "Language_Def")
        UserDefaults.standard.synchronize()
        let path = Bundle.main.path(forResource: lang, ofType: "lproj")
        bundle = Bundle(path: path!)
        exit(0)
    }
    
    public static func SetLanguageInApp(lang: String) {
        UserDefaults.standard.set(["en"], forKey: "AppleLanguages")
        UserDefaults.standard.set(lang, forKey: "Language_Def")
        UserDefaults.standard.synchronize()
        let path = Bundle.main.path(forResource: lang, ofType: "lproj")
        bundle = Bundle(path: path!)
    }
    
    public static func SetDefaultLoc(){
        UserDefaults.standard.set(["en"], forKey: "AppleLanguages")
        let appLang =  UserDefaults.standard.string(forKey: "Language_Def")
        UserDefaults.standard.set(appLang, forKey: "Language_Def")
        UserDefaults.standard.synchronize()
        let path = Bundle.main.path(forResource: appLang, ofType: "lproj")
        bundle = Bundle(path: path!)
        exit(0)
    }
}
extension String {
    func localized() -> String {
        return NSLocalizedString(self, tableName: "Localizable", bundle: Bundle.localizedBundle(), value: "", comment: "")
    }

    func localizeWithFormat(arguments: CVarArg...) -> String{
        return String(format: self.localized(), arguments: arguments)
    }
    
    func GetJSONFromString() throws -> JSON{
        let data = self.data(using: .utf8)!
        do {
            let json = try JSON(data: data, options: .allowFragments)
            return json
        }catch{
            throw JSONError.BadJSON
        }
    }
}

extension UIImage {

    func rotated(byDegrees degree: Double) -> UIImage {
        let radians = CGFloat(degree * .pi) / 180.0 as CGFloat
        let rotatedSize = self.size
        let scale = UIScreen.main.scale
        UIGraphicsBeginImageContextWithOptions(rotatedSize, false, scale)
        let bitmap = UIGraphicsGetCurrentContext()
        bitmap?.translateBy(x: rotatedSize.width / 2, y: rotatedSize.height / 2)
        bitmap?.rotate(by: radians)
        bitmap?.scaleBy(x: 1.0, y: -1.0)
        bitmap?.draw(
            self.cgImage!,
            in: CGRect.init(x: -self.size.width / 2, y: -self.size.height / 2 , width: self.size.width, height: self.size.height))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext() // this is needed
        return newImage!
    }

}

public extension UIImageView {
    
    func LoadImageFromServer(url: URL) {
        DispatchQueue.global().async { [weak self] in
            if let data = try? Data(contentsOf: url) {
                if let image = UIImage(data: data) {
                    DispatchQueue.main.async {
                        self?.image = image
                    }
                }
            }
        }
    }
}

extension Dictionary {
    func GetStringFromDic() throws -> String{
        guard let theJSONData = try? JSONSerialization.data(withJSONObject: self,options: [.prettyPrinted]) else {
            throw SocketError.CantWrite
        }

        if let str = String(data: theJSONData, encoding: .ascii){
            return str
        }else{
            throw SocketError.CantGetString
        }
        
    }
}

extension UICollectionView {
    
    func ScrollToBottom(animated: Bool = true) {
        
        DispatchQueue.main.async {

                let lastSection = self.numberOfSections - 1

                let lastRow = self.numberOfItems(inSection: lastSection)

                let indexPath = IndexPath(row: lastRow - 1, section: 0)

            self.scrollToItem(at: indexPath, at: .bottom, animated: true)
        }
    }
}

extension Array {

    func ShiftArray(by shiftAmount: Int) -> Array<Element> {

        // 1
        guard self.count > 0, (shiftAmount % self.count) != 0 else { return self }

        // 2
        let moduloShiftAmount = shiftAmount % self.count
        let negativeShift = shiftAmount < 0
        let effectiveShiftAmount = negativeShift ? moduloShiftAmount + self.count : moduloShiftAmount

        // 3
        let shift: (Int) -> Int = { return $0 + effectiveShiftAmount >= self.count ? $0 + effectiveShiftAmount - self.count : $0 + effectiveShiftAmount }

        // 4
        return self.enumerated().sorted(by: { shift($0.offset) < shift($1.offset) }).map { $0.element }

    }

}
