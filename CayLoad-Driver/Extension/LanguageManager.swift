//
//  LanguageManager.swift
//  CayLoad-Driver
//
//  Created by Kiyarash Vosough on 3/20/1399 AP.
//  Copyright © 1399 CayLoad. All rights reserved.
//

import Foundation
import UIKit

class LanguageManager {
    
    private var UserSelectedLanguage_LangNotaion:String
    private var UserSelectedLanguage_CompeleteForm:String
    private var Language_Def:String

    
    init(UserSelectedLanguage_CompeleteForm:String) {
        Language_Def = UserDefaults.standard.string(forKey: "Language_Def") ?? "en"
        UserSelectedLanguage_LangNotaion = UserDefaults.standard.string(forKey: "Language_Def") ?? "en"
        self.UserSelectedLanguage_CompeleteForm = UserSelectedLanguage_CompeleteForm
    }
    
    init() {
        Language_Def = UserDefaults.standard.string(forKey: "Language_Def") ?? "en"
        self.UserSelectedLanguage_CompeleteForm = LanguageManager.GetCompleteFormFrom(Notaion: Language_Def)
        UserSelectedLanguage_LangNotaion = Language_Def
    }

    func GetNotationFrom() -> String {
        switch UserSelectedLanguage_CompeleteForm {
        case "English":
            return "en"
        case "Persian":
            return "fa"
        case "Russian":
            return "ru"
        case "Chinese":
            return "ch"
        case "Hindi":
            return "hi"
        case "Arabic":
            return "ar"
        default:
            return "en"
        }
    }
    
    static func GetCompleteFormFrom(Notaion:String) -> String {
        switch Notaion {
        case "en":
            return "English"
        case "fa":
            return "Persian"
        case "ru":
            return "Russian"
        case "ch":
            return "Chinese"
        case "hi":
            return "Hindi"
        case "ar":
            return "Arabic"
        default:
            return "English"
        }
    }
    
    func SetLanguageViewProperites(Lang_Image:UIImageView){
        Lang_Image.image = GetLanguageImage()
    }
    
    private func GetLanguageImage() -> UIImage{
        return UIImage(named: GetUserSelectedLanguage_CompeleteForm() + "Ic") ?? UIImage()
    }
    
    
    func SetUserSelectedLanguage_CompeleteForm(Lang:String){
        UserSelectedLanguage_CompeleteForm = Lang
    }
    
    func GetUserSelectedLanguage_CompeleteForm() -> String{
        return self.UserSelectedLanguage_CompeleteForm
    }
    
    func GetLanguage_Default() ->String{
        return Language_Def
    }
    
}
