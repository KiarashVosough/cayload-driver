//
//  firstViewController.swift
//  CayLoad-Driver
//
//  Created by Kiyarash Vosough on 1/18/1399 AP.
//  Copyright © 1399 CayLoad. All rights reserved.
//

import UIKit

class firstViewController: UIViewController {
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        UserDefaults.standard.set("en", forKey: "Language_Def")
        if UserDefaults.standard.bool(forKey: "Is_Login") == true {
            performSegue(withIdentifier: "From_StartNavigation_To_MainNAvBar_Segue", sender: self)
        }
        else{
            performSegue(withIdentifier: "From_StartNavigation_To_Login_Segue", sender: self)
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        UIView.appearance().semanticContentAttribute = .forceLeftToRight
    }
    
    @IBAction func unwindLogin(segue: UIStoryboardSegue) {
    }

    @IBAction func UnwindFrom_ProfileViewController_To_StartNavigationController(segue:UIStoryboardSegue) {
        
        if segue.source is ProfileViewController {
        }
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    

}
