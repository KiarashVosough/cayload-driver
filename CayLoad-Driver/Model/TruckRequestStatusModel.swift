//
//  TruckRequestStatusModel.swift
//  CayLoad-Driver
//
//  Created by Kiyarash Vosough on 2/15/1399 AP.
//  Copyright © 1399 CayLoad. All rights reserved.
//

import Foundation
import SwiftyJSON

class TruckRequestStatusModel: ModelInitializer {
    
    var id:Int
    var truck_request:TruckRequestModel?
    var status:String
    var user:UserModel?
    var date_created:DateTimeModel?
    
    required init(_ JsonData: JSON) {
        
        id = JsonData["id"].intValue
        
        if JsonData["truck_request"].null == nil {
            truck_request = TruckRequestModel(JsonData["truck_request"])
        }
        
        status = JsonData["status"].stringValue
        
        if JsonData["user"].null == nil {
            user = UserModel(JsonData["user"])
        }
        if JsonData["date_created"].null == nil {
            date_created = DateTimeModel(JsonData["date_created"])
        }
    }
}
