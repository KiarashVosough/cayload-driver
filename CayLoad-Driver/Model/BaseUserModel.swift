//
//  BaseUserModel.swift
//  CayLoad-Driver
//
//  Created by Kiyarash Vosough on 1/30/1399 AP.
//  Copyright © 1399 CayLoad. All rights reserved.
//

import Foundation
import SwiftyJSON

class BaseUserModel:ModelInitializer {
    
    var is_active:Bool
    var language:String
    
    required init(_ JsonData: JSON) {
        is_active = JsonData["is_active"].boolValue
        language = JsonData["language"].stringValue
    }
}
