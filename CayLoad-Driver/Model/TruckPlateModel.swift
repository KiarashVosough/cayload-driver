//
//  TruckPlateModel.swift
//  CayLoad-Driver
//
//  Created by Kiyarash Vosough on 2/3/1399 AP.
//  Copyright © 1399 CayLoad. All rights reserved.
//

import Foundation
import SwiftyJSON

class TruckPlateModel: ModelInitializer {
    
    var id:Int
    var country_plate:CountryPlateModel?
    var truck:TruckModel?
    var data:String
    
    required init(_ JsonData: JSON) {
        id = JsonData["id"].intValue
        if JsonData["country_plate"].null == nil {
            country_plate = CountryPlateModel(JsonData["country_plate"])
        }
        if JsonData["truck"].null == nil {
            truck = TruckModel(JsonData["truck"])
        }
        data = JsonData["data"].stringValue
    }
    
    
}
