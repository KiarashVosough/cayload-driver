//
//  UserNationalitiesModel.swift
//  CayLoad-Driver
//
//  Created by Kiyarash Vosough on 1/28/1399 AP.
//  Copyright © 1399 CayLoad. All rights reserved.
//

import Foundation
import SwiftyJSON

class UserNationalitiesModel:ModelInitializer {
    
    var id:Int?
    var name:String
    var flag:FlagModel?
    var unlimited_usage:Bool
    var tax_amount:Double
    
    required init(_ JsonData: JSON) {
        id = JsonData["id"].intValue
        name = JsonData["name"].stringValue
        if JsonData["flag"].null == nil{
            flag = FlagModel(JsonData["flag"])
        }
        unlimited_usage = JsonData["unlimited_usage"].boolValue
        tax_amount = JsonData["tax_amount"].doubleValue
    }
    
    init(selfModel: UserNationalitiesModel) {
        id = selfModel.id
        name = selfModel.name
        flag = FlagModel(selfModel: selfModel.flag!)
        unlimited_usage = selfModel.unlimited_usage
        tax_amount = selfModel.tax_amount
    }
    
    func getID() -> Int {
        return id ?? 0
    }
    
    
}
