//
//  FlagModel.swift
//  CayLoad-Driver
//
//  Created by Kiyarash Vosough on 1/28/1399 AP.
//  Copyright © 1399 CayLoad. All rights reserved.
//

import Foundation
import SwiftyJSON

class FlagModel:ModelInitializer {
    
    var id:Int
    var file:String
    
    required init(_ JsonData: JSON) {
        id = JsonData["id"].intValue
        file = JsonData["file"].stringValue
    }
    
    init(selfModel:FlagModel) {
        id = selfModel.id
        file = selfModel.file
    }
}
