//
//  CountryPlateModel.swift
//  CayLoad-Driver
//
//  Created by Kiyarash Vosough on 2/3/1399 AP.
//  Copyright © 1399 CayLoad. All rights reserved.
//

import Foundation
import SwiftyJSON

class CountryPlateModel: ModelInitializer {
    
    var id:Int
    var country:CountryModel?
    var first_row:PlateRowModel?
    var second_row:PlateRowModel?
    
    required init(_ JsonData: JSON) {
        id = JsonData["id"].intValue
        if JsonData["country"].null == nil {
            country = CountryModel(JsonData["country"])
        }
        if JsonData["first_row"].null == nil {
            first_row = PlateRowModel(JsonData["first_row"])
        }
        if JsonData["second_row"].null == nil {
            second_row = PlateRowModel(JsonData["second_row"])
        }
    }
    
    
}
