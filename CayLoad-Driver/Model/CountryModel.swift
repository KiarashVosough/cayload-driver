//
//  CountryModel.swift
//  CayLoad-Driver
//
//  Created by Kiyarash Vosough on 2/3/1399 AP.
//  Copyright © 1399 CayLoad. All rights reserved.
//

import Foundation
import SwiftyJSON

class CountryModel:ModelInitializer {
    
    var id:Int
    var dialing_code:String
    var name:String
    var iso_name:String
    var flag_image:ImageModel?
    var continent:ContinentModel?
    
    required init(_ JsonData: JSON) {
        id = JsonData["id"].intValue
        dialing_code = JsonData["dialing_code"].stringValue
        name = JsonData["name"].stringValue
        iso_name = JsonData["iso_name"].stringValue
        if JsonData["flag_image"].null == nil {
            flag_image = ImageModel(JsonData["flag_image"])
        }
        if JsonData["continent"].null == nil {
            continent = ContinentModel(JsonData["continent"])
            
        }
        
    }
    
    
}
