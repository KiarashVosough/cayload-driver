//
//  DriverModel.swift
//  CayLoad-Driver
//
//  Created by Kiyarash Vosough on 2/3/1399 AP.
//  Copyright © 1399 CayLoad. All rights reserved.
//

import Foundation
import SwiftyJSON

class DriverModel: ModelInitializer {
    var id:Int
    var user:UserModel?
    var unique_id:Int
    var profile_image:ImageModel?
    var primary_email:EmailModel?
    var primary_phone:PhoneModel?
    var company_name:String
    var company_description:String
    var company_logo:ImageModel?
    var is_active:Bool
    var license:ImageModel?
    var license_confirmed_date:DateTimeModel?
    var vehicle_card:ImageModel?
    var driver_location:LocationPointModel?
    var truck:TruckModel?
    var plan:PlanModel?
    
    required init(_ JsonData: JSON) {
        id = JsonData["id"].intValue
        
        if JsonData["user"].null == nil {
            user = UserModel(JsonData["user"])
        }
        unique_id = JsonData["unique_id"].intValue
        if JsonData["profile_image"].null == nil {
            profile_image = ImageModel(JsonData["profile_image"])
        }
        if JsonData["primary_email"].null == nil {
            primary_email = EmailModel(JsonData["primary_email"])
        }
        if JsonData["primary_phone"].null == nil {
            primary_phone = PhoneModel(JsonData["primary_phone"])
        }
        
        company_name = JsonData["company_name"].stringValue
        company_description = JsonData["company_description"].stringValue
        
        if JsonData["company_logo"].null == nil {
            company_logo = ImageModel(JsonData["company_logo"])
        }
        is_active = JsonData["is_active"].boolValue
        
        if JsonData["license"].null == nil {
            license = ImageModel(JsonData["license"])
        }
        
        if JsonData["license_confirmed_date"].null == nil {
            license_confirmed_date = DateTimeModel(JsonData["license_confirmed_date"])
        }
        
        if JsonData["vehicle_card"].null == nil {
            vehicle_card = ImageModel(JsonData["vehicle_card"])
        }
        
        if JsonData["driver_location"].null == nil {
            driver_location = LocationPointModel(JsonData["driver_location"])
        }
        
        if JsonData["truck"].null == nil {
            truck = TruckModel(JsonData["truck"])
        }
        if JsonData["plan"].null == nil {
            plan = PlanModel(JsonData["plan"])
        }
    }
    
    
}
