//
//  ContainerTypeModel.swift
//  CayLoad-Driver
//
//  Created by Kiyarash Vosough on 2/15/1399 AP.
//  Copyright © 1399 CayLoad. All rights reserved.
//

import Foundation
import SwiftyJSON

class ContainerTypeModel: ModelInitializer {
    
    var id:Int
    var title:String
    
    required init(_ JsonData: JSON) {
        id = JsonData["id"].intValue
        title = JsonData["title"].stringValue
    }
}
