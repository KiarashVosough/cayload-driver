//
//  MessagePhotoModel.swift
//  CayLoad-Driver
//
//  Created by Kiyarash Vosough on 4/16/1399 AP.
//  Copyright © 1399 CayLoad. All rights reserved.
//

import Foundation
import SwiftyJSON

class MessagePhotoModel:ModelInitializer {
    
    var id:Int
    var url:String
    var image:UIImage?
    
    required init(_ JsonData: JSON) {
        id = JsonData["id"].intValue
        url = JsonData["url"].stringValue
        if let data = try? Data(contentsOf: try! ("https://chat.cayload.com" + url).asURL()) {
            if let image = UIImage(data: data) {
                self.image = image
            }
        }
    }
    
    init(image:UIImage) {
        id = 0
        url = ""
    }
    
}
