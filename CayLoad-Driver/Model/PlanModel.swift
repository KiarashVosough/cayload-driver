//
//  PlanModel.swift
//  CayLoad-Driver
//
//  Created by Kiyarash Vosough on 2/1/1399 AP.
//  Copyright © 1399 CayLoad. All rights reserved.
//

import Foundation
import SwiftyJSON

class PlanModel:ModelInitializer {
    
    var id:Int
    var nationality:UserNationalitiesModel?
    var name:String
    var description:String
    var price:Int
    var valid_days:Int
    
    required init(_ JsonData: JSON) {
        id = JsonData["id"].intValue
        if JsonData["nationality"].null == nil{
            nationality = UserNationalitiesModel(JsonData["nationality"])
        }
        name = JsonData["name"].stringValue
        description = JsonData["description"].stringValue
        price = JsonData["price"].intValue
        valid_days = JsonData["valid_days"].intValue
    }
    
    
}
