//
//  TruckTypeModel.swift
//  CayLoad-Driver
//
//  Created by Kiyarash Vosough on 2/3/1399 AP.
//  Copyright © 1399 CayLoad. All rights reserved.
//

import Foundation
import SwiftyJSON

class TruckTypeModel: ModelInitializer {
    
    var id:Int
    var title:String
    var icon:ImageModel?
    
    required init(_ JsonData: JSON) {
        id = JsonData["id"].intValue
        title = JsonData["title"].stringValue
        if JsonData["icon"].null == nil {
            icon = ImageModel(JsonData["icon"])
        }
    }
    
    
}
