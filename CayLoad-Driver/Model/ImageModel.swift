//
//  ImageModel.swift
//  CayLoad-Driver
//
//  Created by Kiyarash Vosough on 2/3/1399 AP.
//  Copyright © 1399 CayLoad. All rights reserved.
//

import Foundation
import SwiftyJSON

class ImageModel: ModelInitializer {
    var id:Int
    var file:String
    var base_user:BaseUserModel?
    var date_created:String
    
    required init(_ JsonData: JSON) {
        id = JsonData["id"].intValue
        file = JsonData["file"].stringValue
        if JsonData["base_user"].null == nil {
            base_user = BaseUserModel(JsonData["base_user"])
        }
        date_created = JsonData["date_created"].stringValue
    }
    
    
}
