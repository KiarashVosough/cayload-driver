//
//  DriverFavoriteRouteModel.swift
//  CayLoad-Driver
//
//  Created by Kiyarash Vosough on 2/16/1399 AP.
//  Copyright © 1399 CayLoad. All rights reserved.
//

import Foundation
import SwiftyJSON

class DriverFavoriteRouteModel: ModelInitializer {
    
    var id:Int
    var driver:DriverModel?
    var route:RouteModel?
    
    required init(_ JsonData: JSON) {
        
        id = JsonData["id"].intValue
               
        if JsonData["driver"].null == nil {
            driver = DriverModel(JsonData["driver"])
        }
        
        if JsonData["route"].null == nil {
            route = RouteModel(JsonData["route"])
        }
    }
    
    
}
