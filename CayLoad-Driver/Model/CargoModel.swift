//
//  CargoModel.swift
//  CayLoad-Driver
//
//  Created by Kiyarash Vosough on 2/15/1399 AP.
//  Copyright © 1399 CayLoad. All rights reserved.
//

import Foundation
import SwiftyJSON

class CargoModel: ModelInitializer {
    
    var id:Int
    var container_type:ContainerTypeModel?
    var container_size:ContainerSizeModel?
    var container_owner:String
    var commodity:String
    var net_weight:Double
    var total_weight:Double
    var packages_count:Int
    var hs_code:String
    var description:String
    
    required init(_ JsonData: JSON) {
        id = JsonData["id"].intValue
        
        if JsonData["container_type"].null == nil {
            container_type = ContainerTypeModel(JsonData["container_type"])
        }
        if JsonData["container_size"].null == nil {
            container_size = ContainerSizeModel(JsonData["container_size"])
        }
        container_owner = JsonData["container_owner"].stringValue
        commodity = JsonData["commodity"].stringValue
        net_weight = JsonData["net_weight"].doubleValue
        total_weight = JsonData["total_weight"].doubleValue
        packages_count = JsonData["packages_count"].intValue
        hs_code = JsonData["hs_code"].stringValue
        description = JsonData["description"].stringValue
    }
    
    
    
}
