//
//  LocationPointModel.swift
//  CayLoad-Driver
//
//  Created by Kiyarash Vosough on 2/3/1399 AP.
//  Copyright © 1399 CayLoad. All rights reserved.
//

import Foundation
import SwiftyJSON

class LocationPointModel: ModelInitializer {
    
    var id:Int
    var latitude:Double
    var response_latitude:Int
    var longitude:Double
    var response_longitude:Int
    var formatted_address:String
    var confidence:Int
    var country:CountryModel?
    var county:String
    var city:CityModel?
    var postcode:String
    
    required init(_ JsonData: JSON) {
        id = JsonData["id"].intValue
        latitude = JsonData["latitude"].doubleValue
        response_latitude = JsonData["response_latitude"].intValue
        longitude = JsonData["longitude"].doubleValue
        response_longitude = JsonData["response_longitude"].intValue
        formatted_address = JsonData["formatted_address"].stringValue
        confidence = JsonData["confidence"].intValue
        if JsonData["country"].null == nil {
            country = CountryModel(JsonData["country"])
        }
        county = JsonData["county"].stringValue
        if JsonData["city"].null == nil {
            city = CityModel(JsonData["city"])
        }
        postcode = JsonData["postcode"].stringValue
    }
}
