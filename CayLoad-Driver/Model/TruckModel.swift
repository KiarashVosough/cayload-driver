//
//  TruckModel.swift
//  CayLoad-Driver
//
//  Created by Kiyarash Vosough on 2/3/1399 AP.
//  Copyright © 1399 CayLoad. All rights reserved.
//

import Foundation
import SwiftyJSON
class TruckModel: ModelInitializer {
    
    var id:Int
    var driver:DriverModel?
    var model:String
    var type:TruckTypeModel?
    var color:String
    var truck_plate_data:String
    var trailer_plate_data:String
    var national_plate:TruckPlateModel?
    var capacity:Double
    
    required init(_ JsonData: JSON) {
        id = JsonData["id"].intValue
        if JsonData["driver"].null == nil {
            driver = DriverModel(JsonData["driver"])
        }
        model = JsonData["model"].stringValue
        if JsonData["type"].null == nil {
            type = TruckTypeModel(JsonData["type"])
        }
        color = JsonData["color"].stringValue
        truck_plate_data = JsonData["truck_plate_data"].stringValue
        trailer_plate_data = JsonData["trailer_plate_data"].stringValue
        if JsonData["national_plate"].null == nil {
            national_plate = TruckPlateModel(JsonData["national_plate"])
            
        }
        capacity = JsonData["capacity"].doubleValue
    }
    
    
}
