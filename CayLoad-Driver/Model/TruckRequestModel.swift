//
//  TruckRequestModel.swift
//  CayLoad-Driver
//
//  Created by Kiyarash Vosough on 2/15/1399 AP.
//  Copyright © 1399 CayLoad. All rights reserved.
//

import Foundation
import SwiftyJSON

class TruckRequestModel: ModelInitializer {
    
    var id:Int
    var customer:CustomerModel?
    var request_id:Int
    var truck_type:TruckTypeModel?
    var cargo:CargoModel?
    var description:String
    var route:RouteModel?
    var driver:DriverModel?
    var price:Double
    var date_created:DateTimeModel?
    var Buttons:Array<UIButton>?
    var offers_count:Int
    var truck_request_status:TruckRequestStatusModel?
    var tracks_count:Int
    
    
    required init(_ JsonData: JSON) {
        id = JsonData["id"].intValue
        
        if JsonData["customer"].null == nil {
            customer = CustomerModel(JsonData["customer"])
        }
        
        request_id = JsonData["request_id"].intValue
        
        if JsonData["truck_type"].null == nil {
            truck_type = TruckTypeModel(JsonData["truck_type"])
        }
        if JsonData["cargo"].null == nil {
            cargo = CargoModel(JsonData["cargo"])
        }
        
        description = JsonData["description"].stringValue
        
        if JsonData["route"].null == nil {
            route = RouteModel(JsonData["route"])
        }
        
        if JsonData["driver"].null == nil {
            driver = DriverModel(JsonData["driver"])
        }
        
        price = JsonData["price"].doubleValue
        
        if JsonData["date_created"].null == nil {
            date_created = DateTimeModel(JsonData["date_created"])
        }
        
        //-Buttons array
        offers_count = JsonData["offers_count"].intValue
        
        if JsonData["truck_request_status"].null == nil {
            truck_request_status = TruckRequestStatusModel(JsonData["truck_request_status"])
        }
        
        tracks_count = JsonData["tracks_count"].intValue
    }
    
}
