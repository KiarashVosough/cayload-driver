//
//  User.swift
//  CayLoad-Driver
//
//  Created by Kiyarash Vosough on 1/30/1399 AP.
//  Copyright © 1399 CayLoad. All rights reserved.
//

import Foundation
import SwiftyJSON

class UserModel:ModelInitializer {
    
    var id:Int = 0
    var first_name:String
    var last_name:String
    var base_user:BaseUserModel?
    var date_agreement_accepted:DateTimeModel?
    var date_joined:DateTimeModel?
    var nationality:UserNationalitiesModel?
    
    required init(_ JsonData: JSON) {
        id = JsonData["id"].intValue
        first_name = JsonData["first_name"].stringValue
        last_name = JsonData["last_name"].stringValue
        if JsonData["base_user"].null == nil {
            base_user = BaseUserModel(JsonData["base_user"])
        }
        if JsonData["date_agreement_accepted"].null == nil {
            date_agreement_accepted = DateTimeModel(JsonData["date_agreement_accepted"])
        }
        if JsonData["date_joined"].null == nil {
            date_joined = DateTimeModel(JsonData["date_joined"])
        }
        if JsonData["nationality"].null == nil {
            nationality = UserNationalitiesModel(JsonData["nationality"])
        }
    }
}

