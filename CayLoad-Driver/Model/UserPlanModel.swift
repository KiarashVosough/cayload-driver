//
//  UserPlanModel.swift
//  CayLoad-Driver
//
//  Created by Kiyarash Vosough on 2/13/1399 AP.
//  Copyright © 1399 CayLoad. All rights reserved.
//

import Foundation
import SwiftyJSON

class UserPlanModel: ModelInitializer {
    
    var id:Int
    var plan:PlanModel?
    var is_customer:Bool
    var transaction:TransactionModel?
    
    required init(_ JsonData: JSON) {
        id = JsonData["id"].intValue
        if JsonData["plan"].null == nil {
            plan = PlanModel(JsonData["plan"])
        }
        if JsonData["transaction"].null == nil {
            transaction = TransactionModel(JsonData["transaction"])
        }
        is_customer = JsonData["is_customer"].boolValue
    }
    
    
}
