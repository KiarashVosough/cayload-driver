//
//  ContinentModel.swift
//  CayLoad-Driver
//
//  Created by Kiyarash Vosough on 2/3/1399 AP.
//  Copyright © 1399 CayLoad. All rights reserved.
//

import Foundation
import SwiftyJSON

class ContinentModel: ModelInitializer {
    
    var id:Int
    var name:String
    
    required init(_ JsonData: JSON) {
        id = JsonData["id"].intValue
        name = JsonData["name"].stringValue
    }
}
