//
//  ErrorModel.swift
//  CayLoad-Driver
//
//  Created by Kiyarash Vosough on 2/2/1399 AP.
//  Copyright © 1399 CayLoad. All rights reserved.
//

import Foundation
import SwiftyJSON

class ErrorModel: ModelInitializer {
    
    var body:String
    var title:String
    var code:Int
    
    required init(_ JsonData: JSON) {
        code = JsonData["code"].intValue
        body = JsonData["body"].stringValue
        title = JsonData["title"].stringValue
    }
    func PrintError(){
        print(body)
        print(title)
        print(code)
    }
    
    func GetErrorBody() ->String?{
        return body
    }
    
}
