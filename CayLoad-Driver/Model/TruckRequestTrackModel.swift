//
//  TruckRequestTrack.swift
//  CayLoad-Driver
//
//  Created by Kiyarash Vosough on 2/21/1399 AP.
//  Copyright © 1399 CayLoad. All rights reserved.
//

import Foundation
import SwiftyJSON

class TruckRequestTrackModel: ModelInitializer {
    
    var id:Int
    var truck_request:TruckRequestModel?
    var tracker:CustomerModel?
    var date_created:DateTimeModel?
    var date_accepted:DateTimeModel?
    var date_payed:DateTimeModel?

    
    required init(_ JsonData: JSON) {
           
        id = JsonData["id"].intValue
               
        if JsonData["truck_request"].null == nil {
            truck_request = TruckRequestModel(JsonData["truck_request"])
        }
        if JsonData["tracker"].null == nil {
            tracker = CustomerModel(JsonData["tracker"])
        }
        if JsonData["date_created"].null == nil {
            date_created = DateTimeModel(JsonData["date_created"])
        }
        if JsonData["date_accepted"].null == nil {
            date_accepted = DateTimeModel(JsonData["date_accepted"])
        }
        if JsonData["date_payed"].null == nil {
            date_payed = DateTimeModel(JsonData["date_payed"])
        }
    }
    
}
