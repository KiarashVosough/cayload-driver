//
//  MessageModel.swift
//  CayLoad-Driver
//
//  Created by Kiyarash Vosough on 4/16/1399 AP.
//  Copyright © 1399 CayLoad. All rights reserved.
//

import Foundation
import SwiftyJSON

class MessageModel {
    
    var id:Int
    var date_created:DateTimeModel?
    var image:MessagePhotoModel?
    var receiver_user:Int
    var sender_user:Int
    var text:String
    var this_User_Id:Int
    var Sender:MessageSender = .None
    var MessageType:MessageType = .None
    var With:ChatWith = .Customer
    
    init(JsonData: JSON, thisID:Int) throws {
        
        //message text never shouldnt be empty
        if let text = JsonData["text"].string {
            self.text = text
        }
        else{
            self.text = ""
//            throw JSONError.MissingAnAtrribute
        }
        
        id = JsonData["id"].intValue
        
        this_User_Id = thisID
        
        receiver_user = MessageModel.TokenizeId(text: JsonData["receiver_user"].stringValue)
        sender_user = MessageModel.TokenizeId(text: JsonData["sender_user"].stringValue)
        //the id of aadmin is 0
        if sender_user == 0{
            With = .Admin
        }
        
        //if equal message is sent by this user
        if this_User_Id == receiver_user {
            Sender = .Itself
        }
        else{
            //if not message was sent to this user
            Sender = .Me
        }
        
        if JsonData["date_created"].null == nil {
            date_created = DateTimeModel(JsonData["date_created"])
        }
        //there could be an image in message
        if JsonData["image"].null == nil {
            image = MessagePhotoModel(JsonData["image"])
        }
    }
    
    class func TokenizeId(text:String)-> Int{
        if text.contains("c") {
            return Int(text.filter{$0 != "c"}) ?? 0
        }
        else if text.contains("d"){
            return Int(text.filter{$0 != "d"}) ?? 0
        }
        return Int(text) ?? 0
    }
    
}
