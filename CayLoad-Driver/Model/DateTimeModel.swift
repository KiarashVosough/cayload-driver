//
//  DateTimeModel.swift
//  CayLoad-Driver
//
//  Created by Kiyarash Vosough on 1/30/1399 AP.
//  Copyright © 1399 CayLoad. All rights reserved.
//

import Foundation
import SwiftyJSON

class DateTimeModel:ModelInitializer {
    var year:Int = 0
    var month:Int = 0
    var day:Int = 0
    var hour:Int = 0
    var minute:Int = 0
    var second:Int = 0
    var microsecond:Int = 0

    required init(_ JsonData: JSON) {
        year = JsonData["year"].intValue
        month = JsonData["month"].intValue
        day = JsonData["day"].intValue
        hour = JsonData["hour"].intValue
        minute = JsonData["minute"].intValue
        second = JsonData["second"].intValue
        microsecond = JsonData["microsecond"].intValue
    }
    
    init(date:Date) {
        let calendar = Calendar.current
        year = calendar.component(.year, from: date)
        month = calendar.component(.month, from: date)
        day = calendar.component(.day, from: date)
        hour = calendar.component(.hour, from: date)
        minute = calendar.component(.minute, from: date)
        second = calendar.component(.second, from: date)
    }
    
    
    
    func GetFormatedDate() -> String? {
//        if month != 0 && year != 0 && day != 0 && hour != 0 && minute != 0 {
            return GetMonthAsString(Month: self.month) + " \(day),\(year)-\(hour):\(minute)"
//        }else{
//            return nil
//        }
    }
    
    func GetFormatedTime() -> String {
        return "\(hour):\(minute)"
    }
    
    private func GetMonthAsString(Month:Int) -> String{
        switch Month {
        case 1:
            return "Jan"
        case 2:
            return "Feb"
        case 3:
            return "Mar"
        case 4:
            return "April"
        case 5:
            return "May"
        case 6:
            return "June"
        case 7:
            return "July"
        case 8:
            return "Aug"
        case 9:
            return "Sep"
        case 10:
            return "Oct"
        case 11:
            return "Nov"
        case 12:
            return "Dec"
        default:
            return ""
        }
    }
}
