//
//  Transaction.swift
//  CayLoad-Driver
//
//  Created by Kiyarash Vosough on 2/13/1399 AP.
//  Copyright © 1399 CayLoad. All rights reserved.
//

import Foundation
import SwiftyJSON
class TransactionModel: ModelInitializer {
    
    var id:Int
    var amount:Int
    var before:Int
    var description:String
    var user:UserModel?
    var date_created:String
    
    required init(_ JsonData: JSON) {
        id = JsonData["id"].intValue
        amount = JsonData["amount"].intValue
        before = JsonData["before"].intValue
        description = JsonData["description"].stringValue
       
        if JsonData["user"].null == nil {
            user = UserModel(JsonData["user"])
        }
        
        date_created = JsonData["date_created"].stringValue
    }
    

}
