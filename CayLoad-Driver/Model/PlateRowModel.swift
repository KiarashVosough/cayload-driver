//
//  PlateRowModel.swift
//  CayLoad-Driver
//
//  Created by Kiyarash Vosough on 2/3/1399 AP.
//  Copyright © 1399 CayLoad. All rights reserved.
//

import Foundation
import SwiftyJSON

class PlateRowModel: ModelInitializer {
    
    var id:Int
    var background_color:String
    var text_color:String
    
    required init(_ JsonData: JSON) {
        id = JsonData["id"].intValue
        background_color = JsonData["background_color"].stringValue
        text_color = JsonData["text_color"].stringValue
    }
    
    
}
