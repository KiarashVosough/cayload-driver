//
//  ChatModel.swift
//  CayLoad-Driver
//
//  Created by Kiyarash Vosough on 4/16/1399 AP.
//  Copyright © 1399 CayLoad. All rights reserved.
//

import Foundation
import SwiftyJSON

class ChatModel:MessageModel {
    
    var action:String?
    var estimated_Width:CGFloat = 30
    var estimated_Height:CGFloat = 20
    var Cell_Height:CGFloat = 20
    
    override init(JsonData: JSON, thisID: Int) throws {
        do {
            try super.init(JsonData: JsonData["message"], thisID: thisID)
            action = JsonData["action"].stringValue
            if let est_CGSize = ChatModel.GetEstimatedSize(text: super.text){
                self.estimated_Width = est_CGSize.width
                self.estimated_Height = est_CGSize.height
            }
            SetMessageType()
        }catch {
            throw JSONError.MissingAnAtrribute
        }
    }
    
    init(JsonData: JSON, thisID: Int,is_Server:Bool) throws {
        do {
            try super.init(JsonData: JsonData, thisID: thisID)
            action = "create"
            if let est_CGSize = ChatModel.GetEstimatedSize(text: super.text){
                self.estimated_Width = est_CGSize.width
                self.estimated_Height = est_CGSize.height
            }
            SetMessageType()
        }catch {
            throw JSONError.MissingAnAtrribute
        }
    }
    
    func SetMessageType() {
        if !(text.isEmpty) && image != nil{
            //if text width is smaller than image width -> assign image width as estimated width
            let width:CGFloat = 205 + 16
            if estimated_Width < width {
                estimated_Width = width
            }
            MessageType = .TextAndImage
            Cell_Height = estimated_Height + 200
        }
        else if !text.isEmpty {
            MessageType = .JustText
            if estimated_Width < 20 {
                estimated_Width = 40
            }
            Cell_Height = estimated_Height + 40
        }
        else if image != nil {
            MessageType = .JustImage
            Cell_Height = 190
        }
        else{
            MessageType = .None
        }
    }
    
    func GetEstimatedHeight() -> CGFloat {
        return Cell_Height
    }
    
    class func GetEstimatedSize(text:String) -> CGRect? {
        let size = CGSize(width: 250, height: 1000)
        let option = NSStringDrawingOptions.usesFontLeading.union(.usesLineFragmentOrigin)
        let EstimatedFrame = NSString(string: text).boundingRect(with: size, options: option , attributes: [NSAttributedString.Key.font: UIFont(name: "Poppins-Regular", size: 15) ?? UIFont.systemFont(ofSize: 15)], context: nil)
        if EstimatedFrame.width == 0 || EstimatedFrame.height == 0 {
            return nil
        }
        return EstimatedFrame
    }
}
