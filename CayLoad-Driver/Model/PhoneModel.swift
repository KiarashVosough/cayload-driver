//
//  PhoneModel.swift
//  CayLoad-Driver
//
//  Created by Kiyarash Vosough on 1/31/1399 AP.
//  Copyright © 1399 CayLoad. All rights reserved.
//

import Foundation
import SwiftyJSON

class PhoneModel:ModelInitializer {
    
    var id: Int?
    var number: String
    var date_verified: DateTimeModel?
    
    required init(_ JsonData: JSON) {
        id = JsonData["id"].intValue
        number = JsonData["number"].stringValue
        if JsonData["date_verified"].null == nil{
            date_verified = DateTimeModel(JsonData["date_verified"])
        }
    }
    
    func getID() -> Int {
        return id ?? 0
    }
    
}
