//
//  UserConnectionManager.swift
//  CayLoad-Driver
//
//  Created by Kiyarash Vosough on 1/23/1399 AP.
//  Copyright © 1399 CayLoad. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON


class UserConnectionManager {
    
    class func SetToken(Data:Any){
//        if let token = JSON(Data)["token"].string {
//            TOKEN = token
//        }
        if UserDefaults.standard.bool(forKey: "Is_Login") {
            if let token = JSON(Data)["token"].string {
//                print("user set header")
                UserDefaults.standard.set(token, forKey: "VALID_TOKEN")
            }
            
        }else {
            if let token = JSON(Data)["token"].string {
                TOKEN = token
            }
        }
    }
    
    class func Request(URL:String,RequestMethod: HTTPMethod, Parameter:[String:Any]?, _ OnSuccess: @escaping (Int,JSON?)-> Void,_ OnError: @escaping (Int,[ErrorModel]?)->Void) {
        
        if NetworkReachabilityManager()!.isReachable {
            
            AF.request(URL, method: RequestMethod, parameters: Parameter, encoding: JSONEncoding.default).responseJSON { (Response) in
                let HTTPStatus = Response.response?.statusCode ?? 504
                switch Response.result {
                    case let .success(value):
                        SetToken(Data: value)
                        if HTTPStatus == 200{
                            OnSuccess(HTTPStatus,JSON(value))
                        }else{
                            let Error:[ErrorModel] = JsonParsingUtils.GetErrorArray(From: JSON(value))
                            OnError(HTTPStatus,Error)
                        }
                    case .failure(_):
                        OnError(HTTPStatus,nil)
                }//end switch
            }// end AF request
        }//end if
        else{
            OnError(12163,nil)//no internet connection
        }
    }
    
    class func RequestWithHeader(URL:String,RequestMethod: HTTPMethod, Parameter:[String:Any]?, _ OnSuccess: @escaping (Int,JSON?)-> Void,_ OnError: @escaping (Int,[ErrorModel]?)->Void) {
        
        if NetworkReachabilityManager()!.isReachable {

            AF.request(URL, method: RequestMethod, parameters: Parameter, encoding: JSONEncoding.default,headers: GetHeader()).responseJSON { (Response) in
                let HTTPStatus = Response.response?.statusCode ?? 504
                switch Response.result {
                    case let .success(value):
                        SetToken(Data: value)
                        if HTTPStatus == 200{
                            OnSuccess(HTTPStatus,JSON(value))
                        }else{
                            let Error:[ErrorModel] = JsonParsingUtils.GetErrorArray(From: JSON(value))
                            OnError(HTTPStatus,Error)
                        }
                    case .failure(_):
                        OnError(HTTPStatus,nil)
                }//end switch
            }// end AF request
        }//end if
        else{
            OnError(12163,nil)//no internet connection
        }
    }
    
    class func GetUserAgreement(_ WhenCompeleted: @escaping (Int,JSON?)->()){
        
        let Address  = "\(DOMAIN_URL)\(Get_User_Agreement_URL)"
        
         if NetworkReachabilityManager()!.isReachable{
            AF.request(Address, method: .post, parameters: nil, encoding: JSONEncoding.default,headers: GetHeader()).responseJSON { (response) in

            let HttpStatus = response.response?.statusCode ?? 504
                
            switch response.result {
                case let .success(value):
                    TOKEN = JSON(value)["token"].stringValue
                    WhenCompeleted(HttpStatus,JSON(value))
                case .failure(_):
                    WhenCompeleted(HttpStatus,JSON())
                }
            }
        }
        else{
            WhenCompeleted(504,nil)
        }
    }
    
    class func GetPlans(User_Id:Int, _ WhenCompeleted: @escaping (Int,JSON?)->()){
        
        let Address  = "\(DOMAIN_URL)\(User_URL)\(User_Id)\(Get_Plans_URL)"
        
         if NetworkReachabilityManager()!.isReachable{
            AF.request(Address, method: .get, parameters: nil, encoding: JSONEncoding.default,headers: GetHeader()).responseJSON { (response) in

            let HttpStatus = response.response?.statusCode ?? 504
                
            switch response.result {
                case let .success(value):
                    TOKEN = JSON(value)["token"].stringValue
                    WhenCompeleted(HttpStatus,JSON(value))
                case .failure(_):
                    WhenCompeleted(HttpStatus,JSON())
                }
            }
        }
        else{
            WhenCompeleted(504,nil)
        }
    }
    
    //Upload Image as multipart
    class func UploadImage(Address: String, ImageData: Data,ImageName:String, _ OnSuccess: @escaping (Int,JSON?)-> Void,_ OnError: @escaping (Int,[ErrorModel]?)->Void){
        
        if NetworkReachabilityManager()!.isReachable {
            AF.upload(multipartFormData: { (MultipartFormData) in
                MultipartFormData.append(ImageData, withName: ImageName,fileName: "\(ImageName).jpg",mimeType: "\(ImageName)/jpg")
            },//, headers: GetHeader()
            to: Address ).responseJSON { (Response) in
                let HTTPStatus = Response.response?.statusCode ?? 504
                switch Response.result {
                    case let .success(value):
                        SetToken(Data: value)
                        if HTTPStatus == 200{
                            OnSuccess(HTTPStatus,JSON(value))
                        }else{
                            let Error:[ErrorModel] = JsonParsingUtils.GetErrorArray(From: JSON(value))
                            OnError(HTTPStatus,Error)
                        }
                    case .failure(_):
                        OnError(HTTPStatus,nil)
                }//end switch
            }
        }//end if
        else{
            OnError(12163,nil)//no internet connection
        }
    }
    
    class func GetHeader() -> HTTPHeaders {
        
        if UserDefaults.standard.bool(forKey: "Is_Login") {
            return ["Authorization":"Token " + (UserDefaults.standard.string(forKey: "VALID_TOKEN") ?? "")]
            
        }else {
           return ["Authorization":"Token \(TOKEN)"]
        }
        
        
    }
    
}
