//
//  JsonParsingUtils.swift
//  CayLoad-Driver
//
//  Created by Kiyarash Vosough on 3/29/1399 AP.
//  Copyright © 1399 CayLoad. All rights reserved.
//
import SwiftyJSON

class JsonParsingUtils {
    
    class func readJSONFromFile(fileName: String) -> JSON?{
        var json: JSON?
        if let path = Bundle.main.path(forResource: fileName, ofType: "json") {
            do {
                let fileUrl = URL(fileURLWithPath: path)
                // Getting data from JSON file using the file URL
                let data = try Data(contentsOf: fileUrl, options: .mappedIfSafe)
                json = try JSON(data: data)
            } catch {
                // Handle error here
            }
        }
        return json
    }
    
    class func GetArrayList<DataModelType:ModelInitializer>(From Json:JSON,withRootTag RootTag: String) -> [DataModelType] {
        var DataArray = [DataModelType]()
        if Json[RootTag].arrayValue.count > 0 {
            for item in Json[RootTag].arrayValue {
                DataArray.append(DataModelType(item))
            }
        }
        return DataArray
    }
    
    class func GetErrorArray<DataModelType:ModelInitializer>(From Json:JSON) -> [DataModelType] {
        var DataArray = [DataModelType]()
        if Json["messages"]["error"].arrayValue.count > 0 {
            for item in Json["messages"]["error"].arrayValue {
                DataArray.append(DataModelType(item))
            }
        }
        return DataArray
    }
}
