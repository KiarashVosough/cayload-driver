//
//  ConnectionURL.swift
//  CayLoad-Driver
//
//  Created by Kiyarash Vosough on 1/27/1399 AP.
//  Copyright © 1399 CayLoad. All rights reserved.
//

import Foundation

var TOKEN = String() 

let DOMAIN_URL = "https://dev3.cayload.com/api";
let PURE_DOMAIN_URL = "https://dev3.cayload.com";
let PHONE_LOGIN_URL = "/av1/drivers/phone_login";
let EMAIL_LOGIN_URL = "/av1/drivers/email_login";
let PHONE_SIGNUP_URL = "/av1/drivers/phone_register";
let EMAIL_SIGNUP_URL = "/av1/drivers/email_register";
let Get_User_Nationalities_URL = "/av1/user_nationalities/all"
let Set_User_Nationality_URL = "/set_user_nationality"
let Set_User_Language_URL = "/set_user_language"
let User_URL = "/av1/users/"
let Pure_User_URL = "/users"
let Get_Transactions = "/get_user_transactions"
let Email_URL = "/av1/emails/"
let Phone_URL = "/av1/phones/"
let Start_Verification_URL = "/start_verification"
let Check_Verification_URL = "/check_verification"
let Get_User_Agreement_URL = "/av1/users/accept_agreement"
let Get_Plans_URL = "/get_plans"
let Profile_URL = "/v1/profile"
let TruckType_URL = "/av1/truck_types"
let Create_My_Truck_URL = "/av1/trucks/create_my_truck"
let Accept_Agreement_URL = "/av1/users/accept_agreement"
let Forgot_Password_URL = "/av1/drivers/forget_password_start"
let Forgot_Password_Verification_URL = "/av1/drivers/forget_password_check"
let Forgot_Password_Change_URL = "/av1/drivers/forget_password_change"
let Set_License_Image_URL = "/av1/drivers/set_license"
let Set_Vehicle_Card_Image_URL = "/av1/drivers/set_vehicle_card"
let Update_Location = "/av1/drivers/update_location"
let Edit_Profile_URL = "/av1/drivers/edit_profile"
let Set_Profile_Image_URL = "/av1/drivers/set_profile_image"
let Set_Company_Image_URL = "/av1/drivers/set_company_logo"
let Get_User_Current_Plan_URL = "/get_user_current_plan"
let Driver_URL = "/av1/drivers/"
let Pending_Offers_URL = "/av1/truck_requests/pending_offers"
let Truck_Request_URL = "/av1/truck_requests"
let Add_New_Route = "/av1/driver_favorite_routes/add_favorite_route"
let Get_Location_URL = "/av1/location_points/create_with_open_cage"
let My_Favorite_Route_URL = "/av1/driver_favorite_routes/my_favorite_routes"
let Favorite_Route_URL = "/av1/driver_favorite_routes/"
let Total_Offers_URL = "/av1/truck_requests/driver_total"
let Get_Offer_URL = "/get_offers"
let Track_Request = "/track_requests"
let Customers_URL = "/av1/customers/"
let Send_Offer_URL = "/send_offer"
let Upload_Location_Point = "/av1/drivers/upload_location_points"
