//
//  AppDelegate+LocationTracking.swift
//  CayLoad-Driver
//
//  Created by Kiyarash Vosough on 3/14/1399 AP.
//  Copyright © 1399 CayLoad. All rights reserved.
//

import UIKit
import Foundation
import CoreLocation

extension AppDelegate: CLLocationManagerDelegate{
    
    func StartTracking(){
         if locationManager == nil {
             locationManager = CLLocationManager()
             locationManager?.delegate = self
             locationManager?.distanceFilter = 10
             locationManager?.desiredAccuracy = kCLLocationAccuracyBest
             locationManager?.allowsBackgroundLocationUpdates = true
             locationManager?.startUpdatingLocation()
         } else {
             locationManager = nil
             locationManager = CLLocationManager()
             locationManager?.delegate = self
             locationManager?.distanceFilter = 10
             locationManager?.desiredAccuracy = kCLLocationAccuracyBest
             locationManager?.allowsBackgroundLocationUpdates = true
             locationManager?.startUpdatingLocation()
         }
    }
    
    func CreateRegion(location:CLLocation?) {
        guard let location = location else {
            return
        }
        if CLLocationManager.isMonitoringAvailable(for: CLCircularRegion.self) {
            
            let coordinate = CLLocationCoordinate2DMake((location.coordinate.latitude), (location.coordinate.longitude))
            let regionRadius = 5.0
            
            let region = CLCircularRegion(center: CLLocationCoordinate2D(latitude: coordinate.latitude,longitude: coordinate.longitude),radius: regionRadius, identifier: "aabb")
            
            region.notifyOnExit = true
            
            do {
                let LocationTracker1 = LocationTracker(AppDelegate_Context:self.persistentContainer.viewContext)
                try LocationTracker1.SaveLocation(Latitude: location.coordinate.latitude, Longitude: location.coordinate.longitude, Accuracy: location.verticalAccuracy)
//                scheduleLocalNotification(alert: "LocationSaved", title: "")
            } catch {
                print("cant save loc")
            }
            self.locationManager?.stopUpdatingLocation()
            self.locationManager?.startMonitoring(for: region)
        }
    }
    
    //MARK:- Location Manager Delegates
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedAlways || status == .authorizedWhenInUse {
            manager.startUpdatingLocation()
        }
    }
    
    func locationManagerDidPauseLocationUpdates(_ manager: CLLocationManager) {
        UserDefaults.standard.set(false, forKey: "Is_Location_OnRecord")
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = locations.last
        if (location?.horizontalAccuracy)! <= Double(65.0) {
            myLocation = location
            UserDefaults.standard.set(true, forKey: "Is_Location_OnRecord")
            if !(UIApplication.shared.applicationState == .active) || UIApplication.shared.applicationState == .active {
                self.CreateRegion(location: location)
            }
        } else {
            manager.stopUpdatingLocation()
            manager.startUpdatingLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didEnterRegion region: CLRegion) {
    }
    
    func locationManager(_ manager: CLLocationManager, didExitRegion region: CLRegion) {
        if  UIApplication.shared.applicationState == .active {
            let LocationTracker1 = LocationTracker(AppDelegate_Context:self.persistentContainer.viewContext)
                LocationTracker1.CheckForRequesting({ (status) in
//                    self.scheduleLocalNotification(alert: "loca uploaded with code\(status)", title: "")
                })
        }
        manager.stopMonitoring(for: region)
        manager.startUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, monitoringDidFailFor region: CLRegion?, withError error: Error) {
        UserDefaults.standard.set(false, forKey: "Is_Location_OnRecord")
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        UserDefaults.standard.set(false, forKey: "Is_Location_OnRecord")
    }
}
