//
//  AppDelegate.swift
//  CayLoad-Driver
//
//  Created by Kiyarash Vosough on 1/9/1399 AP.
//  Copyright © 1399 CayLoad. All rights reserved.
//

import UIKit
import CoreLocation
import CoreData
import BackgroundTasks
import Firebase

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    // MARK: - Core Data stack
    lazy var persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "DriverLocationCoordinateData")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                 
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    var locationManager:CLLocationManager? = CLLocationManager()
    var myLocation:CLLocation?
    let gcmMessageIDKey = "gcm.message_id"
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        //Notification Handlers
        FirebaseApp.configure()
        StartFirebase(application: application)
        Messaging.messaging().delegate = self
        
        
        
        // Register BG Task
        ResgisterBackgroundTask()
        
        //Set Value for first launch
        let launchedBefore = UserDefaults.standard.bool(forKey: "launchedBefore")
        if !launchedBefore {
            UserDefaults.standard.set(true, forKey: "launchedBefore")
        }
        
        //Location Handlers
        if UserDefaults.standard.bool(forKey: "Is_Login") {
            if launchOptions?[UIApplication.LaunchOptionsKey.location] != nil {
                StartTracking()
            }
            else {
                locationManager?.delegate = self
                locationManager?.distanceFilter = 10
                locationManager?.desiredAccuracy = kCLLocationAccuracyBest
                locationManager?.allowsBackgroundLocationUpdates = true
//                    if CLLocationManager.authorizationStatus() == .notDetermined {
//                        locationManager?.requestAlwaysAuthorization()
//                    }
//                    else if CLLocationManager.authorizationStatus() == .denied {
//                    }
//                    else if CLLocationManager.authorizationStatus() == .authorizedWhenInUse {
//                        locationManager?.requestAlwaysAuthorization()
//                    }
                 if CLLocationManager.authorizationStatus() == .authorizedAlways {
                    locationManager?.startUpdatingLocation()
                 }else{
                    UserDefaults.standard.set(false, forKey: "Is_Location_OnRecord")
                }
            }
        }
        return true
    }

    
    
    // MARK: UISceneSession Lifecycle

    @available(iOS 13.0, *)
    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    @available(iOS 13.0, *)
    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        print("application Did Enter Foreground")
        if UserDefaults.standard.bool(forKey: "Is_Login") {
            self.CreateRegion(location: myLocation)
        }
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        print("application Did Enter Background")
        if UserDefaults.standard.bool(forKey: "Is_Login") {
            self.CreateRegion(location: myLocation)
        }
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        print("application Will Terminate")
        if UserDefaults.standard.bool(forKey: "Is_Login") {
            self.CreateRegion(location: myLocation)
        }
    }

    

}

