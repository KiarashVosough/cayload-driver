//
//  AppDelegate+CoreData+BG.swift
//  CayLoad-Driver
//
//  Created by Kiyarash Vosough on 4/7/1399 AP.
//  Copyright © 1399 CayLoad. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import BackgroundTasks

extension AppDelegate {
    
    func ResgisterBackgroundTask(){
            UIApplication.shared.setMinimumBackgroundFetchInterval(UIApplication.backgroundFetchIntervalMinimum)

    }
    // MARK: - Core Data Saving support
    
    func application(_ application: UIApplication, performFetchWithCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        let LocationTracker1 = LocationTracker(AppDelegate_Context:self.persistentContainer.viewContext)
        LocationTracker1.CheckForRequesting({ (status) in
//            self.scheduleLocalNotification(alert: "loction uploaded with code\(status)", title: "hi")
            switch status {
                case 200 : completionHandler(.newData)
                break
                case 0 : completionHandler(.failed)
                break
                case 20 : completionHandler(.failed)
                break
                case 30 : completionHandler(.failed)
                break
                default : completionHandler(.failed)
                break
            }
        })
        completionHandler(.failed)
    }
    
}
